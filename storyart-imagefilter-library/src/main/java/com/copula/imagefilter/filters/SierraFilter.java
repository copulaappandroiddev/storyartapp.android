package com.copula.imagefilter.filters;

import android.content.Context;
import android.graphics.*;
import com.copula.imagefilter.ImageFilter;

import java.io.IOException;

public class SierraFilter extends ImageFilter {
    public SierraFilter(Context context, Bitmap sourceImage, boolean overDraw) {
        super(context, sourceImage, "sierra", overDraw);
    }

    @Override
    protected void applyFilter(Canvas canvas) {
        try {
            //map(canvas);
            smokeOverlay(canvas);
            vignette(canvas);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void map(Canvas canvas) throws IOException {
        Bitmap map = BitmapFactory.decodeStream(openFilterComponentStream("map.png"));
        Paint paint = new Paint();
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.OVERLAY));


        int right = getSourceBitmap().getWidth();
        int bottom = getSourceBitmap().getHeight();

        Rect mapSrc = new Rect(220, 0, 221, 1);
        Rect mapDst = new Rect(0, 0, right, bottom);
        canvas.drawBitmap(map, mapSrc, mapDst, paint);
    }

    private void smokeOverlay(Canvas canvas) throws IOException {
        Bitmap smoke = BitmapFactory.decodeStream(openFilterComponentStream("soft_light.png"));
        Paint paint = new Paint();
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.OVERLAY));

        int right = getSourceBitmap().getWidth();
        int bottom = getSourceBitmap().getHeight();

        Rect dest = new Rect(0, 0, right, bottom);
        Rect src = new Rect(94, 100, 97, 106);
        canvas.drawBitmap(smoke, src, dest, paint);
    }

    private void vignette(Canvas canvas) throws IOException {
        Bitmap smoke = BitmapFactory.decodeStream(openFilterComponentStream("vignette.png"));
        Paint paint = new Paint();
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.OVERLAY));

        int right = getSourceBitmap().getWidth();
        int bottom = getSourceBitmap().getHeight();

        Rect dest = new Rect(0, 0, right, bottom);
        canvas.drawBitmap(smoke, null, dest, paint);
    }
}
