package com.copula.imagefilter.filters;

import android.content.Context;
import android.graphics.*;
import com.copula.imagefilter.ImageFilter;

import java.io.IOException;

public class BrannanFilter extends ImageFilter {
    public BrannanFilter(Context context, Bitmap sourceImage, boolean overDraw) {
        super(context, sourceImage, "brannan", overDraw);
    }

    @Override
    protected void applyFilter(Canvas canvas) {
        try {
            map(canvas);
            contrastMap(canvas);
            screenMap(canvas);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void screenMap(Canvas canvas) throws IOException {
        Bitmap overlayBitmap = BitmapFactory.decodeStream(openFilterComponentStream("map1.png"));
        Paint paint = new Paint();
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.OVERLAY));

        int imageWidth = getSourceBitmap().getWidth();
        int imageHeight = getSourceBitmap().getHeight();

        Rect mapDest = new Rect(0, 0, imageWidth, imageHeight);
        Rect srcMap = new Rect(60, 0, 61, 1);
        canvas.drawBitmap(overlayBitmap, srcMap, mapDest, paint);
    }

    private void contrastMap(Canvas canvas) throws IOException {
        Bitmap overlayBitmap = BitmapFactory.decodeStream(openFilterComponentStream("map2.png"));
        Paint paint = new Paint();
        PorterDuff.Mode mode = PorterDuff.Mode.OVERLAY;
        paint.setXfermode(new PorterDuffXfermode(mode));

        int imageWidth = getSourceBitmap().getWidth();
        int imageHeight = getSourceBitmap().getHeight();

        Rect mapDest = new Rect(0, 0, imageWidth, imageHeight);
        Rect srcMap = new Rect(170, 0, 171, 1);
        canvas.drawBitmap(overlayBitmap, srcMap, mapDest, paint);
    }

    private void map(Canvas canvas) throws IOException {
        Bitmap overlayBitmap = BitmapFactory.decodeStream(openFilterComponentStream("map.png"));
        Paint paint = new Paint();
        PorterDuff.Mode mode = PorterDuff.Mode.OVERLAY;
        paint.setXfermode(new PorterDuffXfermode(mode));

        int imageWidth = getSourceBitmap().getWidth();
        int imageHeight = getSourceBitmap().getHeight();

        Rect mapDest = new Rect(0, 0, imageWidth, imageHeight);
        Rect srcMap = new Rect(150, 0, 151, 1);
        canvas.drawBitmap(overlayBitmap, srcMap, mapDest, paint);
    }
}
