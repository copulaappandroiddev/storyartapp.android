package com.copula.imagefilter.filters;

import android.content.Context;
import android.graphics.*;
import com.copula.imagefilter.ImageFilter;

import java.io.IOException;

public class WaldenFilter extends ImageFilter {
    public WaldenFilter(Context context, Bitmap sourceImage, boolean overDraw) {
        super(context, sourceImage, "walden", overDraw);
    }

    @Override
    protected void applyFilter(Canvas canvas) {
        try {
            overlay(canvas);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void overlay(Canvas canvas) throws IOException {
        Bitmap overlayBitmap = BitmapFactory.decodeStream(openFilterComponentStream("map.png"));
        Paint paint = new Paint();
        PorterDuff.Mode mode = PorterDuff.Mode.SCREEN;
        paint.setXfermode(new PorterDuffXfermode(mode));


        int right = getSourceBitmap().getWidth();
        int bottom = getSourceBitmap().getHeight();

        Rect src = new Rect(4, 0, 8, 3);
        Rect dest = new Rect(0, 0, right, bottom);
        canvas.drawBitmap(overlayBitmap, src, dest, paint);

    }
}
