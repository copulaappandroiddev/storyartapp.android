package com.copula.imagefilter.filters;

import android.content.Context;
import android.graphics.*;
import com.copula.imagefilter.ImageFilter;

import java.io.IOException;

public class MayfairFilter extends ImageFilter {
    public MayfairFilter(Context context, Bitmap sourceImage, boolean overDraw) {
        super(context, sourceImage, "mayfair", overDraw);
    }

    @Override
    protected void applyFilter(Canvas canvas) {
        try {
            overlay(canvas);
            glowField(canvas);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void overlay(Canvas canvas) throws IOException {
        Bitmap colorOverlay = BitmapFactory.decodeStream(openFilterComponentStream("colorOverlay.png"));
        Paint paint = new Paint();
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.OVERLAY));

        int imageWidth = getSourceBitmap().getWidth();
        int imageHeight = getSourceBitmap().getHeight();

        Rect dest = new Rect(0, 0, imageWidth, imageHeight);
        Rect src = new Rect(180, 0, 181, 1);
        canvas.drawBitmap(colorOverlay, src, dest, paint);
    }

    private void glowField(Canvas canvas) throws IOException {
        Bitmap bitmap = BitmapFactory.decodeStream(openFilterComponentStream("glowField.png"));
        Paint paint = new Paint();
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.OVERLAY));

        int imageWidth = getSourceBitmap().getWidth();
        int imageHeight = getSourceBitmap().getHeight();

        Rect dest = new Rect(0, 0, imageWidth, imageHeight);
        Rect src = new Rect(47, 28, 60, 41);
        canvas.drawBitmap(bitmap, src, dest, paint);
    }
}
