package com.copula.imagefilter.filters;

import android.content.Context;
import android.graphics.*;
import com.copula.imagefilter.ImageFilter;

import java.io.IOException;

public class ClarendonFilter extends ImageFilter {
    public ClarendonFilter(Context context, Bitmap sourceImage, boolean overDraw) {
        super(context, sourceImage, "clarendon", overDraw);
    }

    @Override
    protected void applyFilter(Canvas canvas) {
        try {
            glacial1(canvas);
            glacial2(canvas);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void glacial2(Canvas canvas) throws IOException {
        Bitmap overlayBitmap = BitmapFactory.decodeStream(openFilterComponentStream("Glacial2.png"));
        Paint paint = new Paint();
        PorterDuff.Mode mode = PorterDuff.Mode.OVERLAY;
        paint.setXfermode(new PorterDuffXfermode(mode));

        int imageWidth = getSourceBitmap().getWidth();
        int imageHeight = getSourceBitmap().getHeight();

        Rect mapDest = new Rect(0, 0, imageWidth, imageHeight);
        Rect srcMap = new Rect(100, 0, 101, 1);
        canvas.drawBitmap(overlayBitmap, srcMap, mapDest, paint);
    }

    private void glacial1(Canvas canvas) throws IOException {
        Bitmap overlayBitmap = BitmapFactory.decodeStream(openFilterComponentStream("Glacial1.png"));
        Paint paint = new Paint();
        PorterDuff.Mode mode = PorterDuff.Mode.OVERLAY;
        paint.setXfermode(new PorterDuffXfermode(mode));

        int imageWidth = getSourceBitmap().getWidth();
        int imageHeight = getSourceBitmap().getHeight();

        Rect mapDest = new Rect(0, 0, imageWidth, imageHeight);
        Rect srcMap = new Rect(170, 0, 171, 1);
        canvas.drawBitmap(overlayBitmap, srcMap, mapDest, paint);
    }
}
