package com.copula.imagefilter.filters;

import android.content.Context;
import android.graphics.*;
import com.copula.imagefilter.ColorFilterGenerator;
import com.copula.imagefilter.ImageFilter;

import java.io.IOException;

public class MoonFilter extends ImageFilter {
    public MoonFilter(Context context, Bitmap sourceImage, boolean overDraw) {
        super(context, sourceImage, "moon", overDraw);
    }

    @Override
    protected Paint mainBitmapPaint() {
        ColorFilterGenerator.Builder b = new ColorFilterGenerator.Builder();
        Paint paint = new Paint();
        paint.setColorFilter(paint.setColorFilter(b.setSaturation(-100).build()));

        return paint;
    }

    @Override
    protected void applyFilter(Canvas canvas) {
        try {
            map(canvas);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void map(Canvas canvas) throws IOException {
        Bitmap overlayBitmap = BitmapFactory.decodeStream(openFilterComponentStream("curves1.png"));
        Paint paint = new Paint();
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.OVERLAY));

        int imageWidth = getSourceBitmap().getWidth();
        int imageHeight = getSourceBitmap().getHeight();

        Rect mapDest = new Rect(0, 0, imageWidth, imageHeight);
        Rect srcMap = new Rect(255, 0, 256, 1);
        canvas.drawBitmap(overlayBitmap, srcMap, mapDest, paint);
    }
}
