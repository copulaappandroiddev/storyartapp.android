package com.copula.imagefilter.filters;

import android.content.Context;
import android.graphics.*;
import com.copula.imagefilter.ImageFilter;

import java.io.IOException;

public class LarkFilter extends ImageFilter {
    public LarkFilter(Context context, Bitmap sourceImage, boolean overDraw) {
        super(context, sourceImage, "lark", overDraw);
    }

    @Override
    protected void applyFilter(Canvas canvas) {
        try {
            map(canvas);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void map(Canvas canvas) throws IOException {
        Bitmap overlayBitmap = BitmapFactory.decodeStream(openFilterComponentStream("map.png"));
        Paint paint = new Paint();
        PorterDuff.Mode mode = PorterDuff.Mode.SCREEN;
        paint.setXfermode(new PorterDuffXfermode(mode));
        paint.setAlpha(80);

        int imageWidth = getSourceBitmap().getWidth();
        int imageHeight = getSourceBitmap().getHeight();

        Rect mapDest = new Rect(0, 0, imageWidth, imageHeight);
        Rect srcMap = new Rect(0, 1056, 33, 1089);
        canvas.drawBitmap(overlayBitmap, srcMap, mapDest, paint);
    }
}
