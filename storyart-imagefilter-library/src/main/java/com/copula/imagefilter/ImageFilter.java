package com.copula.imagefilter;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public abstract class ImageFilter {
    private Context mContext;
    private AssetManager mAssetManager;
    private Bitmap sourceBitmap, filteredBitmap;
    private String filterPackage;
    private boolean overDraw;

    protected ImageFilter(Context context, Bitmap sourceBitmap, String filterPackage, boolean overDraw) {
        this.mContext = context;
        this.sourceBitmap = sourceBitmap;
        this.overDraw = overDraw;

        init("filters" + File.separatorChar + filterPackage);
    }

    protected void init(String filterPackage) {
        mAssetManager = mContext.getAssets();
        this.filterPackage = filterPackage;

        try {
            if (!overDraw || !sourceBitmap.isMutable()) {//create new bitmap if overdraw is not allowed.
                filteredBitmap = Bitmap.createBitmap(sourceBitmap.getWidth(), sourceBitmap.getHeight(), Bitmap.Config.ARGB_8888);
            } else {
                filteredBitmap = sourceBitmap;
            }

            Canvas canvas = new Canvas(filteredBitmap);
            canvas.drawBitmap(sourceBitmap, 0, 0, mainBitmapPaint());

            applyFilter(canvas);
        } catch (OutOfMemoryError ignored) {
        }
    }

    protected Paint mainBitmapPaint() {
        return new Paint();
    }

    protected InputStream openFilterComponentStream(String component) throws IOException {
        return mAssetManager.open(filterPackage + File.separatorChar + component);
    }

    private InputStream open(String file) throws IOException {
        return mAssetManager.open(file);

    }

    protected abstract void applyFilter(Canvas canvas);

    public Context getContext() {
        return mContext;
    }

    public Bitmap getSourceBitmap() {
        return sourceBitmap;
    }

    public Bitmap getFilteredBitmap() {
        return filteredBitmap;
    }

    protected void rgbMap(Canvas canvas, int r, int g, int b) throws IOException {
        Bitmap overlayBitmap = BitmapFactory.decodeStream(open("filters/shared/map.png"));
        Paint paint = new Paint();
        PorterDuff.Mode mode = PorterDuff.Mode.OVERLAY;
        paint.setXfermode(new PorterDuffXfermode(mode));


        int right = getSourceBitmap().getWidth();
        int bottom = getSourceBitmap().getHeight();

        Rect dest = new Rect(0, 0, right, bottom);

        Rect srcR = new Rect(r, 0, r + 1 == 1 ? 0 : r + 1, 1);
        canvas.drawBitmap(overlayBitmap, srcR, dest, paint);

        Rect srcG = new Rect(r, 1, g + 1 == 1 ? 0 : g + 1, 2);
        canvas.drawBitmap(overlayBitmap, srcG, dest, paint);

        Rect srcB = new Rect(r, 2, b + 1 == 1 ? 0 : b + 1, 3);
        canvas.drawBitmap(overlayBitmap, srcB, dest, paint);
    }
}
