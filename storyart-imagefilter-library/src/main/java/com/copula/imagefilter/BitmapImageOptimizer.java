package com.copula.imagefilter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;

import java.io.*;

public class BitmapImageOptimizer {
    private int mOutputWidth, mOutputHeight;
    private ScaleType mScaleType;
    private File mImageFile;

    private BitmapImageOptimizer(File imageFile, int outputWidth, int outputHeight, ScaleType scaleType) {
        mImageFile = imageFile;
        mOutputWidth = outputWidth;
        mOutputHeight = outputHeight;
        mScaleType = scaleType;
    }

    public static Bitmap load(File imageFile, int outputWidth, int outputHeight) {
        return new BitmapImageOptimizer(imageFile,
                outputWidth, outputHeight, ScaleType.CENTER_INSIDE).loadResizedImage();
    }

    public static boolean writeBitmapToFile(Bitmap bitmap, String outPath) {
        if (bitmap == null) return false;

        BufferedOutputStream fos = null;
        try {
            fos = new BufferedOutputStream(new FileOutputStream(new File(outPath)));
            return bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (IOException e) {
            return false;
        } finally {
            if (fos != null) try {
                fos.close();
            } catch (IOException ignored) {
            }
        }
    }

    public static int calcInSampleSize(InputStream stream, int maxSize) throws IOException {
        // Decode image size
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeStream(stream, null, options);

        int scale = 1;
        if (maxSize > 0 && (options.outHeight > maxSize || options.outWidth > maxSize)) {
            double log = Math.log(maxSize / (double) Math.max(options.outHeight, options.outWidth));
            scale = (int) Math.pow(2, (int) Math.ceil(log / Math.log(0.5)));
        }
        return scale;
    }

    private Bitmap loadResizedImage() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        decode(options);
        int scale = 1;
        while (checkSize(options.outWidth / scale > mOutputWidth,
                options.outHeight / scale > mOutputHeight)) {
            scale++;
        }

        scale--;
        if (scale < 1) {
            scale = 1;
        }

        options = new BitmapFactory.Options();
        options.inSampleSize = scale;
        //options.inPreferredConfig = Bitmap.Config.RGB_565;
        options.inPurgeable = true;
        options.inTempStorage = new byte[16 * 1024];
        options.inJustDecodeBounds = false;

        Bitmap bitmap = decode(options);
        if (bitmap == null) {
            return null;
        }

        bitmap = scaleBitmap(bitmap);
        bitmap = rotateImage(bitmap);
        return bitmap;
    }

    private Bitmap scaleBitmap(Bitmap bitmap) {
        // resize to desired dimensions
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int[] newSize = getScaleSize(width, height);

        Bitmap workBitmap = Bitmap.createScaledBitmap(bitmap, newSize[0], newSize[1], true);
        if (workBitmap != bitmap) {
            bitmap.recycle();
            bitmap = workBitmap;
            System.gc();
        }

        if (mScaleType == ScaleType.CENTER_CROP) {
            // Crop it
            int diffWidth = newSize[0] - mOutputWidth;
            int diffHeight = newSize[1] - mOutputHeight;
            workBitmap = Bitmap.createBitmap(bitmap, diffWidth / 2, diffHeight / 2,
                    newSize[0] - diffWidth, newSize[1] - diffHeight);
            if (workBitmap != bitmap) {
                bitmap.recycle();
                bitmap = workBitmap;
            }
        }

        return bitmap;
    }

    private Bitmap decode(BitmapFactory.Options options) {
        options.inMutable = true;
        return BitmapFactory.decodeFile(mImageFile.getAbsolutePath(), options);
    }

    /**
     * Retrieve the scaling size for the image dependent on the ScaleType.<br>
     * <br>
     * If CROP: sides are same size or bigger than output's sides<br>
     * Else   : sides are same size or smaller than output's sides
     */
    private int[] getScaleSize(int width, int height) {
        float newWidth;
        float newHeight;

        float withRatio = (float) width / mOutputWidth;
        float heightRatio = (float) height / mOutputHeight;

        boolean adjustWidth = mScaleType == ScaleType.CENTER_CROP ? withRatio > heightRatio : withRatio < heightRatio;

        if (adjustWidth) {
            newHeight = mOutputHeight;
            newWidth = (newHeight / height) * width;
        } else {
            newWidth = mOutputWidth;
            newHeight = (newWidth / width) * height;
        }
        return new int[]{Math.round(newWidth), Math.round(newHeight)};
    }

    private boolean checkSize(boolean widthBigger, boolean heightBigger) {
        if (mScaleType == ScaleType.CENTER_CROP) {
            return widthBigger && heightBigger;
        } else {
            return widthBigger || heightBigger;
        }
    }

    private Bitmap rotateImage(final Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        Bitmap rotatedBitmap = bitmap;
        try {
            int orientation = getImageOrientation();
            if (orientation != 0) {
                Matrix matrix = new Matrix();
                matrix.postRotate(orientation);
                rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                        bitmap.getHeight(), matrix, true);
                bitmap.recycle();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rotatedBitmap;
    }

    private int getImageOrientation() throws IOException {
        ExifInterface exif = new ExifInterface(mImageFile.getAbsolutePath());
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
        switch (orientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                return 0;
            case ExifInterface.ORIENTATION_ROTATE_90:
                return 90;
            case ExifInterface.ORIENTATION_ROTATE_180:
                return 180;
            case ExifInterface.ORIENTATION_ROTATE_270:
                return 270;
            default:
                return 0;
        }
    }

    public enum ScaleType {
        CENTER_CROP, CENTER_INSIDE
    }
}
