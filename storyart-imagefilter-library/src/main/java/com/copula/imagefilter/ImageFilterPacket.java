package com.copula.imagefilter;

import android.content.Context;
import android.graphics.Bitmap;
import com.copula.imagefilter.filters.*;

/**
 * Created by heeleaz on 2/4/18.
 */

public class ImageFilterPacket {
    private static String[] filterNames = {"", "Walden", "Lark", "Sierra", "Mayfair", "Valencia", "Greyscale", "Moon", "Clarendon", "Brannan"};
    private int filterIndex = 0;
    private Context mContext;
    private boolean overDraw = false;

    public ImageFilterPacket(Context context) {
        this.mContext = context;
    }

    public void overDraw(boolean overDraw) {
        this.overDraw = overDraw;
    }

    public Bitmap applyFilter(String filterName, Bitmap bitmap) {
        switch (filterName) {
            case "Sierra":
                return sierra(bitmap);
            case "Walden":
                return walden(bitmap);
            case "Mayfair":
                return mayfair(bitmap);
            case "Lark":
                return lark(bitmap);
            case "Chrome":
                return chrome(bitmap);
            case "Clare":
                return clare(bitmap);
            case "Ville":
                return ville(bitmap);
            case "Valencia":
                return valencia(bitmap);
            case "Greyscale":
                return greyscale(bitmap);
            case "Moon":
                return moon(bitmap);
            case "Clarendon":
                return clarendon(bitmap);
            case "Brannan":
                return brannan(bitmap);
            default:
                return null;
        }
    }

    private Bitmap ville(Bitmap image) {
        //GPUImageFilterGroup filterGroup = new GPUImageFilterGroup();
        //filterGroup.addFilter(new GPUImageSaturationFilter(0.54f));
        //filterGroup.addFilter(new GPUImageBrightnessFilter(0.08f));
        //return filterGroup;

        return null;
    }

    private Bitmap chrome(Bitmap image) {
        //GPUImageToneCurveFilter filter = new GPUImageToneCurveFilter();

        //filter.setRgbCompositeControlPoints(new PointF[]{new PointF(0, 0), new PointF(0.45f, 0.52f), new PointF(1, 1)});
        //filter.setRedControlPoints(new PointF[]{new PointF(0, 0), new PointF(0.40f, 0.52f), new PointF(1, 1)});

        //filter.setBlueControlPoints(new PointF[]{new PointF(0, 0), new PointF(0.5f, 0.5f), new PointF(1, 1)});
        //filter.setGreenControlPoints(new PointF[]{new PointF(0, 0), new PointF(0.5f, 0.5f), new PointF(1, 1)});
        //return filter;

        return null;
    }

    private Bitmap clare(Bitmap image) {
        //GPUImageLevelsFilter imageFilter = new GPUImageLevelsFilter();
        //imageFilter.setGreenMin(0f, 1f, 255f, 0.132f, 255);
        //imageFilter.setBlueMin(0f, 1f, 255f, 0.2f, 255f);
        //imageFilter.setRedMin(0f, 1f, 255f, 0.15f, 255f);
        //return imageFilter;

        return null;
    }

    private Bitmap vignette(Bitmap image) {
        //GPUImageFilterGroup filterGroup = new GPUImageFilterGroup();

        //filterGroup.addFilter(clare());

        //GPUImageVignetteFilter vignette = new GPUImageVignetteFilter();
        //vignette.setVignetteCenter(new PointF(0.5f, 0.5f));
        //vignette.setVignetteColor(new float[]{0f, 0f, 0f});
        //vignette.setVignetteStart(0);
        //vignette.setVignetteEnd(1);
        //filterGroup.addFilter(vignette);

        //return filterGroup;

        return null;
    }

    private Bitmap grayscale(Bitmap bitmap) {
        //return (new WaldenFilter(mContext, bitmap)).getFilteredBitmap();
        return null;
    }

    private Bitmap walden(Bitmap bitmap) {
        return (new WaldenFilter(mContext, bitmap, overDraw)).getFilteredBitmap();
    }

    private Bitmap mayfair(Bitmap bitmap) {
        return (new MayfairFilter(mContext, bitmap, overDraw).getFilteredBitmap());
    }

    private Bitmap lark(Bitmap bitmap) {
        return (new LarkFilter(mContext, bitmap, overDraw).getFilteredBitmap());
    }

    private Bitmap sierra(Bitmap bitmap) {
        return (new SierraFilter(mContext, bitmap, overDraw).getFilteredBitmap());
    }

    private Bitmap valencia(Bitmap bitmap) {
        return (new ValenciaFilter(mContext, bitmap, overDraw).getFilteredBitmap());
    }

    private Bitmap greyscale(Bitmap bitmap) {
        return (new GreyscaleFilter(mContext, bitmap, overDraw).getFilteredBitmap());
    }

    private Bitmap moon(Bitmap bitmap) {
        return (new MoonFilter(mContext, bitmap, overDraw).getFilteredBitmap());
    }

    private Bitmap clarendon(Bitmap bitmap) {
        return (new ClarendonFilter(mContext, bitmap, overDraw).getFilteredBitmap());
    }

    private Bitmap brannan(Bitmap bitmap) {
        return (new BrannanFilter(mContext, bitmap, overDraw).getFilteredBitmap());
    }

    public String getCurrentFilterName() {
        return filterNames[filterIndex];
    }

    public String nextFilterName() {
        filterIndex = ++filterIndex % filterNames.length;
        return filterNames[filterIndex];
    }


    public String prevFilterName() {
        int i = filterIndex - 1;
        filterIndex = (i + filterNames.length) % filterNames.length;
        return filterNames[filterIndex];
    }
}
