package com.copula.storyart.media.observer;

import com.copula.smartjson.OJsonMapper;

import java.io.Serializable;

public class MediaModel implements Serializable {
    public int _id;
    public long duration;

    public String data, filename;
    public long filesize;
    public int mediaType;
    public long createdAt;
    public long updatedAt;

    public static class Audio extends MediaModel {
        public String artist, album, title;
        public int albumId;

        public Audio() {
        }

        public Audio(Audio audioMedia) {
            try {
                OJsonMapper.mapOne(this, OJsonMapper.toJSON(audioMedia));
            } catch (Exception e) {
            }
        }
    }
}
