package com.copula.storyart.media.audiogallery;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.copula.storyart.R;
import com.copula.storyart.media.observer.MediaModel;
import com.copula.storyart.media.observer.SystemMediaProvider;
import com.storyart.support.widget.BaseAdapter;

public class AudioListAdapter extends BaseAdapter<MediaModel.Audio> {
    AudioListAdapter(Context ctx) {
        super(ctx);
    }

    @Override
    public View getView(int position, View convertView, LayoutInflater inflater) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.adp_media_selector_fragment, null, false);
            viewHolder.thumbnail = convertView.findViewById(R.id.img_media_thumbnail);
            viewHolder.artist = convertView.findViewById(R.id.txt_media_artist);
            viewHolder.title = convertView.findViewById(R.id.txt_media_title);
            convertView.setTag(viewHolder);
        } else viewHolder = (ViewHolder) convertView.getTag();


        MediaModel.Audio audioMedia = getItem(position);

        Uri uri = SystemMediaProvider.getAudioAlbumUri(audioMedia.albumId);
        Glide.with(getContext()).loadFromMediaStore(uri).override(128, 128)
                .placeholder(R.drawable.img_song_thumb_white_128dp).into(viewHolder.thumbnail);

        viewHolder.artist.setText(audioMedia.artist);
        viewHolder.title.setText(audioMedia.title);
        return convertView;
    }

    private class ViewHolder {
        ImageView thumbnail;
        TextView title, artist;
    }
}
