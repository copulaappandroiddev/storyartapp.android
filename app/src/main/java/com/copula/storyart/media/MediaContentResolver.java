package com.copula.storyart.media;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.provider.MediaStore;
import com.storyart.support.widget.Logger;

import java.net.URLDecoder;

/**
 * Created by heeleaz on 3/19/17.
 */
public class MediaContentResolver {
    private static final Logger logger = Logger.getInstance("MediaContentResolver");

    public static void updateContentProvider(Context context, String[] paths) {
        MediaScannerConnection.scanFile(context, paths, null, null);
    }

    private static MediaInfo getCursorToMediaInfo(Cursor cu) {
        MediaInfo info = new MediaInfo();
        info.data = cu.getString(cu.getColumnIndex(MediaStore.Audio.Media.DATA));
        info.duration = cu.getLong(cu.getColumnIndex(MediaStore.Audio.Media.DURATION));
        return info;
    }

    private static MediaInfo getMediaInfoFromDetectedId(Context ctx, Uri contentUri) {
        Cursor cu = null;
        try {
            String encodedPath = URLDecoder.decode(contentUri.toString(), "UTF-8");
            String[] splitted = encodedPath.split(":");
            String id = splitted[splitted.length - 1];

            String sel = MediaStore.Images.Media._ID + "=?";
            cu = ctx.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    null, sel, new String[]{id}, null);
            return (cu.moveToNext()) ? getCursorToMediaInfo(cu) : null;
        } catch (Exception e) {
            logger.e("File Not Read: getMediaInfoFromDetectedId", e);
            return null;
        } finally {
            if (cu != null) cu.close();
        }
    }

    private static MediaInfo getMediaInfoFromUri(Context ctx, Uri contentUri) {
        Cursor cu = null;
        try {
            cu = ctx.getContentResolver().query(contentUri,
                    null, null, null, null);
            cu.moveToNext();
            String data = cu.getString(cu.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA));
            return getMediaInfoFromDataPath(ctx, data);
        } catch (Exception e) {
            logger.e("File Not Read: getMediaInfoFromUri", e);
            return null;
        } finally {
            if (cu != null) cu.close();
        }
    }

    private static MediaInfo getMediaInfoFromDataPath(Context ctx, String dataPath) {
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.DATA + "=?";
        String[] sArgs = new String[]{dataPath};

        Cursor cursor = null;
        try {
            ContentResolver cr = ctx.getContentResolver();
            cursor = cr.query(uri, null, selection, sArgs, null);
            return (cursor.moveToNext()) ? getCursorToMediaInfo(cursor) : null;
        } catch (Exception e) {
            logger.e("File Not Read: getMediaInfoFromDataPath", e);
            return null;
        } finally {
            if (cursor != null) cursor.close();
        }
    }

    public static MediaInfo forceGetMediaInfo(Context ctx, Uri contentUri) {
        MediaInfo mediaInfo = getMediaInfoFromUri(ctx, contentUri);
        if (mediaInfo == null) {
            mediaInfo = getMediaInfoFromDataPath(ctx, contentUri.getPath());
        }
        if (mediaInfo == null) {
            mediaInfo = getMediaInfoFromDetectedId(ctx, contentUri);
        }

        return mediaInfo;
    }

    public static class MediaInfo {
        public String data;
        public long duration;
    }
}
