package com.copula.storyart.media;

import android.content.Context;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.FFmpegExecuteResponseHandler;
import com.storyart.support.widget.Logger;

import java.io.File;

/**
 * Created by heeleaz on 10/23/17.
 */
public class AudioTrimmerCommand implements ICommand, FFmpegExecuteResponseHandler {
    private static final Logger logger = Logger.getInstance("AudioTrimmerCommand");

    private String inputPath, outputPath;
    private int startTimeInSecs, readTimeInSecs;

    private Context mContext;
    private ICommandListener<AudioTrimmerCommand> mICommandListener;

    public AudioTrimmerCommand(Context context) {
        mContext = context;
    }

    public void setInputPath(String inputPath) {
        this.inputPath = inputPath;
    }

    public void setOutputPath(String outputPath) {
        this.outputPath = outputPath;
        //delete if it exists to avoid any unexpected result
        new File(outputPath).delete();
    }

    public void setRange(int start, int end) {
        this.startTimeInSecs = start;
        this.readTimeInSecs = end - start;
    }

    public int getReadTimeInSecs() {
        return readTimeInSecs;
    }

    @Override
    public String[] getCommand() {
        String ss = "" + startTimeInSecs;
        String t = "" + readTimeInSecs;
        return new String[]{"-i", inputPath, "-ss", ss, "-t", t, "-acodec", "copy", outputPath};
    }

    public void execute(ICommandListener<AudioTrimmerCommand> listener) {
        this.mICommandListener = listener;

        try {
            FFmpeg ffmpeg = FFmpeg.getInstance(mContext);
            ffmpeg.killRunningProcesses();
            ffmpeg.execute(getCommand(), this);
        } catch (Exception e) {
            logger.e("Media crop failed", e);
            if (mICommandListener != null)
                mICommandListener.onFailure(this, "Media Crop Failed");
        }
    }

    @Override
    public void onSuccess(String s) {
        logger.i("Media crop successful: " + s);
        if (mICommandListener != null) mICommandListener.onSuccessful(this, s);
    }

    @Override
    public void onProgress(String s) {
        logger.i("Media crop progress: " + s);
        if (mICommandListener != null) mICommandListener.onProgress(this, s);
    }

    @Override
    public void onFailure(String s) {
        logger.i("Media crop failed: " + s);
        if (mICommandListener != null) mICommandListener.onProgress(this, s);
    }

    @Override
    public void onStart() {
        //TODO: Intentionally ignored this method call for no reason
    }

    @Override
    public void onFinish() {
        //TODO: Intentionally ignored this method call for no reason
    }
}
