package com.copula.storyart.media.observer;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.support.v4.app.NotificationCompat;
import com.copula.storyart.R;
import com.copula.storyart.app.boot.LandingPageActivity;
import com.storyart.support.widget.Logger;

import java.util.List;

public class MediaObserverService extends JobIntentService implements UriObserver.OnChangeListener {

    private static final int JOB_ID = 1000;

    private static final String NOTIFICATION_CHANNEL_ID = "MediaObserver";
    private static final int NOTIFICATION_ID = 0x0004;
    private final MyBinder myBinder = new MyBinder();
    private NotificationManager mNotificationManager;
    private UriObserver mUriObserver;
    private SystemMediaObserver mSystemMediaObserver;

    /**
     * Convenience method for enqueuing work in to this service.
     */
    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, MediaObserverService.class, JOB_ID, work);
    }

    public static void ackLatestMedia(Context context) {
        new SystemMediaObserver(context).ackAllLatestMedia(context);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mSystemMediaObserver = new SystemMediaObserver(this);
        mUriObserver = UriObserver.getInstance(this, SystemMediaProvider.AUDIO_URL, this);
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        if (mUriObserver != null) mUriObserver.stop();
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public IBinder onBind(@NonNull Intent intent) {
        return myBinder;
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
    }

    @Override
    public void onChange(Uri uri) {
        if (mSystemMediaObserver.hasAddedNewMedia()) {
            try {
                Notification notification = mediaChangesNotification().build();
                mNotificationManager.notify(NOTIFICATION_ID, notification);
            } catch (Exception e) {
                Logger.getInstance("MediaObserverService").e("onChange", e);
            }
        }
    }

    private String composeLatestMediaMessage(List<MediaModel.Audio> l) {
        StringBuilder stringBuilder = new StringBuilder();
        for (MediaModel.Audio audio : l) {
            stringBuilder.append(audio.title).append(", ");
        }

        return stringBuilder.substring(0, stringBuilder.length() - 2);
    }

    private List<MediaModel.Audio> getLatestMedia() {
        return mSystemMediaObserver.getLatestMedia(SystemMediaProvider.AUDIO_MEDIA, "4");
    }

    private NotificationCompat.Builder mediaChangesNotification() {
        List<MediaModel.Audio> latestMedia = getLatestMedia();
        if (latestMedia == null) throw new IllegalStateException("latest media cannot be null");

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        builder.setAutoCancel(true).setGroup("media_observer").setDefaults(0);

        builder.setContentTitle(getString(R.string.ntf_tit_media_observer));
        String message = composeLatestMediaMessage(latestMedia);
        builder.setContentText(message);

        int albumId = latestMedia.get(0).albumId;
        Bitmap largeIcon = SystemMediaProvider.getAlbumThumbnail(this, albumId, 128, 128);
        if (largeIcon == null)
            largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.img_song_thumb_white_128dp);
        builder.setLargeIcon(largeIcon);

        Bitmap bigPicture = SystemMediaProvider.getAlbumThumbnail(this, albumId, 384, 384);
        if (bigPicture != null) {
            builder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bigPicture).setSummaryText(message));
        }

        Intent intent = new Intent(this, LandingPageActivity.class);
        intent.setData(Uri.parse("https://storyartapp.com/system_recent_media"));
        PendingIntent pendingIntent =
                PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        if (pendingIntent != null) builder.setContentIntent(pendingIntent);
        builder.setSmallIcon(R.mipmap.ic_notification).setColor(0xef3131);

        return builder;
    }

    public class MyBinder extends Binder {
        public MediaObserverService getService() {
            return MediaObserverService.this;
        }
    }
}
