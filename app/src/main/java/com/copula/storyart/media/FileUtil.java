package com.copula.storyart.media;

import android.os.Environment;
import com.storyart.support.widget.Logger;

import java.io.*;

/**
 * Created by heeleaz on 1/1/17.
 */
public class FileUtil {
    private static final Logger logger = Logger.getInstance("FileUtil");

    public static final String GRAPHIC_WRITE_PATH = hiddenDirectory(".tmp") + File.separator + "1f22a13c.png";
    public static final String AUDIO_CROP_PATH = hiddenDirectory(".tmp") + File.separator + "2d34a19c.mp3";
    public static final String AUDIO_RECORD_PATH = hiddenDirectory(".tmp") + File.separator + "34ab5e1a.aac";
    public static final String MOVIE_SAVE_PATH = hiddenDirectory(".tmp") + File.separator + "6a28fc1a.mp4";
    public static final String IMAGE_SAVE_PATH = hiddenDirectory(".tmp") + File.separator + "41ac43e7.png";


    public static String hiddenDirectory(String directory) {
        File dir = Environment.getExternalStoragePublicDirectory("Storyart/" + directory);
        //create .nomedia file to as to hide media in tmp from content provider
        if (!dir.exists() && dir.mkdirs()) {
            File output = new File(dir, ".nomedia");
            try {
                output.createNewFile();
            } catch (Exception e) {
                logger.e("Error creating app dir", e);
            }
        }
        return dir.getAbsolutePath();
    }

    public static String getMediaSavePath(String filename) {
        File dir = Environment.getExternalStoragePublicDirectory("Storyart");
        if (!dir.exists()) dir.mkdirs();
        return dir.getAbsolutePath() + File.separator + filename;
    }

    public static boolean copy(File source, File destination) {
        BufferedOutputStream bos = null;
        BufferedInputStream bis = null;
        try {
            bis = new BufferedInputStream((new FileInputStream((source))));
            bos = new BufferedOutputStream(new FileOutputStream(destination));

            int p;
            byte[] buffer = new byte[1024];
            while ((p = bis.read(buffer)) != -1) {
                bos.write(buffer, 0, p);
            }
            return true;
        } catch (Exception e) {
            logger.e("Issue  copying file", e);
            return false;
        } finally {
            try {
                if (bos != null) bos.close();
                if (bis != null) bis.close();
            } catch (Exception ignored) {
            }
        }
    }
}
