package com.copula.storyart.media.observer;

import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;

class UriObserver extends ContentObserver {
    private boolean mRunning = true;
    private OnChangeListener listener;
    private Context context;

    private UriObserver(Context ctx, Uri uri, OnChangeListener l) {
        super(new Handler(Looper.myLooper()));

        this.context = ctx;
        this.listener = l;
        ctx.getContentResolver().registerContentObserver(uri, true, this);
    }

    static UriObserver getInstance(Context ctx, Uri uri, OnChangeListener listener) {
        return new UriObserver(ctx, uri, listener);
    }

    public void stop() {
        context.getContentResolver().unregisterContentObserver(this);
        mRunning = false;
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        if (mRunning) listener.onChange(uri);
    }

    @Override
    public void onChange(boolean selfChange) {
        onChange(selfChange, null);

    }

    public interface OnChangeListener {
        void onChange(Uri uri);
    }
}