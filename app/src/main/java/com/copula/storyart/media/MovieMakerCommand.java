package com.copula.storyart.media;

import android.content.Context;
import com.copula.storyart.design.GraphicAudioProperty;
import com.copula.storyart.design.GraphicImageProperty;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.FFmpegExecuteResponseHandler;
import com.storyart.support.widget.Logger;

import java.io.File;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by heeleaz on 10/30/17.
 */
public class MovieMakerCommand implements ICommand, FFmpegExecuteResponseHandler {
    private static final Logger logger = Logger.getInstance("MovieMakerCommand");
    private static final int DEFAULT_MOVIE_LENGTH = 5;

    private GraphicAudioProperty mGraphicAudioProperty;
    private List<GraphicImageProperty> mGifsAssetsProperty;
    private String outPath, imagePath;
    private Context mContext;
    private ICommand.ICommandListener<MovieMakerCommand> mICommandListener;

    public MovieMakerCommand(Context context) {
        this.mContext = context;
    }

    public static void destroyMovieMakingProgress(Context context) {
        FFmpeg.getInstance(context).killRunningProcesses();
    }

    public int getRenderedTimeInSecs(String progressMessage) {
        if (progressMessage == null) return -1;

        Pattern pattern = Pattern.compile("(\\d{2}:\\d{2}:\\d{2})");
        Matcher matcher = pattern.matcher(progressMessage);

        int seconds = -1;
        try {
            if (matcher.find()) {//get the first occurrence only
                String time = matcher.group(0);
                seconds = Integer.parseInt(time.split(":")[2]);
            }
        } catch (Exception e) {
            logger.e("Error parsing time", e);
        }

        return seconds;
    }


    private String[] buildAudioVideoCommand() {
        //qp(Quantization Parameter) in libx264 set to 29 === -qp 35 :: not used
        //bitrate setup (1mb with 1 pass): -b:v 1M -pass 1 -f mp4 /dev/null
        //for fast encoding in streaming: -tune zerolatency and -preset ultrafast
        //input and output framerate: -framerate 1 :: since we dealing with still image
        //video bitrate is set 56kbps for low SD: -b:v 56k
        //audio codec is native aac for h264 support: -c:a acc
        //audio bitrate is set 24kbps for low SD: -b:v 26k(lossy) 64k(c-transparent)
        //audio channel is set mono for low SD: -ac 1
        //video length is set min(audio_len, video_len): -shortest

        int inputCount = 0;
        CommandChainAdapter commandChain = new CommandChainAdapter();

        int fps = getPresetFramerate();
        commandChain.put("-framerate", fps).put("-loop", 1).put("-i", imagePath);
        inputCount += 1;

        if (isRenderingAudio()) {
            inputCount += 1;
            commandChain.put("-i", mGraphicAudioProperty.editorAudioPath);
        }

        if (isRenderingGifs()) {
            int lastInputIndex = inputCount - 1;
            gifsOverlayCommand(commandChain, mGifsAssetsProperty, lastInputIndex);
        }

        commandChain.put("-c:v", "libx264");
        if (!isRenderingAudio()) commandChain.put("-t", DEFAULT_MOVIE_LENGTH);

        String bitrate = calculateBitrateGuage(600, 1066, fps);
        commandChain.put("-tune", "zerolatency")
                .put("-preset", "ultrafast").put("-profile:v", "baseline")
                .put("-level", "3.0").put("-movflags", "faststart")
                .put("-pix_fmt", "yuv420p").put("-b:v", bitrate);

        if (isRenderingAudio()) {
            commandChain.put("-c:a", "aac").put("-b:a", "64k").put("-ac", 1)
                    .put("-strict", "experimental").put("-shortest");
        }

        commandChain.put("-vf", "scale=516:trunc(ow/a/2)*2, setdar=9:16").put("-y").put(outPath);
        return commandChain.toArray();
    }

    private String calculateBitrateGuage(int w, int h, int fps) {
        double bitrate = ((w * h * fps) * 1 * 0.07) / 1024;//kush
        bitrate = Math.min(350, bitrate);//maintain at 500k max
        return (int) (bitrate) + "k";
    }

    private int getPresetFramerate() {
        return isRenderingGifs() ? 12 : 1;
    }

    private boolean isRenderingAudio() {
        return mGraphicAudioProperty != null && mGraphicAudioProperty.editorAudioPath != null;
    }

    private boolean isRenderingGifs() {
        return mGifsAssetsProperty != null && mGifsAssetsProperty.size() > 0;
    }

    private void gifsOverlayCommand(CommandChainAdapter command, List<GraphicImageProperty> images, int lastInputIndex) {
        for (GraphicImageProperty assetProperty : images) {
            command.put("-ignore_loop", 0).put("-i", assetProperty.path);
        }

        StringBuilder filterComplex = new StringBuilder();
        for (int i = 0; i < images.size(); i++) {
            GraphicImageProperty p = images.get(i);

            String scalef = "[" + ++lastInputIndex + "]scale=" + p.width + ":-1[inner];";
            String inA = (i == 0) ? "[0]" : "[tmp]";
            String inB = "[inner]";
            String overlay = inA + inB + "overlay=" + p.x + ":" + p.y;
            String out = "[tmp];";

            filterComplex.append(scalef).append(overlay).append(out);
        }
        filterComplex.delete(filterComplex.length() - 6, filterComplex.length());
        command.put("-filter_complex", filterComplex.toString());
    }

    @Override
    public String[] getCommand() {
        return buildAudioVideoCommand();
    }

    public void execute(ICommandListener listener) {
        this.mICommandListener = listener;

        try {
            FFmpeg ffmpeg = FFmpeg.getInstance(mContext);
            ffmpeg.killRunningProcesses();
            ffmpeg.execute(getCommand(), this);
        } catch (Exception e) {
            if (mICommandListener != null)
                mICommandListener.onFailure(this, e.getMessage());
        }
    }

    public MovieMakerCommand setImageProperty(File imagePath) {
        this.imagePath = imagePath.getPath();
        return this;
    }

    public MovieMakerCommand setAudioProperty(GraphicAudioProperty property) {
        this.mGraphicAudioProperty = property;
        return this;
    }

    public MovieMakerCommand setGifsAssetsList(List<GraphicImageProperty> property) {
        this.mGifsAssetsProperty = property;
        return this;
    }

    public MovieMakerCommand setOutPath(File outPath) {
        if (outPath.exists()) outPath.delete();
        this.outPath = outPath.getPath();

        return this;
    }

    public int getDuration() {
        if (mGraphicAudioProperty != null) return mGraphicAudioProperty.editorDuration;
        return DEFAULT_MOVIE_LENGTH;
    }

    @Override
    public void onSuccess(String s) {
        logger.i("Movie maker successful: " + s);
        if (mICommandListener != null) mICommandListener.onSuccessful(this, s);
    }

    @Override
    public void onProgress(String s) {
        logger.i("Movie maker progress: " + s);
        if (mICommandListener != null) mICommandListener.onProgress(this, s);
    }

    @Override
    public void onFailure(String s) {
        logger.i("Movie maker failed: " + s);
        if (mICommandListener != null) mICommandListener.onProgress(this, s);
    }

    @Override
    public void onStart() {
        logger.i("Movie maker started");
    }

    @Override
    public void onFinish() {
        logger.i("Movie making finished");
    }
}
