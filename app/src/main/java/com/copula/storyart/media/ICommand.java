package com.copula.storyart.media;

/**
 * Created by heeleaz on 10/23/17.
 */
public interface ICommand {
    String[] getCommand();

    interface ICommandListener<T> {
        void onProgress(T object, String message);

        void onSuccessful(T object, String message);

        void onFailure(T object, String message);
    }
}
