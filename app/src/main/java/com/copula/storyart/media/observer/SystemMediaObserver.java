package com.copula.storyart.media.observer;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 12/28/16.
 */
public class SystemMediaObserver {
    private static final String TAG = SystemMediaObserver.class.getSimpleName();

    private SharedPreferences preference;
    private Context mContext;

    public SystemMediaObserver(@NonNull Context context) {
        mContext = context;
        preference = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
    }

    public void ackAllLatestMedia(Context context) {
        SystemMediaObserver observer = new SystemMediaObserver(context);
        observer.ackLatestMedia(SystemMediaProvider.AUDIO_MEDIA);
    }

    private void ackLatestMedia(int mediaType) {
        long date = getMostRecentMediaAddedDate(mediaType);
        if (date == -1) return;

        SharedPreferences.Editor editor = preference.edit();
        editor.putLong("recent_media_date_" + mediaType, date).apply();
    }

    public boolean hasAddedNewMedia() {
        long topMediaDate = getMostRecentMediaAddedDate(SystemMediaProvider.AUDIO_MEDIA);
        long ackDate = getLastAckMediaDate(SystemMediaProvider.AUDIO_MEDIA);

        return topMediaDate > ackDate;
    }

    private long getMostRecentMediaAddedDate(int mediaType) {
        String orderBy = MediaStore.MediaColumns.DATE_ADDED + " DESC LIMIT 1";

        Uri uri = SystemMediaProvider.getMediaUrl(mediaType);
        Cursor cu = SystemMediaProvider
                .runQuery(mContext, uri, null, null, null, orderBy, null);

        if (cu != null && cu.moveToNext()) {
            return cu.getLong(cu.getColumnIndex(MediaStore.MediaColumns.DATE_ADDED));
        }
        return -1;
    }

    private <T> List<T> getLatestMediaFrom(int mediaType, long fromDate, String limit) {
        String selection = MediaStore.MediaColumns.DATE_ADDED + ">=" + fromDate;
        String orderBy = MediaStore.MediaColumns.DATE_ADDED + " DESC";
        if (limit != null) orderBy += " LIMIT " + limit;

        if (mediaType == SystemMediaProvider.AUDIO_MEDIA) {
            List<MediaModel.Audio> a = SystemMediaProvider.getAudios(mContext, selection, orderBy);
            if (a.size() > 0) {
                List<T> list = new ArrayList<>(a.size());
                for (MediaModel.Audio audio : a) list.add((T) audio);
                return list;
            }
        }
        return null;
    }

    private long getLastAckMediaDate(int mediaType) {
        return preference.getLong("recent_media_date_" + mediaType, 0);
    }

    public <T> List<T> getLatestMedia(int mediaType, @Nullable String limit) {
        return getLatestMediaFrom(mediaType, getLastAckMediaDate(mediaType), limit);
    }
}
