package com.copula.storyart.media;

import android.content.Context;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.FFmpegLoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

/**
 * Created by heeleaz on 11/1/17.
 */
public class MediaEngineInitializer implements FFmpegLoadBinaryResponseHandler {
    private ICommand.ICommandListener<MediaEngineInitializer> listener;
    private Context mContext;

    public MediaEngineInitializer(Context context) {
        this.mContext = context;
    }

    public void initialize(ICommand.ICommandListener l) {
        this.listener = l;
        try {
            FFmpeg.getInstance(mContext).loadBinary(this);
        } catch (FFmpegNotSupportedException e) {
            if (listener != null)
                listener.onFailure(this, "Device not supported");
        }
    }

    @Override
    public void onFailure() {
        if (listener != null)
            listener.onFailure(this, "Initialization failed");
    }

    @Override
    public void onSuccess() {
        if (listener != null)
            listener.onSuccessful(this, "Initialization successful");
    }

    @Override
    public void onStart() {
        if (listener != null)
            listener.onProgress(this, "Initialization started");
    }

    @Override
    public void onFinish() {
    }
}
