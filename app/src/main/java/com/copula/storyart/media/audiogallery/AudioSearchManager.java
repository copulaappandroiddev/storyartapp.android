package com.copula.storyart.media.audiogallery;


import com.copula.storyart.media.observer.MediaModel;
import com.storyart.support.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 7/11/17.
 */
public class AudioSearchManager {
    private List<MediaModel.Audio> audioList = new ArrayList<>();
    private List<MediaModel.Audio> matchedEntry = new ArrayList<>();

    public void setAudioList(List<MediaModel.Audio> audioList) {
        this.audioList = audioList;
    }

    public boolean isSearchable() {
        return audioList != null && audioList.size() > 0;
    }

    public List<MediaModel.Audio> resolve(String query) {
        matchedEntry.clear();

        for (MediaModel.Audio a : audioList) {
            if (StringUtil.containsIgnoreCase(a.title, query) || StringUtil.containsIgnoreCase(a.artist, query) ||
                    StringUtil.containsIgnoreCase(a.album, query)) {
                matchedEntry.add(a);
            }
        }
        return matchedEntry;
    }

    public List<MediaModel.Audio> getAudioList() {
        return audioList;
    }
}
