package com.copula.storyart.media;

import android.media.MediaRecorder;
import android.os.Handler;
import android.os.Looper;

/**
 * Created by heeleaz on 11/2/17.
 */
public class AudioRecordHelper {
    private MediaRecorder mMediaRecorder;
    private int currentDurationMilli, recordMaxDuration;
    private int notifyAtIntervalMilli = 1000;
    private boolean isRecording = false;

    private OnRecordPositionUpdateListener listener;

    public AudioRecordHelper(OnRecordPositionUpdateListener listener) {
        if ((this.listener = listener) == null)
            throw new IllegalArgumentException("OnCategorySelected cannot be null");

        mMediaRecorder = new MediaRecorder();
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
        mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
    }

    public void startRecording(String audioWritePath) {
        try {
            mMediaRecorder.setOutputFile(audioWritePath);
            mMediaRecorder.prepare();
            mMediaRecorder.start();

            isRecording = true;

            setupAudioDurationCounter(0);
        } catch (Exception e) {
        }
    }

    private void setupAudioDurationCounter(final int firstCounterDelay) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isRecording) return;

                currentDurationMilli += notifyAtIntervalMilli;
                listener.onPeriodicNotification(AudioRecordHelper.this);

                if (recordMaxDuration == currentDurationMilli)
                    listener.onMaxDurationReached(AudioRecordHelper.this);

                //recall this current editorDuration counter
                setupAudioDurationCounter(notifyAtIntervalMilli);
            }
        }, firstCounterDelay);
    }

    public void setListener(OnRecordPositionUpdateListener l) {
        this.listener = l;
    }

    public int getCurrentDurationMilli() {
        return currentDurationMilli;
    }

    public void setRecordMaxDuration(int timeInSeconds) {
        this.recordMaxDuration = timeInSeconds * 1000;
    }

    public void stopRecording() {
        try {
            isRecording = false;

            mMediaRecorder.stop();
            mMediaRecorder.release();
            mMediaRecorder = null;
        } catch (Exception ignored) {
        }
    }

    public interface OnRecordPositionUpdateListener {
        void onMaxDurationReached(AudioRecordHelper recorder);

        void onPeriodicNotification(AudioRecordHelper recorder);
    }
}
