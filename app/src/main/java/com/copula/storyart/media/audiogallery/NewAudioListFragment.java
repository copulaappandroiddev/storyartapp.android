package com.copula.storyart.media.audiogallery;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import com.copula.storyart.R;
import com.copula.storyart.media.observer.MediaModel;
import com.copula.storyart.media.observer.SystemMediaObserver;
import com.copula.storyart.media.observer.SystemMediaProvider;
import com.storyart.support.widget.IResultCallback;

import java.util.List;

public class NewAudioListFragment extends DialogFragment implements AdapterView.OnItemClickListener {
    public static final int REQUEST_CODE = 0x0A10;

    private AudioListAdapter mMediaListAdapter;
    private AsyncMediaLoader mAsyncMediaLoader;

    public static void show(FragmentManager fragmentManager) {
        FragmentTransaction ft = fragmentManager.beginTransaction();

        Fragment oldFragment = fragmentManager.findFragmentByTag("NewAudioListFragment");
        if (oldFragment != null) ft.remove(oldFragment);

        NewAudioListFragment fragment = new NewAudioListFragment();
        fragment.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogTheme);
        ft.add(fragment, "NewAudioListFragment").show(fragment).commit();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstance) {
        View view = inflater.inflate(R.layout.frg_new_audio_list, container, false);

        ListView listView = view.findViewById(R.id.listview);
        listView.setAdapter(mMediaListAdapter = new AudioListAdapter(getActivity()));
        listView.setOnItemClickListener(this);

        view.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        (mAsyncMediaLoader = new AsyncMediaLoader()).execute();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        MediaModel.Audio media = (MediaModel.Audio) parent.getItemAtPosition(position);

        if (getActivity() instanceof IResultCallback) {
            Intent intent = new Intent().putExtra("audioMedia", media);
            ((IResultCallback) getActivity()).onFragmentResult(REQUEST_CODE, IResultCallback.RESULT_OK, intent);
            dismiss();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAsyncMediaLoader != null) mAsyncMediaLoader.cancel(true);
    }

    private class AsyncMediaLoader extends AsyncTask<Void, Void, List<MediaModel.Audio>> {
        @Override
        protected List<MediaModel.Audio> doInBackground(Void... voids) {
            SystemMediaObserver systemMediaObserver = new SystemMediaObserver(getActivity());
            return systemMediaObserver.getLatestMedia(SystemMediaProvider.AUDIO_MEDIA, null);
        }

        @Override
        protected void onPostExecute(List<MediaModel.Audio> audioMedia) {
            if (!isAdded() || isCancelled() || audioMedia == null) {
                dismiss();//dismiss since entry is null / cancelled or dialog is removed
                return;
            }

            mMediaListAdapter.addOrReplaceHandlers(audioMedia);
            mMediaListAdapter.notifyDataSetChanged();
        }
    }

}
