package com.copula.storyart.media;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 12/18/17.
 */
public class CommandChainAdapter {
    private List<String> arrayList = new ArrayList<>();

    CommandChainAdapter put(String v) {
        arrayList.add(v);
        return this;
    }

    CommandChainAdapter put(int v) {
        return put(v + "");
    }

    CommandChainAdapter put(String k, String v) {
        return put(k).put(v);
    }

    CommandChainAdapter put(String k, int v) {
        return put(k).put(v);
    }

    String[] toArray() {
        return arrayList.toArray(new String[arrayList.size()]);
    }
}
