package com.copula.storyart.media.observer;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import com.copula.imagefilter.BitmapImageOptimizer;
import com.copula.storyart.design.MediaBackgroundProperty;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class SystemMediaProvider {
    public static final int AUDIO_MEDIA = 0x0001;
    public static final int IMAGE_MEDIA = 0x0002;
    public static final int VIDEO_MEDIA = 0x0003;

    public static Uri AUDIO_URL = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
    public static Uri IMAGE_URL = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
    public static Uri VIDEO_URL = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;

    public static Cursor runQuery(Context ctx, Uri uri, String[] projection, String selection
            , String[] selectionArgs, String sortOrder, String limit) {
        try {
            ContentResolver cr = ctx.getContentResolver();
            String l = (limit != null) ? " LIMIT " + limit : "";
            return cr.query(uri, projection, selection, selectionArgs, sortOrder + l);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Nullable
    public static Uri getMediaUrl(int mediaType) {
        switch (mediaType) {
            case AUDIO_MEDIA:
                return AUDIO_URL;
            case VIDEO_MEDIA:
                return VIDEO_URL;
            case IMAGE_MEDIA:
                return IMAGE_URL;
            default:
                return null;
        }
    }

    public static List<MediaModel.Audio> getAudios(Context ctx, String selection, String sortOrder) {
        Cursor cu = runQuery(ctx, AUDIO_URL, null, selection, null, sortOrder, null);
        if (cu == null || cu.getCount() <= 0) return new ArrayList<>();

        List<MediaModel.Audio> entries = new ArrayList<>(cu.getCount());
        while (cu.moveToNext()) {
            MediaModel.Audio entry = new MediaModel.Audio();
            entry._id = cu.getInt(cu.getColumnIndex(MediaStore.MediaColumns._ID));
            entry.artist = cu.getString(cu.getColumnIndex(MediaStore.Audio.Media.ARTIST));
            entry.albumId = cu.getInt(cu.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ID));
            entry.album = cu.getString(cu.getColumnIndex(MediaStore.Audio.Albums.ALBUM));
            entry.title = cu.getString(cu.getColumnIndex(MediaStore.Audio.Media.TITLE));
            entry.duration = cu.getLong(cu.getColumnIndex(MediaStore.Audio.Media.DURATION));
            entry.data = cu.getString(cu.getColumnIndex(MediaStore.MediaColumns.DATA));
            entry.filename = cu.getString(cu.getColumnIndex(MediaStore.MediaColumns.DISPLAY_NAME));
            entry.createdAt = cu.getLong(cu.getColumnIndex(MediaStore.MediaColumns.DATE_ADDED));
            entry.mediaType = SystemMediaProvider.AUDIO_MEDIA;
            entries.add(entry);
        }
        cu.close();
        return entries;
    }

    public static MediaModel.Audio getAudio(Context ctx, int id) {
        List<MediaModel.Audio> e = getAudios(ctx, "_id=" + id, null);
        return (e != null && e.size() > 0) ? e.get(0) : null;
    }

    public static Uri getAudioAlbumUri(long albumId) {
        Uri albumArtUri = Uri.parse("content://media/external/audio/albumart");
        return ContentUris.withAppendedId(albumArtUri, albumId);
    }

    public static Bitmap getAlbumThumbnail(Context context, long albumId, int maxWidth, int maxHeight) {
        InputStream is = null;
        try {
            Uri uri = getAudioAlbumUri(albumId);

            ContentResolver res = context.getContentResolver();
            is = res.openInputStream(uri);

            int scale = BitmapImageOptimizer.calcInSampleSize(is, Math.max(maxWidth, maxHeight));
            if (is != null && is.available() > 0) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = scale;
                options.inPreferredConfig = Bitmap.Config.RGB_565;
                options.inPurgeable = true;
                options.inTempStorage = new byte[16 * 1024];
                options.inJustDecodeBounds = false;

                return BitmapFactory.decodeStream(res.openInputStream(uri), null, options);
            }
            return null;
        } catch (Exception e) {
            return null;
        } finally {
            if (is != null) try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static Bitmap getVideoThumbnail(String videoUrl, int maxWidth, int maxHeight) {
        MediaMetadataRetriever retriever = null;
        try {
            retriever = new MediaMetadataRetriever();
            retriever.setDataSource(videoUrl);

            Bitmap bitmap = retriever.getFrameAtTime(-1);

            int maxSize = Math.max(maxHeight, maxWidth);
            int h = (maxSize / bitmap.getHeight()) * maxSize;
            return Bitmap.createScaledBitmap(bitmap, maxSize, h, true);
        } catch (Exception e) {
            return null;
        } finally {
            if (retriever != null) retriever.release();
        }
    }

    public static List<MediaBackgroundProperty> getImageAndVideo(Context context) {
        Uri uri = MediaStore.Files.getContentUri("external");

        ContentResolver cr = context.getContentResolver();
        String selection = "mime_type LIKE ? OR mime_type LIKE ?";
        String[] sArgs = new String[]{"image/%", "video/%"};
        String sortOrder = MediaStore.MediaColumns.DATE_ADDED + " DESC ";
        Cursor cu = cr.query(uri, null, selection, sArgs, sortOrder);

        List<MediaBackgroundProperty> mediaEntries = new ArrayList<>(cu.getCount());
        while (cu.moveToNext()) {
            MediaBackgroundProperty e = new MediaBackgroundProperty();
            e.id = cu.getInt(cu.getColumnIndex(MediaStore.MediaColumns._ID));
            e.backgroundUrl = cu.getString(cu.getColumnIndex(MediaStore.MediaColumns.DATA));
            e.mimeType = cu.getString(cu.getColumnIndex(MediaStore.MediaColumns.MIME_TYPE));
            e.createdAt = cu.getLong(cu.getColumnIndex(MediaStore.MediaColumns.DATE_ADDED));
            e.duration = cu.getLong(cu.getColumnIndex(MediaStore.Video.VideoColumns.DURATION));
            e.contentWidth = cu.getInt(cu.getColumnIndex(MediaStore.Video.VideoColumns.WIDTH));
            e.contentHeight = cu.getInt(cu.getColumnIndex(MediaStore.Video.VideoColumns.HEIGHT));
            mediaEntries.add(e);
        }
        cu.close();
        return mediaEntries;
    }

    public static List<MediaBackgroundProperty> getImages(Context context) {
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        ContentResolver cr = context.getContentResolver();
        String sortOrder = MediaStore.MediaColumns.DATE_ADDED + " DESC ";
        Cursor cu = cr.query(uri, null, null, null, sortOrder);
        if (cu == null || cu.getColumnCount() == 0) return null;

        List<MediaBackgroundProperty> mediaEntries = new ArrayList<>(cu.getCount());
        while (cu.moveToNext()) {
            MediaBackgroundProperty e = new MediaBackgroundProperty();
            e.id = cu.getInt(cu.getColumnIndex(MediaStore.MediaColumns._ID));
            e.backgroundUrl = cu.getString(cu.getColumnIndex(MediaStore.MediaColumns.DATA));
            e.mimeType = cu.getString(cu.getColumnIndex(MediaStore.MediaColumns.MIME_TYPE));
            e.createdAt = cu.getLong(cu.getColumnIndex(MediaStore.MediaColumns.DATE_ADDED));
            e.source = MediaBackgroundProperty.GALLERY_IMAGE;
            mediaEntries.add(e);
        }
        cu.close();
        return mediaEntries;
    }
}