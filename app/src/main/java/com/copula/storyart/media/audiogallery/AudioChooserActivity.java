package com.copula.storyart.media.audiogallery;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.copula.storyart.R;
import com.copula.storyart.media.observer.MediaModel;
import com.copula.storyart.media.observer.SystemMediaProvider;
import com.storyart.support.util.ViewUtils;

import java.util.List;

public class AudioChooserActivity extends FragmentActivity implements AdapterView.OnItemClickListener {
    private AudioListAdapter mMediaListAdapter;
    private AsyncMediaLoader mAsyncMediaLoader;
    private AudioSearchManager mAudioSearchManager = new AudioSearchManager();

    private ListView listView;
    private TextView txtEmptyListDescriptor;

    public static void launch(Activity activity, int resultCode) {
        activity.startActivityForResult(new Intent(activity, AudioChooserActivity.class), resultCode);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.transition.fade_in, R.transition.fade_out);

        ViewUtils.colorStatusBar(this, R.color.blackMediaBackground, 1);
        setContentView(R.layout.act_audio_gallery);

        (listView = findViewById(R.id.listview)).setOnItemClickListener(this);
        listView.setAdapter(mMediaListAdapter = new AudioListAdapter(this));
        (mAsyncMediaLoader = new AsyncMediaLoader()).execute();

        txtEmptyListDescriptor = findViewById(R.id.txt_empty_list);
        ((EditText) findViewById(R.id.search_view)).addTextChangedListener(new SearchViewTextWatcher());
    }

    @Override
    protected void onStop() {
        if (mAsyncMediaLoader != null) mAsyncMediaLoader.cancel(true);

        super.onStop();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        MediaModel.Audio media = (MediaModel.Audio) parent.getItemAtPosition(position);
        Intent resultIntent = new Intent().putExtra("audioMedia", media);

        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    private void search(String query) {
        mMediaListAdapter.clear();

        List<MediaModel.Audio> audioMediaList = mAudioSearchManager.resolve(query);
        if (audioMediaList.size() > 0) {
            mMediaListAdapter.addOrReplaceHandlers(audioMediaList);
            mMediaListAdapter.notifyDataSetChanged();
        }

        if (mMediaListAdapter.getCount() > 0) {
            ViewUtils.showOnly(listView);
        } else {
            String text = (String) getText(R.string.msg_audiogallery_audio_not_found);
            txtEmptyListDescriptor.setText(String.format(text, query));
            ViewUtils.showOnly(txtEmptyListDescriptor);
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.transition.fade_in, R.transition.fade_out);
    }

    private class AsyncMediaLoader extends AsyncTask<Void, Void, List<MediaModel.Audio>> {
        @Override
        protected void onPreExecute() {
            ViewUtils.showOnly(findViewById(R.id.progress_bar));
        }

        @Override
        protected List<MediaModel.Audio> doInBackground(Void... voids) {
            String orderBy = MediaStore.MediaColumns.TITLE;
            return SystemMediaProvider.getAudios(AudioChooserActivity.this, null, orderBy);
        }

        @Override
        protected void onPostExecute(List<MediaModel.Audio> audioMedia) {
            if (audioMedia == null || audioMedia.size() == 0) {
                ViewUtils.showOnly(txtEmptyListDescriptor);
            } else {
                ViewUtils.showOnly(listView);
                mMediaListAdapter.addOrReplaceHandlers(audioMedia);
                mMediaListAdapter.notifyDataSetChanged();

                mAudioSearchManager.setAudioList(audioMedia);
            }
        }
    }

    private class SearchViewTextWatcher implements TextWatcher {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() > 0 && mAudioSearchManager.isSearchable()) {
                search(s.toString());
            } else {
                mMediaListAdapter.addOrReplaceHandlers(mAudioSearchManager.getAudioList());
                mMediaListAdapter.notifyDataSetChanged();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
    }
}
