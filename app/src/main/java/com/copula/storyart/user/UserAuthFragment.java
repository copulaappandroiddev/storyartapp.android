package com.copula.storyart.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.copula.storyart.R;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.util.Arrays;

public class UserAuthFragment extends DialogFragment {
    private static final String TAG = "UserAuthFragment";
    private LoginButton loginButton;
    private CallbackManager callbackManager;

    public static void launch(FragmentManager fragmentManager) {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        Fragment oldFragment = fragmentManager.findFragmentByTag(TAG);
        if (oldFragment != null) ft.remove(oldFragment);


        UserAuthFragment fragment = new UserAuthFragment();
        fragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        ft.add(fragment, TAG).show(fragment).commit();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle si) {
        View view = inflater.inflate(R.layout.frg_autentication, container, false);
        loginButton = view.findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        callbackManager = CallbackManager.Factory.create();
        loginButton.registerCallback(callbackManager, new MyCallbackManager());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    private class MyCallbackManager implements FacebookCallback<LoginResult> {
        @Override
        public void onSuccess(LoginResult loginResult) {
            Log.d("Hello", UserStarter.isLogged() + "");
            if (UserStarter.isLogged()) {

            }
        }

        @Override
        public void onCancel() {
            //do nothing
        }

        @Override
        public void onError(FacebookException error) {
            if (!isAdded()) return;
            Toast.makeText(getActivity(), R.string.msg_auth_failed, Toast.LENGTH_LONG).show();
        }
    }
}
