package com.copula.storyart.user;

import android.support.v4.app.FragmentActivity;
import com.facebook.AccessToken;

public class UserStarter {
    public static boolean isLogged() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null && !accessToken.isExpired();
    }

    public static void launch(FragmentActivity activity) {
        if (!isLogged()) {
            UserAuthFragment.launch(activity.getSupportFragmentManager());
        }
    }
}
