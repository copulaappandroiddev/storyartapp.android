package com.copula.storyart.app.creator.emojiimage;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.copula.imagefilter.BitmapImageOptimizer;
import com.copula.storyart.R;
import com.copula.storyart.app.creator.Configuration;
import com.copula.storyart.app.creator.helper.cache.FileCacheSystem;
import com.copula.storyart.app.creator.storymedia.gallery.MediaGalleryCActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class AddEmojiFragment extends DialogFragment implements View.OnClickListener {
    private static final String TAG = "AddEmojiFragment";
    private EditText edtStickerName;
    private ImageView imgStickerSample;

    public static void launch(FragmentManager fragmentManager) {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        Fragment oldFragment = fragmentManager.findFragmentByTag(TAG);
        if (oldFragment != null) ft.remove(oldFragment);


        AddEmojiFragment fragment = new AddEmojiFragment();
        fragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        ft.add(fragment, TAG).show(fragment).commit();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup c, @Nullable Bundle si) {
        View view = inflater.inflate(R.layout.frg_add_emoji, c, false);
        (imgStickerSample = view.findViewById(R.id.img_sticker_sample)).setOnClickListener(this);
        edtStickerName = view.findViewById(R.id.edt_sticker_name);
        (view.findViewById(R.id.btn_submit)).setOnClickListener(this);

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            String url = data.getDataString();
            Glide.with(getActivity()).load(url).override(256, 256).into(imgStickerSample);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.img_sticker_sample) {
            Intent intent = new Intent(getActivity(), MediaGalleryCActivity.class);
            startActivityForResult(intent, 100);
        } else if (v.getId() == R.id.btn_submit) {
            new SaveStickerTask().execute();
        }
    }

    private class SaveStickerTask extends AsyncTask<Void, Void, Boolean> {
        FileCacheSystem fileCacheSystem = new FileCacheSystem(
                Configuration.Stickers.CUSTOM_STICKERS_DIR, Configuration.Stickers.CUSTOM_STICKERS_SIZE);

        @Override
        protected Boolean doInBackground(Void... voids) {
            Drawable d = imgStickerSample.getDrawable();
            if (d == null) return false;

            String cacheName = "custom-sticker-" + System.currentTimeMillis();
            if (writeToCache(((BitmapDrawable) d).getBitmap(), cacheName)) {
                //proceed to writing to db
            }
            return true;
        }

        private boolean writeToCache(Bitmap bitmap, String fileCacheName) {
            FileInputStream fis = null;
            try {
                File tmp = File.createTempFile("ci_temp", null);
                return BitmapImageOptimizer.writeBitmapToFile(bitmap, tmp.getPath())
                        && fileCacheSystem.cache(fis = new FileInputStream(tmp), fileCacheName);

            } catch (IOException e) {
                return false;
            } finally {
                if (fis != null) try {
                    fis.close();
                } catch (IOException ignore) {
                }
            }
        }
    }
}
