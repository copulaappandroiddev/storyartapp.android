package com.copula.storyart.app.boot;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.transition.Fade;
import android.support.transition.Transition;
import android.support.transition.TransitionManager;
import android.view.View;
import android.view.ViewGroup;
import com.copula.storyart.R;
import com.storyart.support.widget.Logger;

/**
 * Created by heeleaz on 12/3/16.
 */
public class OnboardingPageActivity extends AbsBootstrapActivity implements View.OnClickListener {
    private static Logger logger = Logger.getInstance("OnboardingPageActivity");

    private ViewGroup[] splashView = new ViewGroup[4];
    private int[] transitioningBackgroundColorList = {
            Color.parseColor("#8b6990"), Color.parseColor("#26c4dc"),
            Color.parseColor("#a62c71"), Color.parseColor("#7acba5")};

    private ViewGroup fragmentView;
    private boolean isTransiting = true;

    private Handler mHandler = new Handler(Looper.getMainLooper());

    public static void launch(Context ctx) {
        ctx.startActivity(new Intent(ctx, OnboardingPageActivity.class));
    }

    private static ViewGroup createView(Context ctx, int viewId) {
        ViewGroup viewGroup = null;
        try {
            viewGroup = (ViewGroup) View.inflate(ctx, viewId, null);
        } catch (Exception ignored) {
            logger.e("View cannot be created", ignored);
        }
        return viewGroup;
    }

    @Override
    protected void onInitProgress(int progress, String message) {

    }

    @Override
    protected void onInitCompleted(String message) {
        LandingPageActivity.launch(this);
        finish();
    }

    @Override
    protected void onInitFailure(int cause, String message) {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_splash_screen);
        fragmentView = findViewById(R.id.splash_fragment);

        findViewById(R.id.btn_start).setOnClickListener(this);

        splashView[0] = createView(this, R.layout.view_splash_typeface_desc);
        splashView[1] = createView(this, R.layout.view_splash_audio_desc);
        splashView[2] = createView(this, R.layout.view_splash_emocion_desc);
        splashView[3] = createView(this, R.layout.view_splash_share_desc);
    }

    @Override
    protected void onStart() {
        super.onStart();
        isTransiting = true;
        postPagerSliderWithSpecifiedDelay(0, 0);
    }

    private void postPagerSliderWithSpecifiedDelay(final int delay, final int startIndex) {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isTransiting) return;

                try {
                    int start = startIndex % splashView.length;
                    int stop = (startIndex + 1) % splashView.length;

                    addViewWithTransition(splashView[start]);
                    setContentBackgroundWithTransition(transitioningBackgroundColorList[start], transitioningBackgroundColorList[stop]);

                    postPagerSliderWithSpecifiedDelay(2000, startIndex + 1);
                } catch (Exception e) {
                    //in case of crash. just jump to requesting permission for app proceed
                    requestVerifyV23ApplicationPermission();
                }
            }
        }, delay);
    }

    private void addViewWithTransition(ViewGroup inView) {
        Transition transition = new Fade();
        transition.setDuration(1200);
        TransitionManager.beginDelayedTransition(fragmentView, transition);
        fragmentView.removeAllViews();

        TransitionManager.beginDelayedTransition(fragmentView, transition);
        if (inView != null) fragmentView.addView(inView, 0);
    }

    private void setContentBackgroundWithTransition(int fromColor, int toColor) {
        final View content = findViewById(android.R.id.content);
        ValueAnimator animator = ValueAnimator.ofInt(fromColor, toColor);
        animator.setEvaluator(new ArgbEvaluator());
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int colorValue = (Integer) animation.getAnimatedValue();
                content.setBackgroundColor(colorValue);
            }
        });
        animator.setDuration(2000).start();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_start) requestVerifyV23ApplicationPermission();
    }

    @Override
    protected void onStop() {
        super.onStop();
        isTransiting = false;
    }
}
