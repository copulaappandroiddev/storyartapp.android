package com.copula.storyart.app.creator.addonimages.recentaddonimages;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.copula.storyart.R;
import com.copula.storyart.app.creator.addonimages.recentaddonimages.data.RecentAddonDao;
import com.copula.storyart.app.creator.addonimages.recentaddonimages.data.RecentAddonEntity;
import com.copula.storyart.app.creator.emojiimage.AddEmojiFragment;
import com.copula.storyart.app.creator.emojiimage.inappemojis.AssetEmojiIconUtil;
import com.copula.storyart.app.creator.helper.AbsImageListAdapter;
import com.copula.storyart.app.creator.helper.AbsImageListFragment;
import com.copula.storyart.design.GraphicImageProperty;

/**
 * Created by heeleaz on 2/10/18.
 */
public class RecentAddonFragment extends AbsImageListFragment {
    private MyAdapter mAdapter;
    private RecentAddonDao mRecentAddonDao;

    public static RecentAddonFragment instantiate() {
        return new RecentAddonFragment();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mRecentAddonDao = RecentAddonDao.getInstance(getContext());
        onLoadMore();
    }

    @Override
    protected BaseAdapter getBaseAdapter(Context context) {
        return mAdapter = new MyAdapter(getContext());
    }

    @Override
    protected int getLoadingTriggerThreshold() {
        return 100;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (position == 0) {
            AddEmojiFragment.launch(getChildFragmentManager());
        } else {
            RecentAddonEntity m = (RecentAddonEntity) parent.getItemAtPosition(position);
            if (m.resourceUrl == null) return;

            Intent intent = new Intent().putExtra("data", m.resourceUrl);
            intent.putExtra("source", m.type);
            Activity activity = getActivity();
            activity.setResult(Activity.RESULT_OK, intent);
            activity.finish();
        }
    }

    @Override
    public void onLoadMore() {
        mAdapter.addHandler(0, new RecentAddonEntity());
        mAdapter.addHandlers(mRecentAddonDao.fetchAllRecord("0,50"));
        mAdapter.notifyDataSetChanged();

        if (mAdapter.isEmpty())
            invalidateDataFetchFailed(getString(R.string.msg_recent_sticker_empty), null);
    }

    @Override
    public boolean hasLoadedAllItems() {
        return true;
    }

    private class MyAdapter extends AbsImageListAdapter<RecentAddonEntity> {
        private static final int ADD_EMOJI_VIEW = SELECT_IMAGE_VIEW_TYPE + 1;

        MyAdapter(Context context) {
            super(context, null, R.layout.adp_sticker_image);
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public int getItemViewType(int position) {
            return position == 0 ? ADD_EMOJI_VIEW : SELECT_IMAGE_VIEW_TYPE;
        }

        @Override
        public View getView(int position, View convertView, LayoutInflater inflater) {
            return super.getView(position, convertView, inflater);
        }

        @Override
        protected String getTitle(int position) {
            return getItem(position).title;
        }

        @Override
        protected String getImageResource(int position) {
            return getItem(position).resourceUrl;
        }

        @Override
        protected boolean loadImage(int position, String url, ImageView imageView, ViewHolder holder) {
            if (getItem(position).type == GraphicImageProperty.ASSET_BITMAP_IMAGE) {
                url = AssetEmojiIconUtil.getIconPath(getContext(), url);
            }

            Glide.with(getContext()).load(url).into(imageView);
            holder.contentView.getChildAt(1).setVisibility(View.GONE);
            return true;

            //TODO: Check how this actually shows GIF without specifying it.
        }

        @Override
        protected View getCustomView(int position, View convertView, LayoutInflater inflater) {
            return inflater.inflate(R.layout.adp_add_custom_sticker, null, false);
        }
    }
}
