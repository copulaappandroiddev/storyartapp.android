package com.copula.storyart.app.creator.storymedia.gallery;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import com.copula.storyart.app.creator.storymedia.MediaSourceCallback;

public class MediaGalleryCActivity extends FragmentActivity implements MediaSourceCallback {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(android.R.id.content, MediaGalleryFragment.instantiate(MediaGalleryFragment.HIDE_OPEN_CAMERA_FLAG));
        ft.commit();
    }

    @Override
    public void requestSource(int request) {
        //do nothing
    }

    @Override
    public void onMediaAvailable(String mediaUrl) {
        setResult(Activity.RESULT_OK, new Intent().setData(Uri.parse(mediaUrl)));
        finish();
    }
}
