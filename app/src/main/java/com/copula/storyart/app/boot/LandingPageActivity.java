package com.copula.storyart.app.boot;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Toast;
import com.copula.storyart.R;
import com.copula.storyart.app.creator.storymedia.TextGraphicCreatorActivity;


/**
 * Created by heeleaz on 12/13/16.
 */
public class LandingPageActivity extends AbsBootstrapActivity {
    public static void launch(Context context) {
        Intent intent = new Intent(context, LandingPageActivity.class);
        context.startActivity(intent.setAction(Intent.ACTION_MAIN));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences preferences = getPreferences(Context.MODE_PRIVATE);
        if (!preferences.getBoolean("is_splash_shown", false)) {
            OnboardingPageActivity.launch(this);
            preferences.edit().putBoolean("is_splash_shown", true).apply();
            finish();
        } else requestVerifyV23ApplicationPermission();
    }

    @Override
    protected void onInitProgress(int progress, String message) {
        //TODO: Log a message
    }

    @Override
    protected void onInitCompleted(String message) {
        Intent intent = IntentLaunchAdapter.getLauncherIntent(this, getIntent());
        if (intent != null) startActivity(intent);
        else TextGraphicCreatorActivity.launch(this, (String) null);

        finish();
    }

    @Override
    protected void onInitFailure(int cause, String message) {
        Toast.makeText(this, R.string.msg_ffmpeg_init_failed, Toast.LENGTH_LONG).show();
        finish();//close application
    }
}
