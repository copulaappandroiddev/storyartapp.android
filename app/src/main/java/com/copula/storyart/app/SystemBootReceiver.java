package com.copula.storyart.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.copula.storyart.media.observer.MediaObserverService;
import com.storyart.emojikeyboard.EmojiManager;
import com.storyart.emojikeyboard.systememoji.SystemEmojiProvider;

public class SystemBootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        EmojiManager.install(new SystemEmojiProvider());
        MediaObserverService.enqueueWork(context, new Intent());
    }
}