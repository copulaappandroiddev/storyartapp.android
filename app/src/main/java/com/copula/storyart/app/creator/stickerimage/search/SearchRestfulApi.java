package com.copula.storyart.app.creator.stickerimage.search;

import android.content.Context;
import android.os.AsyncTask;
import com.copula.smartjson.OJsonMapper;
import com.copula.storyart.app.StoryartApiEndpoint;
import com.copula.storyart.app.creator.helper.restful.AbsImageResourceApi;
import com.copula.storyart.app.creator.helper.restful.AbsRestfulApi;
import com.storyart.support.webservice.GetParam;
import com.storyart.support.webservice.NettyApiProvider;
import com.storyart.support.webservice.RestResponse;

import java.util.List;

/**
 * Created by heeleaz on 2/11/18.
 */
public class SearchRestfulApi extends AbsRestfulApi<List<AbsImageResourceApi.ApiModel>> {
    private static final int REST_CACHE_SIZE = 1024 * 1024; //1MB
    private static final String ENDPOINT = "/stickers/search_stickers";

    private NettyApiProvider apiProvider = new NettyApiProvider(StoryartApiEndpoint.ENDPOINT);
    private Context mContext;
    private String query;

    SearchRestfulApi(Context context, String query, String page) {
        super(page);
        this.query = query;
        this.mContext = context;
    }

    @Override
    protected RestResponse<List<AbsImageResourceApi.ApiModel>> get(String page) throws Exception {
        GetParam param = new GetParam();
        param.put("q", query).put("page", page);

        apiProvider.cache(mContext.getCacheDir(), REST_CACHE_SIZE);
        NettyApiProvider.Result result = apiProvider.get(ENDPOINT, param);
        if (result == null || result.responseCode != 200) return null;

        RestResponse<List<AbsImageResourceApi.ApiModel>> rest = OJsonMapper.mapOne(RestResponse.class, result.response);
        if (rest != null) {
            rest.result = OJsonMapper.mapList(AbsImageResourceApi.ApiModel.class, result.response);
        }

        //this process may have been cancelled.
        //inother to attain sanity in workflow, we intend to return an empty data
        if (isCancelled()) return null;
        return rest;
    }

    @Override
    public void execute(Listener listener) {
        this.mListener = listener;
        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
