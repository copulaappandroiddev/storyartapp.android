package com.copula.storyart.app.creator.graphicbackground.patterns;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.LayoutTransition;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.copula.storyart.R;
import com.copula.storyart.app.creator.Configuration;
import com.copula.storyart.app.creator.helper.cache.FileCacheSystem;
import com.copula.storyart.design.MediaBackgroundProperty;
import com.storyart.support.util.ViewUtils;
import com.storyart.support.widget.Logger;

import java.io.File;
import java.io.InputStream;

/**
 * Created by heeleaz on 1/29/18.
 */
public class PatternLoadProgress extends LinearLayout {
    private static final Logger logger = Logger.getInstance("PatternDrawingBoard");

    private FileCacheSystem mFileCacheSystem;
    private BackgroundLoader mBackgroundLoader;
    private MediaBackgroundProperty mMediaBackgroundProperty;

    private View anchorView;

    public PatternLoadProgress(Context context) {
        this(context, null, 0);
    }

    public PatternLoadProgress(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PatternLoadProgress(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        setLayoutTransition(new LayoutTransition());
        setOrientation(HORIZONTAL);
        setGravity(Gravity.CENTER);

        int padding = ViewUtils.dpToPx(10);
        setPadding(padding, padding, padding, padding);

        int progressViewDimension = ViewUtils.dpToPx(16);
        ProgressBar progressBar = new ProgressBar(context);
        progressBar.setIndeterminateDrawable(ContextCompat.getDrawable(context, R.drawable.indeterminate_progress_drawable));
        progressBar.setLayoutParams(new ViewGroup.LayoutParams(progressViewDimension, progressViewDimension));
        addView(progressBar);

        LayoutParams textViewParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textViewParams.setMargins(ViewUtils.dpToPx(5), 0, 0, 0);
        TextView textView = new TextView(context);
        textView.setLayoutParams(textViewParams);
        textView.setText(R.string.prg_loading_background);
        textView.setTextColor(Color.WHITE);
        textView.setTextSize(15);
        addView(textView);

        (mFileCacheSystem = new FileCacheSystem(Configuration.Stickers.BACKGROUNDS_CACHE_DIR,
                Configuration.Stickers.BACKGROUNDS_CACHE_SIZE)).openIgnoreError();
    }

    public boolean anchorBackground(View view, MediaBackgroundProperty background) {
        stopBackgroundLoad();//stop any previous background load

        mMediaBackgroundProperty = background;

        if (mFileCacheSystem == null) return false;
        if (!new File(background.placeholderUrl).exists()) return false;

        anchorView = view;
        setVisibility(View.VISIBLE);

        (mBackgroundLoader = new BackgroundLoader(background)).execute();
        return true;
    }

    public void stopBackgroundLoad() {
        if (mBackgroundLoader != null && mBackgroundLoader.getStatus() != AsyncTask.Status.FINISHED)
            mBackgroundLoader.cancel(true);
    }

    @Override
    protected void onDetachedFromWindow() {
        if (mFileCacheSystem != null) mFileCacheSystem.release();
        super.onDetachedFromWindow();
    }

    private void setAnchorBackgroundWithAnimation(@Nullable Drawable background) {
        if (background == null) return;

        Drawable prevBackground = anchorView.getBackground();
        if (prevBackground == null) prevBackground = background;

        TransitionDrawable transitionDrawable =
                new TransitionDrawable(new Drawable[]{prevBackground, background});
        transitionDrawable.startTransition(400);
        anchorView.setBackground(transitionDrawable);
    }

    public String getWallpaperFile() {
        return mFileCacheSystem.getCacheFile(mMediaBackgroundProperty.backgroundUrl);
    }

    private class BackgroundLoader extends AsyncTask<Void, Void, Drawable> {
        private MediaBackgroundProperty mBackgroundProperty;

        BackgroundLoader(MediaBackgroundProperty backgroundProperty) {
            this.mBackgroundProperty = backgroundProperty;
            Drawable drawable = new BitmapDrawable(getResources(),
                    BitmapFactory.decodeFile(mBackgroundProperty.placeholderUrl));
            setAnchorBackgroundWithAnimation(drawable);
        }

        @Override
        protected Drawable doInBackground(Void... voids) {
            try {
                InputStream is = mFileCacheSystem.getInputStream(mBackgroundProperty.backgroundUrl);
                return new BitmapDrawable(getResources(), BitmapFactory.decodeStream(is));
            } catch (Exception e) {
                logger.e("BackgroundLoader: doInBackground", e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(final Drawable drawable) {
            if (drawable != null) setAnchorBackgroundWithAnimation(drawable);

            animate().alpha(0).setDuration(400).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    setVisibility(View.GONE);

                }
            }).start();
        }

        @Override
        protected void onCancelled(Drawable drawable) {
            if (isCancelled()) setVisibility(View.GONE);
        }
    }//END
}
