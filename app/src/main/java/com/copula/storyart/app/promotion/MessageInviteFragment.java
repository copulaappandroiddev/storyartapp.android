package com.copula.storyart.app.promotion;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.copula.storyart.R;

/**
 * Created by heeleaz on 12/28/17.
 */
public class MessageInviteFragment extends DialogFragment implements View.OnClickListener {
    public static void show(FragmentManager fragmentManager) {
        FragmentTransaction ft = fragmentManager.beginTransaction();

        Fragment oldFragment = fragmentManager.findFragmentByTag("MessageInviteFragment");
        if (oldFragment != null) ft.remove(oldFragment);

        MessageInviteFragment fragment = new MessageInviteFragment();
        fragment.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogTheme);
        ft.add(fragment, "MessageInviteFragment").show(fragment).commit();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle si) {
        View view = inflater.inflate(R.layout.frg_message_invite, container, false);
        view.findViewById(R.id.btn_messenger_invite).setOnClickListener(this);
        view.findViewById(R.id.btn_whatsapp_invite).setOnClickListener(this);
        view.findViewById(R.id.btn_message_invite).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_messenger_invite) {
            shareToPackage("com.facebook.orca", "Messenger not Installed");
        } else if (v.getId() == R.id.btn_whatsapp_invite) {
            try {
                shareAndThrowIfNotFound("com.whatsapp");
            } catch (ActivityNotFoundException exception) {
                shareToPackage("com.whatsapp.w4b", "WhatsApp not Installed");
            }
        } else if (v.getId() == R.id.btn_message_invite) {
            shareToPackage(getDefaultSmsPackage(getContext()), "");
        }
    }

    private void shareAndThrowIfNotFound(String toPackage) throws ActivityNotFoundException {
        String message = "Storyart: Add songs, voice record and animated emoji to your WhatsApp status" +
                " https://play.google.com/store/apps/details?id=com.copula.storyart";

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setPackage(toPackage).putExtra(Intent.EXTRA_TEXT, message);
        intent.setType("text/plain");

        getActivity().startActivity(intent);
    }

    private void shareToPackage(String toPackage, String pkgNotFoundMessage) {
        try {
            shareAndThrowIfNotFound(toPackage);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getContext(), pkgNotFoundMessage, Toast.LENGTH_LONG).show();
        }
    }

    private String getDefaultSmsPackage(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return Telephony.Sms.getDefaultSmsPackage(context);
        } else {
            String defApp = Settings.Secure.getString(context.getContentResolver(), "sms_default_application");
            PackageManager pm = context.getApplicationContext().getPackageManager();
            Intent iIntent = pm.getLaunchIntentForPackage(defApp);
            ResolveInfo mInfo = pm.resolveActivity(iIntent, 0);
            return mInfo.activityInfo.packageName;
        }
    }
}
