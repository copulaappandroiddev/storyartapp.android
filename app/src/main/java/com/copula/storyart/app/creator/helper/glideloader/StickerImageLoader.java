package com.copula.storyart.app.creator.helper.glideloader;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.model.ModelLoader;
import com.copula.storyart.app.creator.helper.cache.FileCacheSystem;

import java.io.IOException;
import java.io.InputStream;

public class StickerImageLoader implements ModelLoader<StickerImageLoader.Load, InputStream> {
    @Override
    public DataFetcher<InputStream> getResourceFetcher(Load load, int i, int i1) {
        if (load != null) return new MyDataFetcher(load);
        else return null;
    }

    public static class Load {
        private String uri;
        private FileCacheSystem mFileCacheSystem;

        public Load(String uri, FileCacheSystem mFileCacheSystem) {
            this.uri = uri;
            this.mFileCacheSystem = mFileCacheSystem;
        }
    }

    public class MyDataFetcher implements DataFetcher<InputStream> {
        private Load mLoad;
        private InputStream data;

        MyDataFetcher(Load load) {
            this.mLoad = load;
        }

        @Override
        public InputStream loadData(Priority priority) throws Exception {
            if (!mLoad.mFileCacheSystem.isCached(mLoad.uri)) {
                InputStream data = FileCacheSystem.downloadWithOkHttp(mLoad.uri);
                try {
                    mLoad.mFileCacheSystem.cache(data, mLoad.uri);
                    if (data != null) data.close();
                } catch (Exception exception) {
                    //logger.e("Couldn't cache image: " + uri, exception);
                }
            }

            return data = mLoad.mFileCacheSystem.getDataFromCache(mLoad.uri);
        }

        @Override
        public void cleanup() {
            try {
                if (data != null) data.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int hashCode() {
            return mLoad.uri.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            return mLoad.uri.equals(((MyDataFetcher) obj).mLoad.uri);
        }

        @Override
        public String getId() {
            return mLoad.uri != null ? String.valueOf(mLoad.uri.hashCode()) : null;
        }

        @Override
        public void cancel() {
        }
    }
}
