package com.copula.storyart.app.boot;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.copula.storyart.app.creator.storymedia.GraphicCreatorActivity;
import com.copula.storyart.app.creator.storymedia.TextGraphicCreatorActivity;

public class IntentLaunchAdapter {
    public static Intent urlBasedIntentLauncher(Context context, Intent dataIntent) {
        Uri data = dataIntent.getData();
        if (data == null) return null;

        Intent intent;
        switch (data.getPath()) {
            case "/search":
                intent = new Intent(context, TextGraphicCreatorActivity.class);
                intent.putExtra("commandFlag", GraphicCreatorActivity.STICKER_SEARCH_FLAG);
                return intent.putExtra("q", data.getQueryParameter("q"));

            case "/system_recent_media":
                intent = new Intent(context, TextGraphicCreatorActivity.class);
                return intent.putExtra("commandFlag", GraphicCreatorActivity.RECENT_MEDIA_LIST_FLAG);

            default:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                return intent.setData(data);
        }
    }

    public static Intent mimeTypeBasedIntentLauncher(Context context, Intent dataIntent) {
        String type = dataIntent.getType() != null ? dataIntent.getType() : "";

        if (type.startsWith("audio/")) {
            Intent intent = new Intent(context, TextGraphicCreatorActivity.class);
            return intent.putExtra("audioUri", dataIntent.getParcelableExtra(Intent.EXTRA_STREAM));
        } else if ("text/plain".equals(type)) {
            Intent intent = new Intent(context, TextGraphicCreatorActivity.class);
            return intent.putExtra("text", dataIntent.getStringExtra(Intent.EXTRA_TEXT));
        } else return null;
    }

    public static Intent getLauncherIntent(Context context, Intent dataIntent) {
        Intent intent = urlBasedIntentLauncher(context, dataIntent);
        if (intent == null) mimeTypeBasedIntentLauncher(context, dataIntent);

        return intent;

    }
}
