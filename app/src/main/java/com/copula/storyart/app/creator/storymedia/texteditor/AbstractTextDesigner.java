package com.copula.storyart.app.creator.storymedia.texteditor;

import android.support.v4.app.Fragment;
import com.storyart.support.widget.emoji.StoryartEditText;

public abstract class AbstractTextDesigner extends Fragment {
    public abstract void setEditorTextView(StoryartEditText textView);

    public abstract CharSequence getMarkupText();
}
