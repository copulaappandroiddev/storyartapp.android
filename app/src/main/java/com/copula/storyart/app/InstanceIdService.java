package com.copula.storyart.app;

import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;

public class InstanceIdService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        FirebaseMessaging messaging = FirebaseMessaging.getInstance();

        AppUpdateManager manager = new AppUpdateManager(getApplicationContext());

        String currentUpdateName = manager.compileAppCurrentUpdateNotificationName();
        messaging.subscribeToTopic(currentUpdateName);

        String prevUpdateName = manager.getAppPrevUpdateNotificationName();
        if (!prevUpdateName.equals(currentUpdateName)) {
            messaging.unsubscribeFromTopic(manager.getAppPrevUpdateNotificationName());
            manager.saveAppCurrentUpdateNotificationName(currentUpdateName);
        }

        messaging.subscribeToTopic("STICKERS_UPDATE");
    }//END

}