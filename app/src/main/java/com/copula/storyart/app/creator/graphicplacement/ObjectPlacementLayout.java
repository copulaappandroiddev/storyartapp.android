package com.copula.storyart.app.creator.graphicplacement;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.copula.storyart.design.GraphicImageProperty;
import com.copula.storyart.design.GraphicTextProperty;
import com.storyart.support.widget.emoji.StoryartTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 11/1/17.
 */
public class ObjectPlacementLayout extends FrameLayout {
    private ObjectDisposerAdapter mObjectDisposerAdapter;

    private ImagePlacementAdapter mImagePlacementAdapter;
    private TextPlacementAdapter mTextPlacementAdapter;

    public ObjectPlacementLayout(Context context) {
        this(context, null, 0);
    }

    public ObjectPlacementLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ObjectPlacementLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    public ObjectDisposerAdapter getItemRemover() {
        return mObjectDisposerAdapter;
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        mObjectDisposerAdapter = new ObjectDisposerAdapter(context, this);
        mImagePlacementAdapter = new ImagePlacementAdapter(getContext(), this);
        mTextPlacementAdapter = new TextPlacementAdapter(getContext(), this);
    }

    public ImagePlacementAdapter image() {
        return mImagePlacementAdapter;
    }

    public TextPlacementAdapter text() {
        return mTextPlacementAdapter;
    }

    public List<GraphicImageProperty> getPlacedImageList(int type) {
        List<GraphicImageProperty> list = new ArrayList<>();

        //limit loop in viewCount-1, so as to eliminate the trashBin view
        for (int i = 0; i < getChildCount() - 1; i++) {
            View view = getChildAt(i);
            if (view instanceof ImageView) {
                GraphicImageProperty p = ImagePlacementAdapter.compile((ImageView) view);
                if (p != null) {
                    p.placementPosition = i;
                    if ((type & p.imageType) == p.imageType) list.add(p);
                }
            }
        }

        return list;
    }

    public List<GraphicTextProperty> getPlacedTextList() {
        List<GraphicTextProperty> list = new ArrayList<>();

        //limit loop in viewCount-1, so as to eliminate the trashBin view
        for (int i = 0; i < getChildCount() - 1; i++) {
            View view = getChildAt(i);
            if (view instanceof StoryartTextView) {
                GraphicTextProperty p = TextPlacementAdapter.getProperty((StoryartTextView) view);
                if (p != null) {
                    p.placementPosition = i;
                    list.add(p);
                }
            }
        }
        return list;
    }
}