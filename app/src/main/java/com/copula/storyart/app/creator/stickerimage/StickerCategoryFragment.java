package com.copula.storyart.app.creator.stickerimage;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import com.copula.storyart.R;
import com.copula.storyart.app.creator.Configuration;
import com.copula.storyart.app.creator.helper.AbsImageListAdapter;
import com.copula.storyart.app.creator.helper.AbsImageListFragment;
import com.copula.storyart.app.creator.helper.cache.FileCacheSystem;
import com.copula.storyart.app.creator.helper.restful.AbsImageCategoryApi;
import com.copula.storyart.app.creator.helper.restful.AbsRestfulApi;

import java.util.List;

/**
 * Created by heeleaz on 12/12/17.
 */
public class StickerCategoryFragment extends AbsImageListFragment implements AdapterView.OnItemClickListener,
        AbsRestfulApi.Listener<List<AbsImageCategoryApi.ApiModel>> {

    private FileCacheSystem mFileCacheSystem;

    private OnCategorySelected mListener;
    private RestfulApi mStickerCategoryApi;

    private MyAdapter mAdapter;

    public static StickerCategoryFragment instantiate(OnCategorySelected listener) {
        StickerCategoryFragment fragment = new StickerCategoryFragment();
        fragment.mListener = listener;

        return fragment;
    }

    @Override
    protected View onRequestFragmentView(LayoutInflater inflater, ViewGroup c) {
        (mFileCacheSystem = new FileCacheSystem(Configuration.Stickers.STICKERS_CACHE_DIR,
                Configuration.Stickers.MAX_STICKERS_CACHE_SIZE)).openIgnoreError();
        return super.onRequestFragmentView(inflater, c);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        onLoadMore();
    }

    @Override
    protected BaseAdapter getBaseAdapter(Context context) {
        return mAdapter = new MyAdapter(getContext());
    }

    @Override
    protected int getLoadingTriggerThreshold() {
        return 30;
    }

    @Override
    public void onLoadMore() {
        String nextPage = mStickerCategoryApi != null ? mStickerCategoryApi.getNextPageToken() : "0,30";
        (mStickerCategoryApi = new RestfulApi(getContext(), nextPage)).execute(this);
    }

    @Override
    public boolean hasLoadedAllItems() {
        return mStickerCategoryApi != null && mStickerCategoryApi.getNextPageToken() == null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        RestfulApi.ApiModel model = (RestfulApi.ApiModel) parent.getItemAtPosition(position);
        mListener.onCategorySelected(model.category);
    }

    @Override
    public void onStickerFetchStarted() {
        super.invalidateDataFetchStarted();
    }

    @Override
    public void onDataFetchedSuccess(List<RestfulApi.ApiModel> models, String pageModel) {
        if (!isFragmentInForeground()) return;

        mAdapter.addHandlers(models);
        mAdapter.notifyDataSetChanged();
        super.invalidateDataFetchSuccessful(null);
    }

    @Override
    public void onDataFetchFailed(String message) {
        if (!isFragmentInForeground()) return;

        super.invalidateDataFetchFailed(getString(R.string.msg_sticker_load_failed));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mFileCacheSystem != null) mFileCacheSystem.release();
    }

    public interface OnCategorySelected {
        void onCategorySelected(String category);
    }

    private class RestfulApi extends AbsImageCategoryApi {
        RestfulApi(Context context, String page) {
            super(context, page);
        }

        @Override
        protected String getUrl() {
            return "/stickers/get_stickers_category";
        }
    }

    private class MyAdapter extends AbsImageListAdapter<RestfulApi.ApiModel> {
        MyAdapter(Context context) {
            super(context, mFileCacheSystem, R.layout.adp_sticker_image);
        }

        @Override
        protected String getTitle(int position) {
            return getItem(position).categoryName;
        }

        @Override
        protected String getImageResource(int position) {
            return getItem(position).iconUrl;
        }
    }
}
