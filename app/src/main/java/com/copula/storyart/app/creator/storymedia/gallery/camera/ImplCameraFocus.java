package com.copula.storyart.app.creator.storymedia.gallery.camera;

import android.graphics.Rect;
import android.hardware.Camera;
import android.view.MotionEvent;
import com.storyart.support.widget.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 2/7/18.
 */
public class ImplCameraFocus implements Camera.AutoFocusCallback {
    private static final Logger logger = Logger.getInstance("ImplCameraFocus");

    private int defFocusAreaSize = 200;

    private CameraTextureView mCameraPreview;
    private Listener mListener;

    public ImplCameraFocus(CameraTextureView preview, Listener l) {
        this.mCameraPreview = preview;
        this.mListener = l;
    }

    public static boolean isFlashSupported(Camera camera) {
        if (camera == null) return false;
        Camera.Parameters parameters = camera.getParameters();
        return parameters.getSupportedFlashModes()
                != null && !parameters.getSupportedFlashModes().isEmpty();
    }

    public void setDefFocusAreaSize(int defFocusAreaSize) {
        this.defFocusAreaSize = defFocusAreaSize;
    }

    public Rect focusOnTouch(MotionEvent event) {
        Camera camera = mCameraPreview.getCamera();
        if (camera == null) return null;

        Rect rect = calculateFocusArea(event.getX(), event.getY());

        List<String> focusModes = camera.getParameters().getSupportedFocusModes();
        if (focusModes != null && focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
            Camera.Parameters parameters = camera.getParameters();
            if (parameters.getMaxNumMeteringAreas() > 0) {
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                List<Camera.Area> meteringAreas = new ArrayList<Camera.Area>();
                meteringAreas.add(new Camera.Area(rect, 800));
                parameters.setFocusAreas(meteringAreas);
                camera.setParameters(parameters);
            }
            camera.autoFocus(this);
        }

        return rect;//return focus points anyway.. even if not supported(dummy values)..
    }

    public Rect safeFocusOnTouch(MotionEvent event) {
        try {
            return focusOnTouch(event);
        } catch (RuntimeException e) {
            logger.e(e);
            return null;
        }
    }

    private Rect calculateFocusArea(float x, float y) {
        int left = clamp(Float.valueOf((x / mCameraPreview.getWidth()) * 2000 - 1000).intValue(), defFocusAreaSize);
        int top = clamp(Float.valueOf((y / mCameraPreview.getHeight()) * 2000 - 1000).intValue(), defFocusAreaSize);

        return new Rect(left, top, left + defFocusAreaSize, top + defFocusAreaSize);
    }

    private int clamp(int touchCoordinateInCameraReper, int focusAreaSize) {
        int result;
        if (Math.abs(touchCoordinateInCameraReper) + focusAreaSize / 2 > 1000) {
            if (touchCoordinateInCameraReper > 0) {
                result = 1000 - focusAreaSize / 2;
            } else {
                result = -1000 + focusAreaSize / 2;
            }
        } else {
            result = touchCoordinateInCameraReper - focusAreaSize / 2;
        }
        return result;
    }

    @Override
    public void onAutoFocus(boolean success, Camera camera) {
        if (mListener != null) mListener.onAutoFocus(success, camera);
    }

    public interface Listener {
        void onAutoFocus(boolean success, Camera camera);
    }
}
