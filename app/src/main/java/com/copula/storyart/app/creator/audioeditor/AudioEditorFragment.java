package com.copula.storyart.app.creator.audioeditor;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.copula.storyart.R;
import com.copula.storyart.design.GraphicAudioProperty;
import com.copula.storyart.media.AudioTrimmerCommand;
import com.copula.storyart.media.FileUtil;
import com.copula.storyart.media.ICommand;
import com.copula.storyart.widget.MediaSeekerView;
import com.storyart.support.util.ViewUtils;
import com.storyart.support.widget.IResultCallback;
import com.storyart.support.widget.Logger;

import java.io.File;
import java.util.Locale;

/**
 * Created by heeleaz on 10/22/17.
 */

//TODO: This class should be reviewed
public class AudioEditorFragment extends DialogFragment implements AudioPlayerAdapter.Listener,
        ICommand.ICommandListener<AudioTrimmerCommand>, MediaSeekerView.OnMediaSeekListener, View.OnClickListener {
    private static final Logger logger = Logger.getInstance("AudioEditorFragment");

    public static final String RECORD_OUT_PATH = FileUtil.AUDIO_CROP_PATH;

    private static final int MAX_DURATION_ALLOWED = 30;
    private static final int MAX_IMPORTABLE_MEDIA_DURATION = 648000;//3hrs max
    private static final int MIN_DURATION_ALLOWED = 5;//5s min
    private static final int INVALID_DURATION_COLOR = Color.RED;
    private static final int VALID_DURATION_COLOR = Color.BLACK;

    private TextView txtDurationRange;
    private Button btnPlayPause, btnDone;
    private View prgCroppingProgress;
    private MediaSeekerView mMediaSeekerView;

    private AudioPlayerAdapter mAudioPlayerAdapter;
    private AudioTrimmerCommand mAudioTrimmerCommand;
    private GraphicAudioProperty audioMedia;

    private int requestCode = 0;

    public static void show(FragmentManager fragmentManager, GraphicAudioProperty editorAudioMedia, int requestCode) {
        FragmentTransaction ft = fragmentManager.beginTransaction();

        Fragment oldFragment = fragmentManager.findFragmentByTag("AudioEditorFragment");
        if (oldFragment != null) ft.remove(oldFragment);

        AudioEditorFragment fragment = new AudioEditorFragment();
        Bundle arguments = new Bundle(1);
        arguments.putSerializable("editorAudioMedia", editorAudioMedia);
        fragment.setArguments(arguments);

        fragment.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogTheme);
        fragment.setCancelable(false);
        fragment.requestCode = requestCode;
        ft.add(fragment, "AudioEditorFragment").show(fragment).commit();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle si) {
        ViewUtils.showDialogAsBottomPop(getDialog());
        View v = inflater.inflate(R.layout.frg_audio_editor, container, false);

        v.findViewById(R.id.btn_cancel).setOnClickListener(this);
        (btnDone = v.findViewById(R.id.btn_done)).setOnClickListener(this);
        txtDurationRange = v.findViewById(R.id.txt_duration_range);
        prgCroppingProgress = v.findViewById(R.id.prg_cropping_progress);
        (btnPlayPause = v.findViewById(R.id.btn_play_pause)).setOnClickListener(this);
        (mMediaSeekerView = v.findViewById(R.id.media_seeker_view)).setOnMediaSeekListener(this);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        audioMedia = (GraphicAudioProperty) getArguments().getSerializable("editorAudioMedia");

        //validate to check if the audio is truly importable and can proceed with trim command
        if (!validateImportableAudioMedia(audioMedia)) {
            dismiss();
            return;
        }

        //setup audio trimmer property
        mAudioTrimmerCommand = new AudioTrimmerCommand(getActivity());
        mAudioTrimmerCommand.setInputPath(audioMedia.source.data);
        mAudioTrimmerCommand.setOutputPath(RECORD_OUT_PATH);

        //setup minified audio player property
        mAudioPlayerAdapter = new AudioPlayerAdapter(audioMedia.source.data, this);

        //setup seekBar property
        int mediaDurationInSec = (int) (audioMedia.source.duration / 1000);
        mMediaSeekerView.setDurationInSecs(mediaDurationInSec);

        int maxSeekBarDuration = Math.min(mediaDurationInSec, MAX_DURATION_ALLOWED);
        mMediaSeekerView.setRangeSeekbarValues(0, maxSeekBarDuration);

        if (audioMedia.isEdited) {
            mMediaSeekerView.setStartValues(audioMedia.editorStartFrom,
                    audioMedia.editorEndAt, audioMedia.editorDurationStartPoint);
        }

        mMediaSeekerView.setTag(false);//representing play as false
        onSeekerValueChanged(mMediaSeekerView, 0, maxSeekBarDuration);
    }

    private boolean validateImportableAudioMedia(GraphicAudioProperty editorAudioMedia) {
        boolean result = false;
        if (new File(editorAudioMedia.source.data).exists()) {
            int durationInSec = (int) (editorAudioMedia.source.duration / 1000);

            if (durationInSec < MIN_DURATION_ALLOWED) {
                ViewUtils.sToast(getActivity(), R.string.msg_audio_too_short);
            } else if (durationInSec > MAX_IMPORTABLE_MEDIA_DURATION) {
                ViewUtils.sToast(getActivity(), R.string.msg_audio_too_long);
            } else result = true;
        } else {
            ViewUtils.sToast(getActivity(), R.string.msg_bad_audio_file);
        }

        return result;
    }

    private void performAudioCrop(ICommand.ICommandListener listener) {
        int range = mAudioTrimmerCommand.getReadTimeInSecs();
        if (range < MIN_DURATION_ALLOWED) {
            ViewUtils.sToast(getActivity(), R.string.msg_range_lt_min_d);
        } else if (range > MAX_DURATION_ALLOWED) {
            ViewUtils.sToast(getActivity(), R.string.msg_range_gt_max_d);
        } else mAudioTrimmerCommand.execute(listener);
    }

    @Override
    public void onSeekerValueChanged(MediaSeekerView seekerView, int start, int stop) {
        int diff = stop - start;//clamp diff to max values if exceeded
        stop = (diff > MAX_DURATION_ALLOWED) ? stop - (diff - MAX_DURATION_ALLOWED) : stop;

        mAudioTrimmerCommand.setRange(start, stop);

        int range = stop - start;
        txtDurationRange.setText(createDurationCharacter(range));

        if (range > MAX_DURATION_ALLOWED || range < MIN_DURATION_ALLOWED) {
            txtDurationRange.setTextColor(INVALID_DURATION_COLOR);
        } else txtDurationRange.setTextColor(VALID_DURATION_COLOR);

        try {
            mAudioPlayerAdapter.setPlayRange(start, stop, ((Boolean) seekerView.getTag()));
            mAudioPlayerAdapter.preparePlayer();
            seekerView.setTag(true);//restore to true representing start on prepared
        } catch (Exception exception) {
            logger.e("Error Starting up Player", exception);
        }
    }

    @Override
    public void onPlayStateChanged(int state) {
        if (state == AudioPlayerAdapter.PLAY_STARTED) {
            btnPlayPause.setBackgroundResource(R.drawable.ic_pause_black_128px);
        } else {
            btnPlayPause.setBackgroundResource(R.drawable.ic_play_black_128px);
        }
    }

    @Override
    public void onPlayProgress(int duration, int position) {
        mMediaSeekerView.setCurrentMediaPosition(duration, position);
    }

    @Override
    public void onProgress(AudioTrimmerCommand cropCommand, String s) {
        prgCroppingProgress.setVisibility(View.VISIBLE);
        logger.i("cropping progress:" + s);

        btnDone.setEnabled(false);
    }

    @Override
    public void onSuccessful(AudioTrimmerCommand cropCommand, String message) {
        prgCroppingProgress.setVisibility(View.VISIBLE);

        audioMedia.isEdited = true;
        audioMedia.editorStartFrom = mMediaSeekerView.getSelectedMinRangeValue();
        audioMedia.editorEndAt = mMediaSeekerView.getSelectedMaxRangeValue();
        audioMedia.editorAudioPath = RECORD_OUT_PATH;
        audioMedia.editorDurationStartPoint = mMediaSeekerView.getDurationStartPoint();
        audioMedia.editorDuration = mAudioTrimmerCommand.getReadTimeInSecs();


        Intent data = new Intent().putExtra("editorAudioMedia", audioMedia);
        finishWithResult(IResultCallback.RESULT_OK, data);
    }

    @Override
    public void onFailure(AudioTrimmerCommand cropCommand, String s) {
        btnDone.setEnabled(true);
        prgCroppingProgress.setVisibility(View.GONE);
        ViewUtils.sToast(getActivity(), R.string.msg_audio_crop_failed);
        logger.i("audio cropping failed: " + s);
    }

    private void finishWithResult(int resultCode, Intent data) {
        if (getActivity() instanceof IResultCallback) {
            ((IResultCallback) getActivity()).onFragmentResult(requestCode, resultCode, data);
        }
        try {
            dismiss();
        } catch (IllegalStateException ignore) {
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_cancel) {
            finishWithResult(IResultCallback.RESULT_CANCEL, null);
        } else if (view.getId() == R.id.btn_done) {
            performAudioCrop(this);
        } else if (view.getId() == R.id.btn_play_pause) {
            mAudioPlayerAdapter.togglePlayPause();//pause or play current playing audio
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mAudioPlayerAdapter != null) mAudioPlayerAdapter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAudioPlayerAdapter != null) mAudioPlayerAdapter.release();
    }

    private String createDurationCharacter(int duration) {
        Locale locale = Locale.getDefault();

        if (duration == 0) return "00s";
        int m = duration / 60;
        int s = duration % 60;

        if (m == 0) return String.format(locale, "%02ds", s);
        else return String.format(locale, "%02d", m) + ":" + String.format(locale, "%02d", s);
    }
}