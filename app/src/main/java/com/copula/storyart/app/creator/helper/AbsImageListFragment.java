package com.copula.storyart.app.creator.helper;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;
import com.copula.storyart.R;
import com.storyart.support.pagination.Paginate;
import com.storyart.support.util.ViewUtils;

/**
 * Created by heeleaz on 1/23/18.
 */
public abstract class AbsImageListFragment extends Fragment implements View.OnClickListener, Paginate.Callbacks,
        AdapterView.OnItemClickListener {
    private View viewProgressBar, pageFailureView;
    private GridView absListView;
    private BaseAdapter mAdapter;

    protected View onRequestFragmentView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.abs_image_list_fragment, container, false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle si) {
        View v = onRequestFragmentView(inflater, container);
        viewProgressBar = v.findViewById(R.id.view_progress_bar);
        (pageFailureView = v.findViewById(R.id.view_load_failed)).setOnClickListener(this);
        (absListView = v.findViewById(R.id.gridview)).setOnItemClickListener(this);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        absListView.setAdapter(mAdapter = getBaseAdapter(getContext()));

        int t = getLoadingTriggerThreshold();
        Paginate.with(absListView, this).setLoadingTriggerThreshold(t).build();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.view_load_failed) onLoadMore();
    }

    protected abstract BaseAdapter getBaseAdapter(Context context);

    protected abstract int getLoadingTriggerThreshold();

    protected void invalidateDataFetchStarted() {
        if (!isFragmentInForeground()) return;

        if (mAdapter.isEmpty()) ViewUtils.showOnly(viewProgressBar);
        else viewProgressBar.setVisibility(View.VISIBLE);
    }

    protected void invalidateDataFetchSuccessful(String message) {
        if (!isFragmentInForeground()) return;

        ViewUtils.showOnly(absListView);
    }

    protected void invalidateDataFetchFailed(String message) {
        if (!isFragmentInForeground()) return;

        invalidateDataFetchFailed(message, getString(R.string.txt_tap_to_reload));
    }

    protected void invalidateDataFetchFailed(String message, String subMessage) {
        if (!isFragmentInForeground()) return;

        if (mAdapter.isEmpty()) {
            ((TextView) pageFailureView.findViewById(R.id.txt_not_found_message)).setText(message);
            ((TextView) pageFailureView.findViewById(R.id.txt_not_found_sub_message)).setText(subMessage);
            ViewUtils.showOnly(pageFailureView);
        }
    }

    protected boolean isFragmentInForeground() {
        return getActivity() != null && isAdded();
    }
}
