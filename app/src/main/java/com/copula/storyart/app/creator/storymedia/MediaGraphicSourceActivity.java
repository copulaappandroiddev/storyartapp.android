package com.copula.storyart.app.creator.storymedia;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import com.copula.storyart.R;
import com.copula.storyart.app.RationalePermsDialog;
import com.copula.storyart.app.creator.storymedia.gallery.MediaGalleryFragment;
import com.copula.storyart.app.creator.storymedia.gallery.camera.CameraFragment;
import com.storyart.support.widget.PermissionHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 2/9/18.
 */
public class MediaGraphicSourceActivity extends FragmentActivity implements MediaSourceCallback {
    private static final int PERMISSION_REQUEST_CODE = 100;
    private CameraFragment mCameraFragment;
    private ViewPager viewPager;

    public static void launch(Activity activity) {
        activity.startActivity(new Intent(activity, MediaGraphicSourceActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frg_graphic_media_source);

        List<Fragment> fragmentList = new ArrayList<>(2);
        fragmentList.add(mCameraFragment = CameraFragment.instantiate());
        fragmentList.add(MediaGalleryFragment.instantiate());

        (viewPager = findViewById(R.id.view_pager)).setAdapter(new MyPagerAdapter(fragmentList));
    }

    private void verifyV23ApplicationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PermissionHelper permissionHelper = new PermissionHelper(this);

            String permission = Manifest.permission.ACCESS_FINE_LOCATION;
            int responseCode = permissionHelper.requestPermission(permission, PERMISSION_REQUEST_CODE);
            if (responseCode == PermissionHelper.RATIONALE_PERM) {
                RationalePermsDialog.launch(getSupportFragmentManager(), permission);
            } else if (responseCode == PermissionHelper.PERMITTED) onPermissionGranted();
        } else onPermissionGranted();
    }//end

    private void onPermissionGranted() {
        //startService(new Intent(this, LocationUpdateService.class));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((viewPager.getCurrentItem() == 0) &&
                (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN || keyCode == KeyEvent.KEYCODE_VOLUME_UP)) {
            mCameraFragment.takeShot();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void requestSource(int request) {
        if (request == MediaSourceCallback.SOURCE_CAMERA) {
            viewPager.setCurrentItem(0, true);
        } else if (request == MediaSourceCallback.SOURCE_GALLERY) {
            viewPager.setCurrentItem(1, true);
        }
    }

    @Override
    public void onMediaAvailable(String mediaUrl) {
        MediaGraphicCreatorActivity.launch(this, mediaUrl);
    }

    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() != 0) {
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
        } else super.onBackPressed();
    }

    private class MyPagerAdapter extends FragmentStatePagerAdapter {
        private List<Fragment> fragmentList;

        MyPagerAdapter(List<Fragment> fragmentList) {
            super(getSupportFragmentManager());
            this.fragmentList = fragmentList;
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }
}
