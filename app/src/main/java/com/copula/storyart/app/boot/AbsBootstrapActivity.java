package com.copula.storyart.app.boot;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import com.copula.storyart.app.AppUpdateManager;
import com.copula.storyart.app.RationalePermsDialog;
import com.copula.storyart.media.ICommand;
import com.copula.storyart.media.MediaEngineInitializer;
import com.google.firebase.messaging.FirebaseMessaging;
import com.storyart.support.widget.IResultCallback;
import com.storyart.support.widget.Logger;
import com.storyart.support.widget.PermissionHelper;

/**
 * Created by heeleaz on 11/11/17.
 */
public abstract class AbsBootstrapActivity extends FragmentActivity implements IResultCallback,
        ICommand.ICommandListener<MediaEngineInitializer> {
    private static final Logger logger = Logger.getInstance("AbsBootstrapActivity");

    public static int CAUSE_FFMpeg_PROCESS = 0x10;
    private static int PERMISSION_REQUEST_CODE = 100;

    protected abstract void onInitProgress(int progress, String message);

    protected abstract void onInitCompleted(String message);

    protected abstract void onInitFailure(int cause, String message);

    protected void requestVerifyV23ApplicationPermission() {
        FirebaseMessaging messaging = FirebaseMessaging.getInstance();

        AppUpdateManager manager = new AppUpdateManager(getApplicationContext());

        String currentUpdateName = manager.compileAppCurrentUpdateNotificationName();
        messaging.subscribeToTopic(currentUpdateName);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PermissionHelper h = new PermissionHelper(this);

            String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
            int permissionResponseCode = h.requestPermission(permission, PERMISSION_REQUEST_CODE);
            if (permissionResponseCode == PermissionHelper.RATIONALE_PERM) {
                RationalePermsDialog.launch(getSupportFragmentManager(), permission);
            } else if (permissionResponseCode == PermissionHelper.PERMITTED) onPermissionGranted();
        } else onPermissionGranted();
    }//end

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] p, @NonNull int[] r) {
        if (requestCode == PERMISSION_REQUEST_CODE) try {
            requestVerifyV23ApplicationPermission();
        } catch (Exception e) {
            logger.e(e);
        }
    }//END

    @Override
    public void onFragmentResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RationalePermsDialog.REQUEST_CODE) {
            if (resultCode == IResultCallback.RESULT_CANCEL) finish();
            else onPermissionGranted();
        }
    }

    private void onPermissionGranted() {
        MediaEngineInitializer initializer = new MediaEngineInitializer(this);
        initializer.initialize(this);
    }

    @Override
    public void onSuccessful(MediaEngineInitializer mediaInitializer, String message) {
        onInitCompleted(message);
    }

    @Override
    public void onFailure(MediaEngineInitializer initializer, String message) {
        onInitFailure(CAUSE_FFMpeg_PROCESS, message);
    }

    @Override
    public void onProgress(MediaEngineInitializer initializer, String message) {
        onInitProgress(0, null);
    }
}
