package com.copula.storyart.app.creator.helper.cache;

import com.copula.storyart.media.FileUtil;
import com.jakewharton.disklrucache.DiskLruCache;
import com.storyart.support.widget.Logger;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by heeleaz on 1/29/18.
 */
public class FileCacheSystem {
    private static final Logger logger = Logger.getInstance("FileCacheSystem");

    private String cacheDirectory;
    private int maxSize;

    private DiskLruCache mDiskLruCache;

    public FileCacheSystem(String directory, int maxSize) {
        this.cacheDirectory = FileUtil.hiddenDirectory(directory);
        this.maxSize = maxSize;
    }

    private static String createLruCacheKeyFromUrl(String url) {
        String extension = "";
        if (url.contains("."))
            extension = "-" + url.substring(url.lastIndexOf(".") + 1);
        
        return String.valueOf(url.hashCode()) + extension;
    }

    public static InputStream downloadWithOkHttp(String url) throws IOException {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(20, TimeUnit.SECONDS);
        client.readTimeout(60, TimeUnit.SECONDS);

        Request request = new Request.Builder().url(url).build();
        Response response = client.build().newCall(request).execute();

        if (response.isSuccessful()) return response.body().byteStream();
        else return null;
    }

    public void open() throws IOException {
        File cacheDir = new File(cacheDirectory);
        mDiskLruCache = DiskLruCache.open(cacheDir, 1, 1, maxSize);
    }

    public boolean openIgnoreError() {
        try {
            open();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public boolean isCached(String url) {
        try {
            return mDiskLruCache.get(createLruCacheKeyFromUrl(url)) != null;
        } catch (IOException e) {
            return false;
        }
    }

    public InputStream getDataFromCache(String url) throws IOException {
        String key = createLruCacheKeyFromUrl(url);
        return mDiskLruCache.get(key).getInputStream(0);
    }

    public String getCacheFile(String url) {
        try {
            String key = createLruCacheKeyFromUrl(url);
            return mDiskLruCache.get(key).getCleanFile(0).getAbsolutePath();
        } catch (Exception e) {
            return null;
        }
    }

    public boolean cache(InputStream is, String url) throws IOException {
        if (is == null) return false;

        String key = createLruCacheKeyFromUrl(url);

        DiskLruCache.Editor editor = mDiskLruCache.edit(key);
        OutputStream bos = editor.newOutputStream(0);

        int size;
        byte[] buffer = new byte[1024];
        BufferedInputStream bis = new BufferedInputStream(is);
        try {
            while ((size = bis.read(buffer)) != -1)
                bos.write(buffer, 0, size);
        } catch (IOException e) {
            mDiskLruCache.remove(key);
            logger.e("Error writing data stream to file", e);
        }

        try {
            editor.commit();
            bos.close();
        } catch (IOException ignore) {
            logger.e("Ignore closing buffer e", ignore);
        }

        return true;
    }

    public InputStream getInputStream(String url) throws Exception {
        if (!isCached(url)) {
            InputStream data = downloadWithOkHttp(url);
            try {
                cache(data, url);
            } catch (Exception e) {
                logger.e("Couldn't cache data", e);
            } finally {
                try {
                    if (data != null) data.close();
                } catch (IOException ignored) {
                }
            }
        }
        return mDiskLruCache.get(createLruCacheKeyFromUrl(url)).getInputStream(0);
    }

    public void release() {
        try {
            mDiskLruCache.close();
        } catch (Exception ignored) {
        }
    }
}
