package com.copula.storyart.app.creator.helper.restful;

import android.content.Context;
import com.copula.smartjson.JsonInner;
import com.copula.smartjson.OJsonMapper;
import com.copula.storyart.app.StoryartApiEndpoint;
import com.storyart.support.webservice.GetParam;
import com.storyart.support.webservice.NettyApiProvider;
import com.storyart.support.webservice.RestResponse;

import java.util.List;

public abstract class AbsImageCategoryApi extends AbsRestfulApi<List<AbsImageCategoryApi.ApiModel>> {
    private NettyApiProvider apiProvider = new NettyApiProvider(StoryartApiEndpoint.ENDPOINT);
    private Context mContext;

    public AbsImageCategoryApi(Context context, String page) {
        super(page);
        this.mContext = context;
    }

    protected abstract String getUrl();

    @Override
    protected RestResponse<List<ApiModel>> get(String page) throws Exception {
        apiProvider.cache(mContext.getCacheDir(), 1024 * 1024);

        GetParam params = new GetParam().put("page", page);
        NettyApiProvider.Result result = apiProvider.get(getUrl(), params);
        if (result == null || result.responseCode != 200) return null;

        RestResponse<List<ApiModel>> rest = OJsonMapper.mapOne(RestResponse.class, result.response);
        if (rest != null) {
            rest.result = OJsonMapper.mapList(ApiModel.class, result.response);
        }
        return rest;
    }

    @JsonInner("result")
    public static class ApiModel {
        public String iconUrl, categoryName, category;
    }
}