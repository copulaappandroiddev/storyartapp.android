package com.copula.storyart.app.creator.graphicplacement;

import android.content.Context;
import android.view.Gravity;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.copula.storyart.app.creator.storymedia.texteditor.EditorMarkupManager;
import com.copula.storyart.design.GraphicTextProperty;
import com.storyart.support.util.ViewUtils;
import com.storyart.support.widget.emoji.StoryartTextView;

import java.util.regex.Pattern;

public class TextPlacementAdapter {
    private Context mContext;
    private ObjectPlacementLayout placementLayout;
    private TextPlacementAdapter.Callback mCallback;

    @SuppressWarnings("SuspiciousNameCombination")
    TextPlacementAdapter(Context context, ObjectPlacementLayout l) {
        this.mContext = context;
        this.placementLayout = l;
    }

    public static GraphicTextProperty getProperty(StoryartTextView textView) {
        GraphicTextProperty textProperty = (GraphicTextProperty) textView.getTag();
        if (textProperty == null) return null;

        int[] coordinate = new int[2];
        textView.getLocationOnScreen(coordinate);
        textProperty.xPosition = coordinate[0];
        textProperty.yPosition = coordinate[1];
        textProperty.rotationAngle = textView.getRotation();

        textProperty.width = textView.getTextWidth();
        textProperty.height = textView.getTextHeight();
        textProperty.textSize = (float) Math.floor(textView.getTextSize());
        textProperty.textColor = textView.getCurrentTextColor();

        return textProperty;
    }

    public StoryartTextView createEditable(final GraphicTextProperty p, Builder builder) {
        StoryartTextView textView = openEditable(p);

        if (builder.removeIfTextEmpty) {
            p.text = p.text == null ? "" : p.text;//make sure p.text is not null

            Pattern pattern = Pattern.compile("[\\r\\n]{" + p.text.length() + "}");
            if (pattern.matcher(p.text).find()) {
                placementLayout.removeView(textView);
                return null;
            }
        }

        if (textView == null) {
            textView = new StoryartTextView(placementLayout.getContext());
            textView.setMaxTextSize(builder.maxTextSize);
            textView.setMinTextSize(builder.minTextSize);

            FrameLayout.LayoutParams params = new
                    FrameLayout.LayoutParams(builder.editorViewWidth, builder.editorViewHeight);
            params.gravity = Gravity.CENTER;

            int position = placementLayout.getItemRemover().getPositionInParent();
            placementLayout.addView(textView, position, params);

            if (builder.touchMotion) {
                setItemRemoverAdapter(textView).setEnabled(builder.enableRemover);
                ViewUtils.increaseTouchArea(textView, ViewUtils.dpToPx(48));
            } else {
                final StoryartTextView finalTextView = textView;
                finalTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        GraphicTextProperty p = getProperty(finalTextView);
                        if (p != null && openEditable(p) != null) {
                            if (mCallback != null) mCallback.onTextEditorSelected(finalTextView, p);
                        }
                    }
                });
            }
        }

        updateEditor(textView, p);

        return textView;
    }

    private ObjectTouchDelegate setItemRemoverAdapter(final StoryartTextView textView) {
        ObjectTouchDelegate listener = new ObjectTouchDelegate(mContext,
                placementLayout.getItemRemover(), new MyScaleListener(textView));
        textView.setOnTouchListener(listener);
        listener.setClickGestureListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GraphicTextProperty p = getProperty(textView);
                if (p != null && openEditable(p) != null) {
                    if (mCallback != null) mCallback.onTextEditorSelected(textView, p);
                }
            }
        });

        return listener;
    }

    private StoryartTextView openEditable(GraphicTextProperty p) {
        for (int i = 0; i < placementLayout.getChildCount() - 1; i++) {
            View v = placementLayout.getChildAt(i);
            if (!(v instanceof StoryartTextView)) continue;

            GraphicTextProperty sp = getProperty((StoryartTextView) v);
            if (sp != null && sp.id == p.id) return (StoryartTextView) v;
        }
        return null;
    }

    private void updateEditor(StoryartTextView textView, GraphicTextProperty p) {
        textView.setTag(p);

        EditorMarkupManager.Applyer applyer = EditorMarkupManager.getApplyer(mContext, textView);
        applyer.apply(p.text);
    }

    public void setCallback(Callback l) {
        this.mCallback = l;
    }

    public interface Callback {
        void onTextEditorSelected(StoryartTextView textView, GraphicTextProperty textProperty);
    }

    public static class Builder {
        private int editorViewWidth, editorViewHeight;
        private int maxTextSize, minTextSize;
        private boolean enableRemover = true, touchMotion = true, removeIfTextEmpty = true;

        public Builder setEditorViewHeight(int editorViewHeight) {
            this.editorViewHeight = editorViewHeight;
            return this;
        }

        public Builder setEditorViewWidth(int editorViewWidth) {
            this.editorViewWidth = editorViewWidth;
            return this;
        }

        public Builder setMaxTextSize(int maxTextSize) {
            this.maxTextSize = maxTextSize;
            return this;
        }

        public Builder setMinTextSize(int minTextSize) {
            this.minTextSize = minTextSize;
            return this;
        }

        public Builder setEnableTouchMotion(boolean enable) {
            this.touchMotion = enable;
            return this;
        }

        public Builder removeIfEmpty(boolean removeIfTextEmpty) {
            this.removeIfTextEmpty = removeIfTextEmpty;
            return this;
        }
    }

    private class MyScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        private TextView mTextView;

        MyScaleListener(TextView textView) {
            this.mTextView = textView;
        }

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            float span = (detector.getCurrentSpan() - detector.getPreviousSpan());
            mTextView.setWidth((int) (mTextView.getWidth() + span));
            mTextView.setHeight((int) (mTextView.getHeight() + span));
            return true;
        }
    }//END
}
