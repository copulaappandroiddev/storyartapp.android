package com.copula.storyart.app;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import com.copula.storyart.R;
import com.copula.storyart.app.boot.LandingPageActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MessagingService extends FirebaseMessagingService {
    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        RemoteMessage.Notification n = remoteMessage.getNotification();
        updateWithLinkNotification(n);
    }

    private void updateWithLinkNotification(RemoteMessage.Notification n) {
        Intent viewIntent = new Intent(this, LandingPageActivity.class);
        viewIntent.setData(n.getLink());


        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(viewIntent);
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setAutoCancel(true).setGroup("action_notification");
        builder.setDefaults(Notification.DEFAULT_ALL);
        builder.setContentTitle(n.getTitle()).setContentText(n.getBody());
        builder.setSmallIcon(R.mipmap.ic_notification).setColor(0xef3131);
        builder.setContentIntent(pendingIntent);
        show(builder.build());
    }

    private void show(Notification notification) {
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(100, notification);
    }
}