package com.copula.storyart.app.creator.emojiimage.inappemojis;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.caverock.androidsvg.SVG;
import com.storyart.support.util.SVGBitmapHelper;
import com.storyart.support.widget.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by heeleaz on 11/1/17.
 */
public class EmojiPacketLibrary implements Iterator<String> {
    private static final String[] categories = new String[]{
            "emotions", "foods", "landmarks", "transportation", "badges", "news"};

    private static Logger logger = Logger.getInstance("EmojiPacketLibrary");
    private List<String> emojiList = new ArrayList<>();
    private int index = 0;

    private EmojiPacketLibrary(Context context) {
        try {
            for (String category : categories) {
                String parent = "emojis/" + category;
                String[] paths = context.getAssets().list(parent);
                for (String s : paths) emojiList.add(parent + "/" + s);
            }
        } catch (Exception e) {
            logger.e("Emojis couldn't be fetched from assets", e);
        }
    }

    public static EmojiPacketLibrary getInstance(Context context) {
        return new EmojiPacketLibrary(context);
    }

    public static Bitmap getEmojiImage(Context context, String icoPath) {
        if (icoPath.endsWith(".svg")) {
            return getSvgBitmapFromAsset(context, icoPath);
        } else return getPngBitmapFromAsset(context, icoPath);
    }//END

    private static Bitmap getSvgBitmapFromAsset(Context context, String path) {
        try {
            SVG svg = SVG.getFromAsset(context.getAssets(), path);
            return SVGBitmapHelper.pictureToBitmap(svg.renderToPicture());
        } catch (Exception e) {
            return null;
        }
    }

    private static Bitmap getPngBitmapFromAsset(Context context, String path) {
        try {
            InputStream is = context.getAssets().open(path);
            return BitmapFactory.decodeStream(is);
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public boolean hasNext() {
        return true;
    }

    @Override
    public String next() {
        return emojiList.get(index++ % emojiList.size());
    }

    public List<String> getEmojiList() {
        return emojiList;
    }
}
