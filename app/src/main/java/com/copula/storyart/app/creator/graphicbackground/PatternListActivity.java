package com.copula.storyart.app.creator.graphicbackground;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import com.copula.storyart.R;
import com.copula.storyart.app.creator.graphicbackground.colorbackground.ColorBackgroundFragment;
import com.copula.storyart.app.creator.graphicbackground.patterns.PatternListFragment;
import com.storyart.support.slidinguppanel.SlidingUpPanelLayout;

/**
 * Created by heeleaz on 10/22/17.
 */
public class PatternListActivity extends FragmentActivity {
    private SlidingUpPanelLayout slidingPaneLayout;

    public static void launch(Activity ctx, int requestCode) {
        ctx.startActivityForResult(new Intent(ctx, PatternListActivity.class), requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.transition.fade_in, R.transition.fade_out);

        setContentView(R.layout.frg_graphic_background);
        slidingPaneLayout = findViewById(R.id.sliding_pane_layout);

        fragment(ColorBackgroundFragment.instantiate(), R.id.fragment_color_background);
        fragment(PatternListFragment.instantiate("common"), R.id.fragment_background_main);
    }

    @Override
    public void onBackPressed() {
        if (slidingPaneLayout.getPanelState() != SlidingUpPanelLayout.PanelState.COLLAPSED) {
            slidingPaneLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        } else {
            super.onBackPressed();
        }
    }

    private void fragment(Fragment fragment, int viewId) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(viewId, fragment).commit();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.transition.fade_in, R.transition.fade_out);
    }
}
