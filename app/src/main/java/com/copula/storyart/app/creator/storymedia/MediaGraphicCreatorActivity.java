package com.copula.storyart.app.creator.storymedia;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.copula.storyart.R;
import com.copula.storyart.app.creator.graphicplacement.DrawingBoard;
import com.copula.storyart.app.creator.graphicplacement.TextPlacementAdapter;
import com.copula.storyart.app.creator.storymedia.gallery.camera.PictureProcessor;
import com.copula.storyart.app.creator.storymedia.mediafilter.MediaFilterFragment;
import com.copula.storyart.app.creator.storymedia.texteditor.TextEditorActivity;
import com.copula.storyart.design.GraphicCreatorModel;
import com.copula.storyart.design.GraphicTextProperty;
import com.copula.storyart.design.MediaBackgroundProperty;
import com.copula.storyart.media.FileUtil;
import com.storyart.support.util.ViewUtils;
import com.storyart.support.widget.emoji.StoryartTextView;

/**
 * Created by heeleaz on 1/31/18.
 */
public class MediaGraphicCreatorActivity extends GraphicCreatorActivity {
    private static final int MAX_GRAPHIC_TEXT_SIZE = TextGraphicCreatorActivity.TEXT_EDITOR_HEIGHT;
    private static final int GRAPHIC_TEXT_WIDTH = TextGraphicCreatorActivity.TEXT_EDITOR_HEIGHT * 10;
    private static final int GRAPHIC_TEXT_HEIGHT = ViewGroup.LayoutParams.WRAP_CONTENT;

    private TextPlacementAdapter.Builder mBuilder;
    private MediaFilterFragment mMediaFilterFragment;
    private MediaBackgroundProperty mMediaBackgroundProperty = new MediaBackgroundProperty();

    public static void launch(Activity ctx, String imageUrl) {
        Intent intent = new Intent(ctx, MediaGraphicCreatorActivity.class);
        ctx.startActivity(intent.putExtra("imageUrl", imageUrl));
    }

    @SuppressWarnings("SuspiciousNameCombination")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        ViewUtils.fullscreenActivity(this);
        super.onCreate(savedInstanceState);

        getObjectPlacementLayout().text().setCallback(this);
        mBuilder = new TextPlacementAdapter.Builder();
        mBuilder.setEditorViewWidth(GRAPHIC_TEXT_WIDTH)
                .setEditorViewHeight(GRAPHIC_TEXT_HEIGHT).setMaxTextSize(MAX_GRAPHIC_TEXT_SIZE)
                .setMinTextSize(ViewUtils.dpToPx(5));

        String imageUrl = getIntent().getStringExtra("imageUrl");
        if (imageUrl.equals(PictureProcessor.CameraImageCache.CAMERA_BITMAP_KEY)) {
            mMediaBackgroundProperty.source = MediaBackgroundProperty.CAMERA_IMAGE;
        } else {
            mMediaBackgroundProperty.backgroundUrl = imageUrl;
            mMediaBackgroundProperty.source = MediaBackgroundProperty.GALLERY_IMAGE;
        }

        mMediaFilterFragment = MediaFilterFragment.instantiate(imageUrl);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.background_view, mMediaFilterFragment).commit();
    }

    @Override
    protected View onRequestActivityView(LayoutInflater inflater, ViewGroup container) {
        View v = inflater.inflate(R.layout.act_media_graphic_creator, container, false);
        v.findViewById(R.id.btn_close).setOnClickListener(this);
        v.findViewById(R.id.btn_create_text).setOnClickListener(this);
        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TextEditorActivity.REQUEST_EDITOR) {
            onRequestHideOptionButtons(false);

            GraphicTextProperty text = (GraphicTextProperty) data.getSerializableExtra("text");
            TextView textView = getObjectPlacementLayout().text().createEditable(text, mBuilder);
            if (textView != null) textView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected boolean onPrepareExportProcess(final GraphicCreatorModel model) {
        super.onPrepareExportProcess(model);
        model.setBackgroundProperty(mMediaBackgroundProperty);
        return true;
    }

    @Override
    protected MediaBackgroundProperty overrideBackgroundProperty() {
        DrawingBoard view = findViewById(R.id.graphic_drawingboard);
        view.writeDrawingBoard(Bitmap.Config.ARGB_8888, FileUtil.GRAPHIC_WRITE_PATH);
        mMediaBackgroundProperty.backgroundUrl = FileUtil.GRAPHIC_WRITE_PATH;

        return mMediaBackgroundProperty;
    }

    private boolean hasMadeChanges() {
        GraphicCreatorModel cm = currentGraphicContext;
        return mMediaFilterFragment.hasEditedImage() ||
                cm.isAudioAttached() || cm.isImagesAttached() || cm.isTextAttached();
    }

    @Override
    public void onTextEditorSelected(@Nullable StoryartTextView textView, GraphicTextProperty textProperty) {
        if (textView != null) textView.setVisibility(View.GONE);
        onRequestHideOptionButtons(true);

        TextEditorActivity.launch(this, textProperty,
                TextEditorActivity.FLAG_RETURN_LINE_BREAK | TextEditorActivity.FLAG_DESIGN_SELECT);
    }


    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view.getId() == R.id.btn_create_text) {
            onTextEditorSelected(null, GraphicTextProperty.loadDefault());
        } else if (view.getId() == R.id.btn_close) onBackPressed();
    }

    @Override
    public void onBackPressed() {
        if (!hasMadeChanges()) {
            super.onBackPressed();
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.tit_cancel_editing);
        builder.setMessage(R.string.msg_cancel_editing);

        DialogInterface.OnClickListener clickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) finish();
                dialog.dismiss();
            }
        };

        builder.setPositiveButton(R.string.act_dialog_discard, clickListener);
        builder.setNegativeButton(R.string.act_dialog_no, clickListener);
        builder.create().show();
    }
}
