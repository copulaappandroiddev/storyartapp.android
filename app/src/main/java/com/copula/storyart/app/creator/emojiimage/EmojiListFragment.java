package com.copula.storyart.app.creator.emojiimage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.copula.storyart.R;
import com.copula.storyart.app.creator.addonimages.recentaddonimages.data.RecentAddonDao;
import com.copula.storyart.app.creator.addonimages.recentaddonimages.data.RecentAddonEntity;
import com.copula.storyart.app.creator.emojiimage.inappemojis.EmojiPacketLibrary;
import com.copula.storyart.app.creator.emojiimage.inappemojis.glideloader.AssetsEmojiLoader;
import com.copula.storyart.design.GraphicImageProperty;
import com.storyart.support.widget.BaseAdapter;

import java.io.InputStream;

/**
 * Created by heeleaz on 10/30/17.
 */
public class EmojiListFragment extends Fragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {
    private GridView gridView;

    public static EmojiListFragment instantiate() {
        return new EmojiListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup c, Bundle si) {
        View v = inflater.inflate(R.layout.frg_emoji_list, c, false);
        (gridView = v.findViewById(R.id.gridview)).setOnItemClickListener(this);
        gridView.setOnItemLongClickListener(this);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        EmojiListAdapter adapter = new EmojiListAdapter(getActivity());

        EmojiPacketLibrary emojis = EmojiPacketLibrary.getInstance(getContext());
        adapter.addHandler("");
        adapter.addHandlers(emojis.getEmojiList());
        gridView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String item = (String) parent.getItemAtPosition(position);

        RecentAddonEntity entity = new RecentAddonEntity(null, item, GraphicImageProperty.ASSET_BITMAP_IMAGE);
        RecentAddonDao.getInstance(getContext()).insertAsync(entity);

        Intent intent = new Intent().putExtra("data", item);
        intent.putExtra("source", GraphicImageProperty.ASSET_BITMAP_IMAGE);

        Activity activity = getActivity();
        activity.setResult(Activity.RESULT_OK, intent);
        activity.finish();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        return false;
    }

    private class EmojiListAdapter extends BaseAdapter<String> {
        private static final int SELECT_EMOJI_VIEW = 0;
        private static final int ADD_EMOJI_VIEW = 1;

        EmojiListAdapter(Context context) {
            super(context);
        }

        @Override
        public View getView(final int position, View convertView, LayoutInflater inflater) {
            final ViewHolder holder;
            int viewType = 0;
            if (convertView == null) {
                holder = new ViewHolder();
                if ((viewType = getItemViewType(position)) == SELECT_EMOJI_VIEW) {
                    convertView = inflater.inflate(R.layout.adp_emoji_list, null, false);
                    holder.image = convertView.findViewById(R.id.img_image);
                } else {
                    convertView = inflater.inflate(R.layout.adp_add_custom_sticker, null, false);
                }
                convertView.setTag(holder);
            } else holder = (ViewHolder) convertView.getTag();


            if (holder.image != null && viewType == SELECT_EMOJI_VIEW) {
                String url = getItem(position);
                Glide.with(getContext()).using(new AssetsEmojiLoader(getContext()), InputStream.class)
                        .load(new AssetsEmojiLoader.Load(url)).as(Bitmap.class).into(holder.image);
            }
            return convertView;
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public int getItemViewType(int position) {
            return (position == 0) ? ADD_EMOJI_VIEW : SELECT_EMOJI_VIEW;
        }

        private class ViewHolder {
            ImageView image;
        }
    }//END
}
