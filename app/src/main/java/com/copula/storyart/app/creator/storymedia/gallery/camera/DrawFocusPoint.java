package com.copula.storyart.app.creator.storymedia.gallery.camera;

import android.graphics.*;
import com.storyart.support.widget.Logger;

/**
 * Created by heeleaz on 2/9/18.
 */
public class DrawFocusPoint {
    public static final int FOCUSING = 0, FOCUSED = 1;

    public static Bitmap draw(int state) {
        Rect rect = new Rect(0, 0, 100, 100);

        Bitmap bitmap = null;
        try {
            bitmap = Bitmap.createBitmap(rect.width(), rect.height(), null);
            Canvas canvas = new Canvas(bitmap);

            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(4);

            if (state == FOCUSED) paint.setColor(Color.GREEN);
            else paint.setColor(Color.RED);

            //int rx = rect.width() / 2, ry = rect.height() / 2;
            canvas.drawRect(new RectF(rect), paint);
        } catch (Exception e) {
            Logger.getInstance("DrawFocusPoint").e("Couldn't draw point", e);
        }

        return bitmap;
    }
}
