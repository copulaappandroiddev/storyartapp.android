package com.copula.storyart.app.creator.addonimages;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import com.copula.storyart.R;
import com.copula.storyart.app.creator.addonimages.recentaddonimages.RecentAddonFragment;
import com.copula.storyart.app.creator.emojiimage.EmojiListFragment;
import com.copula.storyart.app.creator.stickerimage.MainStickersFragment;
import com.copula.storyart.app.creator.stickerimage.search.StickersSearchActivity;
import com.storyart.support.util.ViewUtils;
import com.storyart.support.widget.PagerTabStrip;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 2/9/18.
 */
public class MainAddonImagesActivity extends FragmentActivity implements ViewPager.OnPageChangeListener,
        PagerTabStrip.OnPageSelectedListener, View.OnClickListener {
    public static final int REQUEST_ADDON_IMAGES = 0xA1;

    private SharedPreferences mSharedPreference;

    private ViewPager viewPager;
    private PagerTabStrip pagerTabStrip;

    private MainStickersFragment mainStickersFragment;

    public static void launch(Activity activity) {
        Intent intent = new Intent(activity, MainAddonImagesActivity.class);
        activity.startActivityForResult(intent, REQUEST_ADDON_IMAGES);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewUtils.fullscreenActivity(this);
        overridePendingTransition(R.transition.fade_in, R.transition.fade_out);

        setContentView(R.layout.act_main_addon_images);
        findViewById(R.id.btn_back).setOnClickListener(this);
        findViewById(R.id.search_view).setOnClickListener(this);
        (viewPager = findViewById(R.id.view_pager)).addOnPageChangeListener(this);
        (pagerTabStrip = findViewById(R.id.pager_tab_strip)).setOnPageSelectedListener(this);

        pagerTabStrip.addIndicator(R.layout.view_main_addon_pager_tab, R.string.tab_recent);
        pagerTabStrip.addIndicator(R.layout.view_main_addon_pager_tab, R.string.tab_sticker);
        pagerTabStrip.addIndicator(R.layout.view_main_addon_pager_tab, R.string.tab_emoji);

        mSharedPreference = getPreferences(Context.MODE_PRIVATE);

        List<Fragment> fragmentList = new ArrayList<>(3);
        fragmentList.add(RecentAddonFragment.instantiate());
        fragmentList.add(mainStickersFragment = MainStickersFragment.instantiate());
        fragmentList.add(EmojiListFragment.instantiate());
        viewPager.setAdapter(new MyPagerAdapter(fragmentList));

        int currentCachePage = mSharedPreference.getInt("addon_page", 1);
        if (currentCachePage == 0) {
            //no change will occur, if we call ViewPager.setCurrentItem. since the init page is always 0
            //so we called ViewPager#onPageSelected manually
            onPageSelected(currentCachePage);
        } else viewPager.setCurrentItem(currentCachePage);
    }

    @Override
    public void onPageSelected(int position) {
        mSharedPreference.edit().putInt("addon_page", position).apply();
        pagerTabStrip.selectPage(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onBackPressed() {
        if (!mainStickersFragment.allowBackPressed()) return;
        super.onBackPressed();
    }

    @Override
    public void onPageSelected(PagerTabStrip indicator, int index) {
        viewPager.setCurrentItem(index, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ADDON_IMAGES) {
            setResult(resultCode, data);
            finish();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.search_view) {
            StickersSearchActivity.launch(this, null);
        } else if (v.getId() == R.id.btn_back) finish();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.transition.no_change, R.transition.no_change);
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        private List<Fragment> fragmentList;

        MyPagerAdapter(List<Fragment> fragmentList) {
            super(getSupportFragmentManager());
            this.fragmentList = fragmentList;
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }
}
