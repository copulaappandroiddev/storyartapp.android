package com.copula.storyart.app.creator.storymedia;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.copula.storyart.R;
import com.copula.storyart.app.creator.AudioOptionFragment;
import com.copula.storyart.app.creator.Configuration;
import com.copula.storyart.app.creator.GraphicAudioDetailView;
import com.copula.storyart.app.creator.addonimages.MainAddonImagesActivity;
import com.copula.storyart.app.creator.audioeditor.AudioEditorFragment;
import com.copula.storyart.app.creator.graphicplacement.ObjectDisposerAdapter;
import com.copula.storyart.app.creator.graphicplacement.ObjectPlacementLayout;
import com.copula.storyart.app.creator.graphicplacement.TextPlacementAdapter;
import com.copula.storyart.app.creator.stickerimage.search.StickersSearchActivity;
import com.copula.storyart.app.rendering.MediaRenderFragment;
import com.copula.storyart.design.*;
import com.copula.storyart.media.audiogallery.NewAudioListFragment;
import com.copula.storyart.media.observer.MediaModel;
import com.copula.storyart.media.observer.MediaObserverService;
import com.storyart.support.util.ViewUtils;
import com.storyart.support.widget.CustomButton;
import com.storyart.support.widget.IResultCallback;
import com.storyart.support.widget.emoji.StoryartTextView;

/**
 * Created by heeleaz on 1/1/17.
 */
public abstract class GraphicCreatorActivity extends FragmentActivity implements View.OnClickListener, IResultCallback,
        TextPlacementAdapter.Callback {
    public static final int STICKER_SEARCH_FLAG = 0x0001;
    public static final int RECENT_MEDIA_LIST_FLAG = 0x0010;

    private GraphicAudioDetailView viewSelectedAudio;
    private CustomButton btnSelectAudio;

    private ObjectPlacementLayout mObjectPlacementLayout;
    private GraphicAudioProperty mGraphicAudioProperty;

    static final GraphicCreatorModel currentGraphicContext =
            new GraphicCreatorModel(Configuration.DISPLAY_WIDTH, Configuration.DISPLAY_HEIGHT);

    protected abstract View onRequestActivityView(LayoutInflater inflater, ViewGroup container);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.transition.no_change, R.transition.no_change);
        setContentView(R.layout.act_graphic_creator);

        View childView = onRequestActivityView(getLayoutInflater(), (ViewGroup) getWindow().getDecorView());
        if (childView != null) {
            ((ViewGroup) findViewById(R.id.view_parent)).addView(childView);
        }

        findViewById(R.id.btn_share).setOnClickListener(new ShareClickListener());
        findViewById(R.id.btn_save).setOnClickListener(new ShareClickListener());
        findViewById(R.id.btn_emoji_select).setOnClickListener(this);
        (mObjectPlacementLayout = findViewById(R.id.placement_view)).text().setCallback(this);
        (viewSelectedAudio = findViewById(R.id.view_audio_info)).setListener(new AudioInfoActionListener());
        (btnSelectAudio = findViewById(R.id.btn_audio_select)).setOnClickListener(this);

        ObjectDisposerAdapter placement = mObjectPlacementLayout.getItemRemover();
        placement.setPlacementListener(new ObjectDisposerAdapter.Listener() {
            @Override
            public void onPlacedObjectInMotion(boolean inMotion) {
                onRequestHideOptionButtons(inMotion);
            }
        });

        int commandFlag = getIntent().getIntExtra("commandFlag", 0);
        if ((commandFlag & STICKER_SEARCH_FLAG) == STICKER_SEARCH_FLAG) {
            StickersSearchActivity.launch(this, getIntent().getStringExtra("q"));
        }

        if ((commandFlag & RECENT_MEDIA_LIST_FLAG) == RECENT_MEDIA_LIST_FLAG) {
            NewAudioListFragment.show(getSupportFragmentManager());
            MediaObserverService.ackLatestMedia(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        onRequestHideOptionButtons(false);

        if (currentGraphicContext.isAudioAttached()) {
            setupShowGalleryAudioForGraphic(currentGraphicContext.getAudioAssets());
        } else {
            ViewUtils.showOnly(btnSelectAudio);
        }
    }

    protected boolean onPrepareExportProcess(GraphicCreatorModel model) {
        model.setGraphicTextList(mObjectPlacementLayout.getPlacedTextList());
        //TODO: to improve processing. I removed all images except GIF
        model.setGraphicImages(mObjectPlacementLayout.getPlacedImageList(GraphicImageProperty.GIF_IMAGE));
        return true;
    }

    protected MediaBackgroundProperty overrideBackgroundProperty() {
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AudioOptionFragment.REQUEST_AUDIO_GALLERY && data != null) {
            MediaModel.Audio audioMedia = (MediaModel.Audio) data.getSerializableExtra("audioMedia");
            setupOpenGalleryAudioEditor(audioMedia);
        } else if (requestCode == AudioOptionFragment.REQUEST_MIC_RECORD && resultCode == Activity.RESULT_OK) {
            MediaModel.Audio audioMedia = (MediaModel.Audio) data.getSerializableExtra("audioMedia");
            setupShowRecordAudioForGraphic(audioMedia);
        } else if ((requestCode == MainAddonImagesActivity.REQUEST_ADDON_IMAGES
                || requestCode == StickersSearchActivity.REQUEST_SEARCH)) {
            if (resultCode != Activity.RESULT_OK) return; //end with no warning message

            mObjectPlacementLayout.image().creatorEditable(data.getStringExtra("data"));
        }
    }//END

    private void setupShowRecordAudioForGraphic(MediaModel.Audio audioMedia) {
        mGraphicAudioProperty = new GraphicAudioProperty(audioMedia);
        mGraphicAudioProperty.editorAudioPath = mGraphicAudioProperty.source.data;
        mGraphicAudioProperty.editorDuration = (int) mGraphicAudioProperty.source.duration;

        viewSelectedAudio.setAudioInfo(mGraphicAudioProperty.editorAudioPath, mGraphicAudioProperty.editorDuration);
        currentGraphicContext.setAudioProperty(mGraphicAudioProperty);

        ViewUtils.showOnly(viewSelectedAudio);
    }

    private void setupOpenGalleryAudioEditor(MediaModel.Audio audioMedia) {
        mGraphicAudioProperty = new GraphicAudioProperty(audioMedia);
        AudioEditorFragment.show(getSupportFragmentManager(), mGraphicAudioProperty, AudioOptionFragment.REQUEST_MIC_RECORD);
    }

    private void setupShowGalleryAudioForGraphic(GraphicAudioProperty graphicAudioProperty) {
        viewSelectedAudio.setAudioInfo(graphicAudioProperty.editorAudioPath, graphicAudioProperty.editorDuration);
        currentGraphicContext.setAudioProperty(graphicAudioProperty);

        ViewUtils.showOnly(viewSelectedAudio);
    }

    StoryartTextView createEditableText(GraphicTextProperty text, TextPlacementAdapter.Builder builder) {
        return getObjectPlacementLayout().text().createEditable(text, builder);
    }

    @Override
    public void onFragmentResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AudioOptionFragment.REQUEST_MIC_RECORD) {
            if (resultCode != IResultCallback.RESULT_OK) return;//end here with no message

            mGraphicAudioProperty = (GraphicAudioProperty) data.getSerializableExtra("editorAudioMedia");
            setupShowGalleryAudioForGraphic(mGraphicAudioProperty);

        } else if (requestCode == NewAudioListFragment.REQUEST_CODE) {
            if (resultCode != IResultCallback.RESULT_OK) return;//end here with no message

            mGraphicAudioProperty = new GraphicAudioProperty((MediaModel.Audio) data.getSerializableExtra("audioMedia"));
            AudioEditorFragment.show(getSupportFragmentManager(), mGraphicAudioProperty, AudioOptionFragment.REQUEST_MIC_RECORD);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == btnSelectAudio) AudioOptionFragment.show(getSupportFragmentManager());
        else if (view.getId() == R.id.btn_emoji_select) {
            MainAddonImagesActivity.launch(this);
            onRequestHideOptionButtons(true);
        }
    }

    protected void onRequestHideOptionButtons(boolean hide) {
        View view = findViewById(R.id.view_control_container);
        if (hide) view.animate().alpha(0).setDuration(200).start();
        else view.animate().alpha(1).setDuration(200).start();
    }

    protected boolean isOptionButtonVisible() {
        return findViewById(R.id.view_control_container).getAlpha() == 1;
    }

    protected ObjectPlacementLayout getObjectPlacementLayout() {
        return mObjectPlacementLayout;
    }

    @Override
    public void onTextEditorSelected(StoryartTextView textView, GraphicTextProperty textProperty) {
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.transition.no_change, R.transition.no_change);
    }

    private class AudioInfoActionListener implements GraphicAudioDetailView.ActionListener {
        @Override
        public void onSelectedAudioCancel() {
            currentGraphicContext.removeAudioAssets();
            ViewUtils.showOnly(btnSelectAudio);
        }

        @Override
        public void onSelectedAudioOpen() {
            GraphicAudioProperty audio = currentGraphicContext.getAudioAssets();
            if (audio.editorAudioPath.equals(AudioEditorFragment.RECORD_OUT_PATH)) {
                AudioEditorFragment.show(getSupportFragmentManager(), mGraphicAudioProperty, AudioOptionFragment.REQUEST_MIC_RECORD);
            }
        }
    }

    class ShareClickListener implements View.OnClickListener, MediaRenderFragment.MediaRenderCallback {
        @Override
        public void onClick(View v) {
            if (!onPrepareExportProcess(currentGraphicContext)) return;

            int launchFlag = 0;
            if (v.getId() == R.id.btn_save) launchFlag = MediaRenderFragment.SAVE_MEDIA_FLAG;

            String graphic = currentGraphicContext.toJson();
            MediaRenderFragment.launch(getSupportFragmentManager(), graphic, launchFlag, this);
        }

        @Override
        public MediaBackgroundProperty overrideBackgroundProperty() {
            return GraphicCreatorActivity.this.overrideBackgroundProperty();
        }
    }
}