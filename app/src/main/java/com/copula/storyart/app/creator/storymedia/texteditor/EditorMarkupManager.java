package com.copula.storyart.app.creator.storymedia.texteditor;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EditorMarkupManager {
    private static final String TEXT_COLOR_TAG = "&#c";
    private static final String TEXT_BACKGROUND_TAG = "&#b";
    private static final String TEXT_TYPEFACE_TAG = "&#f";
    private static final String TEXT_TEXTSIZE_TAG = "&#s";

    public static Creator getCreator() {
        return new Creator();
    }

    public static Applyer getApplyer(Context context, TextView textView) {
        return new Applyer(context, textView);
    }


    public final static class Creator {
        private static final String MARKUP_DEFAULT = "";
        private String backgroundMarkup = MARKUP_DEFAULT;
        private String fontMarkup = MARKUP_DEFAULT;
        private String textColorMarkup = MARKUP_DEFAULT;
        private String textSizeMarkup = MARKUP_DEFAULT;


        public void setTextBackground(int color, float alpha) {
            float r = (float) Color.red(color) / 255;
            float g = (float) Color.green(color) / 255;
            float b = (float) Color.blue(color) / 255;

            int backgroundColor = Color.valueOf(r, g, b, alpha).toArgb();
            backgroundMarkup = TEXT_BACKGROUND_TAG + backgroundColor + ";";
        }

        public void setTypeface(String typeface) {
            fontMarkup = TEXT_TYPEFACE_TAG + typeface + ";";
        }

        public void setTextColor(int textColor) {
            textColorMarkup = TEXT_COLOR_TAG + textColor + ";";
        }

        public void setTextSize(int textSize) {
            textSizeMarkup = TEXT_TEXTSIZE_TAG + textSize + ";";
        }

        public String getMarkupText(CharSequence text) {
            return backgroundMarkup + fontMarkup + textColorMarkup + textSizeMarkup + text;
        }
    }


    public final static class Applyer {
        private Context mContext;
        private TextView mTextView;

        private Applyer(Context context, TextView view) {
            this.mContext = context;
            this.mTextView = view;
        }

        private void applyTextTypeface(String typefacePath) {
            Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), typefacePath);
            mTextView.setTypeface(typeface);
        }

        private void applyTextColor(int color) {
            mTextView.setTextColor(color);
        }

        private void applyTextBackground(int backgroundColor, CharSequence text) {
            ClassicTextDesignSpan span = new ClassicTextDesignSpan(mContext, 15);
            span.setColor(backgroundColor);
            SpannableString str = new SpannableString(text);
            str.setSpan(span, 0, str.length(), 0);

            mTextView.setText(str, TextView.BufferType.SPANNABLE);
        }

        private void applyWithTag(final String tagName, String tagValue, CharSequence text) {
            switch (tagName) {
                case TEXT_COLOR_TAG:
                    applyTextColor(Integer.parseInt(tagValue));
                    break;
                case TEXT_BACKGROUND_TAG:
                    applyTextBackground(Integer.parseInt(tagValue), text);
                    break;
                case TEXT_TYPEFACE_TAG:
                    applyTextTypeface(tagValue);
                    break;
            }
        }

        public void apply(CharSequence text) {
            if (text == null) return;

            String regex = "((&#b[-0-9]+;)|(&#f[-0-9A-Za-z./]+;)|(&#c[-0-9]+;)|(&#s[0-9]{1,3};))";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(text);

            String mainText = ((String) text).replaceAll(regex, "");
            while (matcher.find()) {
                String value = matcher.group();
                String tagName = value.substring(0, 3);
                String tagValue = value.substring(3, value.length() - 1);
                applyWithTag(tagName, tagValue, mainText);
            }
        }
    }
}
