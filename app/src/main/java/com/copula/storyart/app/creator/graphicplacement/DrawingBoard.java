package com.copula.storyart.app.creator.graphicplacement;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import com.copula.imagefilter.BitmapImageOptimizer;
import com.storyart.support.widget.Logger;

public class DrawingBoard extends FrameLayout {
    private static final Logger logger = Logger.getInstance("DrawingBoard");

    private static final int drawIfNull = Color.BLACK;

    public DrawingBoard(Context context) {
        super(context);
    }

    public DrawingBoard(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DrawingBoard(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public DrawingBoard(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public Bitmap drawingBoard(Bitmap.Config config) {
        return draw(config);
    }

    public boolean writeDrawingBoard(Bitmap.Config config, String file) {
        try {
            Bitmap image = drawingBoard(config);
            return BitmapImageOptimizer.writeBitmapToFile(image, file);
        } catch (Exception e) {
            logger.e("file: ", e);
            return false;
        }
    }

    private Bitmap draw(Bitmap.Config config) {
        Bitmap bitmap = Bitmap.createBitmap(this.getWidth(), this.getHeight(), config);
        Canvas canvas = new Canvas(bitmap);        //Bind a canvas to it


        Drawable bgDrawable = this.getBackground();
        if (bgDrawable != null) bgDrawable.draw(canvas);
        else canvas.drawColor(drawIfNull);


        this.draw(canvas);                         // draw the view on the canvas

        return bitmap;                             //return the bitmap;
    }
}
