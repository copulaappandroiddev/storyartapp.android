package com.copula.storyart.app.creator.emojiimage.inappemojis;

import android.content.Context;
import android.graphics.Bitmap;
import com.caverock.androidsvg.SVG;
import com.storyart.support.util.BitmapUtils;
import com.storyart.support.util.SVGBitmapHelper;

import java.io.*;

/**
 * Created by heeleaz on 12/7/17.
 */
public class AssetEmojiIconUtil {
    private static final float MAX_ICO_WIDTH = 128f;//Maximum icon width

    private static float calcScaleToIcon(int documentWidth) {
        return documentWidth / MAX_ICO_WIDTH;
    }

    public static boolean isIconExists(Context context, String path) {
        return new File(getIconPath(context, path)).exists();
    }

    public static String getIconPath(Context context, String child) {
        File file = new File(context.getCacheDir(), child);
        file.getParentFile().mkdirs();

        return file.getAbsolutePath().replace(".svg", ".png");
    }

    private static boolean isSvgImage(String emojiPath) {
        return emojiPath.endsWith(".svg");
    }

    public static void createIcon(Context context, String emojiPath) throws Exception {
        if (!isSvgImage(emojiPath)) return;

        SVG svg = SVG.getFromAsset(context.getAssets(), emojiPath);
        Bitmap bitmap = SVGBitmapHelper.pictureToBitmap(svg.renderToPicture());

        if (bitmap == null) throw new NullPointerException("Bitmap is null");

        String destination = getIconPath(context, emojiPath);

        BufferedOutputStream bos = null;
        try {
            float scale = calcScaleToIcon(bitmap.getWidth());
            bos = new BufferedOutputStream(new FileOutputStream(destination));
            bitmap = BitmapUtils.createScaledBitmap(bitmap, scale);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
        } finally {
            try {
                if (bos != null) bos.close();
            } catch (IOException ignored) {
            }
        }
    }

    public static InputStream getIconStream(Context context, String path) throws IOException {
        if (isSvgImage(path)) {
            return new BufferedInputStream(new FileInputStream(getIconPath(context, path)));
        } else return context.getAssets().open(path);
    }
}
