package com.copula.storyart.app.creator.stickerimage;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.copula.storyart.R;

/**
 * Created by heeleaz on 12/13/17.
 */
public class MainStickersFragment extends Fragment implements StickerCategoryFragment.OnCategorySelected {
    private String currentCategory;

    public static MainStickersFragment instantiate() {
        return new MainStickersFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle si) {
        return inflater.inflate(R.layout.frg_sticker_main, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fragment(StickerCategoryFragment.instantiate(this));
    }

    @Override
    public void onCategorySelected(String category) {
        fragment(StickerImageListFragment.instantiate(currentCategory = category));
    }

    private void fragment(Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager != null) {//getFragmentManager is Nullable
            fragmentManager.beginTransaction().replace(R.id.fragment, fragment).commit();
        }
    }

    public boolean allowBackPressed() {
        if (currentCategory == null) return true;

        fragment(StickerCategoryFragment.instantiate(this));
        currentCategory = null;
        return false;
    }
}
