package com.copula.storyart.app.creator.emojiimage.inappemojis.glideloader;

import android.content.Context;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.model.ModelLoader;
import com.copula.storyart.app.creator.emojiimage.inappemojis.AssetEmojiIconUtil;

import java.io.InputStream;
import java.lang.ref.WeakReference;

public class AssetsEmojiLoader implements ModelLoader<AssetsEmojiLoader.Load, InputStream> {
    private Context mContext;

    public AssetsEmojiLoader(Context context) {
        this.mContext = context;
    }

    @Override
    public DataFetcher<InputStream> getResourceFetcher(Load load, int i, int i1) {
        return (load != null) ? new MyFetcher(mContext, load.assetPath) : null;
    }

    public static class MyFetcher implements DataFetcher<InputStream> {
        private InputStream data;
        private WeakReference<Context> contextRef;
        private String uri;

        MyFetcher(Context context, String uri) {
            this.contextRef = new WeakReference<>(context);
            this.uri = uri;
        }

        @Override
        public InputStream loadData(Priority priority) throws Exception {
            Context context = contextRef.get();
            if (context == null) {
                throw new NullPointerException("Context is null: " + uri);
            }

            String relativePath = uri.replace("file:///android_asset/", "");
            if (!AssetEmojiIconUtil.isIconExists(context, relativePath)) {
                AssetEmojiIconUtil.createIcon(context, relativePath);
            }

            return data = AssetEmojiIconUtil.getIconStream(context, relativePath);
        }

        @Override
        public void cleanup() {
            try {
                if (data != null) data.close();
            } catch (Exception ignored) {
            }
        }

        @Override
        public String getId() {
            return uri != null ? String.valueOf(uri.hashCode()) : null;
        }

        @Override
        public void cancel() {
            //DO NOTHING
        }
    }

    public static class Load {
        String assetPath;

        public Load(String assetPath) {
            this.assetPath = assetPath;
        }
    }
}
