package com.copula.storyart.app.creator.audioeditor;

import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Looper;
import com.storyart.support.widget.Logger;

import java.io.IOException;

/**
 * Created by eliasigbalajobi on 4/30/16.
 */

class AudioPlayerAdapter implements MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener {
    static final int PLAY_STARTED = 1, PLAY_STOPPED = 2, PLAY_PAUSED = 3;

    private final Logger logger = Logger.getInstance("AudioEditorTextPlayer");

    private final MediaPlayer mMediaPlayer;

    private Listener mListener;
    private Handler mainUiLooper = new Handler(Looper.getMainLooper());
    private Runnable progressUpdaterHandlerRunnable;
    private boolean startOnPrepared = true;
    private int playStart, playStop;
    private String playUrl;

    private int currentState = PLAY_STOPPED;

    AudioPlayerAdapter(String playUrl, Listener listener) {
        this.playUrl = playUrl;
        this.mListener = listener;

        (mMediaPlayer = new MediaPlayer()).setOnCompletionListener(this);
        mMediaPlayer.setOnPreparedListener(this);
    }

    public void setPlayRange(int start, int stop, boolean playOnPrepared) {
        this.playStart = start * 1000;
        this.playStop = stop * 1000;
        this.startOnPrepared = playOnPrepared;
    }

    public void preparePlayer() throws IOException, IllegalStateException {
        mMediaPlayer.stop();
        mMediaPlayer.reset();
        mMediaPlayer.setDataSource(playUrl);

        mMediaPlayer.prepareAsync();
    }

    private void reset() {
        currentState = PLAY_STOPPED;

        if (mMediaPlayer.isPlaying()) {
            stop();

            mMediaPlayer.reset();
        }
    }

    public void togglePlayPause() {
        if (currentState != PLAY_STOPPED) {
            try {
                if (mMediaPlayer.isPlaying()) pause();
                else start();
            } catch (Exception exception) {
                mListener.onPlayStateChanged(PLAY_STOPPED);
                logger.e("Error In Play/Pause Operation", exception);
            }
        } else {
            try {
                preparePlayer();
                start();
            } catch (Exception exception) {
                logger.e("Audio Play Error", exception);
            }
        }
    }

    private void start() {
        mMediaPlayer.start();
        playProgressMonitor(0);
        if (mListener != null) mListener.onPlayStateChanged(currentState = PLAY_STARTED);
    }

    public void pause() {
        mMediaPlayer.pause();
        if (mListener != null) mListener.onPlayStateChanged(currentState = PLAY_PAUSED);
    }

    public void stop() {
        mMediaPlayer.stop();

        if (progressUpdaterHandlerRunnable != null)
            mainUiLooper.removeCallbacks(progressUpdaterHandlerRunnable);

        if (mListener != null) mListener.onPlayStateChanged(currentState = PLAY_STOPPED);
    }

    private synchronized void playProgressMonitor(int waitMilli) {
        mainUiLooper.postDelayed(progressUpdaterHandlerRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    if (mMediaPlayer == null || !mMediaPlayer.isPlaying()) return;
                } catch (Exception e) {
                    return;
                }

                int currentPosition = mMediaPlayer.getCurrentPosition();
                if (currentPosition >= playStop) {
                    stop();
                    return;
                }

                if (mListener != null)
                    mListener.onPlayProgress(mMediaPlayer.getDuration(), currentPosition);
                playProgressMonitor(1000);
            }
        }, waitMilli);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if (mListener != null) mListener.onPlayStateChanged(PLAY_STOPPED);
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        try {
            if (startOnPrepared) {
                mMediaPlayer.start();
                mMediaPlayer.seekTo(playStart);
                if (mListener != null) mListener.onPlayStateChanged(PLAY_STARTED);
            }
            playProgressMonitor(0);
        } catch (IllegalStateException exception) {
            logger.e("Error Preparing Player", exception);
        }
    }


    public void release() {
        try {
            if (mMediaPlayer != null) {
                reset();
                mMediaPlayer.release();
            }
        } catch (IllegalStateException exception) {
            logger.e("Error Releasing Player", exception);
        }
    }

    public interface Listener {
        void onPlayStateChanged(int state);

        void onPlayProgress(int duration, int position);
    }
}
