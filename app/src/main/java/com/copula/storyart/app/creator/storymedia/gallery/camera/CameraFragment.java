package com.copula.storyart.app.creator.storymedia.gallery.camera;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.*;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.*;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.copula.storyart.R;
import com.copula.storyart.app.RationalePermsDialog;
import com.copula.storyart.app.creator.Configuration;
import com.copula.storyart.app.creator.storymedia.MediaSourceCallback;
import com.storyart.support.widget.Logger;
import com.storyart.support.widget.PermissionHelper;

import java.util.List;

/**
 * Created by heeleaz on 2/6/18.
 */
public class CameraFragment extends Fragment implements View.OnClickListener {
    private static final Logger logger = Logger.getInstance("CameraFragment");
    private static int PERMISSION_REQUEST_CODE = 100;

    private Button btnCameraCapture, btnFlashToggle;
    private FrameLayout cameraPreviewContainer;
    private GestureDetector mGestureDetector;
    private ImageView imgImageFocusPoint;

    private SharedPreferences mSharedPreference;

    private Camera mCamera;
    private Camera.Parameters mCameraParameters;
    private PictureProcessor mPictureProcessor;
    private ImplCameraFocus mImplCameraFocus;

    public static CameraFragment instantiate() {
        return new CameraFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup c, Bundle si) {
        View v = inflater.inflate(R.layout.act_camera_shutter, c, false);

        cameraPreviewContainer = v.findViewById(R.id.camera_preview_container);

        imgImageFocusPoint = v.findViewById(R.id.img_focus_indicator);
        (btnCameraCapture = v.findViewById(R.id.btn_camera_capture)).setOnClickListener(this);
        (btnFlashToggle = v.findViewById(R.id.btn_flash_toggle)).setOnClickListener(this);
        v.findViewById(R.id.btn_camera_toggle).setOnClickListener(this);
        v.findViewById(R.id.btn_open_gallery).setOnClickListener(this);

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.mSharedPreference = getActivity().getPreferences(Context.MODE_PRIVATE);
    }

    private int getCachedCameraId() {
        return mSharedPreference.getInt("camera_id", 0);
    }

    private void preparePictureCamera(Camera camera, int cameraId, @Nullable List<Camera.Area> focusAreas) {
        CameraTextureView preview = new CameraTextureView(getActivity(), camera, cameraId);

        mCameraParameters = camera.getParameters();
        mCameraParameters.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);

        float aspectRatio = Configuration.MOVIE_ASPECT_RATIO;
        Camera.Size pictureSize = preview.getCameraPictureSize(aspectRatio, 1);
        if (pictureSize != null) {
            mCameraParameters.setPictureSize(pictureSize.width, pictureSize.height);
            mCameraParameters.setPreviewSize(pictureSize.width, pictureSize.height);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            camera.enableShutterSound(true);
        }

        if (focusAreas != null) mCameraParameters.setFocusAreas(focusAreas);
        invalidateFlashMode(false);

        camera.setParameters(mCameraParameters);

        cameraPreviewContainer.removeAllViews();
        cameraPreviewContainer.addView(preview, -1);

        preview.addCallback(new PictureSnapSurfaceHolder(preview));//callback for torch auto-focus
    }

    private void initAsync(final int cameraId, @Nullable final List<Camera.Area> focusAreas) {
        new AsyncTask<Void, Void, Camera>() {
            @Override
            protected Camera doInBackground(Void... voids) {
                try {
                    return mCamera = Camera.open(cameraId);
                } catch (Exception e) {
                    logger.e("Camera Init Error", e);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Camera camera) {
                if (isAdded()) preparePictureCamera(camera, cameraId, focusAreas);
            }
        }.execute();
    }

    private void init(int cameraId, @Nullable List<Camera.Area> focusAreas) {
        try {
            preparePictureCamera(mCamera = Camera.open(cameraId), cameraId, focusAreas);
        } catch (Exception e) {
            logger.e("Camera Init Error", e);
        }
    }

    private void toggleCameraMode() {
        releaseCamera();

        int cameraId = mSharedPreference.getInt("camera_id", 0);
        mSharedPreference.edit().putInt("camera_id", cameraId = ++cameraId % 2).apply();
        init(cameraId, null);
    }

    private String getCachedFlashMode() {
        return mSharedPreference.getString("flash_mode", Camera.Parameters.FLASH_MODE_OFF);
    }

    private void invalidateFlashMode(boolean toggle) {
        String cachedFlashMode = getCachedFlashMode();
        if (toggle) {
            if (cachedFlashMode.equals(Camera.Parameters.FLASH_MODE_OFF)) {
                cachedFlashMode = Camera.Parameters.FLASH_MODE_ON;
            } else cachedFlashMode = Camera.Parameters.FLASH_MODE_OFF;
        }

        if (ImplCameraFocus.isFlashSupported(mCamera)) {
            mCameraParameters.setFlashMode(cachedFlashMode);
            mCamera.setParameters(mCameraParameters);//update changes
        }

        mSharedPreference.edit().putString("flash_mode", cachedFlashMode).apply();
        btnFlashToggle.setSelected(cachedFlashMode.equals(Camera.Parameters.FLASH_MODE_ON));
    }

    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.release();        // release the camera for other applications
            mCamera = null;
        }
    }

    public void takeShot() {
        if (mCamera == null) return;

        if (getCachedCameraId() != 0 && getCachedFlashMode().equals(Camera.Parameters.FLASH_MODE_ON)
                && !ImplCameraFocus.isFlashSupported(mCamera)) {
            //TODO: preparePictureCamera a front screen shot flash
        }

        mCamera.takePicture(new MyShutterCallback(), null, mPictureProcessor);
    }


    @Override
    public void onResume() {
        super.onResume();
        if (btnCameraCapture != null) btnCameraCapture.setClickable(true);
        verifyV23ApplicationPermission();
    }

    @Override
    public void onPause() {
        super.onPause();
        releaseCamera();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_camera_capture) takeShot();
        else if (v.getId() == R.id.btn_flash_toggle) invalidateFlashMode(true);
        else if (v.getId() == R.id.btn_camera_toggle) toggleCameraMode();
        else if (v.getId() == R.id.btn_open_gallery) {
            if (getActivity() instanceof MediaSourceCallback) {
                ((MediaSourceCallback) getActivity()).requestSource(MediaSourceCallback.SOURCE_GALLERY);
            }
        }
    }

    ////////////////////// Camera permission request methods \\\\\\\\\\\\\\\\\\\\\\\
    private void verifyV23ApplicationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PermissionHelper h = new PermissionHelper(getActivity());

            String perm = Manifest.permission.CAMERA;
            int p = h.requestPermission(perm, PERMISSION_REQUEST_CODE);
            if (p == PermissionHelper.RATIONALE_PERM) {
                RationalePermsDialog.launch(getFragmentManager(), perm);
            } else if (p == PermissionHelper.PERMITTED)
                initAsync(getCachedCameraId(), null);
        } else initAsync(getCachedCameraId(), null);
    }//end

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] p, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) try {
            verifyV23ApplicationPermission();
        } catch (Exception e) {
            logger.e(e);
        }
    }//END

    ////////////////////////// Camera Callback Classes \\\\\\\\\\\\\\\\\\\\\\\\
    private class MyShutterCallback implements Camera.ShutterCallback {
        @Override
        public void onShutter() {
            if (btnCameraCapture != null) btnCameraCapture.setClickable(false);
        }
    }

    private class MyAutoFocusCallback implements ImplCameraFocus.Listener {
        private Handler handler = new Handler(Looper.getMainLooper());

        @Override
        public void onAutoFocus(boolean success, Camera camera) {
            invalidateFlashMode(false);
            imgImageFocusPoint.setImageBitmap(DrawFocusPoint.draw(DrawFocusPoint.FOCUSED));
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    imgImageFocusPoint.setImageBitmap(null);
                }
            }, 500);
        }
    }

    private class MyPictureCallback implements PictureProcessor.Listener {
        @Override
        public void onImageProcessing() {
            if (getView() == null) return;

            getView().findViewById(R.id.view_image_processing).setVisibility(View.VISIBLE);
        }

        @Override
        public void onImageProcessed(Bitmap image) {
            if (getView() == null) return;
            getView().findViewById(R.id.view_image_processing).setVisibility(View.GONE);

            PictureProcessor.CameraImageCache.getInstance().setCameraImage(image);
            if (getActivity() instanceof MediaSourceCallback) {
                ((MediaSourceCallback) getActivity())
                        .onMediaAvailable(PictureProcessor.CameraImageCache.CAMERA_BITMAP_KEY);
            }
        }
    }

    /**
     * this is meant to be init when surfaceView is created. If not, an error will occur
     * when trying to set autoFocus on some devices.
     */
    private class PreviewGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (getCachedCameraId() != 0 || mCamera == null) return true;

            mImplCameraFocus.safeFocusOnTouch(e);

            //in situation where the flash is off, we don't want focus to turn on torch
            String currentFlashMode = mCameraParameters.getFlashMode();
            if (currentFlashMode.equals(Camera.Parameters.FLASH_MODE_ON)) {
                String torchFlashMode = Camera.Parameters.FLASH_MODE_TORCH;
                mCameraParameters.setFlashMode(torchFlashMode);
                mCamera.setParameters(mCameraParameters);
            }

            imgImageFocusPoint.setX(e.getRawX() - (imgImageFocusPoint.getWidth() / 2));
            imgImageFocusPoint.setY(e.getRawY() - (imgImageFocusPoint.getHeight() / 2));

            imgImageFocusPoint.setImageBitmap(DrawFocusPoint.draw(DrawFocusPoint.FOCUSING));
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            toggleCameraMode();
            return true;
        }
    }//END

    private class PictureSnapSurfaceHolder implements TextureView.SurfaceTextureListener {
        private CameraTextureView mPortraitCameraPreview;

        PictureSnapSurfaceHolder(CameraTextureView preview) {
            this.mPortraitCameraPreview = preview;
        }

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            mPictureProcessor = new PictureProcessor(mPortraitCameraPreview, new MyPictureCallback());
            mImplCameraFocus = new ImplCameraFocus(mPortraitCameraPreview, new MyAutoFocusCallback());
            mGestureDetector = new GestureDetector(getContext(), new PreviewGestureListener());

            cameraPreviewContainer.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return mGestureDetector.onTouchEvent(event);
                }
            });
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {

        }
    }
}
