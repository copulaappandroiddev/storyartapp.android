package com.copula.storyart.app.creator;

import android.content.res.Resources;

/**
 * Created by heeleaz on 10/27/17.
 */
public class Configuration {
    public static final int DISPLAY_HEIGHT = Resources.getSystem().getDisplayMetrics().heightPixels;
    public static final int DISPLAY_WIDTH = Resources.getSystem().getDisplayMetrics().widthPixels;

    public static final int MOVIE_WIDTH = DISPLAY_WIDTH;
    public static final int MOVIE_HEIGHT = DISPLAY_HEIGHT;


    public static final float MOVIE_ASPECT_RATIO = (float) MOVIE_HEIGHT / (float) MOVIE_WIDTH;


    public static class Stickers {
        public static final int MAX_STICKERS_CACHE_SIZE = 5242880;  //5MB
        public static final String STICKERS_CACHE_DIR = ".Stickers";

        public static final int BACKGROUNDS_CACHE_SIZE = 5242880;     //5MB
        public static final String BACKGROUNDS_CACHE_DIR = ".Backgrounds";

        public static final int SEARCH_IMAGE_CACHE_SIZE = 2097152;  //2MB
        public static final String SEARCH_IMAGE_CACHE_DIR = ".Search";

        public static final int CUSTOM_STICKERS_SIZE = 5242880 * 10;     //50MB
        public static final String CUSTOM_STICKERS_DIR = ".Custom_Stickers";
    }
}
