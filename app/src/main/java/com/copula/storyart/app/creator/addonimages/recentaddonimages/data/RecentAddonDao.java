package com.copula.storyart.app.creator.addonimages.recentaddonimages.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.storyart.support.widget.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by heeleaz on 2/10/18.
 */
public class RecentAddonDao extends SQLiteOpenHelper {
    private static final Logger logger = Logger.getInstance("RecentAddonDao");
    private static final ExecutorService executorService = Executors.newCachedThreadPool();

    private static final String DATABASE_NAME = "recent_addon_images.db";
    private static final String TABLE_NAME = "recent_addon_images";
    private static final int DATABASE_VERSION = 5;

    private RecentAddonDao(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static RecentAddonDao getInstance(Context context) {
        return new RecentAddonDao(context);
    }

    private void insert(RecentAddonEntity entry) {
        if (!updateRecord(entry.resourceUrl)) {
            newRecord(entry);
            deleteOlderRecords();
        }
    }

    public void insertAsync(final RecentAddonEntity entity) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                insert(entity);
            }
        });
    }

    private boolean newRecord(RecentAddonEntity entry) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("title", entry.title);
            values.put("resource_url", entry.resourceUrl);
            values.put("resource_type", entry.type);
            values.put("updated_at", System.currentTimeMillis());

            return db.insert(TABLE_NAME, null, values) > -1;
        } catch (Exception e) {
            logger.e("Error adding new record", e);
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    public List<RecentAddonEntity> fetchAllRecord(String limit) {
        limit = " LIMIT " + ((limit == null) ? "0,50" : limit);
        SQLiteDatabase db = null;
        Cursor cu = null;
        try {
            db = getReadableDatabase();
            cu = db.query(TABLE_NAME, null, null, null,
                    null, null, "updated_at DESC " + limit);
            if (cu == null || cu.getCount() == 0) return new ArrayList<>();

            int id_idx = cu.getColumnIndex("_id");
            int title_idx = cu.getColumnIndex("title");
            int type_idx = cu.getColumnIndex("resource_type");
            int url_idx = cu.getColumnIndex("resource_url");

            List<RecentAddonEntity> items = new ArrayList<>(cu.getCount());
            while (cu.moveToNext()) {
                RecentAddonEntity entity = new RecentAddonEntity();
                entity.id = cu.getInt(id_idx);
                entity.title = cu.getString(title_idx);
                entity.type = cu.getInt(type_idx);
                entity.resourceUrl = cu.getString(url_idx);
                items.add(entity);
            }
            return items;
        } catch (Exception e) {
            logger.e("Error fetching recent_addon_images", e);
            return new ArrayList<>();
        } finally {
            if (cu != null) cu.close();
            if (db != null) db.close();
        }
    }

    private boolean updateRecord(String resourceUrl) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("resource_url", resourceUrl);
            values.put("updated_at", System.currentTimeMillis());

            String whereClause = "resource_url=?";
            String[] whereArgs = new String[]{resourceUrl};
            return db.update(TABLE_NAME, values, whereClause, whereArgs) > 0;

        } catch (Exception e) {
            logger.e("Error updating record", e);
            return false;
        } finally {
            if (db != null) db.close();
        }
    }

    private void deleteOlderRecords() {
        String sql = "DELETE FROM " + TABLE_NAME +
                " WHERE ROWID IN (SELECT ROWID FROM "
                + TABLE_NAME + " ORDER BY ROWID DESC LIMIT -1 OFFSET 50)";

        SQLiteDatabase db = null;
        Cursor cu = null;
        try {
            db = getReadableDatabase();
            cu = db.rawQuery(sql, null);
        } catch (Exception e) {
            logger.e("No data deleted", e);
        } finally {
            if (cu != null) cu.close();
            if (db != null) db.close();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME;
        sql += "(_id integer primary key autoincrement,";
        sql += "title VARCHAR(50),";
        sql += "resource_url VARCHAR(255),";
        sql += "resource_type VARCHAR(255),";
        sql += "updated_at integer(11))";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE " + TABLE_NAME);
        onCreate(db);
    }
}
