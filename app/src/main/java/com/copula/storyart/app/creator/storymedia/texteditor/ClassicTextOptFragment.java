package com.copula.storyart.app.creator.storymedia.texteditor;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import com.copula.storyart.R;
import com.copula.storyart.design.TypefaceLibrary;
import com.storyart.support.widget.CustomButton;
import com.storyart.support.widget.emoji.StoryartEditText;

public class ClassicTextOptFragment extends AbstractTextDesigner {
    private int designStyleIndex = 0;

    private CustomButton btnSelector;
    private StoryartEditText mEditText;

    private TextDesignerAdapter mTextDesignerAdapter;

    public static ClassicTextOptFragment instantiate() {
        return new ClassicTextOptFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle si) {
        View view = inflater.inflate(R.layout.frg_regular_man_text, container, false);
        (btnSelector = view.findViewById(R.id.btn_selector)).setOnClickListener(styleUpdateListener);

        styleSelector(designStyleIndex);

        return view;
    }

    public void setEditorTextView(StoryartEditText textView) {
        this.mEditText = textView;
    }

    @Override
    public CharSequence getMarkupText() {
        return mTextDesignerAdapter.mMarkupCreator.getMarkupText(mEditText.getText());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        prepare(context);
    }

    private void prepare(@NonNull Context context) {
        mTextDesignerAdapter = new TextDesignerAdapter(context, mEditText);
        mEditText.setPropertyChangedListener(new TextProperty());
    }

    private class TextProperty implements StoryartEditText.PropertyChangedListener {
        @Override
        public boolean beforePropertyChanged(StoryartEditText e, int property, Object object) {
            mTextDesignerAdapter.design((Integer) object, designStyleIndex);
            return designStyleIndex != 0;
        }

        @Override
        public boolean afterPropertyChanged(StoryartEditText e, int property, Object object) {
            return false;
        }
    }

    private View.OnClickListener styleUpdateListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            designStyleIndex = ++designStyleIndex % 3;

            styleSelector(designStyleIndex);
            mTextDesignerAdapter.design(mTextDesignerAdapter.getColor(), designStyleIndex);
        }
    };


    private void styleSelector(int index) {
        Drawable bg;
        int textColor;
        switch (index) {
            case 2:
                bg = shape(0xCCFFFFFF, 0x0000000);
                textColor = 0xFFFFFFFF;
                break;
            case 1:
                bg = shape(0xFFFFFFFF, 0xFFFFFFFF);
                textColor = 0xFF000000;
                break;
            default:
                bg = shape(0x00000000, 0xFFFFFFFF);
                textColor = 0xFFFFFFFF;
        }

        btnSelector.setTextColor(textColor);
        btnSelector.setBackground(bg);
    }

    private static GradientDrawable shape(int backgroundColor, int borderColor) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadii(new float[]{5, 5, 5, 5, 5, 5, 5, 5});
        shape.setColor(backgroundColor);
        shape.setStroke(3, borderColor);
        return shape;
    }

    private static class TextDesignerAdapter {
        private static final String TYPEFACE = TypefaceLibrary.ARCHIVO_BLACK;

        private ClassicTextDesignSpan mClassicTextDesignSpan;
        private EditorMarkupManager.Creator mMarkupCreator = EditorMarkupManager.getCreator();

        private EditText mEditText;

        TextDesignerAdapter(Context context, EditText editText) {
            this.mEditText = editText;
            mEditText.setTypeface(Typeface.createFromAsset(context.getAssets(), TYPEFACE));

            mClassicTextDesignSpan = new ClassicTextDesignSpan(context, 15);
            SpannableString str = new SpannableString(mEditText.getText());
            str.setSpan(mClassicTextDesignSpan, 0, str.length(), 0);

            mEditText.setText(str, TextView.BufferType.SPANNABLE);
            mEditText.setSelection(editText.getText().length());

            mMarkupCreator.setTypeface(TYPEFACE);
        }

        public void design(int textColor, int index) {
            float backgroundAlpha;
            switch (index) {
                case 2:
                    backgroundAlpha = 0.3f;
                    break;
                case 1:
                    backgroundAlpha = 1;
                    break;
                default:
                    backgroundAlpha = 0;
            }

            mClassicTextDesignSpan.setColor(textColor, backgroundAlpha);
            mEditText.invalidate();

            mMarkupCreator.setTextBackground(textColor, backgroundAlpha);
            mMarkupCreator.setTextColor(mClassicTextDesignSpan.getTextColor().getDefaultColor());
        }

        public int getColor() {
            return mClassicTextDesignSpan.getColor();
        }
    }
}
