package com.copula.storyart.app.creator.graphicplacement;

import android.content.Context;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.TextView;
import com.storyart.support.widget.emoji.StoryartTextView;

public class ObjectTouchDelegate implements View.OnTouchListener {
    // we can be in one of these 3 states
    private static final int NONE = 0, DRAG = 1, ZOOM = 2;
    private int mode = NONE;

    private float startPointX, startPointY;
    private float initRotationAngle = 0f;

    private ScaleGestureDetector mScaleGestureDetector;
    private ObjectDisposerAdapter mObjectDisposerAdapter;
    private GestureDetector mGestureDetector;

    private View.OnClickListener mOnClickListener;
    private View view;
    private boolean touchDelegateEnabled = true;

    ObjectTouchDelegate(Context context, ObjectDisposerAdapter adapter, ScaleGestureDetector.SimpleOnScaleGestureListener
            scaleGestureListener) {
        mObjectDisposerAdapter = adapter;
        mScaleGestureDetector = new ScaleGestureDetector(context, scaleGestureListener);

        mGestureDetector = new GestureDetector(context, new SingleTapConfirm());
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        this.view = view;
        if (mGestureDetector.onTouchEvent(motionEvent)) return true;

        mScaleGestureDetector.onTouchEvent(motionEvent);

        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                startPointX = view.getX() - motionEvent.getRawX();
                startPointY = view.getY() - motionEvent.getRawY();
                mode = DRAG;
                break;

            case MotionEvent.ACTION_POINTER_DOWN:
                //if ((spacing(motionEvent)) > 10f) mode = ZOOM;
                mode = ZOOM;
                initRotationAngle = rotation(motionEvent);
                break;

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                mode = NONE;
                break;

            case MotionEvent.ACTION_MOVE:
                if (mode == DRAG) {
                    view.setX(motionEvent.getRawX() + startPointX);
                    view.setY(motionEvent.getRawY() + startPointY);
                } else if (mode == ZOOM) {
                    int pointerCount = motionEvent.getPointerCount();
                    if (pointerCount == 2 || pointerCount == 3) {
                        float newRotation = rotation(motionEvent);
                        float rotation = newRotation - initRotationAngle;
                        view.setRotation(rotation + view.getRotation());
                    }
                }
                break;
        }
        view.invalidate();

        if (touchDelegateEnabled) dispatchMonitorRemover(motionEvent);

        return true;
    }

    private void dispatchMonitorRemover(MotionEvent motionEvent) {
        if (mObjectDisposerAdapter.monitorRemover(view, motionEvent)) {
            view.setEnabled(false);
            view = null;
        }
    }

    public void setClickGestureListener(View.OnClickListener clickListener) {
        mOnClickListener = clickListener;
    }

    private boolean isMotionEventInTextPaintBound(TextView view, MotionEvent e) {
        CharSequence text = view.getText();
        if (TextUtils.isEmpty(text)) text = view.getHint();

        int textWidth = (int) view.getPaint().measureText(text.toString());

        int leftSpace = (view.getWidth() - textWidth) >> 1;
        int rightSpace = textWidth + leftSpace;//down to textWidth end
        //int topSpace = (view.getHeight() - textHeight) >> 1;
        //int bottomSpace = (textHeight + topSpace);

        return (e.getX() >= leftSpace && e.getX() <= rightSpace);
    }

    public void setEnabled(boolean enable) {
        this.touchDelegateEnabled = enable;
    }

    /**
     * Determine the space between the first two fingers
     */
    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        float s = x * x + y * y;
        return (float) Math.sqrt(s);
    }

    /**
     * Calculate the degree to be rotated by.
     *
     * @param event
     * @return Degrees
     */
    private float rotation(MotionEvent event) {
        double delta_x = (event.getX(0) - event.getX(1));
        double delta_y = (event.getY(0) - event.getY(1));
        double radians = Math.atan2(delta_y, delta_x);
        return (float) Math.toDegrees(radians);
    }


    private class SingleTapConfirm extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            //for TextView, we make only clickable part to be the textpaint only,
            //so, all other parts including padding will not be registered at clicked
            if (view instanceof StoryartTextView) {
                if (isMotionEventInTextPaintBound((TextView) view, e)) {
                    if (mOnClickListener != null) mOnClickListener.onClick(view);
                }
            } else if (mOnClickListener != null) mOnClickListener.onClick(view);

            return true;
        }
    }//END
}
