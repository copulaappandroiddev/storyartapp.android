package com.copula.storyart.app.creator.helper.restful;

import android.content.Context;
import com.copula.smartjson.JsonInner;
import com.copula.smartjson.OJsonMapper;
import com.storyart.support.webservice.GetParam;
import com.storyart.support.webservice.NettyApiProvider;
import com.copula.storyart.app.StoryartApiEndpoint;
import com.storyart.support.webservice.RestResponse;

import java.util.List;

/**
 * Created by heeleaz on 1/27/18.
 */
public abstract class AbsImageResourceApi extends AbsRestfulApi<List<AbsImageResourceApi.ApiModel>> {
    private NettyApiProvider apiProvider = new NettyApiProvider(StoryartApiEndpoint.ENDPOINT);
    private String category;
    private Context context;

    public AbsImageResourceApi(Context context, String category, String page) {
        super(page);
        this.context = context;
        this.category = category;
    }

    protected abstract String getUrl();

    @Override
    protected RestResponse<List<ApiModel>> get(String page) throws Exception {
        GetParam param = new GetParam();
        param.put("category", category).put("page", page);

        apiProvider.cache(context.getCacheDir(), 1024 * 1024);
        NettyApiProvider.Result result = apiProvider.get(getUrl(), param);
        if (result == null || result.responseCode != 200) return null;

        RestResponse<List<ApiModel>> rest = OJsonMapper.mapOne(RestResponse.class, result.response);
        if (rest != null) {
            rest.result = OJsonMapper.mapList(ApiModel.class, result.response);
        }
        return rest;
    }


    @JsonInner("result")
    public static class ApiModel {
        public String resourceUrl, category, title, placeholderUrl;

        @Override
        public boolean equals(Object obj) {
            return resourceUrl.equals(((ApiModel) obj).resourceUrl);
        }
    }
}
