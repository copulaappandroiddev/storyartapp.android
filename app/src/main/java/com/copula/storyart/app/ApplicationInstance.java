package com.copula.storyart.app;

import android.app.Application;
import android.content.Intent;
import android.os.StrictMode;
import com.copula.storyart.media.observer.MediaObserverService;
import com.facebook.FacebookSdk;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.storyart.emojikeyboard.EmojiManager;
import com.storyart.emojikeyboard.systememoji.SystemEmojiProvider;


/**
 * Created by heeleaz on 10/23/17.
 */
public class ApplicationInstance extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        //VmPolicy to enable app share data to external application process
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        FirebaseAnalytics.getInstance(this);
        FacebookSdk.sdkInitialize(this);

        EmojiManager.install(new SystemEmojiProvider());

        MediaObserverService.enqueueWork(this, new Intent());
    }
}
