package com.copula.storyart.app.creator.storymedia.gallery.camera;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.util.LruCache;
import com.storyart.support.widget.Logger;

/**
 * Created by heeleaz on 2/8/18.
 */
public class PictureProcessor implements Camera.PictureCallback {
    private static final Logger logger = Logger.getInstance("PictureProcessor");

    private CameraTextureView mCameraPreview;
    private Listener mListener;

    PictureProcessor(CameraTextureView preview, Listener l) {
        this.mCameraPreview = preview;
        this.mListener = l;
    }

    private Bitmap rotate(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Matrix mtx = new Matrix();
        mtx.postRotate(degree);

        if (mCameraPreview.isCameraFacingFront())
            mtx.preScale(-1.0f, 1.0f);

        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        new Processor(data, mCameraPreview.getCameraDisplayOrientation()).execute(mListener);
    }

    public interface Listener {
        void onImageProcessing();

        void onImageProcessed(Bitmap image);
    }

    public static class CameraImageCache extends LruCache<String, Bitmap> {
        public static final String CAMERA_BITMAP_KEY = "file:///CAMERA_BITMAP";
        private static final CameraImageCache instance = new CameraImageCache();

        private CameraImageCache() {
            super(((int) (Runtime.getRuntime().maxMemory() / 1024)) / 2);
        }

        public static CameraImageCache getInstance() {
            return instance;
        }

        public synchronized void setCameraImage(Bitmap bitmap) {
            remove(CAMERA_BITMAP_KEY);
            put(CAMERA_BITMAP_KEY, bitmap);
        }

        public synchronized Bitmap getCameraImage(String key) {
            return get(key);
        }
    }

    private class Processor extends AsyncTask<Void, Void, Bitmap> {
        private byte[] data;
        private int cameraOrientation;
        private Listener mListener;

        private Processor(byte[] data, int cameraOrientation) {
            this.data = data;
            this.cameraOrientation = cameraOrientation;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mListener != null) mListener.onImageProcessing();
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {
            try {
                Bitmap b = BitmapFactory.decodeByteArray(data, 0, data.length);
                return rotate(b, cameraOrientation);
            } catch (OutOfMemoryError e) {
                logger.e("PictureProcessor", e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (mListener != null) mListener.onImageProcessed(bitmap);
        }

        private void execute(Listener listener) {
            mListener = listener;
            execute();
        }
    }
}
