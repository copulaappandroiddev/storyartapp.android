package com.copula.storyart.app.creator.storymedia.texteditor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import com.copula.storyart.R;
import com.copula.storyart.app.creator.Configuration;
import com.copula.storyart.design.GraphicTextProperty;
import com.storyart.emojikeyboard.EmojiPopup;
import com.storyart.support.widget.CustomTextView;
import com.storyart.support.widget.emoji.StoryartEditText;

/**
 * Created by heeleaz on 2/1/18.
 */
public class TextEditorActivity extends FragmentActivity implements View.OnClickListener, StoryartEditText.KeyPreImeListener {
    public static final int REQUEST_EDITOR = 0x1034;
    public static final int FLAG_RETURN_LINE_BREAK = 0x1000;
    public static final int FLAG_DESIGN_SELECT = 0x0100;
    public static final int MASK_MAX_LINES = 0xFFFF;
    private int editorFlagValue;

    private EmojiPopup mEmojiPopup;

    private StoryartEditText editText;
    private GraphicTextProperty graphicTextProperty;

    private ImageButton btnEmojiKeyboard;
    private TextDesignerSelector mTextDesignerSelector;

    public static void launch(Activity activity, GraphicTextProperty text, int flags) {
        Intent intent = new Intent(activity, TextEditorActivity.class);
        intent.putExtra("text", text).putExtra("flags", flags);

        activity.startActivityForResult(intent, REQUEST_EDITOR);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.transition.fade_in, R.transition.fade_out);
        setContentView(R.layout.act_text_editor);

        editorFlagValue = getIntent().getIntExtra("flags", 0);
        findViewById(R.id.btn_close).setOnClickListener(this);
        findViewById(R.id.btn_done).setOnClickListener(this);
        (editText = findViewById(R.id.edt_editor)).setKeyPreImeListener(this);
        (btnEmojiKeyboard = findViewById(R.id.btn_emoji_keyboard)).setOnClickListener(this);

        graphicTextProperty = (GraphicTextProperty) getIntent().getSerializableExtra("text");
        invalidateTextDesign(graphicTextProperty.text);

        if ((editorFlagValue & FLAG_DESIGN_SELECT) == FLAG_DESIGN_SELECT) {
            CustomTextView typefaceSelect = findViewById(R.id.btn_typeface_select);
            typefaceSelect.setOnClickListener(mTextDesignerSelector = new TextDesignerSelector(typefaceSelect));
            typefaceSelect.setVisibility(View.VISIBLE);
        }

        mEmojiPopup = EmojiPopup.Builder.with(this).fromRootView(editText.getRootView()).build(editText);

        int maxLines = (editorFlagValue) & 0x000F;
        if (maxLines == 0) editText.setMaxLineBasedOnHeight(Configuration.DISPLAY_HEIGHT);
        else editText.setMaxLines(maxLines);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        EditorTextColorFragment fragment = EditorTextColorFragment.instantiate();
        fragment.setEditorTextView(editText);
        ft.replace(R.id.fragment_color_selector, fragment).commit();
    }

    private void updateEditorChanges(GraphicTextProperty tp, boolean update) {
        if (update) {
            tp.text = (String) mTextDesignerSelector.getMarkupText();
            tp.textColor = editText.getTextColors().getDefaultColor();
            editText.setTag(tp);
        }

        Intent data = new Intent().putExtra("text", tp);
        setResult(Activity.RESULT_OK, data);
        finish();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_done) {
            updateEditorChanges(graphicTextProperty, true);
        } else if (v.getId() == R.id.btn_close) {
            updateEditorChanges(graphicTextProperty, false);
        } else if (v.getId() == R.id.btn_emoji_keyboard) {
            if (mEmojiPopup.isShowing()) {
                btnEmojiKeyboard.setImageResource(R.drawable.ic_emoji_keyboard_78px);
            } else {
                btnEmojiKeyboard.setImageResource(R.drawable.ic_keyboard_white_78px);
            }
            mEmojiPopup.toggle(); // Toggles visibility of the Popup.
        }
    }

    @Override
    public void onBackPressed() {
        updateEditorChanges(graphicTextProperty, true);
    }

    @Override
    public boolean onKeyPreIme(StoryartEditText editText, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            updateEditorChanges(graphicTextProperty, true);
        }
        return true;
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.transition.no_change, R.transition.no_change);
    }


    private void invalidateTextDesign(String text) {
        EditorMarkupManager.getApplyer(this, editText).apply(text);
    }

    private class TextDesignerSelector implements View.OnClickListener {
        private final String[] typefaceName = new String[]{"Normal", "Classic"};
        private int position = 0;

        private final AbstractTextDesigner[] abstractTextDesigners = new AbstractTextDesigner[2];

        private TextDesignerSelector(TextView textView) {
            go(position, textView);
        }

        @Override
        public void onClick(View v) {
            go(++position % typefaceName.length, ((CustomTextView) v));
        }

        private void go(int position, TextView textView) {
            textView.setText(typefaceName[position]);

            if (abstractTextDesigners[position] == null) {
                abstractTextDesigners[position] = getDesignerForPosition(position);
                abstractTextDesigners[position].setEditorTextView(editText);
            }

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.frg_editor_type, abstractTextDesigners[position]).commit();
        }

        private AbstractTextDesigner getDesignerForPosition(int position) {
            switch (position) {
                case 1:
                    return ClassicTextOptFragment.instantiate();
                default:
                    return NormalTextOptFragment.instantiate();
            }
        }

        private CharSequence getMarkupText() {
            return abstractTextDesigners[position].getMarkupText();
        }
    }
}
