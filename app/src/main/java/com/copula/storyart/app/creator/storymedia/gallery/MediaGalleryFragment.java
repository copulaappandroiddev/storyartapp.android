package com.copula.storyart.app.creator.storymedia.gallery;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.bumptech.glide.Glide;
import com.copula.storyart.R;
import com.copula.storyart.app.creator.storymedia.MediaSourceCallback;
import com.copula.storyart.design.MediaBackgroundProperty;
import com.copula.storyart.media.observer.SystemMediaProvider;
import com.storyart.support.util.ViewUtils;
import com.storyart.support.widget.BaseAdapter;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by heeleaz on 1/30/18.
 */
public class MediaGalleryFragment extends Fragment implements AdapterView.OnItemClickListener, View.OnClickListener {
    public static final int HIDE_OPEN_CAMERA_FLAG = 0x1000;

    private static final Executor loaderThreadPool = Executors.newSingleThreadExecutor();
    private GridView gridView;
    private View viewEmptyList;

    public static MediaGalleryFragment instantiate(int flags) {
        Bundle arguments = new Bundle(1);
        arguments.putInt("flags", flags);
        MediaGalleryFragment fragment = new MediaGalleryFragment();
        fragment.setArguments(arguments);

        return fragment;
    }

    public static MediaGalleryFragment instantiate() {
        return instantiate(0);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle si) {
        View view = inflater.inflate(R.layout.frg_media_gallery, container, false);
        viewEmptyList = view.findViewById(R.id.txt_not_found_message);
        (gridView = view.findViewById(R.id.gridview)).setOnItemClickListener(this);

        int flags = getArguments().getInt("flags");
        Button btnOpenCamera = view.findViewById(R.id.btn_camera_open);
        btnOpenCamera.setOnClickListener(this);

        if ((flags & HIDE_OPEN_CAMERA_FLAG) == HIDE_OPEN_CAMERA_FLAG) {
            btnOpenCamera.setVisibility(View.GONE);
        }

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new ListLoaderAsync().executeOnExecutor(loaderThreadPool);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        MediaBackgroundProperty media = (MediaBackgroundProperty) parent.getItemAtPosition(position);
        if (getActivity() instanceof MediaSourceCallback) {
            ((MediaSourceCallback) getActivity()).onMediaAvailable(media.backgroundUrl);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_camera_open) {
            if (getActivity() instanceof MediaSourceCallback) {
                ((MediaSourceCallback) getActivity()).requestSource(MediaSourceCallback.SOURCE_CAMERA);
            }
        }
    }

    private class ListLoaderAsync extends AsyncTask<Void, Void, List<MediaBackgroundProperty>> {
        @Override
        protected List<MediaBackgroundProperty> doInBackground(Void... voids) {
            return SystemMediaProvider.getImages(getContext());
        }

        @Override
        protected void onPostExecute(List<MediaBackgroundProperty> list) {
            if (!isAdded()) return;

            if (list != null && !list.isEmpty()) {
                ViewUtils.showOnly(gridView);
                gridView.setAdapter(new MyAdapter(getContext(), list));
            } else ViewUtils.showOnly(viewEmptyList);
        }
    }

    private class MyAdapter extends BaseAdapter<MediaBackgroundProperty> {
        MyAdapter(Context context, List<MediaBackgroundProperty> e) {
            super(context, e);
        }

        @Override
        public View getView(final int position, View convertView, LayoutInflater i) {
            final ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = i.inflate(R.layout.adp_media_gallery, null, false);
                holder.txtDuration = convertView.findViewById(R.id.txt_duration);
                holder.image = convertView.findViewById(R.id.img_image);
                holder.videoIndicator = convertView.findViewById(R.id.view_video_indicator);
                convertView.setTag(holder);
            } else holder = (ViewHolder) convertView.getTag();

            MediaBackgroundProperty e = getItem(position);
            if (e.mimeType.startsWith("video/")) {
                holder.txtDuration.setText(ViewUtils.formatDuration(e.duration));
                holder.videoIndicator.setVisibility(View.VISIBLE);
            } else holder.videoIndicator.setVisibility(View.GONE);

            Glide.with(getContext())
                    .load(e.backgroundUrl).placeholder(R.color.galleryItemPlaceholder).into(holder.image);
            return convertView;
        }

        private class ViewHolder {
            private TextView txtDuration;
            private ImageView image;
            private View videoIndicator;
        }
    }//END
}
