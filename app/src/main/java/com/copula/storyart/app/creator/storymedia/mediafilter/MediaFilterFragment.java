package com.copula.storyart.app.creator.storymedia.mediafilter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.copula.imagefilter.BitmapImageOptimizer;
import com.copula.imagefilter.ImageFilterPacket;
import com.copula.storyart.R;
import com.copula.storyart.app.creator.Configuration;
import com.copula.storyart.app.creator.storymedia.gallery.camera.PictureProcessor;
import com.storyart.support.util.ViewUtils;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MediaFilterFragment extends Fragment {
    private static final ExecutorService executor = Executors.newSingleThreadExecutor();
    private boolean hasEditedImage = false;
    private EditorImageView imageView;
    private TextView txtFilterName;
    private Bitmap unfilteredImage;
    private ShowFilterNameHandler mShowFilterNameHandler = new ShowFilterNameHandler();
    private ImageFilterPacket mImageFilterPacket;

    private static final int MAX_IMAGE_WIDTH = Configuration.MOVIE_WIDTH > 1080 ? 1080 : Configuration.MOVIE_WIDTH;
    private static final int MAX_IMAGE_HEIGHT = Configuration.MOVIE_HEIGHT > 1920 ? 1920 : Configuration.MOVIE_HEIGHT;

    public static MediaFilterFragment instantiate(String imageUrl) {
        MediaFilterFragment fragment = new MediaFilterFragment();

        Bundle argument = new Bundle(1);
        argument.putString("imageUrl", imageUrl);
        fragment.setArguments(argument);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle si) {
        View view = inflater.inflate(R.layout.frg_image_filter, container, false);
        (imageView = view.findViewById(R.id.img_image)).setOnTouchListener(new ImageTouchListener());
        txtFilterName = view.findViewById(R.id.txt_filter_name);
        View viewTooltip = view.findViewById(R.id.view_tooltip);

        SharedPreferences preferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        if (!preferences.getBoolean("show_tooltip", false)) {
            viewTooltip.setVisibility(View.VISIBLE);
            preferences.edit().putBoolean("show_tooltip", true).apply();
        } else viewTooltip.setVisibility(View.GONE);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mImageFilterPacket = new ImageFilterPacket(getContext());
        loadBitmapAndApplyFilter(getArguments().getString("imageUrl"));
    }

    private void loadBitmapAndApplyFilter(final String imageUrl) {
        Bitmap cameraImage = PictureProcessor.CameraImageCache.getInstance().getCameraImage(imageUrl);
        unfilteredImage = cameraImage;

        if (unfilteredImage != null) {
            applyFilter(null, unfilteredImage, 0);
        } else {
            new ImageLoadTask(imageUrl).executeOnExecutor(executor);
        }
    }

    private void applyFilter(String filterName, Bitmap image, int transition) {
        if (image == null || !isAdded()) return;

        Bitmap filteredBitmap = image;
        if (!TextUtils.isEmpty(filterName)) {
            filteredBitmap = mImageFilterPacket.applyFilter(filterName, image);
        }

        Drawable currentImageDrawable = imageView.getDrawable();

        if (transition <= 0 || currentImageDrawable == null) {
            imageView.setImageBitmap(filteredBitmap);
        } else {
            Drawable newImage = new BitmapDrawable(getResources(), filteredBitmap);

            TransitionDrawable d =
                    new TransitionDrawable(new Drawable[]{currentImageDrawable, newImage});
            imageView.setImageDrawable(d);
            d.startTransition(transition);
        }

        mShowFilterNameHandler.showFilterName(filterName, 2000);
    }

    private BitmapDrawable getPlaceholder(String placeholderUrl) {
        File file = new File(placeholderUrl);
        Bitmap bitmap = BitmapImageOptimizer.load(file, 1, Configuration.DISPLAY_HEIGHT);
        return new BitmapDrawable(getResources(), bitmap);
    }

    public boolean hasEditedImage() {
        return hasEditedImage;
    }

    private boolean onFilterViewSwipe(int position) {
        if (unfilteredImage == null) return true;

        hasEditedImage = true;
        if (position == 1) {//swipe right
            applyFilter(mImageFilterPacket.nextFilterName(), unfilteredImage, 100);
        } else {
            applyFilter(mImageFilterPacket.prevFilterName(), unfilteredImage, 100);
        }
        return true;
    }

    private class ImageLoadTask extends AsyncTask<Void, Void, Bitmap> {
        private String imageUrl;

        private ImageLoadTask(String imageUrl) {
            this.imageUrl = imageUrl;
            imageView.setBackground(getPlaceholder(imageUrl));
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {
            return BitmapImageOptimizer.load(new File(imageUrl), MAX_IMAGE_WIDTH, MAX_IMAGE_HEIGHT);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            applyFilter(null, unfilteredImage = bitmap, 250);
        }
    }

    private class ShowFilterNameHandler implements Runnable {
        private Handler handler = new Handler(Looper.myLooper());

        private void showFilterName(String filterName, int delayMillis) {
            handler.removeCallbacks(this);
            txtFilterName.setAlpha(1);
            txtFilterName.setText(filterName);
            handler.postDelayed(this, delayMillis);
        }

        @Override
        public void run() {
            if (!isAdded()) return;
            txtFilterName.animate().alpha(0f).setDuration(150).start();
        }
    }

    private class ImageTouchListener implements View.OnTouchListener {
        private final int swipeDetectWidthThreshold = ViewUtils.dpToPx(60);

        private int pointerId;

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    v.setTag(event.getX());
                    pointerId = event.getPointerId(0);
                    return true;

                case MotionEvent.ACTION_UP:
                    int pointerIndex = event.findPointerIndex(pointerId);
                    if (pointerIndex != -1) {
                        float dX = ((float) v.getTag()) - event.getX(pointerIndex);
                        if (Math.abs(dX) > swipeDetectWidthThreshold) {
                            if (dX < 0) return onFilterViewSwipe(-1);
                            if (dX > 0) return onFilterViewSwipe(1);
                        }
                    }
                    return false; // no swipe horizontally and no swipe vertically
            }
            return false;
        }
    }
}
