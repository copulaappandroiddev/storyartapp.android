package com.copula.storyart.app.creator.graphicbackground.colorbackground;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import com.copula.storyart.R;
import com.copula.storyart.design.MediaBackgroundProperty;
import com.storyart.support.widget.BaseAdapter;
import com.storyart.support.widget.CircularColorView;

import java.util.List;

/**
 * Created by heeleaz on 1/26/18.
 */
public class ColorBackgroundFragment extends Fragment implements AdapterView.OnItemClickListener {
    private GridView gridView;
    private MyAdapter mColorListAdapter;
    private ColorBackgroundPacket mColorBackgroundPacket;

    public static ColorBackgroundFragment instantiate() {
        return new ColorBackgroundFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle si) {
        View v = inflater.inflate(R.layout.frg_color_background, container, false);
        gridView = v.findViewById(R.id.gridview);
        return v;
    }

    protected GridView getGridView() {
        return gridView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        gridView.setAdapter(mColorListAdapter = new MyAdapter(getContext()));
        gridView.setOnItemClickListener(this);

        mColorBackgroundPacket = ColorBackgroundPacket.getInstance();
        populateColorList(mColorBackgroundPacket.getColorList());
    }

    protected void populateColorList(List<MediaBackgroundProperty> colorList) {
        mColorListAdapter.addHandlers(colorList);
        mColorListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        mColorBackgroundPacket.movePosition(position);
        MediaBackgroundProperty background = (MediaBackgroundProperty) adapterView.getItemAtPosition(position);

        Intent intent = new Intent().putExtra("background", background);
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
    }

    public class MyAdapter extends BaseAdapter<MediaBackgroundProperty> {
        MyAdapter(Context context) {
            super(context);
        }

        @Override
        public View getView(int position, View convertView, LayoutInflater inflater) {
            final ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.adp_color_background, null, false);
                holder.imageView = convertView.findViewById(R.id.img_background);
                convertView.setTag(holder);
            } else holder = (ViewHolder) convertView.getTag();

            holder.imageView.setBackgroundColor(Color.parseColor(getItem(position).backgroundUrl));
            return convertView;
        }

        private class ViewHolder {
            CircularColorView imageView;
        }
    }//END
}
