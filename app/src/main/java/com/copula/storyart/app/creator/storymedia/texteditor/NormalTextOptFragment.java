package com.copula.storyart.app.creator.storymedia.texteditor;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.copula.storyart.design.TypefaceLibrary;
import com.storyart.support.widget.emoji.StoryartEditText;

public class NormalTextOptFragment extends AbstractTextDesigner {
    private static final String TYPEFACE = TypefaceLibrary.SQUADAONE;

    private StoryartEditText mTextView;
    private EditorMarkupManager.Creator mMarkupCreator = EditorMarkupManager.getCreator();

    public static NormalTextOptFragment instantiate() {
        return new NormalTextOptFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public void setEditorTextView(StoryartEditText textView) {
        this.mTextView = textView;
    }

    @Override
    public CharSequence getMarkupText() {
        return mMarkupCreator.getMarkupText(mTextView.getText());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mTextView != null) mTextView.setTypefaceFromAssetPath(TYPEFACE);
    }
}
