package com.copula.storyart.app.creator.stickerimage.search;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import com.copula.storyart.R;
import com.storyart.support.util.ViewUtils;

/**
 * Created by heeleaz on 2/11/18.
 */
public class StickersSearchActivity extends FragmentActivity implements View.OnClickListener, TextWatcher {
    public static final int REQUEST_SEARCH = 100 >> 2;

    private StickersSearchFragment mStickersSearchFragment;
    private EditText mSearchView;

    public static void launch(Activity ctx, String query) {
        Intent intent = new Intent(ctx, StickersSearchActivity.class);
        ctx.startActivityForResult(intent.putExtra("q", query), REQUEST_SEARCH);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewUtils.fullscreenActivity(this);

        setContentView(R.layout.act_sticker_search);
        (mSearchView = findViewById(R.id.search_view)).addTextChangedListener(this);
        findViewById(R.id.btn_back).setOnClickListener(this);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment, mStickersSearchFragment = StickersSearchFragment.instantiate()).commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSearchView.setText(getIntent().getStringExtra("q"));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_back) finish();
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        mStickersSearchFragment.search(s.toString());
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //Do Nothing
    }


    @Override
    public void afterTextChanged(Editable s) {
        //Do Nothing
    }
}
