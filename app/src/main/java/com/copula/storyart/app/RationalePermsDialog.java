package com.copula.storyart.app;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.copula.storyart.R;
import com.storyart.support.widget.PermissionHelper;
import com.storyart.support.widget.IResultCallback;

/**
 * Created by heeleaz on 9/12/16.
 */
public class RationalePermsDialog extends DialogFragment implements View.OnClickListener {
    public static final int REQUEST_CODE = 0xFAB;

    private View btnLaunch, btnCancel;
    private TextView txtTitle, txtMessage;
    private ImageView imgPermissionImage;
    private String permission;

    public static RationalePermsDialog launch(FragmentManager fm, String p) {
        FragmentTransaction ft = fm.beginTransaction();
        Fragment oldFragment = fm.findFragmentByTag("PERMISSION");
        if (oldFragment != null) ft.remove(oldFragment);

        RationalePermsDialog dialog = new RationalePermsDialog();
        Bundle bundle = new Bundle();
        bundle.putString("permission", p);
        dialog.setArguments(bundle);

        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        ft.add(dialog, "PERMISSION").show(dialog).commit();

        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup c, Bundle savedInstance) {
        View v = inflater.inflate(R.layout.frg_rationale_perms, c, false);
        txtMessage = v.findViewById(R.id.txt_message);
        txtTitle = v.findViewById(R.id.txt_title);
        imgPermissionImage = v.findViewById(R.id.img_permission_image);

        (btnLaunch = v.findViewById(R.id.btn_launch_perm)).setOnClickListener(this);
        (btnCancel = v.findViewById(R.id.btn_cancel)).setOnClickListener(this);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        String msgTitle = getString(R.string.tit_perm_rationale_generic);
        int messageResId = 0, imageResId = 0;
        switch (permission = getArguments().getString("permission")) {
            case Manifest.permission.RECORD_AUDIO:
                msgTitle = String.format(msgTitle, "Microphone");
                messageResId = R.string.msg_rationale_perm_microphone;
                imageResId = R.drawable.img_mic_permission_h240px;
                break;
            case Manifest.permission.READ_EXTERNAL_STORAGE:
            case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                msgTitle = String.format(msgTitle, "Storage");
                messageResId = R.string.msg_perm_rational_storage;
                imageResId = R.drawable.img_storage_permission_h240px;
                break;
            case Manifest.permission.CAMERA:
                msgTitle = String.format(msgTitle, "Camera");
                messageResId = R.string.msg_perm_rational_camera;
                imageResId = R.drawable.img_storage_permission_h240px;
                break;
        }

        txtTitle.setText(msgTitle);
        if (messageResId != 0) txtMessage.setText(messageResId);
        imgPermissionImage.setImageResource(imageResId);
    }//end

    @Override
    public void onResume() {
        super.onResume();
        if (PermissionHelper.isPermitted(getActivity(), permission)) dismiss();
    }

    private void resolvePermissionActivatePage() {
        String action;
        switch (permission) {
            case Manifest.permission.WRITE_EXTERNAL_STORAGE:
            case Manifest.permission.RECORD_AUDIO:
                action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS;
                break;
            default:
                action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS;
        }

        Intent intent = new Intent(action);
        intent.setData(Uri.parse("package:" + getActivity().getPackageName()));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getActivity().startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        if (v == btnCancel) dismiss();
        else if (v == btnLaunch) resolvePermissionActivatePage();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (getActivity() instanceof IResultCallback) {
            boolean permitted = PermissionHelper.isPermitted(getActivity(), permission);
            int resultCode = permitted ? IResultCallback.RESULT_OK : IResultCallback.RESULT_CANCEL;
            ((IResultCallback) getActivity()).onFragmentResult(REQUEST_CODE, resultCode, null);
        }
        super.onDismiss(dialog);
    }
}
