package com.copula.storyart.app.creator.graphicbackground.colorbackground;

import com.copula.storyart.design.MediaBackgroundProperty;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by heeleaz on 1/1/17.
 */
public class ColorBackgroundPacket implements Iterator<MediaBackgroundProperty> {
    private static final String[] colorList = {
            "#26c4dc", "#57c9ff", "#74676a", "#7e90a3", "#5696ff", "#6e257e", "#7acba5", "#243640",
            "#8294ca", "#a62c71", "#90a841", "#a62c71", "#c1a03f", "#792138", "#ae8774", "#f0b330",
            "#b6b327", "#c69fcc", "#8b6990", "#ff8a8c", "#54c265", "#ff7b6b", "#e56954", "#e1b763",
            "#e3d9a9", "#76a08d", "#05373a", "#91a171", "#b76057", "#728987", "#dfc17e", "#8f8681",
            "#006a60", "#307a6a", "#28999d", "#000000"};

    private static ColorBackgroundPacket singleton = new ColorBackgroundPacket();
    private int colorIndex = -1;

    private ColorBackgroundPacket() {
    }

    public static ColorBackgroundPacket getInstance() {
        return singleton;
    }

    public List<MediaBackgroundProperty> getColorList() {
        List<MediaBackgroundProperty> list = new ArrayList<>(colorList.length);
        for (String color : colorList) {
            MediaBackgroundProperty background = new MediaBackgroundProperty();
            background.backgroundUrl = color;
            background.source = MediaBackgroundProperty.COLOR;
            list.add(background);
        }
        return list;
    }

    @Override
    public boolean hasNext() {
        return true;
    }

    @Override
    public MediaBackgroundProperty next() {
        return get(++colorIndex);
    }

    private MediaBackgroundProperty get(int position) {
        if (position < 0) position = 0;

        MediaBackgroundProperty background = new MediaBackgroundProperty();
        background.backgroundUrl = colorList[(position % colorList.length)];
        background.source = MediaBackgroundProperty.COLOR;
        return background;
    }

    public MediaBackgroundProperty getCurrentBackground() {
        return get(colorIndex);
    }

    public void movePosition(int newPosition) {
        colorIndex = newPosition % colorList.length;
    }
}
