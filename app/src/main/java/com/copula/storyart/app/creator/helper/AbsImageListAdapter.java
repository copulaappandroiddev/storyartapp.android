package com.copula.storyart.app.creator.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.copula.storyart.R;
import com.copula.storyart.app.creator.helper.cache.FileCacheSystem;
import com.copula.storyart.app.creator.helper.glideloader.StickerImageLoader;
import com.storyart.support.widget.BaseAdapter;
import com.storyart.support.widget.SupportViewHelper;

import java.io.InputStream;

/**
 * Created by heeleaz on 1/23/18.
 */
public abstract class AbsImageListAdapter<T> extends BaseAdapter<T> {
    protected static final int SELECT_IMAGE_VIEW_TYPE = 0;

    private int viewId;
    private FileCacheSystem mFileCacheSystem;

    public AbsImageListAdapter(Context context, FileCacheSystem downloader, int viewId) {
        super(context);
        this.viewId = viewId;
        this.mFileCacheSystem = downloader;
    }

    protected abstract String getTitle(int position);

    protected abstract String getImageResource(int position);

    @SuppressWarnings("unchecked")
    @Override
    public View getView(final int position, View convertView, LayoutInflater inflater) {
        final ViewHolder holder;
        int viewType = 0;

        if (convertView == null) {
            holder = new ViewHolder();
            if ((viewType = getItemViewType(position)) == SELECT_IMAGE_VIEW_TYPE) {
                convertView = inflater.inflate(viewId, null, false);
                holder.title = convertView.findViewById(R.id.txt_title);
                holder.image = convertView.findViewById(R.id.img_image);
                holder.contentView = (ViewGroup) convertView;
            } else {
                convertView = getCustomView(position, convertView, inflater);
            }

            convertView.setTag(holder);
        } else holder = (ViewHolder) convertView.getTag();

        if (viewType == SELECT_IMAGE_VIEW_TYPE && holder.title != null) {
            loadTitle(getTitle(position), holder.title);
            loadImage(position, getImageResource(position), holder.image, holder);
        }
        return convertView;
    }

    protected View getCustomView(final int position, View convertView, LayoutInflater inflater) {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return SELECT_IMAGE_VIEW_TYPE;
    }

    private void loadTitle(String title, TextView textView) {
        if (!SupportViewHelper.isStringEmpty(title)) {
            textView.setVisibility(View.VISIBLE);
            textView.setText(title);
        } else textView.setVisibility(View.GONE);
    }

    @SuppressWarnings("unchecked")
    protected boolean loadImage(int p, String url, ImageView v, ViewHolder holder) {
        if (SupportViewHelper.isStringEmpty(url)) return false;

        Class<?> asType;
        if (url.endsWith(".gif")) asType = GifDrawable.class;
        else asType = Bitmap.class;

        Glide.with(getContext()).using(new StickerImageLoader(), InputStream.class)
                .load(new StickerImageLoader.Load(url, mFileCacheSystem)).as(asType)
                .listener(new MyRequestListener(holder)).into(v);
        return true;
    }

    public class ViewHolder {
        public ViewGroup contentView;
        private ImageView image;
        private TextView title;
    }

    private class MyRequestListener implements RequestListener {
        private ViewHolder viewHolder;

        MyRequestListener(ViewHolder viewHolder) {
            this.viewHolder = viewHolder;
            viewHolder.contentView.getChildAt(1).setVisibility(View.VISIBLE);
        }

        @Override
        public boolean onException(Exception e, Object o, Target target, boolean b) {
            viewHolder.contentView.getChildAt(1).setVisibility(View.GONE);
            return false;
        }

        @Override
        public boolean onResourceReady(Object o, Object o2, Target target, boolean b, boolean b1) {
            viewHolder.contentView.getChildAt(1).setVisibility(View.GONE);
            return false;
        }
    }
}
