package com.copula.storyart.app.creator.locationservice;

public class LocationConstant {
    public static final int MIN_REFRESH_TIME = 1000 * 60; //1min
    public static final int MIN_DISTANCE = 100; //100meters
}
