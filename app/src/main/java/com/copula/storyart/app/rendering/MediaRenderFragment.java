package com.copula.storyart.app.rendering;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.copula.storyart.R;
import com.copula.storyart.app.creator.Configuration;
import com.copula.storyart.design.GraphicCreatorModel;
import com.copula.storyart.design.MediaBackgroundProperty;
import com.copula.storyart.design.StoryartDrawingBoard;
import com.copula.storyart.design.StoryartWatermarkHandler;
import com.copula.storyart.media.FileUtil;
import com.copula.storyart.media.ICommand;
import com.copula.storyart.media.MediaContentResolver;
import com.copula.storyart.media.MovieMakerCommand;
import com.storyart.support.util.Formatter;
import com.storyart.support.util.ViewUtils;
import com.storyart.support.widget.Logger;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;

/**
 * Created by heeleaz on 10/31/17.
 */
public class MediaRenderFragment extends DialogFragment implements View.OnClickListener {
    public static final int SAVE_MEDIA_FLAG = 1 << 5;

    private static final Logger logger = Logger.getInstance("MediaRenderFragment");

    private static final String IMAGE_CACHE_PATH = FileUtil.IMAGE_SAVE_PATH;
    private static final String VIDEO_OUT_PATH = FileUtil.MOVIE_SAVE_PATH;

    private GraphicCreatorModel mGraphicCreatorModel;
    private MediaStoreAsync mSaveMediaAsync;

    private ProgressBar progressBarRenderState;
    private TextView txtPercentageProgress;
    private View shareView;

    private MediaRenderCallback mMediaRenderCallback;

    private int launchFlag;

    public static void launch(FragmentManager fragmentManager, String graphic, int flag, MediaRenderCallback callback) {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        Fragment oldFragment = fragmentManager.findFragmentByTag("Render");
        if (oldFragment != null) ft.remove(oldFragment);


        Bundle argument = new Bundle();
        argument.putString("graphicContext", graphic);
        argument.putInt("launchFlag", flag);
        MediaRenderFragment fragment = new MediaRenderFragment();
        fragment.setArguments(argument);
        fragment.mMediaRenderCallback = callback;

        fragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        ft.add(fragment, "Render").show(fragment).commit();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup c, Bundle si) {
        getDialog().setCanceledOnTouchOutside(false);
        View v = inflater.inflate(R.layout.frg_graphic_render, c, false);

        progressBarRenderState = v.findViewById(R.id.progress_bar);
        txtPercentageProgress = v.findViewById(R.id.txt_percentage_progress);
        shareView = v.findViewById(R.id.view_share);

        OnShareClickListener clickListener = new OnShareClickListener();
        v.findViewById(R.id.view_error).setOnClickListener(this);
        v.findViewById(R.id.btn_share_snapchat).setOnClickListener(clickListener);
        v.findViewById(R.id.btn_share_whatsapp).setOnClickListener(clickListener);
        v.findViewById(R.id.btn_share_instagram).setOnClickListener(clickListener);
        v.findViewById(R.id.btn_share_facebook).setOnClickListener(clickListener);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        launchFlag = getArguments().getInt("launchFlag", 0);

        try {
            String graphicContext = getArguments().getString("graphicContext");
            mGraphicCreatorModel = new GraphicCreatorModel(graphicContext);
            beginRender();
        } catch (Exception e) {
            logger.e("Error with rendering script", e);
            dismiss();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mSaveMediaAsync != null && mSaveMediaAsync.getStatus() == AsyncTask.Status.FINISHED) {
            mSaveMediaAsync.dispatchStatusMessage();
            dismiss();
        }
    }

    private void beginRender() {
        if (mGraphicCreatorModel.isInVideoContext()) new RenderMovieTask().execute();
        else new RenderPictureTask().execute();
    }

    private void createImage(int canvasWidth, int canvasHeight, String path) throws IOException {
        StoryartDrawingBoard drawingBoard = new StoryartDrawingBoard();

        String backgroundUrl = mGraphicCreatorModel.getBackgroundProperty().backgroundUrl;
        drawingBoard.setBoardImageFile(new File(backgroundUrl));

        drawingBoard.setWatermarkHandler(new StoryartWatermarkHandler(getActivity()));

        BufferedOutputStream bos = null;
        try {
            bos = new BufferedOutputStream(new FileOutputStream(new File(path)));
            drawingBoard.draw().compress(Bitmap.CompressFormat.PNG, 100, bos);
        } finally {
            try {
                if (bos != null) bos.close();
            } catch (IOException ignored) {
            }
        }
    }

    private Uri getShareExternalMediaUrl() {
        String path = getSaveInternalMediaUrl();
        try {
            return Uri.fromFile(new File(path));
        } catch (Exception e) {
            //TODO: i really do not get to test this.. it may be irrelevant
            return Uri.parse(path);
        }
    }

    private String getSaveInternalMediaUrl() {
        if (mGraphicCreatorModel.isInVideoContext()) return VIDEO_OUT_PATH;
        else return IMAGE_CACHE_PATH;
    }

    private String getContextMimeType() {
        if (mGraphicCreatorModel.isInVideoContext()) return "video/mpeg";
        else return "image/png";
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.view_error) beginRender();
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        MovieMakerCommand.destroyMovieMakingProgress(getActivity());
        super.onDismiss(dialog);
    }

    private void onMediaRenderSuccessful(String message) {
        logger.i("onSuccess: " + message);

        if ((launchFlag & SAVE_MEDIA_FLAG) == SAVE_MEDIA_FLAG) {
            (mSaveMediaAsync = new MediaStoreAsync()).execute();
        } else {
            if (isAdded()) ViewUtils.showOnly(shareView);
        }
    }

    private void onMediaRenderFailed(String message) {
        if (getView() != null) ViewUtils.showOnly(getView().findViewById(R.id.view_error));
        logger.i("MovieMakerCommand failure: " + message);
    }


    private void showIndeterminateProgressBar(String logMessage) {
        if (logMessage != null) logger.i(logMessage);

        txtPercentageProgress.setVisibility(View.GONE);
        progressBarRenderState.setIndeterminate(true);
    }

    private void dispatchOverrideMediaBackgroundProperty() {
//        if (mMediaRenderCallback != null) {
        MediaBackgroundProperty b = mMediaRenderCallback.overrideBackgroundProperty();
        if (b != null) mGraphicCreatorModel.setBackgroundProperty(b);
//        }
    }

    public interface MediaRenderCallback {
        MediaBackgroundProperty overrideBackgroundProperty();
    }

    private class MediaStoreAsync extends AsyncTask<Void, Void, File> {
        @Override
        protected void onPreExecute() {
            showIndeterminateProgressBar("Saving Media to Public Storyart");
        }

        @Override
        protected File doInBackground(Void... voids) {
            String ext = mGraphicCreatorModel.isInVideoContext() ? ".mp4" : ".png";
            String calenderName = Formatter.getCalenderDateInString("Storyart", ext);

            File source = new File(getSaveInternalMediaUrl());
            File dest = new File(FileUtil.getMediaSavePath(calenderName));
            FileUtil.copy(source, dest);

            return dest;
        }

        @Override
        protected void onPostExecute(File file) {
            if (!isAdded() || getActivity() == null) return;

            MediaContentResolver.updateContentProvider(getActivity(), new String[]{file.getPath()});
            dispatchStatusMessage();
            if (isResumed()) dismiss();
        }

        private void dispatchStatusMessage() {
            String msg = getString(R.string.msg_save_success);
            if (mGraphicCreatorModel.isAudioAttached()) msg = String.format(msg, "Video");
            else msg = String.format(msg, "Image");

            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
        }
    }//END

    private class RenderMovieTask extends
            AsyncTask<Void, Void, Boolean> implements ICommand.ICommandListener<MovieMakerCommand> {
        @Override
        public void onProgress(@NonNull MovieMakerCommand movieMakerCommand, String message) {
            logger.i("onProgress: " + message);

            int rendered = movieMakerCommand.getRenderedTimeInSecs(message);
            if (rendered <= 0) {
                showIndeterminateProgressBar(null);
                return;
            }

            txtPercentageProgress.setVisibility(View.VISIBLE);
            progressBarRenderState.setIndeterminate(false);

            int duration = movieMakerCommand.getDuration();
            int percentage = Math.min((int) ((rendered / (duration * 1f)) * 100), 100);
            progressBarRenderState.setProgress(percentage);
            txtPercentageProgress.setText(
                    String.format(Locale.getDefault(), "%d%s", percentage, "%"));
        }

        @Override
        public void onSuccessful(@NonNull MovieMakerCommand movieMakerCommand, String message) {
            onMediaRenderSuccessful(message);
        }

        @Override
        public void onFailure(@NonNull MovieMakerCommand movieMakerCommand, String message) {
            onMediaRenderFailed("MovieMakerCommand failure: " + message);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                createImage(Configuration.MOVIE_WIDTH, Configuration.MOVIE_HEIGHT, IMAGE_CACHE_PATH);
                return true;
            } catch (Exception e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            MovieMakerCommand movieMakerCommand = new MovieMakerCommand(getContext());
            if (mGraphicCreatorModel.isAudioAttached()) {
                movieMakerCommand.setAudioProperty(mGraphicCreatorModel.getAudioAssets());
            }

            if (mGraphicCreatorModel.isGIFsAttached()) {
                movieMakerCommand.setGifsAssetsList(mGraphicCreatorModel.getGIFImages());
            }

            movieMakerCommand.setImageProperty(new File(IMAGE_CACHE_PATH));
            movieMakerCommand.setOutPath(new File(VIDEO_OUT_PATH));
            movieMakerCommand.execute(this);
        }
    }

    private class RenderPictureTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            showIndeterminateProgressBar("Image Rendering Processing");
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                dispatchOverrideMediaBackgroundProperty();
                createImage(Configuration.DISPLAY_WIDTH, Configuration.DISPLAY_HEIGHT, IMAGE_CACHE_PATH);
                return true;
            } catch (Exception e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            onMediaRenderSuccessful("Image Rendering Completed");
        }
    }

    private class OnShareClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            String mimeType = getContextMimeType();
            Uri mediaUri = getShareExternalMediaUrl();
            String fallback = getString(R.string.msg_x_not_installed);

            if (v.getId() == R.id.btn_share_whatsapp) {
                try {
                    shareThrowIfNotFound("com.whatsapp", mimeType, mediaUri);
                } catch (ActivityNotFoundException e) {
                    share("com.whatsapp.w4b", mimeType, mediaUri, String.format(fallback, "WhatsApp"));
                }
            } else if (v.getId() == R.id.btn_share_instagram) {
                share("com.instagram.android", mimeType, mediaUri, String.format(fallback, "Instagram"));
            } else if (v.getId() == R.id.btn_share_facebook) {
                share("com.facebook.katana", mimeType, mediaUri, String.format(fallback, "Facebook"));
            } else if (v.getId() == R.id.btn_share_snapchat) {
                share("com.snapchat.android", mimeType, mediaUri, String.format(fallback, "Snapchat"));
            }
        }

        private void share(String toPackage, String mime, Uri uri, String fallback) {
            try {
                shareThrowIfNotFound(toPackage, mime, uri);
            } catch (ActivityNotFoundException ex) {
                Toast.makeText(getActivity(), fallback, Toast.LENGTH_SHORT).show();
            }
        }

        private void shareThrowIfNotFound(String toPackage, String mime, Uri uri) throws ActivityNotFoundException {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setPackage(toPackage).putExtra(Intent.EXTRA_STREAM, uri);
            intent.setType(mime);

            getActivity().startActivityForResult(intent, 12345);
        }
    }//END
}