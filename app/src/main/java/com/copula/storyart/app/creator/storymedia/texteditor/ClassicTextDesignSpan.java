package com.copula.storyart.app.creator.storymedia.texteditor;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.ColorInt;
import android.text.Layout;
import android.text.style.LeadingMarginSpan;
import android.text.style.LineBackgroundSpan;
import android.text.style.TextAppearanceSpan;

public class ClassicTextDesignSpan extends TextAppearanceSpan implements LeadingMarginSpan, LineBackgroundSpan {
    private int mPadding;
    private RectF mBgRect;

    private Layout mLayout;

    private int mBackgroundColor;
    private int mTextColor = 0;

    private int mColor = 0xFFFFFFF;

    ClassicTextDesignSpan(Context context, int padding) {
        super(context, 0);

        mPadding = padding;
        mBgRect = new RectF();
    }

    private void setBackgroundColor(int backgroundColor) {
        this.mBackgroundColor = backgroundColor;
    }

    @Override
    public int getLeadingMargin(boolean first) {
        return 0;
    }

    @Override
    public void drawLeadingMargin(Canvas c, Paint p, int x, int dir, int top, int baseline, int bottom, CharSequence text, int start, int end, boolean first, Layout layout) {
        this.mLayout = layout;
    }

    @Override
    public void drawBackground(Canvas c, Paint p, int left, int right, int top, int baseline, int bottom, CharSequence text, int start, int end, int lnum) {
        if (mLayout == null) return;

        final int textWidth = Math.round(p.measureText(text, start, end));
        final int paintColor = p.getColor();
        // Draw the background

        float _left = mLayout.getLineLeft(lnum);

        mBgRect.set(_left - mPadding, top - (mPadding / 2), _left + textWidth + mPadding, bottom + mPadding / 2);
        p.setColor(mBackgroundColor);


        float rx = 5;
        float ry = lnum == mLayout.getLineCount() - 1 ? 5 : 5;
        c.drawRoundRect(mBgRect, rx, ry, p);
        p.setColor(paintColor);
    }

    public void setColor(@ColorInt int color) {
        setBackgroundColor(color);

        if (luminance(color) >= 0.80) {//check if is light color
            mTextColor = 0xFF000000;
        } else {
            mTextColor = 0xFFFFFFFF;
        }
        mColor = color;
    }

    public void setColor(@ColorInt int color, float alpha) {
        float r = (float) Color.red(color) / 255;
        float g = (float) Color.green(color) / 255;
        float b = (float) Color.blue(color) / 255;
        setColor(Color.valueOf(r, g, b, alpha).toArgb());
    }

    private static float luminance(int color) {
        double r = Color.red(color) / 255.0;
        double g = Color.green(color) / 255.0;
        double b = Color.blue(color) / 255.0;


        return (float) ((0.2126 * r) + (0.7152 * g) + (0.0722 * b));
    }

    @Override
    public ColorStateList getTextColor() {
        return ColorStateList.valueOf(mTextColor);
    }

    public int getColor() {
        return mColor;
    }
}
