package com.copula.storyart.app.creator.storymedia.texteditor;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.TextView;
import com.copula.storyart.app.creator.graphicbackground.colorbackground.ColorBackgroundFragment;
import com.copula.storyart.design.MediaBackgroundProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 2/2/18.
 */
public class EditorTextColorFragment extends ColorBackgroundFragment implements AbsListView.OnScrollListener {
    private static String[] colorList = {
            "#FFFFFF", "#000000", "#3897f0", "#70c050", "#fdcb5c", "#fd8d32", "#ed4956", "#a307ba",
            "#ed0013", "#ed858e", "#ffd2d3", "#ffdbb4", "#ffc382", "#d28f46", "#996439", "#432324",
            "#1c4a29", "#262626", "#363636", "#555555", "#737373", "#999999", "#b2b2b2", "#c7c7c7",
            "#dbdbdb", "#efefef"};

    private TextView mTextView;

    public static EditorTextColorFragment instantiate() {
        return new EditorTextColorFragment();
    }

    public void setEditorTextView(TextView textView) {
        this.mTextView = textView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getGridView().setOnScrollListener(this);
    }

    @Override
    protected void populateColorList(List<MediaBackgroundProperty> l) {
        List<MediaBackgroundProperty> list = new ArrayList<>(colorList.length);
        for (String color : colorList) {
            MediaBackgroundProperty background = new MediaBackgroundProperty();
            background.backgroundUrl = color;
            background.source = MediaBackgroundProperty.COLOR;
            list.add(background);
        }

        super.populateColorList(list);
    }

    private static float luminance(int color) {
        double r = Color.red(color) / 255.0;
        double g = Color.green(color) / 255.0;
        double b = Color.blue(color) / 255.0;


        return (float) ((0.2126 * r) + (0.7152 * g) + (0.0722 * b));
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        MediaBackgroundProperty background = (MediaBackgroundProperty) adapterView.getItemAtPosition(position);
        if (mTextView != null) {
            mTextView.setTextColor(Color.parseColor(background.backgroundUrl));
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (mTextView != null) {
            mTextView.requestFocus();
        }
    }
}
