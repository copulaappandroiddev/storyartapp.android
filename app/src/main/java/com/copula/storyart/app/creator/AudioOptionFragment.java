package com.copula.storyart.app.creator;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.copula.storyart.R;
import com.copula.storyart.app.AudioRecorderActivity;
import com.copula.storyart.media.audiogallery.AudioChooserActivity;
import com.storyart.support.util.ViewUtils;

/**
 * Created by heeleaz on 10/22/17.
 */
public class AudioOptionFragment extends DialogFragment implements View.OnClickListener {
    public final static int REQUEST_AUDIO_GALLERY = 0x1A35;
    public final static int REQUEST_MIC_RECORD = 0x1A36;

    public static void show(FragmentManager fragmentManager) {
        FragmentTransaction ft = fragmentManager.beginTransaction();

        Fragment oldFragment = fragmentManager.findFragmentByTag("AudioOptionFragment");
        if (oldFragment != null) ft.remove(oldFragment);

        AudioOptionFragment fragment = new AudioOptionFragment();
        fragment.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogTheme);
        ft.add(fragment, "AudioOptionFragment").show(fragment).commit();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle si) {
        ViewUtils.showDialogAsToastPop(getDialog(), 60);

        View v = inflater.inflate(R.layout.frg_audio_option, container, false);
        v.findViewById(R.id.btn_select_file).setOnClickListener(this);
        v.findViewById(R.id.btn_select_record).setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_select_file) {
            AudioChooserActivity.launch(getActivity(), REQUEST_AUDIO_GALLERY);
        } else if (v.getId() == R.id.btn_select_record) {
            AudioRecorderActivity.launch(getActivity(), REQUEST_MIC_RECORD);
        }
        dismiss();
    }
}

