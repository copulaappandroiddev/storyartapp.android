package com.copula.storyart.app.creator.helper.restful;

import android.os.AsyncTask;
import com.storyart.support.widget.Logger;
import com.storyart.support.webservice.RestResponse;

/**
 * Created by heeleaz on 12/12/17.
 */
public abstract class AbsRestfulApi<T> extends AsyncTask<Void, Void, RestResponse<T>> {
    private static Logger logger = Logger.getInstance("AbsRestfulApi");
    protected Listener mListener;

    private String page;
    private String nextPageToken;

    public AbsRestfulApi(String page) {
        this.page = page;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        logger.i("Preparing data fetch from server..");
        if (mListener != null) mListener.onStickerFetchStarted();
    }

    @Override
    protected RestResponse<T> doInBackground(Void... voids) {
        logger.i("Fetching data from server..");

        try {
            return get(page);
        } catch (Exception e) {
            logger.e("error getting response from server", e);
            return null;
        }
    }

    @Override
    protected void onPostExecute(RestResponse<T> restResponse) {
        super.onPostExecute(restResponse);
        logger.i("Data fetch completed");

        if (restResponse == null || restResponse.statusCode != 1) {
            mListener.onDataFetchFailed("Failed to fetch rest response");
        } else {
            this.nextPageToken = restResponse.nextPageToken;
            mListener.onDataFetchedSuccess(restResponse.result, restResponse.nextPageToken);
        }
    }

    public boolean isExecuting() {
        Status status = getStatus();
        return status == Status.PENDING || status == Status.RUNNING;
    }

    protected abstract RestResponse<T> get(String page) throws Exception;

    public String getNextPageToken() {
        return nextPageToken;
    }

    public void execute(Listener listener) {
        this.mListener = listener;
        execute();
    }

    public interface Listener<T> {
        void onStickerFetchStarted();

        void onDataFetchedSuccess(T models, String pageModel);

        void onDataFetchFailed(String message);
    }
}
