package com.copula.storyart.app;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import com.copula.storyart.R;
import com.copula.storyart.app.creator.graphicbackground.colorbackground.ColorBackgroundPacket;
import com.copula.storyart.design.MediaBackgroundProperty;

/**
 * Created by heeleaz on 11/7/17.
 */
public class AboutPageActivity extends FragmentActivity implements View.OnClickListener {
    public static void launch(Context context) {
        context.startActivity(new Intent(context, AboutPageActivity.class));
    }

    private static void launchWebPage(Context context, String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.setData(Uri.parse(url));
        context.startActivity(i);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.transition.fade_in, R.transition.fade_out);
        setContentView(R.layout.act_about_page);

        MediaBackgroundProperty background = ColorBackgroundPacket.getInstance().getCurrentBackground();
        findViewById(R.id.view_parent).setBackground(background.asColor());

        findViewById(R.id.btn_follow_facebook).setOnClickListener(this);
        findViewById(R.id.btn_follow_instagram).setOnClickListener(this);
        findViewById(R.id.btn_follow_twitter).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_follow_facebook) {
            launchWebPage(this, "https://www.facebook.com/storyartapp/");
        } else if (v.getId() == R.id.btn_follow_instagram) {
            launchWebPage(this, "https://www.instagram.com/storyartapp/");
        } else if (v.getId() == R.id.btn_follow_twitter) {
            launchWebPage(this, "https://www.twitter.com/storyartapp/");
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.transition.fade_in, R.transition.fade_out);
    }
}
