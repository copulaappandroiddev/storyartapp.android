package com.copula.storyart.app.creator.stickerimage.search;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.copula.storyart.R;
import com.copula.storyart.app.creator.Configuration;
import com.copula.storyart.app.creator.addonimages.recentaddonimages.data.RecentAddonDao;
import com.copula.storyart.app.creator.addonimages.recentaddonimages.data.RecentAddonEntity;
import com.copula.storyart.app.creator.helper.AbsImageListAdapter;
import com.copula.storyart.app.creator.helper.AbsImageListFragment;
import com.copula.storyart.app.creator.helper.cache.FileCacheSystem;
import com.copula.storyart.app.creator.helper.restful.AbsImageResourceApi;
import com.copula.storyart.app.creator.helper.restful.AbsRestfulApi;
import com.copula.storyart.design.GraphicImageProperty;
import com.storyart.support.util.ViewUtils;

import java.util.List;

/**
 * Created by heeleaz on 2/11/18.
 */
public class StickersSearchFragment extends AbsImageListFragment implements AbsRestfulApi.Listener<List<AbsImageResourceApi.ApiModel>> {
    private FileCacheSystem mFileCacheSystem;

    private SearchRestfulApi mRestfulApi;
    private String searchQuery;
    private MyAdapter mAdapter;

    private View viewSearchContainer, viewSearchProgress;
    private TextView txtSearchNotFound, txtSearchProgressMessage;

    public static StickersSearchFragment instantiate() {
        return new StickersSearchFragment();
    }

    @Override
    protected View onRequestFragmentView(LayoutInflater inflater, ViewGroup c) {
        View v = inflater.inflate(R.layout.frg_stickers_search, c, false);
        txtSearchNotFound = v.findViewById(R.id.txt_search_not_found);
        viewSearchProgress = v.findViewById(R.id.view_search_progress);
        viewSearchContainer = v.findViewById(R.id.view_search_container);
        txtSearchProgressMessage = v.findViewById(R.id.txt_search_progress_message);

        (mFileCacheSystem = new FileCacheSystem(Configuration.Stickers.SEARCH_IMAGE_CACHE_DIR,
                Configuration.Stickers.SEARCH_IMAGE_CACHE_SIZE)).openIgnoreError();

        return v;
    }

    @Override
    protected BaseAdapter getBaseAdapter(Context context) {
        return mAdapter = new MyAdapter(context);
    }

    @Override
    protected int getLoadingTriggerThreshold() {
        return 20;
    }

    @Override
    public void onLoadMore() {
        String page = mRestfulApi != null ? mRestfulApi.getNextPageToken() : "0,20";
        mRestfulApi = executeSearchQuery(searchQuery, page);
    }

    private SearchRestfulApi executeSearchQuery(String query, String page) {
        this.searchQuery = query;
        SearchRestfulApi api = new SearchRestfulApi(getContext(), query, page);
        api.execute(this);

        return api;
    }

    public void search(String query) {
        if (mRestfulApi != null && mRestfulApi.getStatus() != AsyncTask.Status.FINISHED) {
            mRestfulApi.cancel(true);
        }

        if (!TextUtils.isEmpty(query)) {
            mAdapter.clear();//remove any previous item set. like starting over again
            mAdapter.notifyDataSetChanged();
            executeSearchQuery(query, "0,20");
        }
    }

    @Override
    public boolean hasLoadedAllItems() {
        return mRestfulApi != null && mRestfulApi.getNextPageToken() == null;
    }

    @Override
    public void onStickerFetchStarted() {
        if (!isFragmentInForeground()) return;

        if (mAdapter.isEmpty()) {
            ViewUtils.showOnly(viewSearchProgress);
            String message = getString(R.string.prg_search_message);
            txtSearchProgressMessage.setText(String.format(message, searchQuery));
        } else super.invalidateDataFetchStarted();
    }

    @Override
    public void onDataFetchedSuccess(List<AbsImageResourceApi.ApiModel> models, String pageModel) {
        if (!isFragmentInForeground()) return;

        if (pageModel != null && pageModel.startsWith("0,")) mAdapter.clear();

        mAdapter.addOrReplaceHandlers(models);
        mAdapter.notifyDataSetChanged();

        if (!mAdapter.isEmpty()) {
            super.invalidateDataFetchSuccessful(null);
            ViewUtils.showOnly(viewSearchContainer);
        } else {
            String searchMessage = getString(R.string.msg_search_not_found);
            txtSearchNotFound.setText(String.format(searchMessage, searchQuery));
            ViewUtils.showOnly(txtSearchNotFound);
        }
    }

    @Override
    public void onDataFetchFailed(String message) {
        if (!isFragmentInForeground()) return;

        txtSearchNotFound.setText(R.string.msg_sticker_search_failed_er001);
        ViewUtils.showOnly(txtSearchNotFound);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        AbsImageResourceApi.ApiModel m = (AbsImageResourceApi.ApiModel) parent.getItemAtPosition(position);
        String file = mFileCacheSystem.getCacheFile(m.resourceUrl);
        if (file == null) return;

        //add to recent addon image database
        RecentAddonEntity entity = new RecentAddonEntity(m.title, file, GraphicImageProperty.BITMAP_IMAGE);
        RecentAddonDao.getInstance(getContext()).insertAsync(entity);

        Intent intent = new Intent().putExtra("data", file);
        Activity activity = getActivity();
        activity.setResult(Activity.RESULT_OK, intent);
        activity.finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mFileCacheSystem != null) mFileCacheSystem.release();
    }

    public class MyAdapter extends AbsImageListAdapter<AbsImageResourceApi.ApiModel> {
        MyAdapter(Context context) {
            super(context, mFileCacheSystem, R.layout.adp_sticker_image);
        }

        @Override
        protected String getTitle(int position) {
            return getItem(position).title;
        }

        @Override
        protected String getImageResource(int position) {
            return getItem(position).resourceUrl;
        }
    }//END
}
