package com.copula.storyart.app.creator.storymedia.gallery.camera;

import android.app.Activity;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.view.Surface;
import android.view.TextureView;
import com.storyart.support.widget.Logger;

import java.util.List;

/**
 * A basic Camera preview class
 */
public class CameraTextureView extends TextureView implements TextureView.SurfaceTextureListener {
    private static final Logger logger = Logger.getInstance("CameraTextureView");

    private Camera mCamera;
    private int cameraId;
    private int cameraDisplayOrientation;
    private SurfaceTextureListener mCallback;

    public CameraTextureView(Context context) {
        super(context);
    }

    public CameraTextureView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CameraTextureView(Context context, AttributeSet attrs, int attrStyle) {
        super(context, attrs, attrStyle);
    }

    public CameraTextureView(Activity context, Camera camera, int cameraId) {
        super(context);

        this.mCamera = camera;
        this.cameraId = cameraId;

        setSurfaceTextureListener(this);
        cameraDisplayOrientation = getCameraDisplayOrientation(context, cameraId);
    }

    public void addCallback(SurfaceTextureListener callback) {
        this.mCallback = callback;
    }

    public int getCameraDisplayOrientation() {
        return cameraDisplayOrientation;
    }

    private int getCameraDisplayOrientation(Activity activity, int cameraId) {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();

        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }

        return result;
    }

    public boolean isCameraFacingFront() {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);

        return info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT;
    }

    public int getCameraOrientation() {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);

        return info.orientation;
    }

    private Camera.Size[] getSupportedCameraSizes() {
        List<Camera.Size> pictureSizeList
                = mCamera.getParameters().getSupportedPictureSizes();
        return pictureSizeList.toArray(new Camera.Size[pictureSizeList.size()]);
    }

    public Camera.Size getCameraPictureSize(float aspectRatio, float scale) {
        List<Camera.Size> pictureSizeList = mCamera.getParameters().getSupportedPictureSizes();
        Camera.Size bestSize = null;
        for (Camera.Size size : pictureSizeList) {
            if ((float) size.width / (float) size.height == aspectRatio) {
                if (bestSize != null) {
                    if (bestSize.width * bestSize.height > size.width * size.height) {
                        bestSize = size;
                    }
                } else bestSize = size;
            }

        }

        return bestSize;
    }

    public Camera.Size getCameraPreviewSize(float aspectRatio, float scale) {
        List<Camera.Size> sizeList = mCamera.getParameters().getSupportedPreviewSizes();
        Camera.Size bestSize = null;

        for (int i = 1; i < sizeList.size(); i++) {
            Camera.Size size = sizeList.get(i);
            if ((float) size.width / (float) size.height == aspectRatio) {
                if (bestSize != null) {
                    if (bestSize.width * bestSize.height < size.width * size.height) {
                        bestSize = size;
                    }
                } else bestSize = size;
            }

        }

        return bestSize;
    }

    public Camera getCamera() {
        return mCamera;
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        try {
            mCamera.setDisplayOrientation(cameraDisplayOrientation);
            mCamera.setPreviewTexture(surface);
            mCamera.startPreview();
        } catch (Exception e) {
            logger.e("Error setting camera preview", e);
        }

        if (mCallback != null) mCallback.onSurfaceTextureAvailable(surface, width, height);
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        return (mCallback != null) && mCallback.onSurfaceTextureDestroyed(surface);
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        if (mCallback != null) mCallback.onSurfaceTextureUpdated(surface);
    }
}