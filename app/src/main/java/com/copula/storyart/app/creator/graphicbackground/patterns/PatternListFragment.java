package com.copula.storyart.app.creator.graphicbackground.patterns;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import com.copula.storyart.R;
import com.copula.storyart.app.creator.Configuration;
import com.copula.storyart.app.creator.helper.AbsImageListAdapter;
import com.copula.storyart.app.creator.helper.AbsImageListFragment;
import com.copula.storyart.app.creator.helper.cache.FileCacheSystem;
import com.copula.storyart.app.creator.helper.restful.AbsImageResourceApi;
import com.copula.storyart.app.creator.helper.restful.AbsRestfulApi;
import com.copula.storyart.design.MediaBackgroundProperty;

import java.util.List;

/**
 * Created by heeleaz on 1/27/18.
 */
public class PatternListFragment extends AbsImageListFragment implements AbsRestfulApi.Listener<List<AbsImageResourceApi.ApiModel>> {
    private FileCacheSystem mFileCacheSystem;

    private MyAdapter mAdapter;
    private RestfulApi mRestfulApi;

    private String category;

    public static PatternListFragment instantiate(String category) {
        PatternListFragment fragment = new PatternListFragment();
        fragment.category = category;
        return fragment;
    }


    @Override
    protected View onRequestFragmentView(LayoutInflater inflater, ViewGroup c) {
        View view = inflater.inflate(R.layout.background_list_fragment, c, false);
        (mFileCacheSystem = new FileCacheSystem(Configuration.Stickers.BACKGROUNDS_CACHE_DIR,
                Configuration.Stickers.BACKGROUNDS_CACHE_SIZE)).openIgnoreError();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        onLoadMore();
    }

    @Override
    protected BaseAdapter getBaseAdapter(Context context) {
        return mAdapter = new MyAdapter(context);
    }

    @Override
    protected int getLoadingTriggerThreshold() {
        return 30;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        RestfulApi.ApiModel m = (RestfulApi.ApiModel) parent.getItemAtPosition(position);
        if (!mFileCacheSystem.isCached(m.placeholderUrl)) return;

        MediaBackgroundProperty background = new MediaBackgroundProperty();
        background.source = MediaBackgroundProperty.WALLPAPER;
        background.placeholderUrl = mFileCacheSystem.getCacheFile(m.placeholderUrl);
        background.backgroundUrl = m.resourceUrl;

        Intent intent = new Intent().putExtra("background", background);
        Activity activity = getActivity();
        activity.setResult(Activity.RESULT_OK, intent);
        activity.finish();
    }

    @Override
    public void onLoadMore() {
        String nextPage = mRestfulApi != null ? mRestfulApi.getNextPageToken() : "0,30";
        (mRestfulApi = new RestfulApi(getContext(), category, nextPage)).execute(this);
    }

    @Override
    public boolean hasLoadedAllItems() {
        return mRestfulApi != null && mRestfulApi.getNextPageToken() == null;
    }

    @Override
    public void onStickerFetchStarted() {
        super.invalidateDataFetchStarted();
    }

    @Override
    public void onDataFetchedSuccess(List<AbsImageResourceApi.ApiModel> models, String pageModel) {
        if (!isFragmentInForeground()) return;

        mAdapter.addHandlers(models);
        mAdapter.notifyDataSetChanged();

        super.invalidateDataFetchSuccessful(null);
    }

    @Override
    public void onDataFetchFailed(String message) {
        if (!isFragmentInForeground()) return;

        super.invalidateDataFetchFailed(getString(R.string.msg_background_load_failed));
    }

    @Override
    public void onDestroy() {
        if (mFileCacheSystem != null) mFileCacheSystem.release();
        super.onDestroy();
    }

    public class RestfulApi extends AbsImageResourceApi {
        RestfulApi(Context context, String category, String page) {
            super(context, category, page);
        }

        @Override
        protected String getUrl() {
            return "/background/get_resource";
        }
    }

    public class MyAdapter extends AbsImageListAdapter<RestfulApi.ApiModel> {
        MyAdapter(Context context) {
            super(context, mFileCacheSystem, R.layout.adp_background_image_list);
        }

        @Override
        protected String getTitle(int position) {
            return getItem(position).title;
        }

        @Override
        protected String getImageResource(int position) {
            return getItem(position).placeholderUrl;
        }
    }//END
}
