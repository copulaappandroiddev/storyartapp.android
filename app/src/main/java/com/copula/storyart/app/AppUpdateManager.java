package com.copula.storyart.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;

public class AppUpdateManager {
    private Context mContext;

    public AppUpdateManager(Context context) {
        this.mContext = context;
    }

    private static String getAppVersionName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    public String compileAppCurrentUpdateNotificationName() {
        return "updatesV" + getAppVersionName(mContext);
    }

    public String getAppPrevUpdateNotificationName() {
        //NOTE: defValue is set to updatesV14. this is truly
        //the previous update version in this context

        SharedPreferences p = mContext.getSharedPreferences("app_info", Context.MODE_PRIVATE);
        return p.getString("update_notification_name", "updatesV3.0.0603");

    }

    public void saveAppCurrentUpdateNotificationName(String name) {
        SharedPreferences p = mContext.getSharedPreferences("app_info", Context.MODE_PRIVATE);
        p.edit().putString("update_notification_name", name).apply();
    }
}
