package com.copula.storyart.app.creator.storymedia;

public interface MediaSourceCallback {
    int SOURCE_GALLERY = 0, SOURCE_CAMERA = 1;

    void requestSource(int request);

    void onMediaAvailable(String mediaUrl);
}
