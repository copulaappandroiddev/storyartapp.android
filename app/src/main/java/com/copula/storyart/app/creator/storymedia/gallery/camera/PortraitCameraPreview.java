package com.copula.storyart.app.creator.storymedia.gallery.camera;

import android.app.Activity;
import android.content.Context;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.storyart.support.widget.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * A basic Camera preview class
 */
public class PortraitCameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private static final Logger logger = Logger.getInstance("CameraPreview");

    private Activity activity;

    private SurfaceHolder mHolder;
    private Camera mCamera;
    private int cameraId;
    private int cameraDisplayOrientation;
    private SurfaceHolder.Callback mCallback;

    public PortraitCameraPreview(Context context) {
        super(context);
    }

    public PortraitCameraPreview(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PortraitCameraPreview(Context context, AttributeSet attrs, int attrStyle) {
        super(context, attrs, attrStyle);
    }

    public PortraitCameraPreview(Activity context, Camera camera, int cameraId) {
        super(context);
        this.activity = context;

        this.mCamera = camera;
        this.cameraId = cameraId;

        (mHolder = getHolder()).addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        cameraDisplayOrientation = getCameraDisplayOrientation(activity, cameraId);
    }

    public void addCallback(SurfaceHolder.Callback callback) {
        this.mCallback = callback;
    }

    public void surfaceCreated(SurfaceHolder holder) {
        try {
            mCamera.setDisplayOrientation(cameraDisplayOrientation);
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (Exception e) {
            logger.e("Error setting camera preview", e);
        }

        if (mCallback != null) mCallback.surfaceCreated(holder);
    }

    public int getCameraDisplayOrientation() {
        return cameraDisplayOrientation;
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        if (mCallback != null) mCallback.surfaceDestroyed(holder);
        // empty. Take care of releasing the Camera preview in your activity.
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        if (mCallback != null) mCallback.surfaceChanged(holder, format, w, h);
    }

    private int getCameraDisplayOrientation(Activity activity, int cameraId) {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay()
                .getRotation();

        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }

        return result;
    }

    public boolean isCameraFacingFront() {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);

        return info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT;
    }

    public int getCameraOrientation() {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);

        return info.orientation;
    }

    private Camera.Size[] getSupportedCameraSizes() {
        List<Camera.Size> pictureSizeList
                = mCamera.getParameters().getSupportedPictureSizes();
        return pictureSizeList.toArray(new Camera.Size[pictureSizeList.size()]);
    }

    public Camera.Size getBestPictureSize(int cameraOrientation) {
        int orientation = 1;
        int targetHeight = getResources().getDisplayMetrics().heightPixels;
        int targetWidth = getResources().getDisplayMetrics().widthPixels;

        if (cameraOrientation == 90 || cameraOrientation == 270) {
            orientation = -1; //horizontal camera
            // invert(swap values) contentWidth => contentHeight & contentHeight => contentWidth
            int temp = targetWidth;
            targetWidth = targetHeight;
            targetHeight = temp;
        }

        List<Camera.Size> approximateSizes = new ArrayList<>(0);
        List<Camera.Size> pictureSizeList = mCamera.getParameters().getSupportedPictureSizes();
        for (Camera.Size size : pictureSizeList) {
            if (size.height == targetHeight && size.width == targetWidth)
                return size;

            //get every size with contentHeight=displayHeight | mainWidth=displayWidth
            if (size.height == targetHeight || size.width == targetWidth) {
                approximateSizes.add(size);
            }
        }

        //if null is returned, the picture size resolves to the default
        //picture size, which may be large and hence takes time to process
        //it is assumed that this may not be the case as we should get at least
        //1 approximate
        if (approximateSizes.size() == 0) return null;

        Camera.Size fittingSize = approximateSizes.get(0);
        for (Camera.Size size : approximateSizes) {
            //orientation is horizontal, hence compare mainWidth(large part)
            if (orientation == -1) {
                if (fittingSize.width < size.width) fittingSize = size;
            } else //compare contentHeight(largest part)
                if (fittingSize.height < size.height) fittingSize = size;
        }
        return fittingSize;
    }


    public Camera getCamera() {
        return mCamera;
    }
}