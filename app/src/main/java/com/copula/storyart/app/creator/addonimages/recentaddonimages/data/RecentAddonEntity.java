package com.copula.storyart.app.creator.addonimages.recentaddonimages.data;

import java.io.Serializable;

/**
 * Created by heeleaz on 2/10/18.
 */

public class RecentAddonEntity implements Serializable {
    public String title, resourceUrl;
    public int id, type;

    public RecentAddonEntity() {
    }

    public RecentAddonEntity(String title, String url, int type) {
        this.title = title;
        this.resourceUrl = url;
        this.type = type;
    }
}
