package com.copula.storyart.app;

import android.content.Context;
import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;

public class FirebaseEventLogger {
    private FirebaseAnalytics firebaseAnalytics;

    private FirebaseEventLogger(Context context) {
        firebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    public static FirebaseEventLogger getInstance(Context context) {
        return new FirebaseEventLogger(context);
    }

    public void logWarning(String issue) {
        Bundle bundle = new Bundle(1);
        bundle.putString("issue", issue);
        firebaseAnalytics.logEvent("performanceIssue", bundle);
    }
}
