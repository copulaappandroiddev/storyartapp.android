package com.copula.storyart.app;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.copula.storyart.R;
import com.copula.storyart.app.creator.graphicbackground.colorbackground.ColorBackgroundPacket;
import com.copula.storyart.design.MediaBackgroundProperty;
import com.copula.storyart.media.AudioRecordHelper;
import com.copula.storyart.media.FileUtil;
import com.copula.storyart.media.observer.MediaModel;
import com.storyart.support.util.ViewUtils;
import com.storyart.support.widget.IResultCallback;
import com.storyart.support.widget.Logger;
import com.storyart.support.widget.PermissionHelper;
import com.storyart.support.widget.ViewAnimator;

import java.util.Locale;

/**
 * Created by heeleaz on 1/1/17.
 */
public class AudioRecorderActivity extends FragmentActivity implements
        View.OnClickListener, View.OnTouchListener, AudioRecordHelper.OnRecordPositionUpdateListener, IResultCallback {
    private static final Logger logger = Logger.getInstance("AudioRecorderActivity");

    private static final int PERMISSION_REQUEST_CODE = 100;

    private static final int RECORD_LIMIT_IN_SECS = 30;
    private static String AUDIO_RECORD_PATH = FileUtil.AUDIO_RECORD_PATH;

    private TextView txtRecordingTime, txtRecordedTime;
    private Button btnRecordingControl, btnCancel, btnSave;
    private AudioRecordHelper mAudioRecordHelper;

    public static void launch(Activity ctx, int requestCode) {
        ctx.startActivityForResult(new Intent(ctx, AudioRecorderActivity.class), requestCode);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.transition.fade_in, R.transition.fade_out);
        setContentView(R.layout.act_audio_recorder);

        MediaBackgroundProperty background = ColorBackgroundPacket.getInstance().getCurrentBackground();
        findViewById(R.id.view_parent).setBackground(background.asColor());

        txtRecordingTime = findViewById(R.id.txt_recording_time);
        txtRecordedTime = findViewById(R.id.txt_recorded_time);
        (btnRecordingControl = findViewById(R.id.btn_recording_control)).setOnTouchListener(this);
        (btnSave = findViewById(R.id.btn_save)).setOnClickListener(this);
        (btnCancel = findViewById(R.id.btn_cancel)).setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        verifyV23ApplicationPermission();
    }

    private void verifyV23ApplicationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PermissionHelper h = new PermissionHelper(this);

            String permission = Manifest.permission.RECORD_AUDIO;
            if (h.requestPermission(permission, PERMISSION_REQUEST_CODE) == PermissionHelper.RATIONALE_PERM) {
                RationalePermsDialog.launch(getSupportFragmentManager(), permission);
            }
        }
    }//end

    private void invalidateRecordHomepageView() {
        //hide all view pertaining to recording state
        btnSave.setVisibility(View.GONE);
        btnCancel.setVisibility(View.GONE);
        ViewUtils.showOnly(findViewById(R.id.view_record_homepage));
    }

    private void initAndStartRecordingInvalidateHomeView() {
        mAudioRecordHelper = new AudioRecordHelper(this);
        mAudioRecordHelper.setRecordMaxDuration(RECORD_LIMIT_IN_SECS);
        mAudioRecordHelper.startRecording(AUDIO_RECORD_PATH);

        ViewUtils.showOnly(findViewById(R.id.view_record_progress));
        txtRecordingTime.setText(createDurationCharacter(RECORD_LIMIT_IN_SECS));
        btnSave.setVisibility(View.GONE);
        btnCancel.setVisibility(View.GONE);
        btnRecordEffect(true);
    }

    private String createDurationCharacter(int duration) {
        return String.format(Locale.getDefault(), "00:%02d", duration % 60);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                initAndStartRecordingInvalidateHomeView();
                break;
            case MotionEvent.ACTION_UP:
                onMaxDurationReached(mAudioRecordHelper);
        }
        return true;
    }

    private void btnRecordEffect(boolean isRecording) {
        if (isRecording) {
            btnRecordingControl.setBackgroundResource(R.drawable.ic_record_white_onhold_256px);
            ViewAnimator.scaleXY(btnRecordingControl, 1.4f, 200);
        } else {
            btnRecordingControl.setBackgroundResource(R.drawable.ic_record_white_256px);
            ViewAnimator.scaleXY(btnRecordingControl, 1f, 200);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btnCancel) {
            cancelAndCloseRecording(false);
        } else if (v == btnSave) {
            MediaModel.Audio audioMedia = new MediaModel.Audio();
            audioMedia.data = AUDIO_RECORD_PATH;
            audioMedia.duration = mAudioRecordHelper.getCurrentDurationMilli() / 1000;
            Intent data = new Intent().putExtra("audioMedia", audioMedia);
            setResult(Activity.RESULT_OK, data);
            finish();
        }
    }

    private void cancelAndCloseRecording(final boolean closeActivityIfPositive) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        DialogInterface.OnClickListener l = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    mAudioRecordHelper.stopRecording();
                    mAudioRecordHelper = null;
                    if (closeActivityIfPositive) {
                        setResult(Activity.RESULT_CANCELED);
                        finish();
                    } else invalidateRecordHomepageView();
                }
                dialog.dismiss();
            }
        };
        builder.setMessage(R.string.msg_cancel_recording_dialog);
        builder.setPositiveButton(R.string.act_dialog_yes, l);
        builder.setNegativeButton(R.string.act_dialog_no, l).create().show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] p, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) try {
            verifyV23ApplicationPermission();
        } catch (Exception e) {
            logger.e(e);
        }
    }//END

    @Override
    public void onFragmentResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RationalePermsDialog.REQUEST_CODE) {
            if (resultCode == IResultCallback.RESULT_CANCEL) finish();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAudioRecordHelper != null) mAudioRecordHelper.stopRecording();
    }

    @Override
    public void onBackPressed() {
        if (mAudioRecordHelper != null) cancelAndCloseRecording(true);
        else super.onBackPressed();
    }

    @Override
    public void onMaxDurationReached(AudioRecordHelper recorder) {
        int ts = mAudioRecordHelper.getCurrentDurationMilli() / 1000;
        txtRecordedTime.setText(createDurationCharacter(ts));
        mAudioRecordHelper.stopRecording();

        ViewUtils.showOnly(findViewById(R.id.view_record_completed));
        btnRecordEffect(false);
        btnCancel.setVisibility(View.VISIBLE);
        btnSave.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPeriodicNotification(AudioRecordHelper recorder) {
        int ts = mAudioRecordHelper.getCurrentDurationMilli() / 1000;
        txtRecordingTime.setText(createDurationCharacter(RECORD_LIMIT_IN_SECS - ts));
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.transition.fade_in, R.transition.fade_out);
    }
}
