package com.copula.storyart.app.creator.stickerimage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import com.copula.storyart.R;
import com.copula.storyart.app.creator.Configuration;
import com.copula.storyart.app.creator.addonimages.recentaddonimages.data.RecentAddonDao;
import com.copula.storyart.app.creator.addonimages.recentaddonimages.data.RecentAddonEntity;
import com.copula.storyart.app.creator.helper.AbsImageListAdapter;
import com.copula.storyart.app.creator.helper.AbsImageListFragment;
import com.copula.storyart.app.creator.helper.cache.FileCacheSystem;
import com.copula.storyart.app.creator.helper.restful.AbsImageResourceApi;
import com.copula.storyart.app.creator.helper.restful.AbsRestfulApi;
import com.copula.storyart.design.GraphicImageProperty;

import java.util.List;

/**
 * Created by heeleaz on 12/10/17.
 */
public class StickerImageListFragment extends AbsImageListFragment implements AdapterView.OnItemClickListener,
        View.OnClickListener, AbsRestfulApi.Listener<List<AbsImageResourceApi.ApiModel>> {
    private FileCacheSystem mFileCacheSystem;

    private MyAdapter mAdapter;
    private String category;
    private RestfulApi mRestfulApi;

    public static StickerImageListFragment instantiate(String category) {
        StickerImageListFragment fragment = new StickerImageListFragment();
        Bundle argument = new Bundle(1);
        argument.putString("category", category);
        fragment.setArguments(argument);

        return fragment;
    }

    @Override
    protected View onRequestFragmentView(LayoutInflater inflater, ViewGroup c) {
        (mFileCacheSystem = new FileCacheSystem(Configuration.Stickers.STICKERS_CACHE_DIR,
                Configuration.Stickers.MAX_STICKERS_CACHE_SIZE)).openIgnoreError();

        return super.onRequestFragmentView(inflater, c);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.category = getArguments().getString("category");//getArgument is Not Null
        onLoadMore();
    }

    @Override
    protected BaseAdapter getBaseAdapter(Context context) {
        return mAdapter = new MyAdapter(context);
    }

    @Override
    protected int getLoadingTriggerThreshold() {
        return 30;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        RestfulApi.ApiModel m = (RestfulApi.ApiModel) parent.getItemAtPosition(position);
        String file = mFileCacheSystem.getCacheFile(m.resourceUrl);
        if (file == null) return;

        //add to recent addon image database
        RecentAddonEntity entity = new RecentAddonEntity(m.title, file, GraphicImageProperty.BITMAP_IMAGE);
        RecentAddonDao.getInstance(getContext()).insertAsync(entity);

        Intent intent = new Intent().putExtra("data", file);
        intent.putExtra("source", GraphicImageProperty.BITMAP_IMAGE);
        Activity activity = getActivity(); //getActivity cannot be null @this Instance
        activity.setResult(Activity.RESULT_OK, intent);
        activity.finish();

    }

    @Override
    public void onStickerFetchStarted() {
        super.invalidateDataFetchStarted();
    }

    @Override
    public void onDataFetchedSuccess(List<RestfulApi.ApiModel> models, String pageModel) {
        if (!isFragmentInForeground()) return;

        mAdapter.addHandlers(models);
        mAdapter.notifyDataSetChanged();

        super.invalidateDataFetchSuccessful(null);
    }

    @Override
    public void onDataFetchFailed(String message) {
        if (!isFragmentInForeground()) return;

        super.invalidateDataFetchFailed(getString(R.string.msg_sticker_load_failed));
    }

    @Override
    public void onLoadMore() {
        String nextPage = mRestfulApi != null ? mRestfulApi.getNextPageToken() : "0,30";
        mRestfulApi = new RestfulApi(getContext(), category, nextPage);
        mRestfulApi.execute(this);
    }

    @Override
    public boolean hasLoadedAllItems() {
        return mRestfulApi != null && mRestfulApi.getNextPageToken() == null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mFileCacheSystem != null) mFileCacheSystem.release();
    }

    public class MyAdapter extends AbsImageListAdapter<RestfulApi.ApiModel> {
        MyAdapter(Context context) {
            super(context, mFileCacheSystem, R.layout.adp_sticker_image);
        }

        @Override
        protected String getTitle(int position) {
            return getItem(position).title;
        }

        @Override
        protected String getImageResource(int position) {
            return getItem(position).resourceUrl;
        }
    }//END

    public class RestfulApi extends AbsImageResourceApi {
        RestfulApi(Context context, String category, String page) {
            super(context, category, page);
        }

        @Override
        protected String getUrl() {
            return "/stickers/get_stickers";
        }
    }
}
