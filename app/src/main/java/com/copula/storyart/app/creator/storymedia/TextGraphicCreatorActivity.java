package com.copula.storyart.app.creator.storymedia;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.copula.smartjson.OJsonMapper;
import com.copula.storyart.R;
import com.copula.storyart.app.AboutPageActivity;
import com.copula.storyart.app.creator.Configuration;
import com.copula.storyart.app.creator.graphicbackground.PatternListActivity;
import com.copula.storyart.app.creator.graphicbackground.colorbackground.ColorBackgroundPacket;
import com.copula.storyart.app.creator.graphicbackground.patterns.PatternLoadProgress;
import com.copula.storyart.app.creator.graphicplacement.DrawingBoard;
import com.copula.storyart.app.creator.graphicplacement.TextPlacementAdapter;
import com.copula.storyart.app.creator.storymedia.texteditor.EditorMarkupManager;
import com.copula.storyart.app.creator.storymedia.texteditor.TextEditorActivity;
import com.copula.storyart.app.promotion.MessageInviteFragment;
import com.copula.storyart.design.GraphicCreatorModel;
import com.copula.storyart.design.GraphicTextProperty;
import com.copula.storyart.design.MediaBackgroundProperty;
import com.copula.storyart.design.TypefaceLibrary;
import com.copula.storyart.media.FileUtil;
import com.copula.storyart.user.UserStarter;
import com.storyart.support.util.ViewUtils;
import com.storyart.support.widget.CustomButton;
import com.storyart.support.widget.DoubleBackPressed;
import com.storyart.support.widget.Logger;
import com.storyart.support.widget.PopupWindow;
import com.storyart.support.widget.emoji.StoryartTextView;

public class TextGraphicCreatorActivity extends GraphicCreatorActivity implements View.OnLongClickListener {
    private static final String TAG = TextGraphicCreatorActivity.class.getSimpleName();
    private static final int PATTERN_REQUEST_CODE = 2 << 1;

    static final int TEXT_EDITOR_HEIGHT = (int) (Configuration.DISPLAY_HEIGHT * 0.70);
    private static final int TEXT_EDITOR_WIDTH = (int) (Configuration.DISPLAY_WIDTH * 0.92);
    private static final int MAX_TEXT_SIZE = ViewUtils.dpToPx(44);
    private static final int MIN_TEXT_SIZE = ViewUtils.dpToPx(15);

    private CustomButton btnSelectPalette;
    private PatternLoadProgress patternLoadProgress;
    private ColorBackgroundPacket mColorBackgroundPacket;
    private TextPlacementAdapter.Builder mEditorBuilder;
    private GraphicTextProperty mGraphicTextProperty;
    private TypefaceLibrary mTypefaceLibrary;

    private StoryartTextView editorTextVIew;

    public static void launch(Context ctx, String text) {
        Intent intent = new Intent(ctx, TextGraphicCreatorActivity.class);
        ctx.startActivity(intent.putExtra("text", text));
    }

    public static void launch(Context ctx, Uri audioUrl) {
        Intent intent = new Intent(ctx, TextGraphicCreatorActivity.class);
        ctx.startActivity(intent.putExtra("audioUri", audioUrl));
    }

    @Override
    protected View onRequestActivityView(LayoutInflater inflater, ViewGroup container) {
        View v = inflater.inflate(R.layout.act_text_graphic_creator, container, false);

        patternLoadProgress = v.findViewById(R.id.pattern_load_view);
        v.findViewById(R.id.btn_more_option).setOnClickListener(this);
        v.findViewById(R.id.btn_graphic_media_source).setOnClickListener(this);
        v.findViewById(R.id.img_profile).setOnClickListener(this);
        (v.findViewById(R.id.btn_typeface_select)).setOnClickListener(this);
        (btnSelectPalette = v.findViewById(R.id.btn_pallete_select)).setOnClickListener(this);
        btnSelectPalette.setOnLongClickListener(this);
        return v;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        restoreBackground(mColorBackgroundPacket = ColorBackgroundPacket.getInstance());

        mEditorBuilder = new TextPlacementAdapter.Builder()
                .setEditorViewWidth(TEXT_EDITOR_WIDTH).setEditorViewHeight(TEXT_EDITOR_HEIGHT)
                .setMaxTextSize(MAX_TEXT_SIZE).setMinTextSize(MIN_TEXT_SIZE)
                .setEnableTouchMotion(false).removeIfEmpty(false);
        mGraphicTextProperty = GraphicTextProperty.loadDefault();
        mGraphicTextProperty.text = loadDefaultText();


        typefaceSelected((mTypefaceLibrary = TypefaceLibrary.instantiate(this)).next());

        if (getIntent() != null) loadRequestForExternalIntent(getIntent());
    }

    private String loadDefaultText() {
        EditorMarkupManager.Creator creator = EditorMarkupManager.getCreator();
        creator.setTextColor(Color.WHITE);
        creator.setTextSize(ViewUtils.dpToPx(25));

        return creator.getMarkupText("");
    }

    private void typefaceSelected(String path) {
        EditorMarkupManager.Creator creator = EditorMarkupManager.getCreator();
        creator.setTypeface(path);

        String text = mGraphicTextProperty.text == null ? "" : mGraphicTextProperty.text;
        mGraphicTextProperty.text = creator.getMarkupText(text);
        invalidateShowcaseTextProperty(mGraphicTextProperty);
    }

    @Override
    protected MediaBackgroundProperty overrideBackgroundProperty() {
        MediaBackgroundProperty backgroundProperty = new MediaBackgroundProperty();

        DrawingBoard view = findViewById(R.id.graphic_drawingboard);
        view.writeDrawingBoard(Bitmap.Config.ARGB_8888, FileUtil.GRAPHIC_WRITE_PATH);
        backgroundProperty.backgroundUrl = FileUtil.GRAPHIC_WRITE_PATH;

        return backgroundProperty;
    }

    @Override
    protected boolean onPrepareExportProcess(final GraphicCreatorModel model) {
        super.onPrepareExportProcess(model);

        GraphicTextProperty textProperty = model.getGraphicTextList().get(0);

        if (TextUtils.isEmpty(textProperty.text)) {
            ViewUtils.sToast(this, R.string.msg_showcase_text_empty);
            return false;
        }

        return true;
    }


    private void loadRequestForExternalIntent(Intent argument) {
        if (argument == null) return;

        String text = argument.getStringExtra("text");
        if (!TextUtils.isEmpty(text)) {
            mGraphicTextProperty.text = text;
            invalidateShowcaseTextProperty(mGraphicTextProperty);
        } else {
//            Uri uri = argument.getParcelableExtra("audioUri");
//            if (uri != null) {
//                AudioEditorFragment.show(getSupportFragmentManager(), uri, AudioOptionFragment.REQUEST_MIC_RECORD);
//            }
        }
    }

    private void restoreBackground(ColorBackgroundPacket colorBackgroundPacket) {
        MediaBackgroundProperty backgroundProperty = null;

        SharedPreferences preferences = getPreferences(Context.MODE_PRIVATE);
        String background = preferences.getString("background-property", null);
        if (!TextUtils.isEmpty(background)) try {
            backgroundProperty = OJsonMapper.mapOne(MediaBackgroundProperty.class, background);
        } catch (Exception e) {
            Logger.getInstance(TAG).e("Error reading last background object", e);
        }

        if (backgroundProperty == null) {
            currentGraphicContext.setBackgroundProperty(colorBackgroundPacket.next());
            backgroundProperty = currentGraphicContext.getBackgroundProperty();
        }

        invalidateShowcaseBackground(backgroundProperty);
    }

    private void invalidateShowcaseTextProperty(GraphicTextProperty textProperty) {
        StoryartTextView tv = getObjectPlacementLayout().text().createEditable(textProperty, mEditorBuilder);
        if (editorTextVIew == null) {
            (editorTextVIew = tv).adjustTextRectHeightMult(1);
            editorTextVIew.setGravity(Gravity.CENTER);
            editorTextVIew.setWidthScalable(true);
            editorTextVIew.setHintTextColor(0xC1FFFFFF);
            editorTextVIew.setHint(R.string.hint_showcase_text);

        }
    }

    private void invalidateShowcaseBackground(MediaBackgroundProperty background) {
        currentGraphicContext.setBackgroundProperty(background);

        View drawView = findViewById(R.id.background_view);
        if (background.source == MediaBackgroundProperty.COLOR) {
            drawView.setBackground(background.asColor());
        } else if (background.source == MediaBackgroundProperty.WALLPAPER) {
            patternLoadProgress.anchorBackground(drawView, background);
        }
    }

    @Override
    public void onTextEditorSelected(StoryartTextView textView, GraphicTextProperty textProperty) {
        int lines = TEXT_EDITOR_HEIGHT / MAX_TEXT_SIZE;
        lines += lines * (MIN_TEXT_SIZE / (MAX_TEXT_SIZE * 1f));

        if (isOptionButtonVisible()) {
            textView.setVisibility(View.GONE);
            onRequestHideOptionButtons(true);
            TextEditorActivity.launch(this, textProperty,
                    (lines & TextEditorActivity.MASK_MAX_LINES) | TextEditorActivity.FLAG_DESIGN_SELECT);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PATTERN_REQUEST_CODE) {
            if (resultCode != Activity.RESULT_OK) return;

            MediaBackgroundProperty background = (MediaBackgroundProperty) data.getSerializableExtra("background");
            if (background != null) invalidateShowcaseBackground(background);

        } else if (requestCode == TextEditorActivity.REQUEST_EDITOR) {
            onRequestHideOptionButtons(false);

            if (resultCode != Activity.RESULT_OK) return;

            mGraphicTextProperty = (GraphicTextProperty) data.getSerializableExtra("text");
            createEditableText(mGraphicTextProperty, mEditorBuilder).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onLongClick(View view) {
        if (view == btnSelectPalette) {
            btnSelectPalette.dismissTooltip();

            onRequestHideOptionButtons(true);
            PatternListActivity.launch(this, PATTERN_REQUEST_CODE);
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view == btnSelectPalette) {
            patternLoadProgress.stopBackgroundLoad();
            invalidateShowcaseBackground(mColorBackgroundPacket.next());
            btnSelectPalette.showButtonTooltip(TAG, Gravity.TOP, false);
        } else if (view.getId() == R.id.img_profile) {
            UserStarter.launch(this);//launch user profile activity
        } else if (view.getId() == R.id.btn_typeface_select) {
            typefaceSelected(mTypefaceLibrary.next());
        } else if (view.getId() == R.id.btn_graphic_media_source) {
            MediaGraphicSourceActivity.launch(this);
        } else if (view.getId() == R.id.btn_more_option) showOptionMenu(view);
    }

    private void showOptionMenu(View anchorView) {
        PopupWindow popupWindow = new PopupWindow(this, anchorView);
        popupWindow.addString(getString(R.string.act_new_post));
        popupWindow.addString(getString(R.string.act_invite_friends));
        popupWindow.addString(getString(R.string.act_about));
        popupWindow.show(new PopupWindow.PopupClickListener() {
            @Override
            public void onAction(int position, String text) {
                if (position == 0) {
                    TextGraphicCreatorActivity.launch(TextGraphicCreatorActivity.this, (String) null);
                    TextGraphicCreatorActivity.this.finish();
                } else if (position == 1) {
                    MessageInviteFragment.show(getSupportFragmentManager());
                } else if (position == 2) {
                    AboutPageActivity.launch(TextGraphicCreatorActivity.this);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (DoubleBackPressed.instance().dispatch(this, getString(R.string.msg_doubletap_back_to_close))) {
            patternLoadProgress.stopBackgroundLoad();
            super.onBackPressed();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        //save showcase background. will be restored as background on next launch
        SharedPreferences.Editor editor = getPreferences(Context.MODE_PRIVATE).edit();

        MediaBackgroundProperty background = currentGraphicContext.getBackgroundProperty();
        editor.putString("background-property", OJsonMapper.toJSON(background));
        editor.apply();
    }
}
