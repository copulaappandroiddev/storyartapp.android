package com.copula.storyart.app.creator;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.copula.storyart.R;
import com.storyart.support.util.ViewUtils;
import com.storyart.support.widget.Logger;

import java.io.IOException;
import java.util.Locale;

/**
 * Created by heeleaz on 10/28/17.
 */
public class GraphicAudioDetailView extends LinearLayout {
    private static final Logger logger = Logger.getInstance("SelectedAudioInfoView");

    private final int defCancelIconSize = ViewUtils.dpToPx(28);

    private ImageView imgAudioIcon, imgCancelIcon;
    private TextView txtDurationText;

    private ActionListener mListener;
    private AudioPlayerLoader mAudioPlayerLoader;

    public GraphicAudioDetailView(Context context) {
        this(context, null, 0);
    }

    public GraphicAudioDetailView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GraphicAudioDetailView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        setOrientation(LinearLayout.HORIZONTAL);
        setGravity(Gravity.CENTER);
        setBackgroundColor(Color.WHITE);

        int parentPadding = ViewUtils.dpToPx(5);
        setPadding(parentPadding, parentPadding, parentPadding, parentPadding);
        setBackgroundResource(R.drawable.background_selected_audio);

        addView(imgAudioIcon = new ImageView(context), 0);
        imgAudioIcon.setLayoutParams(new LayoutParams(defCancelIconSize, defCancelIconSize));
        imgAudioIcon.setImageResource(R.drawable.ic_play_black_128px);

        addView(txtDurationText = new TextView(context), 1);
        txtDurationText.setLayoutParams(new LayoutParams(-2, -2));
        txtDurationText.setTextColor(Color.DKGRAY);
        txtDurationText.setTextSize(TypedValue.COMPLEX_UNIT_PX, ViewUtils.dpToPx(15));
        txtDurationText.setTypeface(Typeface.SANS_SERIF);
        txtDurationText.setPadding(parentPadding, 0, parentPadding, 0);

        addView(imgCancelIcon = new ImageView(context), 2);
        imgCancelIcon.setLayoutParams(new LayoutParams(defCancelIconSize, defCancelIconSize));
        imgCancelIcon.setImageResource(R.drawable.ic_cancel_red_64px);

        ViewUtils.increaseTouchArea(imgCancelIcon, ViewUtils.dpToPx(10));

        imgCancelIcon.setOnClickListener(new CancelClickListener());
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAudioPlayerLoader != null) mAudioPlayerLoader.pause();
                if (mListener != null) mListener.onSelectedAudioOpen();
            }
        });
    }

    public void setAudioInfo(String audioUrl, int duration) {
        txtDurationText.setText(createDurationCharacter(duration));

        try {
            //release any previous existing media player in this class instance
            if (mAudioPlayerLoader != null) mAudioPlayerLoader.release();

            mAudioPlayerLoader = new AudioPlayerLoader(audioUrl);
            imgAudioIcon.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mAudioPlayerLoader.togglePlay();
                }
            });
        } catch (IOException e) {
            //TODO: Possibly create a callback to report error and play state
        }
    }

    private String createDurationCharacter(int duration) {
        if (duration == 0) return "00:00s";
        int s = duration % 60;
        return String.format(Locale.getDefault(), "00:%02ds", s);
    }

    public void setListener(final ActionListener listener) {
        this.mListener = listener;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mAudioPlayerLoader != null) mAudioPlayerLoader.release();
    }

    public interface ActionListener {
        void onSelectedAudioCancel();

        void onSelectedAudioOpen();
    }

    private class CancelClickListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

            DialogInterface.OnClickListener l = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (which == DialogInterface.BUTTON_POSITIVE) {
                        if (mAudioPlayerLoader != null) mAudioPlayerLoader.release();

                        if (mListener != null) mListener.onSelectedAudioCancel();
                    }
                    dialog.dismiss();
                }
            };
            builder.setMessage(R.string.msg_cancel_added_audio);
            builder.setPositiveButton(R.string.act_dialog_yes, l);
            builder.setNegativeButton(R.string.act_dialog_no, l).create().show();
        }
    }

    private class AudioPlayerLoader implements MediaPlayer.OnCompletionListener {
        private MediaPlayer mediaPlayer;

        AudioPlayerLoader(String url) throws IllegalStateException, IOException {
            (mediaPlayer = new MediaPlayer()).setOnCompletionListener(this);
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepare();
        }

        private void togglePlay() {
            try {
                if (mediaPlayer.isPlaying()) {
                    pause();
                } else {
                    mediaPlayer.start();
                    imgAudioIcon.setImageResource(R.drawable.ic_pause_black_128px);
                }
            } catch (IllegalStateException e) {
                imgAudioIcon.setImageResource(R.drawable.ic_play_black_128px);
                logger.e("MediaPlayer playback/pause error, using play icon as def", e);
            }
        }

        private void pause() {
            try {
                mediaPlayer.pause();
            } catch (IllegalStateException exception) {
                logger.e("MediaPlayer pause failed", exception);
            }
            imgAudioIcon.setImageResource(R.drawable.ic_play_black_128px);
        }

        private void release() {
            try {
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                    mediaPlayer.reset();
                    mediaPlayer.release();
                }
            } catch (IllegalStateException exception) {
                logger.e("Trying to release MediaPlayer results in error", exception);
            }
        }

        @Override
        public void onCompletion(MediaPlayer mp) {
            imgAudioIcon.setImageResource(R.drawable.ic_play_black_128px);
        }
    }
}
