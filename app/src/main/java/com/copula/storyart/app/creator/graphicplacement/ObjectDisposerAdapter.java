package com.copula.storyart.app.creator.graphicplacement;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.copula.storyart.R;
import com.storyart.support.util.ViewUtils;
import com.storyart.support.widget.ViewAnimator;

/**
 * Created by heeleaz on 2/6/18.
 */
public class ObjectDisposerAdapter {
    private Listener mListener;
    private int binLockSize;
    private ImageView binView;
    private ViewGroup mViewContainer;
    private int binViewBottomMargin = 0;

    private BinAnimation mBinAnimation;
    private ObjAnimation mObjAnimation;

    ObjectDisposerAdapter(Context context, ViewGroup container) {
        this.mViewContainer = container;

        int displayHeight = context.getResources().getDisplayMetrics().heightPixels;
        int displayWidth = context.getResources().getDisplayMetrics().widthPixels;

        container.addView(binView = new ImageView(context));
        int btn_dim = ViewUtils.dpToPx(42);
        binView.setLayoutParams(new FrameLayout.LayoutParams(btn_dim, btn_dim));
        binView.setY(displayHeight - ViewUtils.dpToPx(70));
        binView.setX((displayWidth >> 1) - (btn_dim >> 1));
        binView.setBackgroundResource(R.drawable.ic_trashbin_white_128px);
        binView.setVisibility(View.GONE);

        binLockSize = (int) (btn_dim / 1.5);
        (mBinAnimation = new BinAnimation()).trashBinView = binView;
    }

    public void setBinViewMargin(int bottom) {
        binView.setY(binView.getY() + (binViewBottomMargin = bottom));
    }

    private boolean isViewContains(View container, float contentX, float contentY) {
        int[] l = new int[2];
        container.getLocationOnScreen(l);
        int containerX = l[0];
        int containerY = l[1];
        int containerW = container.getWidth() * 2;
        int containerH = container.getHeight() * 2;

        //TODO: Adjust to center before validating true
        int endX = (containerX + containerW);
        int endY = (containerY + containerH);
        return (contentX > containerX && contentX < endX && contentY > containerY && contentY < endY);
    }

    private void animate(View view, boolean scaleForDispose) {
        if (scaleForDispose) {
            int[] l = new int[2];
            binView.getLocationOnScreen(l);
            l[1] += binViewBottomMargin;

            int binWidth = binView.getWidth();
            int binHeight = binView.getHeight();
            float shift = ((BinAnimation.DISPOSER_VIEW_SCALE * binWidth) - binWidth) / 2f;
            view.setX(l[0] - (view.getWidth() >> 1) + (binWidth >> 1) + shift);
            view.setY(l[1] - (view.getHeight() >> 1) + (binHeight >> 1) + shift);
        }


        mBinAnimation.activate(scaleForDispose);

        if (mObjAnimation == null) mObjAnimation = new ObjAnimation();
        mObjAnimation.activate(view, scaleForDispose);
    }

    public boolean monitorRemover(View v, MotionEvent event) {
        if (mViewContainer.indexOfChild(v) == -1) return false;

        boolean isRemoved = false;
        boolean contains = isViewContains(binView, event.getRawX(), event.getRawY());
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                binView.setVisibility(View.VISIBLE);
                if (mListener != null) mListener.onPlacedObjectInMotion(true);
                break;
            case MotionEvent.ACTION_MOVE:
                if (contains) {
                    animate(v, true);
                } else animate(v, false);
                break;
            case MotionEvent.ACTION_UP:
                if (contains) {
                    mBinAnimation.isRunning = false;
                    animate(v, false);
                    mViewContainer.removeView(v);

                    isRemoved = true;
                }
                binView.setVisibility(View.GONE);
                if (mListener != null) mListener.onPlacedObjectInMotion(false);
        }

        return isRemoved;
    }

    public int getPositionInParent() {
        return mViewContainer.indexOfChild(binView);
    }

    public void setPlacementListener(Listener mListener) {
        this.mListener = mListener;
    }

    public interface Listener {
        void onPlacedObjectInMotion(boolean inMotion);
    }

    private class BinAnimation implements ViewAnimator.AnimationCallback {
        private static final float DISPOSER_VIEW_SCALE = 1.4f;

        private View trashBinView;
        private boolean isRunning;

        private void activate(boolean scaleForDispose) {
            if (isRunning) return;

            float scale = 1f;
            if (scaleForDispose) scale = DISPOSER_VIEW_SCALE;

            ViewPropertyAnimator animator = trashBinView.animate();
            animator.scaleX(scale).scaleY(scale).setDuration(200)
                    .setListener(new ViewAnimator.SimpleAnimatorListener(this)).start();
        }

        @Override
        public void onAnimationStart() {
            isRunning = true;
        }

        @Override
        public void onAnimationEnd() {
            isRunning = false;
        }
    }

    private class ObjAnimation implements ViewAnimator.AnimationCallback {
        private boolean isRunning;

        private void activate(View view, boolean scaleForDispose) {
            if (isRunning) return;

            float scale = 1f;
            if (scaleForDispose) {
                scale = binLockSize / (Math.min(view.getWidth(), view.getHeight()) * 1f);
            }

            ViewPropertyAnimator animator = view.animate();
            animator.scaleX(scale).scaleY(scale).setDuration(100)
                    .setListener(new ViewAnimator.SimpleAnimatorListener(this)).start();
        }

        @Override
        public void onAnimationStart() {
            isRunning = true;
        }

        @Override
        public void onAnimationEnd() {
            isRunning = false;
        }
    }
}