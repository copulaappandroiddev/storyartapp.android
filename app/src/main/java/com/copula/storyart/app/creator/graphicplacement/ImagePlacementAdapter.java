package com.copula.storyart.app.creator.graphicplacement;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Gravity;
import android.view.ScaleGestureDetector;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.copula.storyart.R;
import com.copula.storyart.app.creator.Configuration;
import com.copula.storyart.app.creator.emojiimage.inappemojis.EmojiPacketLibrary;
import com.copula.storyart.design.GraphicImageProperty;
import com.storyart.support.util.ViewUtils;

public class ImagePlacementAdapter {
    private final int imageMinWidth = ViewUtils.dpToPx(50);
    private final int imageMaxWidth = Configuration.DISPLAY_WIDTH * 2;
    private final int defImageWidth = ViewUtils.dpToPx(128);
    private final int defImageHeight = ViewUtils.dpToPx(128);

    //gif should load as the default download dimension from StickerImageAdapter
    //if StickerImageAdapter changes the gif fetch dimension. this should also change
    private final int gifOverrideWidth = ViewUtils.dpToPx(74);
    private final int gifOverrideHeight = ViewUtils.dpToPx(74);

    private ObjectPlacementLayout mObjectPlacementLayout;
    private Context mContext;

    ImagePlacementAdapter(Context context, ObjectPlacementLayout l) {
        this.mContext = context;
        this.mObjectPlacementLayout = l;
    }

    public static GraphicImageProperty compile(ImageView imageView) {
        GraphicImageProperty property = (GraphicImageProperty) imageView.getTag();
        if (property == null) return null;

        property.height = imageView.getHeight();
        property.width = imageView.getWidth();

        int[] coordinate = new int[2];
        imageView.getLocationOnScreen(coordinate);
        property.x = coordinate[0];
        property.y = coordinate[1];
        property.rotationAngle = imageView.getRotation();

        return property;
    }

    private ImageView openEditable() {
        ImageView imageView = new ImageView(mContext);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(defImageWidth, defImageHeight);
        params.gravity = Gravity.CENTER;

        int position = mObjectPlacementLayout.getItemRemover().getPositionInParent();
        mObjectPlacementLayout.addView(imageView, position, params);
        return imageView;
    }

    public void creatorEditable(String imageUrl) {
        if (imageUrl == null) return;

        ImageView imageView = openEditable();
        setItemRemoverAdapter(imageView, mObjectPlacementLayout.getItemRemover());
        ViewUtils.increaseTouchArea(imageView, ViewUtils.dpToPx(64));

        GraphicImageProperty graphicImageProperty = new GraphicImageProperty();
        graphicImageProperty.path = imageUrl;

        if (imageUrl.endsWith("gif.0")) {
            Glide.with(mContext).load(imageUrl).asGif().placeholder(R.drawable.sticker_image_adapter_placeholder)
                    .priority(Priority.IMMEDIATE).override(gifOverrideWidth, gifOverrideHeight).into(imageView);
            graphicImageProperty.imageType = GraphicImageProperty.GIF_IMAGE;
        } else if (imageUrl.startsWith("emojis/")) {
            Bitmap bitmap = EmojiPacketLibrary.getEmojiImage(mContext, imageUrl);
            imageView.setImageBitmap(bitmap);
            graphicImageProperty.imageType = GraphicImageProperty.ASSET_BITMAP_IMAGE;
        } else {
            imageView.setImageBitmap(BitmapFactory.decodeFile(imageUrl));
            graphicImageProperty.imageType = GraphicImageProperty.BITMAP_IMAGE;
        }

        imageView.setTag(graphicImageProperty);
    }

    private void setItemRemoverAdapter(ImageView imageView, ObjectDisposerAdapter remover) {
        ObjectTouchDelegate listener = new ObjectTouchDelegate(mContext, remover, new MyScaleListener(imageView));
        imageView.setOnTouchListener(listener);
    }

    private class MyScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        private ImageView mImageView;

        MyScaleListener(ImageView imageView) {
            this.mImageView = imageView;
        }

        @Override
        public boolean onScale(ScaleGestureDetector d) {
            float span = (d.getCurrentSpan() - d.getPreviousSpan());

            ViewGroup.LayoutParams p = mImageView.getLayoutParams();
            int newWidth = (int) (mImageView.getWidth() + span);
            if (newWidth < imageMinWidth || newWidth > imageMaxWidth) return true;

            p.width = newWidth;
            p.height = (int) (mImageView.getHeight() + span);
            mImageView.setLayoutParams(p);

            ViewGroup.LayoutParams wrapperParams = mImageView.getLayoutParams();
            wrapperParams.width = (int) (mImageView.getWidth() + span);
            wrapperParams.height = (int) (mImageView.getHeight() + span);
            mImageView.setLayoutParams(wrapperParams);
            return true;
        }
    }//END

}
