package com.copula.storyart.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

/**
 * Created by heeleaz on 1/1/17.
 */
public class MediaSeekerView extends LinearLayout implements ViewTreeObserver.OnScrollChangedListener,
        RangeSeekBar.OnRangeSeekBarChangeListener {

    private TimingRulerView timingRulerView;
    private RangeSeekBar rangeSeekBar;
    private HorizontalScrollView durationRulerScrollView;

    private int paddingDueToThumbSpace;

    private OnMediaSeekListener mListener;

    public MediaSeekerView(Context context) {
        this(context, null, 0);
    }

    public MediaSeekerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MediaSeekerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setOrientation(LinearLayout.VERTICAL);
    }

    public void setOnMediaSeekListener(OnMediaSeekListener l) {
        this.mListener = l;
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        if (child instanceof RangeSeekBar) {
            super.addView((rangeSeekBar = (RangeSeekBar) child), index, params);
            paddingDueToThumbSpace = (int) rangeSeekBar.getAbsolutePadding();
            rangeSeekBar.setOnRangeSeekBarChangeListener(this);
        } else if (child instanceof TimingRulerView) {
            durationRulerScrollView = new HorizontalScrollView(getContext());
            durationRulerScrollView.setFillViewport(true);
            durationRulerScrollView.setHorizontalScrollBarEnabled(false);
            durationRulerScrollView.getViewTreeObserver().addOnScrollChangedListener(this);


            LinearLayout layout = new LinearLayout(getContext());
            ViewGroup.LayoutParams p = new ViewGroup.LayoutParams(-1, -1);
            layout.addView(timingRulerView = (TimingRulerView) child, p);
            layout.setOrientation(HORIZONTAL);

            durationRulerScrollView.addView(layout);
            super.addView(durationRulerScrollView, index, params);

            if (rangeSeekBar != null) setupRulerViewMarginBasedOnRange();
        }
    }

    private void setupRulerViewMarginBasedOnRange() {
        final ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) timingRulerView.getLayoutParams();
        params.leftMargin = params.rightMargin = paddingDueToThumbSpace;

        getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int w = rangeSeekBar.getWidth() - params.leftMargin * 2;
                int spacingWidth = timingRulerView.measureLineSpacingWidth(w, rangeSeekBar.getAbsoluteMaxValue());
                timingRulerView.setLineSpacingWidth(spacingWidth);
                getViewTreeObserver().removeOnGlobalLayoutListener(this);

            }
        });
    }

    private void calculateCorrespondingDurationRange() {
        final int scrollX = durationRulerScrollView.getScrollX();

        final int scrollXStart = (int) rangeSeekBar.getLeftThumbPosition() + scrollX;
        final int scrollXEnd = ((int) rangeSeekBar.getRightThumbPosition()) + scrollX;

        if (mListener != null) {
            int start = timingRulerView.getDurationAtPoint(scrollXStart);

            int stop = timingRulerView.getDurationAtPoint(scrollXEnd);
            mListener.onSeekerValueChanged(this, start, stop);
        }
    }

    public void setCurrentMediaPosition(int duration, int position) {
    }

    public void setRangeSeekbarValues(int minValue, int maxValue) {
        rangeSeekBar.setRangeValues(minValue, maxValue);
    }

    public void setStartValues(final int start, final int end, final int durationStartPoint) {
        getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                rangeSeekBar.setSelectedMinValue(start);
                rangeSeekBar.setSelectedMaxValue(end);
                durationRulerScrollView.smoothScrollTo(durationStartPoint, 0);

                getViewTreeObserver().removeOnGlobalLayoutListener(this);

            }
        });
    }

    public int getSelectedMinRangeValue() {
        return rangeSeekBar.getSelectedMinValue();
    }

    public int getSelectedMaxRangeValue() {
        return rangeSeekBar.getSelectedMaxValue();
    }

    public void setDurationInSecs(int durationInSecs) {
        timingRulerView.setDurationInSecs(durationInSecs);
    }

    @Override
    public void onScrollChanged() {
        calculateCorrespondingDurationRange();
    }

    public int getDurationStartPoint() {
        return durationRulerScrollView.getScrollX();
    }


    @Override
    public void onRangeSeekBarValuesChanged(RangeSeekBar bar, int minValue, int maxValue) {
        calculateCorrespondingDurationRange();
    }

    public interface OnMediaSeekListener {
        void onSeekerValueChanged(MediaSeekerView seekerView, int start, int stop);
    }
}
