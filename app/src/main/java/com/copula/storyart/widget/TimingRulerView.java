package com.copula.storyart.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import com.copula.storyart.R;
import com.storyart.support.util.ViewUtils;

import java.util.Locale;

/**
 * Created by heeleaz on 10/23/17.
 */
public class TimingRulerView extends android.support.v7.widget.AppCompatImageView {
    private int defLineSpacingWidth = ViewUtils.dpToPx(7);
    private int defDurationTextSize = 16;

    private Paint bigIntervalPaint;
    private Paint smallIntervalPaint;
    private Paint durationTextPaint;

    private int durationInSecs;
    private int bigLineInterval = 10;
    private int lineInterval = 1;
    private float lineSpacingWidth;
    private float durationTextSize;

    public TimingRulerView(Context context) {
        this(context, null, 0);
    }

    public TimingRulerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TimingRulerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        // Load the styled attributes and set their properties
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.TimingRulerView, defStyle, 0);

        durationTextSize = attributes.getDimension(R.styleable.TimingRulerView_duration_text_size, defDurationTextSize);
        lineSpacingWidth = attributes.getDimension(R.styleable.TimingRulerView_line_spacing_width, defLineSpacingWidth);
        durationInSecs = attributes.getInteger(R.styleable.TimingRulerView_duration_in_secs, 0);

        int bigLineColor = attributes.getColor(R.styleable.TimingRulerView_big_line_color, Color.GRAY);
        int smallLineColor = attributes.getColor(R.styleable.TimingRulerView_small_line_color, Color.LTGRAY);
        int durationTextColor = attributes.getColor(R.styleable.TimingRulerView_duration_text_color, Color.GRAY);
        attributes.recycle();

        bigIntervalPaint = new Paint();
        bigIntervalPaint.setColor(bigLineColor);
        bigIntervalPaint.setStyle(Paint.Style.STROKE);
        bigIntervalPaint.setStrokeWidth(2f);

        smallIntervalPaint = new Paint(bigIntervalPaint);
        smallIntervalPaint.setColor(smallLineColor);

        durationTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        durationTextPaint.setColor(durationTextColor);
        durationTextPaint.setStyle(Paint.Style.STROKE);
        durationTextPaint.setTextSize(durationTextSize);
    }

    public void setDurationInSecs(int durationInSecs) {
        this.durationInSecs = durationInSecs;
        invalidate();
    }

    public void setBigLineInterval(int bigLineInterval) {
        this.bigLineInterval = bigLineInterval;
        invalidate();
    }

    public void setLineInterval(int lineInterval) {
        this.lineInterval = lineInterval;
        invalidate();
    }

    public void setDurationTextSize(float size) {
        durationTextPaint.setTextSize(size);
        invalidate();
    }

    public void setLineSpacingWidth(float lineSpacingWidth) {
        this.lineSpacingWidth = lineSpacingWidth;
        invalidate();
    }

    public void setBigIntervalLineColor(int color) {
        bigIntervalPaint.setColor(color);
        invalidate();
    }

    public void setSmallIntervalLineColor(int color) {
        smallIntervalPaint.setColor(color);
        invalidate();
    }

    public int measureLineSpacingWidth(int width, int duration) {
        return (width / (duration * lineInterval));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int layoutWidth = getMeasuredWidth();
        float fittingWidth = lineSpacingWidth * durationInSecs;

        if (fittingWidth < layoutWidth) {
            lineSpacingWidth = layoutWidth / durationInSecs;
            fittingWidth = lineSpacingWidth * durationInSecs;
        }
        getLayoutParams().width = (int) fittingWidth;
    }

    public int getDurationAtPoint(int point) {
        int width = getWidth();
        if (width == 0) return 0;
        return (int) ((point * 1f / width * 1f) * durationInSecs * lineInterval);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int canvasHeight = canvas.getHeight();

        int intervalCount = (durationInSecs / lineInterval);
        for (int i = 0; i < intervalCount; i++) {
            float x = (i * lineSpacingWidth) + lineInterval;
            float startY = durationTextSize;

            if (i % bigLineInterval != 0) {//draw small interval line
                int startYWithMargin = (canvasHeight / 2) + 7;
                canvas.drawLine(x, startYWithMargin, x, canvasHeight, smallIntervalPaint);
            } else {//draw big interval line
                float startYWithMargin = startY + 7;
                canvas.drawLine(x, startYWithMargin, x, canvasHeight, bigIntervalPaint);

                //draw editorDuration text character
                String text = createDurationCharacter(i * lineInterval);
                float textWidth = durationTextPaint.measureText(text);
                canvas.drawText(text, 0, text.length(), x - (textWidth / 2f), startY, durationTextPaint);
            }
        }
    }

    private String createDurationCharacter(int duration) {
        Locale locale = Locale.getDefault();

        if (duration == 0) return ":00";
        int m = duration / 60;
        int s = duration % 60;
        if (m == 0) return String.format(locale, ":%02d", s);
        else return String.format(locale, "%02d", m) + ":" + String.format(locale, "%02d", s);
    }
}
