package com.copula.storyart.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.*;
import android.graphics.Paint.Style;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import com.copula.storyart.R;
import com.storyart.support.util.BitmapUtils;


public class RangeSeekBar extends android.support.v7.widget.AppCompatImageView {
    private static final int DEF_ACTIVE_COLOR = Color.parseColor("#FF44B5E5");

    private static final int ACTION_POINTER_INDEX_MASK = 0x0000ff00;
    private static final int ACTION_POINTER_INDEX_SHIFT = 8;

    private static final Integer DEFAULT_MINIMUM = 0;
    private static final Integer DEFAULT_MAXIMUM = 100;
    private static final Integer DEFAULT_THUMB_SCALE = 1;

    private static final int INITIAL_PADDING_IN_DP = 8;

    private static final int LINE_HEIGHT_IN_DP = 2;
    private final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final Paint pointerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    private Bitmap thumbNormalImage;
    private Bitmap thumbPressedImage;
    private Bitmap thumbDisabledImage;

    private float mThumbHalfWidth;

    private float padding;
    private int absoluteMinValue = DEFAULT_MINIMUM;
    private int absoluteMaxValue = DEFAULT_MAXIMUM;

    private double normalizedMinValue = 0d;
    private double normalizedMaxValue = 1d;
    private Thumb pressedThumb = null;
    private OnRangeSeekBarChangeListener listener;

    private float mDownMotionX;

    private int mActivePointerId = 255;//An invalid pointer position.

    private int mScaledTouchSlop;

    private boolean mIsDragging;

    private RectF mRect;

    private boolean mSingleThumb;
    private boolean mAlwaysActive;
    private float mInternalPad;
    private int mActiveColor;
    private int mDefaultColor;

    private boolean mActivateOnDefaultValues;

    private float mPointerLocation;


    public RangeSeekBar(Context context) {
        super(context);
        init(context, null, 0);
    }

    public RangeSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public RangeSeekBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        float barHeight, scaleThumbBy;

        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.RangeSeekBar,
                0, defStyle);

        int min = a.getInteger(R.styleable.RangeSeekBar_absoluteMinValue, DEFAULT_MINIMUM);
        int max = a.getInteger(R.styleable.RangeSeekBar_absoluteMaxValue, DEFAULT_MAXIMUM);
        setRangeValues(min, max);

        barHeight = a.getDimensionPixelSize(R.styleable.RangeSeekBar_barHeight, LINE_HEIGHT_IN_DP);
        mSingleThumb = a.getBoolean(R.styleable.RangeSeekBar_singleThumb, false);
        mInternalPad = a.getDimensionPixelSize(R.styleable.RangeSeekBar_internalPadding, INITIAL_PADDING_IN_DP);
        mActiveColor = a.getColor(R.styleable.RangeSeekBar_activeColor, DEF_ACTIVE_COLOR);
        mDefaultColor = a.getColor(R.styleable.RangeSeekBar_defaultColor, Color.GRAY);
        mAlwaysActive = a.getBoolean(R.styleable.RangeSeekBar_alwaysActive, false);
        scaleThumbBy = a.getFloat(R.styleable.RangeSeekBar_scaleThumbBy, DEFAULT_THUMB_SCALE);

        Drawable normalDrawable = a.getDrawable(R.styleable.RangeSeekBar_thumbNormal);
        if (normalDrawable != null) {
            thumbNormalImage = BitmapUtils.drawableToBitmap(normalDrawable);
        }

        Drawable disabledDrawable = a.getDrawable(R.styleable.RangeSeekBar_thumbDisabled);
        if (disabledDrawable != null) {
            thumbDisabledImage = BitmapUtils.drawableToBitmap(disabledDrawable);
        }

        Drawable pressedDrawable = a.getDrawable(R.styleable.RangeSeekBar_thumbPressed);
        if (pressedDrawable != null) {
            thumbPressedImage = BitmapUtils.drawableToBitmap(pressedDrawable);
        }
        mActivateOnDefaultValues = a.getBoolean(R.styleable.RangeSeekBar_activateOnDefaultValues, false);
        a.recycle();


        int thumbDrawableRes = R.drawable.range_thumb;
        if (thumbNormalImage == null) {
            thumbNormalImage = BitmapUtils.createScaledBitmap(context, thumbDrawableRes, scaleThumbBy);
        }
        if (thumbPressedImage == null) {
            thumbPressedImage = BitmapUtils.createScaledBitmap(context, thumbDrawableRes, scaleThumbBy);
        }
        if (thumbDisabledImage == null) {
            thumbDisabledImage = BitmapUtils.createScaledBitmap(context, thumbDrawableRes, scaleThumbBy);
        }

        mThumbHalfWidth = 0.5f * thumbNormalImage.getWidth();
        float thumbHalfHeight = 0.5f * thumbNormalImage.getHeight();

        mRect = new RectF(padding, thumbHalfHeight - barHeight / 2,
                getWidth() - padding, thumbHalfHeight + barHeight / 2);

        setFocusable(true);//set focus in case of EditText stealing focus
        setFocusableInTouchMode(true);
        mScaledTouchSlop = ViewConfiguration.get(getContext()).getScaledTouchSlop();

        pointerPaint.setStyle(Paint.Style.STROKE);
        pointerPaint.setColor(Color.WHITE);
        pointerPaint.setStrokeWidth(5f);
    }

    public void setRangeValues(int minValue, int maxValue) {
        this.absoluteMinValue = minValue;
        this.absoluteMaxValue = maxValue;
    }

    public int getAbsoluteMaxValue() {
        return absoluteMaxValue;
    }

    public int getAbsoluteMinValue() {
        return absoluteMinValue;
    }

    public int getSelectedMinValue() {
        return normalizedToValue(normalizedMinValue);
    }

    public void setSelectedMinValue(int value) {
        // in case absoluteMinValue == absoluteMaxValue,
        // avoid division by zero when normalizing.
        if ((absoluteMaxValue - absoluteMinValue) == 0) {
            setNormalizedMinValue(0d);
        } else {
            setNormalizedMinValue(valueToNormalized(value));
        }
    }

    public int getSelectedMaxValue() {
        return normalizedToValue(normalizedMaxValue);
    }

    public void setSelectedMaxValue(int value) {
        if ((absoluteMaxValue - absoluteMinValue) == 0) {
            setNormalizedMaxValue(1d);
        } else setNormalizedMaxValue(valueToNormalized(value));
    }

    public void setOnRangeSeekBarChangeListener(OnRangeSeekBarChangeListener listener) {
        this.listener = listener;
    }

    /**
     * Handles thumb selection and movement. Notifies listener callback on certain events.
     */
    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        if (!isEnabled()) return false;

        int pointerIndex;

        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                // Remember where the motion event started
                mActivePointerId = event.getPointerId(event.getPointerCount() - 1);
                pointerIndex = event.findPointerIndex(mActivePointerId);
                mDownMotionX = event.getX(pointerIndex);

                pressedThumb = evalPressedThumb(mDownMotionX);

                // Only handle thumb presses.
                if (pressedThumb == null) return super.onTouchEvent(event);

                setPressed(true);
                invalidate();
                onStartTrackingTouch();
                trackTouchEvent(event);
                attemptClaimDrag();

                break;
            case MotionEvent.ACTION_MOVE:
                if (pressedThumb != null) {
                    if (mIsDragging) {
                        trackTouchEvent(event);
                    } else {
                        // Scroll to follow the motion event
                        pointerIndex = event.findPointerIndex(mActivePointerId);
                        final float x = event.getX(pointerIndex);

                        if (Math.abs(x - mDownMotionX) > mScaledTouchSlop) {
                            setPressed(true);
                            invalidate();
                            onStartTrackingTouch();
                            trackTouchEvent(event);
                            attemptClaimDrag();
                        }
                    }

                    if (listener != null) {
                        listener.onRangeSeekBarValuesChanged(this, getSelectedMinValue(), getSelectedMaxValue());
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                if (mIsDragging) {
                    trackTouchEvent(event);
                    onStopTrackingTouch();
                    setPressed(false);
                } else {
                    // Touch up when we never crossed the touch slop threshold
                    // should be interpreted as a tap-seek to that location.
                    onStartTrackingTouch();
                    trackTouchEvent(event);
                    onStopTrackingTouch();
                }

                pressedThumb = null;
                invalidate();
                if (listener != null) {
                    listener.onRangeSeekBarValuesChanged(this, getSelectedMinValue(), getSelectedMaxValue());
                }
                break;
            case MotionEvent.ACTION_POINTER_DOWN: {
                final int index = event.getPointerCount() - 1;
                // final int index = ev.getActionIndex();
                mDownMotionX = event.getX(index);
                mActivePointerId = event.getPointerId(index);
                invalidate();
                break;
            }
            case MotionEvent.ACTION_POINTER_UP:
                onSecondaryPointerUp(event);
                invalidate();
                break;
            case MotionEvent.ACTION_CANCEL:
                if (mIsDragging) {
                    onStopTrackingTouch();
                    setPressed(false);
                }
                invalidate(); // see above explanation
                break;
        }
        return true;
    }

    private void onSecondaryPointerUp(MotionEvent ev) {
        final int pointerIndex = (ev.getAction() & ACTION_POINTER_INDEX_MASK) >> ACTION_POINTER_INDEX_SHIFT;

        final int pointerId = ev.getPointerId(pointerIndex);
        if (pointerId == mActivePointerId) {
            // This was our active pointer going up. Choose
            // a new active pointer and adjust accordingly.
            final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
            mDownMotionX = ev.getX(newPointerIndex);
            mActivePointerId = ev.getPointerId(newPointerIndex);
        }
    }

    private void trackTouchEvent(MotionEvent event) {
        final int pointerIndex = event.findPointerIndex(mActivePointerId);
        final float x = event.getX(pointerIndex);

        if (Thumb.MIN.equals(pressedThumb) && !mSingleThumb) {

            setNormalizedMinValue(screenToNormalized(x));
        } else if (Thumb.MAX.equals(pressedThumb)) {
            setNormalizedMaxValue(screenToNormalized(x));
        }
    }


    //Tries to claim the user's drag motion, and requests
    // disallowing any ancestors from stealing events in the drag.
    private void attemptClaimDrag() {
        if (getParent() != null)
            getParent().requestDisallowInterceptTouchEvent(true);
    }

    //This is called when the user has started touching this widget.
    private void onStartTrackingTouch() {
        mIsDragging = true;
    }

    //This is called when the user either releases his touch or the touch is canceled.
    private void onStopTrackingTouch() {
        mIsDragging = false;
    }

    //Ensure correct widget contentHeight based on thumb dimension
    @Override
    protected synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = 200;
        if (MeasureSpec.getMode(widthMeasureSpec) != MeasureSpec.UNSPECIFIED) {
            width = MeasureSpec.getSize(widthMeasureSpec);
        }

        int height = thumbNormalImage.getHeight();
        if (MeasureSpec.getMode(heightMeasureSpec) != MeasureSpec.UNSPECIFIED) {
            height = Math.min(height, MeasureSpec.getSize(heightMeasureSpec));
        }
        setMeasuredDimension(width, height);
    }

    /**
     * Draws the widget on the given canvas.
     */
    @Override
    protected synchronized void onDraw(@NonNull Canvas canvas) {
        super.onDraw(canvas);

        paint.setStyle(Style.FILL);
        paint.setColor(mDefaultColor);
        paint.setAntiAlias(true);

        padding = mInternalPad + mThumbHalfWidth;

        // draw seek bar background line
        mRect.left = padding;
        mRect.right = getWidth() - padding;
        canvas.drawRect(mRect, paint);

        boolean selectedValuesAreDefault =
                (getSelectedMinValue() == (absoluteMinValue) &&
                        getSelectedMaxValue() == (absoluteMaxValue));

        int colorToUseForButtonsAndHighlightedLine = !mAlwaysActive &&
                !mActivateOnDefaultValues && selectedValuesAreDefault ? mDefaultColor : mActiveColor;

        // draw seek bar active range line
        mRect.left = normalizedToScreen(normalizedMinValue);
        mRect.right = normalizedToScreen(normalizedMaxValue);

        paint.setColor(colorToUseForButtonsAndHighlightedLine);
        canvas.drawRect(mRect, paint);

        // draw minimum thumb (& shadow if requested) if not a single thumb control
        if (!mSingleThumb) {
            drawThumb(normalizedToScreen(normalizedMinValue), Thumb.MIN.equals(pressedThumb), canvas,
                    selectedValuesAreDefault);
        }

        drawThumb(normalizedToScreen(normalizedMaxValue),
                Thumb.MAX.equals(pressedThumb), canvas, selectedValuesAreDefault);
        drawPointerIndicator(canvas);
    }

    public void setPointerLocation(float pointerLocation) {
        this.mPointerLocation = pointerLocation;
        invalidate();
    }

    private void drawPointerIndicator(Canvas canvas) {
        canvas.drawLine(mPointerLocation, 0, mPointerLocation, canvas.getHeight(), pointerPaint);
    }

    public float getLeftThumbPosition() {
        return mRect.left - padding;
    }

    public float getRightThumbPosition() {
        return mRect.right - padding;
    }

    public float getAbsolutePadding() {
        return mInternalPad + mThumbHalfWidth;
    }

    /**
     * Overridden to save instance state when device orientation changes.
     * This method is called automatically if you assign an position to the
     * RangeSeekBar widget using the {@link #setId(int)} method.
     * Other members of this class than the normalized min and max values don't need to be saved.
     */
    @Override
    protected Parcelable onSaveInstanceState() {
        final Bundle bundle = new Bundle();
        bundle.putParcelable("SUPER", super.onSaveInstanceState());
        bundle.putDouble("MIN", normalizedMinValue);
        bundle.putDouble("MAX", normalizedMaxValue);
        return bundle;
    }

    /**
     * Overridden to restore instance state when device orientation changes.
     * This method is called automatically if you assign an position to the RangeSeekBar
     * widget using the {@link #setId(int)} method.
     */
    @Override
    protected void onRestoreInstanceState(Parcelable parcel) {
        final Bundle bundle = (Bundle) parcel;
        super.onRestoreInstanceState(bundle.getParcelable("SUPER"));
        normalizedMinValue = bundle.getDouble("MIN");
        normalizedMaxValue = bundle.getDouble("MAX");
    }

    /**
     * Draws the "normal" resp. "pressed" thumb image on specified mainWidth-coordinate.
     *
     * @param screenCoord The mainWidth-coordinate in screen space where to draw the image.
     * @param pressed     Is the thumb currently in "pressed" state?
     * @param canvas      The canvas to draw upon.
     */
    private void drawThumb(float screenCoord, boolean pressed, Canvas canvas, boolean areSelectedValuesDefault) {
        Bitmap buttonToDraw;
        if (!mActivateOnDefaultValues && areSelectedValuesDefault) {
            buttonToDraw = thumbDisabledImage;
        } else {
            buttonToDraw = pressed ? thumbPressedImage : thumbNormalImage;
        }

        canvas.drawBitmap(buttonToDraw, screenCoord - mThumbHalfWidth, 0, paint);
    }

    /**
     * Decides which (if any) thumb is touched by the given mainWidth-coordinate.
     *
     * @param touchX The mainWidth-coordinate of a touch event in screen space.
     * @return The pressed thumb or null if none has been touched.
     */
    private Thumb evalPressedThumb(float touchX) {
        Thumb result = null;
        boolean minThumbPressed = isInThumbRange(touchX, normalizedMinValue);
        boolean maxThumbPressed = isInThumbRange(touchX, normalizedMaxValue);
        if (minThumbPressed && maxThumbPressed) {
            // if both thumbs are pressed (they lie on top of each other),
            // choose the one with more room to drag. this avoids "stalling"
            // the thumbs in a corner, not being able to drag them apart anymore.
            result = (touchX / getWidth() > 0.5f) ? Thumb.MIN : Thumb.MAX;
        } else if (minThumbPressed) {
            result = Thumb.MIN;
        } else if (maxThumbPressed) {
            result = Thumb.MAX;
        }
        return result;
    }

    /**
     * Decides if given mainWidth-coordinate in screen space needs to be interpreted as
     * "within" the normalized thumb mainWidth-coordinate.
     *
     * @param touchX               The mainWidth-coordinate in screen space to check.
     * @param normalizedThumbValue The normalized mainWidth-coordinate of the thumb to check.
     * @return true if mainWidth-coordinate is in thumb range, false otherwise.
     */
    private boolean isInThumbRange(float touchX, double normalizedThumbValue) {
        return Math.abs(touchX - normalizedToScreen(normalizedThumbValue)) <= mThumbHalfWidth;
    }

    private void setNormalizedMinValue(double value) {
        normalizedMinValue = Math.max(0d, Math.min(1d, Math.min(value, normalizedMaxValue)));
        invalidate();
    }

    private void setNormalizedMaxValue(double value) {
        normalizedMaxValue = Math.max(0d, Math.min(1d, Math.max(value, normalizedMinValue)));
        invalidate();
    }

    public int normalizedToValue(double normalized) {
        double v = absoluteMinValue + normalized * (absoluteMaxValue - absoluteMinValue);
        return (int) (Math.round(v * 100) / 100);
    }

    public double valueToNormalized(int value) {
        if ((absoluteMaxValue - absoluteMinValue) == 0) return 0d;
        return (value - absoluteMinValue) / (float) (absoluteMaxValue - absoluteMinValue);
    }

    public float normalizedToScreen(double normalizedCoord) {
        return (float) (padding + normalizedCoord * (getWidth() - 2 * padding));
    }

    private double screenToNormalized(float screenCoord) {
        int width = getWidth();
        if (width <= 2 * padding) {
            // prevent division by zero, simply return 0.
            return 0d;
        } else {
            double result = (screenCoord - padding) / (width - 2 * padding);
            return Math.min(1d, Math.max(0d, result));
        }
    }

    private enum Thumb {
        MIN, MAX
    }

    public interface OnRangeSeekBarChangeListener {

        void onRangeSeekBarValuesChanged(RangeSeekBar bar, int minValue, int maxValue);
    }
}
