package com.copula.storyart.design;

import android.graphics.Color;
import com.copula.storyart.app.creator.Configuration;
import com.storyart.support.util.ViewUtils;

/**
 * Created by heeleaz on 2/1/18.
 */
public class GraphicTextProperty extends GraphicPlaceableItem {
    public static int LEFT = 0, CENTER = 1, RIGHT = 2;
    public long id;
    public float xPosition, yPosition, textSize;
    public String text, brokenText;
    public int textColor = Color.WHITE, align;
    public int width, height;

    public static GraphicTextProperty loadDefault() {
        GraphicTextProperty t = new GraphicTextProperty();
        t.id = System.currentTimeMillis();
        t.textSize = ViewUtils.dpToPx(25);
        t.xPosition = Configuration.DISPLAY_WIDTH / 2;
        t.yPosition = Configuration.DISPLAY_HEIGHT / 2;
        t.align = CENTER;

        return t;
    }
}
