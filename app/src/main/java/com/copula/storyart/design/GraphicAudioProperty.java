package com.copula.storyart.design;

import com.copula.storyart.media.observer.MediaModel;

import java.io.Serializable;

/**
 * Created by heeleaz on 10/29/17.
 */
public class GraphicAudioProperty implements Serializable {
    public String editorAudioPath;

    public int editorStartFrom;
    public int editorEndAt;
    public int editorDurationStartPoint;
    public int editorDuration;
    public boolean isEdited;

    public MediaModel.Audio source;

    public GraphicAudioProperty() {
    }

    public GraphicAudioProperty(MediaModel.Audio source) {
        this.source = source;
    }
}
