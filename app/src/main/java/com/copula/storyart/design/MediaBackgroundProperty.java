package com.copula.storyart.design;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;

import java.io.Serializable;

/**
 * Created by heeleaz on 10/21/17.
 */
public class MediaBackgroundProperty extends GraphicPlaceableItem implements Serializable {
    public static transient final int COLOR = 8, WALLPAPER = 4, GALLERY_IMAGE = 2, CAMERA_IMAGE = 1;

    public int id;

    public String backgroundUrl, placeholderUrl;
    public int contentWidth, contentHeight;
    public int source;
    public String mimeType;
    public long createdAt, duration;

    public Bitmap asWallpaper() {
        return BitmapFactory.decodeFile(backgroundUrl);
    }

    public Drawable asColor() {
        if (backgroundUrl != null) return new ColorDrawable(Color.parseColor(backgroundUrl));
        return new ColorDrawable(Color.parseColor("#000000"));
    }

    public Bitmap asGalleryImage() {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inMutable = true;
        return BitmapFactory.decodeFile(backgroundUrl, opt);
    }
}
