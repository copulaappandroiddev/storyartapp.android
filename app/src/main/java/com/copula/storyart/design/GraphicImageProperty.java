package com.copula.storyart.design;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.copula.storyart.app.creator.emojiimage.inappemojis.EmojiPacketLibrary;

/**
 * Created by heeleaz on 11/2/17.
 */
public class GraphicImageProperty extends GraphicPlaceableItem {
    public static final int ASSET_BITMAP_IMAGE = 4, BITMAP_IMAGE = 2, GIF_IMAGE = 1;

    public int imageType;
    public String path;
    public int width, height;
    public int x, y;

    public Bitmap toBitmap(Context ctx) {
        if (imageType == ASSET_BITMAP_IMAGE) {
            return EmojiPacketLibrary.getEmojiImage(ctx, path);
        } else if (imageType == BITMAP_IMAGE) {
            return BitmapFactory.decodeFile(path);
        } else return null;
    }
}
