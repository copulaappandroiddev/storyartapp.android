package com.copula.storyart.design;

import java.io.Serializable;

public abstract class GraphicPlaceableItem implements Serializable, Comparable<GraphicPlaceableItem> {
    public int placementPosition;
    public float rotationAngle;

    @Override
    public int compareTo(GraphicPlaceableItem o) {
        if (placementPosition > o.placementPosition) return 1;
        else if (placementPosition < o.placementPosition) return -1;
        else return 0;
    }
}
