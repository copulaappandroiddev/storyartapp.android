package com.copula.storyart.design;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

import java.io.File;

public class StoryartDrawingBoard {
    private File boardImageFile;
    private StoryartWatermarkHandler mStoryartWatermarkHandler;


    public void setBoardImageFile(File boardImageFile) {
        this.boardImageFile = boardImageFile;
    }

    public void setWatermarkHandler(StoryartWatermarkHandler watermark) {
        this.mStoryartWatermarkHandler = watermark;
    }

    private Bitmap getMutableBoardImage() {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inMutable = true;
        return BitmapFactory.decodeFile(boardImageFile.getAbsolutePath(), opt);
    }

    protected void drawAppMontage(Canvas canvas, StoryartWatermarkHandler watermark, int screenWidth, int screenHeight) {
        Paint textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setColor(watermark.textColor);
        textPaint.setTextSize(watermark.textSize);
        textPaint.setTypeface(watermark.getTypeface());

        float textWidth = textPaint.measureText(watermark.text);
        float xText = screenWidth - (watermark.xMargin + textWidth);
        float yText = screenHeight - watermark.yMargin;
        canvas.drawText(watermark.text, xText, yText, textPaint);

        Paint monPaint = new Paint(textPaint);
        monPaint.setTextSize(watermark.montageTextSize);

        xText = screenWidth - (watermark.montageXMargin + textWidth);
        yText = screenHeight - watermark.montageYMargin;
        canvas.drawText(watermark.montageText, xText, yText, monPaint);
    }

    public Bitmap draw() {
        Bitmap bitmap = getMutableBoardImage();

        if (bitmap != null) {
            Canvas canvas = new Canvas(bitmap);
            drawAppMontage(canvas, mStoryartWatermarkHandler, bitmap.getWidth(), bitmap.getHeight());
        }

        return bitmap;
    }

}
