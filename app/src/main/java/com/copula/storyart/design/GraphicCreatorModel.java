package com.copula.storyart.design;


import android.support.annotation.NonNull;
import com.copula.smartjson.OJsonMapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 1/1/17.
 */
public class GraphicCreatorModel {
    private MediaBackgroundProperty mMediaBackgroundProperty = new MediaBackgroundProperty();
    private GraphicAudioProperty mGraphicAudioProperty = new GraphicAudioProperty();
    private List<GraphicImageProperty> mGraphicImageList = new ArrayList<>();
    private List<GraphicTextProperty> mGraphicTextList = new ArrayList<>();

    public GraphicCreatorModel(int w, int h) {
        mMediaBackgroundProperty.contentWidth = w;
        mMediaBackgroundProperty.contentHeight = h;
    }

    public GraphicCreatorModel(String json) throws Exception {
        Hello h = OJsonMapper.mapOne(Hello.class, json);

        mMediaBackgroundProperty = OJsonMapper.mapOne(MediaBackgroundProperty.class, h.backgroundProperty);
        mGraphicImageList = OJsonMapper.mapList(GraphicImageProperty.class, h.graphicImageList);
        mGraphicAudioProperty = OJsonMapper.mapOne(GraphicAudioProperty.class, h.audioAssets);
        mGraphicTextList = OJsonMapper.mapList(GraphicTextProperty.class, h.graphicTextList);
    }

    public List<GraphicImageProperty> getBitmapImages() {
        return mGraphicImageList;
    }

    public void setGraphicImages(List<GraphicImageProperty> images) {
        mGraphicImageList = images;
    }

    public List<GraphicImageProperty> getGIFImages() {
        List<GraphicImageProperty> gifImageList = new ArrayList<>();
        for (GraphicImageProperty p : mGraphicImageList) {
            if (p.imageType == GraphicImageProperty.GIF_IMAGE) gifImageList.add(p);
        }
        return gifImageList;
    }

    @NonNull
    public MediaBackgroundProperty getBackgroundProperty() {
        return mMediaBackgroundProperty;
    }

    public void setBackgroundProperty(MediaBackgroundProperty background) {
        mMediaBackgroundProperty = background;

        mMediaBackgroundProperty.contentHeight = background.contentHeight;
        mMediaBackgroundProperty.contentWidth = background.contentWidth;
    }

    public GraphicAudioProperty getAudioAssets() {
        return mGraphicAudioProperty;
    }

    public void removeAudioAssets() {
        mGraphicAudioProperty = new GraphicAudioProperty();
    }

    public List<GraphicTextProperty> getGraphicTextList() {
        return mGraphicTextList;
    }

    public void setGraphicTextList(List<GraphicTextProperty> textList) {
        this.mGraphicTextList = textList;
    }

    public void setAudioProperty(GraphicAudioProperty audioProperty) {
        mGraphicAudioProperty = audioProperty;
    }

    public boolean isAudioAttached() {
        return mGraphicAudioProperty.editorAudioPath != null && mGraphicAudioProperty.editorDuration > 0;
    }

    public boolean isInVideoContext() {
        return isAudioAttached() || isGIFsAttached();
    }

    public boolean isImagesAttached() {
        return mGraphicImageList != null && mGraphicImageList.size() > 0;
    }

    public boolean isGIFsAttached() {
        if (!isImagesAttached()) return false;

        for (GraphicImageProperty p : mGraphicImageList) {
            if (p.imageType == GraphicImageProperty.GIF_IMAGE) return true;
        }
        return false;
    }

    public boolean isTextAttached() {
        return mGraphicTextList != null && mGraphicTextList.size() > 0;
    }

    public String toJson() {
        Hello hello = new Hello();
        hello.audioAssets = OJsonMapper.toJSON(mGraphicAudioProperty);
        hello.backgroundProperty = OJsonMapper.toJSON(mMediaBackgroundProperty);
        hello.graphicTextList = OJsonMapper.toJSON(mGraphicTextList);
        hello.graphicImageList = OJsonMapper.toJSON(mGraphicImageList);
        return OJsonMapper.toJSON(hello);
    }

    public static class Hello {//NOTE: fields must be public
        public String graphicTextList, backgroundProperty, audioAssets, graphicImageList;
    }
}
