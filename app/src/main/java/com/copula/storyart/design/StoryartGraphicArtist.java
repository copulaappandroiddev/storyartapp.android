package com.copula.storyart.design;

import android.content.Context;
import android.graphics.*;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import com.copula.storyart.app.creator.Configuration;

import java.util.List;
import java.util.TreeMap;

/**
 * Created by heeleaz on 10/30/17.
 */

@Deprecated
public class StoryartGraphicArtist extends StoryartDrawingBoard {
    private final int screenWidth, screenHeight;
    private Context mContext;

    private MediaBackgroundProperty mBackgroundProperty;
    private StoryartWatermarkHandler mStoryartWatermarkHandler;
    private TreeMap<Integer, Object> placementItemList = new TreeMap<>();

    public StoryartGraphicArtist(Context ctx, int screenWidth, int screenHeight) {
        this.mContext = ctx;

        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
    }

    public StoryartGraphicArtist(Context ctx) {
        this(ctx, Configuration.DISPLAY_WIDTH, Configuration.DISPLAY_HEIGHT);
    }

    public void setBackgroundProperty(MediaBackgroundProperty backgroundProperty) {
        this.mBackgroundProperty = backgroundProperty;
    }

    public void setAppMontage(StoryartWatermarkHandler handler) {
        this.mStoryartWatermarkHandler = handler;
    }

    public void putGraphicText(List<GraphicTextProperty> list) {
        for (GraphicTextProperty textProperty : list) {
            placementItemList.put(textProperty.placementPosition, textProperty);
        }
    }

    public void putGraphicImage(List<GraphicImageProperty> list) {
        for (GraphicImageProperty imageProperty : list) {
            placementItemList.put(imageProperty.placementPosition, imageProperty);
        }
    }

    private void drawBackground(Canvas canvas, MediaBackgroundProperty background) {
        if (background.source == MediaBackgroundProperty.COLOR) {
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG);
            paint.setColor(Color.parseColor(background.backgroundUrl));
            canvas.drawRect(new Rect(0, 0, screenWidth, screenHeight), paint);
            return;
        }


        Bitmap bitmap = null;
        Rect[] bitmapDrawScale = null;
        if (background.source == MediaBackgroundProperty.GALLERY_IMAGE
                || background.source == MediaBackgroundProperty.CAMERA_IMAGE) {
            bitmap = background.asGalleryImage();
            bitmapDrawScale = fixCenterScale(bitmap.getWidth(), bitmap.getHeight());
        } else if (background.source == MediaBackgroundProperty.WALLPAPER) {
            bitmap = background.asWallpaper();
            bitmapDrawScale = centerCropScale(bitmap.getWidth(), bitmap.getHeight());
        }

        if (bitmap != null) {
            Rect dest = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            canvas.drawBitmap(bitmap, null, dest, null);
        }
    }

    private Rect[] fixCenterScale(int imageWidth, int imageHeight) {
        int bitmapWidth = screenWidth;
        int bitmapHeight = (int) (imageHeight * (bitmapWidth / (float) imageWidth));

        int t = (screenHeight >> 1) - (bitmapHeight >> 1);
        int l = (screenWidth >> 1) - (bitmapWidth >> 1);
        Rect dest = new Rect(l, t, l + bitmapWidth, t + bitmapHeight);

        return new Rect[]{null, dest};
    }

    private Rect[] centerCropScale(int imageWidth, int imageHeight) {
        int cropHeight = Math.min(screenHeight, imageHeight);
        int cropWidth = (int) (((float) screenWidth / (float) screenHeight) * cropHeight);

        int left = (imageWidth >> 1) - (cropWidth >> 1);
        int top = (imageHeight >> 1) - (cropHeight >> 1);
        int right = left + cropWidth;
        int bottom = top + cropHeight;

        Rect src = new Rect(left, top, right, bottom);
        Rect dst = new Rect(0, 0, screenWidth, screenHeight);
        return new Rect[]{src, dst};
    }

//    @Deprecated
//    private void drawGraphicText(Canvas canvas, GraphicTextProperty textProperty) {
//        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG);
//        paint.setColor(textProperty.textColor);
//        paint.setTextAlign(Paint.Align.CENTER);
//        paint.setTextSize(textProperty.textSize);
//        paint.setTypeface(Typeface.createFromAsset(mContext.getAssets(), textProperty.typeface));
//
//
//        StaticLayout textLayout = new StaticLayout(textProperty.text, new TextPaint(paint), textProperty.width,
//                Layout.Alignment.ALIGN_NORMAL, textProperty.spacingMult, 0.0f, false);
//
//        canvas.save();
//
//        //translate to restore id in rotation instance
//        canvas.rotate(textProperty.rotationAngle, textProperty.xPosition, textProperty.yPosition);
//
//        int dx = (int) ((textProperty.width >> 1) + textProperty.xPosition);
//        canvas.translate(dx, textProperty.yPosition);
//        textLayout.draw(canvas);
//        canvas.restore();
//    }

    private void drawImages(Canvas canvas, GraphicImageProperty image) {
//        Bitmap bitmap = image.toBitmap(mContext);
//        if (bitmap != null) {
//            int t = image.y;
//            int l = image.x;
//
//            canvas.save();
//
//            canvas.rotate(image.rotationAngle, image.x, image.y);
//            Rect dest = new Rect(l, t, l + image.width, t + image.height);
//            canvas.drawBitmap(bitmap, null, dest, null);
//
//            canvas.restore();
//        }
    }

    public Bitmap draw(int canvasWidth, int canvasHeight) {
        canvasWidth = canvasWidth <= 0 ? screenWidth : canvasWidth;
        canvasHeight = canvasHeight <= 0 ? screenHeight : canvasHeight;

        Bitmap bitmap = Bitmap.createBitmap(canvasWidth, canvasHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);

        float scaleXY = canvasWidth / (float) screenWidth;
        float scaledTranslateY = (screenHeight - canvasHeight) * (scaleXY / 2f);

        canvas.scale(scaleXY, scaleXY, 0, scaledTranslateY);
        drawBackground(canvas, mBackgroundProperty);
        for (Integer key : placementItemList.keySet()) {
            Object data = placementItemList.get(key);

            if (data instanceof GraphicTextProperty) {
//                drawGraphicText(canvas, (GraphicTextProperty) data);
            } else if (data instanceof GraphicImageProperty) {
                drawImages(canvas, (GraphicImageProperty) data);
            }
        }

        if (mStoryartWatermarkHandler != null) {
            drawAppMontage(canvas, mStoryartWatermarkHandler, screenWidth, screenHeight);
        }
        return bitmap;
    }

    /*private void drawStickerText(Canvas canvas, StickerTextProperty property) {
        Paint backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG);
        backgroundPaint.setColor(property.backgroundColor);

        float l = property.x;
        float t = property.y;
        float r = l + property.width;
        float b = t + property.height;
        canvas.drawRoundRect(new RectF(l, t, r, b), 5, 5, backgroundPaint);

        Bitmap bm = property.getScaledBitmap(mContext);
        canvas.drawBitmap(bm, property.iconX, property.iconY, null);

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG);
        paint.setColor(property.textColor);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(property.textSize);

        paint.setTypeface(Typeface
                .createFromAsset(mContext.getAssets(), property.typeface));
        StaticLayout textLayout = new StaticLayout(property.text,
                new TextPaint(paint), property.textWidth, Layout.Alignment.ALIGN_NORMAL,
                1.0f, 0.0f, false);
        canvas.save();

        int dy = (int) (property.textY + ((textLayout.getLineTop(1)) >> 1));
        int dx = (int) ((property.textWidth >> 1) + property.textX);
        canvas.translate(dx, dy);
        textLayout.draw(canvas);

        canvas.restore();
    }*/
}
