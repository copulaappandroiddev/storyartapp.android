package com.copula.storyart.design;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import com.storyart.support.widget.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by heeleaz on 10/21/17.
 */
public class TypefaceLibrary implements Iterator<String> {
    private static final Logger logger = Logger.getInstance("TypefaceLibrary");

    public static final String DOSIS_SEMI_BOLD = "fonts/00-Dosis-SemiBold.ttf";
    public static final String ARCHIVO_BLACK = "fonts/02-ArchivoBlack.otf";
    public static final String SQUADAONE = "SquadaOne-Regular.ttf";

    private static List<String> typefaceList = new ArrayList<>();
    private int typefaceIndex = 0;

    private TypefaceLibrary(Context context) {
        if (typefaceList.size() == 0) {
            try {
                String[] paths = context.getAssets().list("fonts");
                for (String s : paths) typefaceList.add("fonts/" + s);
            } catch (IOException e) {
                typefaceList.add("monospace");
            }
        }
    }

    public static TypefaceLibrary instantiate(Context context) {
        return new TypefaceLibrary(context);
    }

    @Nullable
    public static Typeface typefaceFromAsset(Context context, String path) {
        try {
            return Typeface.createFromAsset(context.getAssets(), path);
        } catch (Exception e) {
            logger.e(e);
            return Typeface.SANS_SERIF;
        }
    }

    @Override
    public boolean hasNext() {
        return true;
    }

    @Override
    public String next() {
        return typefaceList.get(typefaceIndex++ % typefaceList.size());
    }
}
