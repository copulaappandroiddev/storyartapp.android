package com.copula.storyart.design;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import com.storyart.support.util.ViewUtils;

/**
 * Created by heeleaz on 11/1/17.
 */
public class StoryartWatermarkHandler extends GraphicPlaceableItem {
    public final int textColor = Color.WHITE;
    public final String text = "Storyart", montageText = "Made with";
    public int montageTextSize = ViewUtils.dpToPx(13);
    public int textSize = ViewUtils.dpToPx(20);

    public int xMargin = ViewUtils.dpToPx(15);
    public int yMargin = ViewUtils.dpToPx(20);
    public int montageXMargin = ViewUtils.dpToPx(5);
    public int montageYMargin = ViewUtils.dpToPx(37);

    private Context mContext;

    public StoryartWatermarkHandler(Context context) {
        this.mContext = context;
    }

    public Typeface getTypeface() {
        String typeface = TypefaceLibrary.ARCHIVO_BLACK;
        return Typeface.createFromAsset(mContext.getAssets(), typeface);
    }
}
