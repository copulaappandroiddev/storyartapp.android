package com.storyart.emojikeyboard;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.GridView;
import com.storyart.emojikeyboard.emoji.EmojiCategory;
import com.storyart.emojikeyboard.listeners.OnEmojiClickListener;
import com.vanniktech.emoji.R;

class EmojiGridView extends GridView {
    EmojiAdapter emojiAdapter;

    EmojiGridView(final Context context) {
        super(context);

        final Resources resources = getResources();
        final int width = resources.getDimensionPixelSize(R.dimen.emoji_grid_view_column_width);
        final int spacing = resources.getDimensionPixelSize(R.dimen.emoji_grid_view_spacing);

        setColumnWidth(width);
        setHorizontalSpacing(spacing);
        setVerticalSpacing(spacing);
        setPadding(spacing, spacing, spacing, spacing);
        setNumColumns(AUTO_FIT);
        setClipToPadding(false);
        setVerticalScrollBarEnabled(false);
    }

    public EmojiGridView init(@Nullable final OnEmojiClickListener onEmojiClickListener,
                              @NonNull final EmojiCategory category) {
        emojiAdapter = new EmojiAdapter(getContext(), category.getEmojis(), onEmojiClickListener);

        setAdapter(emojiAdapter);

        return this;
    }
}
