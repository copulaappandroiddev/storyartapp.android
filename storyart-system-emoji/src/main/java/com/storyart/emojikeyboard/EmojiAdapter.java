package com.storyart.emojikeyboard;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.storyart.emojikeyboard.emoji.Emoji;
import com.storyart.emojikeyboard.listeners.OnEmojiClickListener;
import com.vanniktech.emoji.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import static com.storyart.emojikeyboard.Utils.checkNotNull;

final class EmojiAdapter extends ArrayAdapter<Emoji> {
    @Nullable
    private final OnEmojiClickListener listener;

    EmojiAdapter(@NonNull final Context context, @NonNull final Emoji[] emojis, @Nullable final OnEmojiClickListener listener) {
        super(context, 0, new ArrayList<>(Arrays.asList(emojis)));
        this.listener = listener;
    }

    @NonNull
    @Override
    public View getView(final int position, final View convertView, @NonNull final ViewGroup parent) {
        TextView textView = (TextView) convertView;

        final Emoji emoji = checkNotNull(getItem(position), "emoji == null");

        if (textView == null) {
            textView = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.emoji_item, parent, false);
        }

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onEmojiClick(emoji);
            }
        });
        textView.setText(emoji.getUnicode());
        return textView;
    }

    void updateEmojis(final Collection<Emoji> emojis) {
        clear();
        addAll(emojis);
        notifyDataSetChanged();
    }
}
