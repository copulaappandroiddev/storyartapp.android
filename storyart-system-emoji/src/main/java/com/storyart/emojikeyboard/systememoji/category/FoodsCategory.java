package com.storyart.emojikeyboard.systememoji.category;

import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import com.vanniktech.emoji.R;
import com.storyart.emojikeyboard.emoji.Emoji;
import com.storyart.emojikeyboard.emoji.EmojiCategory;

@SuppressWarnings("PMD.MethodReturnsInternalArray")
public final class FoodsCategory implements EmojiCategory {
    private static final Emoji[] DATA = new Emoji[]{
            Emoji.fromCodePoint(0x1F347),
            Emoji.fromCodePoint(0x1F348),
            Emoji.fromCodePoint(0x1F349),
            Emoji.fromCodePoint(0x1F34A),
            Emoji.fromCodePoint(0x1F34B),
            Emoji.fromCodePoint(0x1F34C),
            Emoji.fromCodePoint(0x1F34D),
            Emoji.fromCodePoint(0x1F34E),
            Emoji.fromCodePoint(0x1F34F),
            Emoji.fromCodePoint(0x1F350),
            Emoji.fromCodePoint(0x1F351),
            Emoji.fromCodePoint(0x1F352),
            Emoji.fromCodePoint(0x1F353),
            Emoji.fromCodePoint(0x1F345),
            Emoji.fromCodePoint(0x1F346),
            Emoji.fromCodePoint(0x1F33D),
            Emoji.fromCodePoint(new int[]{0x1F336, 0xFE0F}),
            Emoji.fromCodePoint(0x1F330),
            Emoji.fromCodePoint(0x1F9C0),
            Emoji.fromCodePoint(0x1F356),
            Emoji.fromCodePoint(0x1F354),
            Emoji.fromCodePoint(0x1F35F),
            Emoji.fromCodePoint(0x1F355),
            Emoji.fromCodePoint(0x1F32D),
            Emoji.fromCodePoint(0x1F32E),
            Emoji.fromCodePoint(0x1F32F),
            Emoji.fromCodePoint(0x1F373),
            Emoji.fromCodePoint(0x1F372),
            Emoji.fromCodePoint(0x1F37F),
            Emoji.fromCodePoint(0x1F371),
            Emoji.fromCodePoint(0x1F358),
            Emoji.fromCodePoint(0x1F359),
            Emoji.fromCodePoint(0x1F35A),
            Emoji.fromCodePoint(0x1F35B),
            Emoji.fromCodePoint(0x1F35C),
            Emoji.fromCodePoint(0x1F35D),
            Emoji.fromCodePoint(0x1F360),
            Emoji.fromCodePoint(0x1F362),
            Emoji.fromCodePoint(0x1F363),
            Emoji.fromCodePoint(0x1F364),
            Emoji.fromCodePoint(0x1F365),
            Emoji.fromCodePoint(0x1F361),
            Emoji.fromCodePoint(0x1F366),
            Emoji.fromCodePoint(0x1F367),
            Emoji.fromCodePoint(0x1F368),
            Emoji.fromCodePoint(0x1F369),
            Emoji.fromCodePoint(0x1F36A),
            Emoji.fromCodePoint(0x1F382),
            Emoji.fromCodePoint(0x1F370),
            Emoji.fromCodePoint(0x1F36B),
            Emoji.fromCodePoint(0x1F36C),
            Emoji.fromCodePoint(0x1F36D),
            Emoji.fromCodePoint(0x1F36E),
            Emoji.fromCodePoint(0x1F36F),
            Emoji.fromCodePoint(0x1F37C),
            Emoji.fromCodePoint(0x2615),
            Emoji.fromCodePoint(0x1F375),
            Emoji.fromCodePoint(0x1F376),
            Emoji.fromCodePoint(0x1F37E),
            Emoji.fromCodePoint(0x1F377),
            Emoji.fromCodePoint(0x1F378),
            Emoji.fromCodePoint(0x1F379),
            Emoji.fromCodePoint(0x1F37A),
            Emoji.fromCodePoint(0x1F37B),
            Emoji.fromCodePoint(new int[]{0x1F37D, 0xFE0F}),
            Emoji.fromCodePoint(0x1F374),
            Emoji.fromCodePoint(0x1F52A),
            Emoji.fromCodePoint(0x1F3FA)
    };

    @Override
    @NonNull
    public Emoji[] getEmojis() {
        return DATA;
    }

    @Override
    @DrawableRes
    public int getIcon() {
        return R.drawable.emoji_google_category_foodanddrink;
    }
}
