package com.storyart.emojikeyboard.systememoji.category;

import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import com.vanniktech.emoji.R;
import com.storyart.emojikeyboard.emoji.Emoji;
import com.storyart.emojikeyboard.emoji.EmojiCategory;

@SuppressWarnings("PMD.MethodReturnsInternalArray")
public final class ActivitiesCategory implements EmojiCategory {
    private static final Emoji[] DATA = new Emoji[]{
            Emoji.fromCodePoint(0x1F383),
            Emoji.fromCodePoint(0x1F384),
            Emoji.fromCodePoint(0x1F386),
            Emoji.fromCodePoint(0x1F387),
            Emoji.fromCodePoint(0x2728),
            Emoji.fromCodePoint(0x1F388),
            Emoji.fromCodePoint(0x1F389),
            Emoji.fromCodePoint(0x1F38A),
            Emoji.fromCodePoint(0x1F38B),
            Emoji.fromCodePoint(0x1F38D),
            Emoji.fromCodePoint(0x1F38E),
            Emoji.fromCodePoint(0x1F38F),
            Emoji.fromCodePoint(0x1F390),
            Emoji.fromCodePoint(0x1F391),
            Emoji.fromCodePoint(0x1F380),
            Emoji.fromCodePoint(0x1F381),
            Emoji.fromCodePoint(new int[]{0x1F397, 0xFE0F}),
            Emoji.fromCodePoint(new int[]{0x1F39F, 0xFE0F}),
            Emoji.fromCodePoint(0x1F3AB),
            Emoji.fromCodePoint(new int[]{0x1F396, 0xFE0F}),
            Emoji.fromCodePoint(0x1F3C6),
            Emoji.fromCodePoint(0x1F3C5),
            Emoji.fromCodePoint(0x26BD),
            Emoji.fromCodePoint(0x26BE),
            Emoji.fromCodePoint(0x1F3C0),
            Emoji.fromCodePoint(0x1F3D0),
            Emoji.fromCodePoint(0x1F3C8),
            Emoji.fromCodePoint(0x1F3C9),
            Emoji.fromCodePoint(0x1F3BE),
            Emoji.fromCodePoint(0x1F3B1),
            Emoji.fromCodePoint(0x1F3B3),
            Emoji.fromCodePoint(0x1F3CF),
            Emoji.fromCodePoint(0x1F3D1),
            Emoji.fromCodePoint(0x1F3D2),
            Emoji.fromCodePoint(0x1F3D3),
            Emoji.fromCodePoint(0x1F3F8),
            Emoji.fromCodePoint(0x26F3),
            Emoji.fromCodePoint(0x1F3AF),
            Emoji.fromCodePoint(new int[]{0x26F8, 0xFE0F}),
            Emoji.fromCodePoint(0x1F3A3),
            Emoji.fromCodePoint(0x1F3BD),
            Emoji.fromCodePoint(0x1F3BF),
            Emoji.fromCodePoint(0x1F3AE),
            Emoji.fromCodePoint(new int[]{0x1F579, 0xFE0F}),
            Emoji.fromCodePoint(0x1F3B2),
            Emoji.fromCodePoint(new int[]{0x2660, 0xFE0F}),
            Emoji.fromCodePoint(new int[]{0x2665, 0xFE0F}),
            Emoji.fromCodePoint(new int[]{0x2666, 0xFE0F}),
            Emoji.fromCodePoint(new int[]{0x2663, 0xFE0F}),
            Emoji.fromCodePoint(0x1F0CF),
            Emoji.fromCodePoint(0x1F004),
            Emoji.fromCodePoint(0x1F3B4)


    };

    @Override
    @NonNull
    public Emoji[] getEmojis() {
        return DATA;
    }

    @Override
    @DrawableRes
    public int getIcon() {
        return R.drawable.emoji_google_category_activities;
    }
}
