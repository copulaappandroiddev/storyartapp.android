package com.storyart.emojikeyboard.systememoji;

import android.support.annotation.NonNull;
import com.storyart.emojikeyboard.EmojiProvider;
import com.storyart.emojikeyboard.emoji.EmojiCategory;
import com.storyart.emojikeyboard.systememoji.category.*;

public final class SystemEmojiProvider implements EmojiProvider {
    @Override
    @NonNull
    public EmojiCategory[] getCategories() {
        return new EmojiCategory[]{
                new SmileysCategory(), new NatureCategory(), new FoodsCategory(),
                new ActivitiesCategory(), new PlacesCategory(), new ObjectsCategory(),
                new SymbolsCategory(), new FlagsCategory()
        };
    }
}
