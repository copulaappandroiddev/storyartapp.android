package com.storyart.emojikeyboard;

import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.graphics.PaintCompat;
import com.storyart.emojikeyboard.emoji.Emoji;
import com.storyart.emojikeyboard.emoji.EmojiCategory;

import java.util.LinkedHashMap;
import java.util.Map;

import static com.storyart.emojikeyboard.Utils.checkNotNull;

/**
 * EmojiManager where an EmojiProvider can be installed for further usage.
 */
@SuppressWarnings("PMD.ForLoopCanBeForeach")
public final class EmojiManager {
    private static final EmojiManager INSTANCE = new EmojiManager();
    private static final int GUESSED_UNICODE_AMOUNT = 3000;
    private static boolean installed = false;
    private final Map<String, Emoji> emojiMap = new LinkedHashMap<>(GUESSED_UNICODE_AMOUNT);
    private EmojiCategory[] categories;

    private EmojiManager() {
        // No instances apart from singleton.
    }

    public static EmojiManager getInstance() {
        return INSTANCE;
    }

    /**
     * Installs the given EmojiProvider.
     * <p>
     * NOTE: That only one can be present at any time.
     *
     * @param provider the provider that should be installed.
     */
    public static void install(@NonNull final EmojiProvider provider) {
        if (installed) return;

        INSTANCE.categories = checkNotNull(provider.getCategories(), "categories == null");
        INSTANCE.emojiMap.clear();

        int categoriesSize = INSTANCE.categories.length;
        for (int i = 0; i < categoriesSize; i++) {
            final Emoji[] emojis = checkNotNull(INSTANCE.categories[i].getEmojis(), "emojies == null");
            for (final Emoji emoji : emojis) {
                boolean hasGlyph = PaintCompat.hasGlyph(new Paint(), emoji.getUnicode());
                if (hasGlyph) {
                    String unicode = emoji.getUnicode();
                    INSTANCE.emojiMap.put(unicode, emoji);
                }
            }
        }

        installed = true;
    }

    /**
     * Destroys the EmojiManager. This means that all internal data structures are released as well as
     * all data associated with installed {@link Emoji}s. For the existing {@link EmojiProvider}s this
     * means the memory-heavy emoji sheet.
     *
     * @see #destroy()
     */
    public static void destroy() {
        release();

        INSTANCE.emojiMap.clear();
        INSTANCE.categories = null;
        installed = false;
    }

    /**
     * Releases all data associated with installed {@link Emoji}s. For the existing {@link EmojiProvider}s this
     * means the memory-heavy emoji sheet.
     * <p>
     * In contrast to {@link #destroy()}, this does <b>not</b> destroy the internal
     * data structures and thus, you do not need to {@link #install(EmojiProvider)} again before using the EmojiManager.
     *
     * @see #destroy()
     */
    public static void release() {
        for (final Emoji emoji : INSTANCE.emojiMap.values()) {
            emoji.destroy();
        }
        installed = false;
    }

    EmojiCategory[] getCategories() {
        verifyInstalled();
        return categories; // NOPMD
    }

    @Nullable
    Emoji findEmoji(@NonNull final CharSequence candidate) {
        verifyInstalled();

        // We need to call toString on the candidate, since the emojiMap may not find the requested entry otherwise, because
        // the type is different.
        return emojiMap.get(candidate.toString());
    }

    public void verifyInstalled() {
        if (categories == null) {
            throw new IllegalStateException("Please install an EmojiProvider through the EmojiManager.install() method first.");
        }
    }
}
