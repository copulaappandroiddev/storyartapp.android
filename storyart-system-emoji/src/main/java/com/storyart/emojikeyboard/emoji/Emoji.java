package com.storyart.emojikeyboard.emoji;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.Serializable;

public class Emoji implements Serializable {
    private static final long serialVersionUID = 3L;

    @NonNull
    private final String unicode;
    @Nullable
    private Emoji base;


    public Emoji(@NonNull String unicode) {
        this.unicode = unicode;
    }

    public static Emoji fromString(String string) {
        return new Emoji(string);
    }

    public static Emoji fromCodePoint(int codePoint) {
        return fromCodePoint(new int[]{codePoint});
    }

    public static Emoji fromCodePoint(int[] codePoint) {
        return new Emoji(new String(codePoint, 0, codePoint.length));
    }


    public static Emoji fromChar(char c) {
        return new Emoji(Character.toString(c));
    }

    @NonNull
    public String getUnicode() {
        return unicode;
    }

    @NonNull
    public Emoji getBase() {
        Emoji result = this;

        while (result.base != null) {
            result = result.base;
        }

        return result;
    }

    public int getLength() {
        return unicode.length();
    }


    public void destroy() {
        // For inheritors to override.
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final Emoji emoji = (Emoji) o;

        return unicode.equals(emoji.unicode);
    }

    @Override
    public int hashCode() {
        return unicode.hashCode();
    }
}
