package com.storyart.emojikeyboard.listeners;

import android.view.View;

public interface OnEmojiBackspaceClickListener {
  void onEmojiBackspaceClick(View v);
}
