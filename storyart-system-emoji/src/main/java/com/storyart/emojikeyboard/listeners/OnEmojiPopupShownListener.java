package com.storyart.emojikeyboard.listeners;

public interface OnEmojiPopupShownListener {
  void onEmojiPopupShown();
}
