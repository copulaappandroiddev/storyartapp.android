package com.storyart.emojikeyboard.listeners;

import android.support.annotation.Px;

public interface OnSoftKeyboardOpenListener {
  void onKeyboardOpen(@Px int keyBoardHeight);
}
