package com.storyart.emojikeyboard.listeners;

import android.support.annotation.NonNull;
import com.storyart.emojikeyboard.emoji.Emoji;

public interface OnEmojiClickListener {
    void onEmojiClick(@NonNull Emoji imageView);
}
