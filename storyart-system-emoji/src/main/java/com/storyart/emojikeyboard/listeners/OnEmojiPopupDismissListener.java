package com.storyart.emojikeyboard.listeners;

public interface OnEmojiPopupDismissListener {
  void onEmojiPopupDismiss();
}
