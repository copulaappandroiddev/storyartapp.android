package com.storyart.emojikeyboard.listeners;

public interface OnSoftKeyboardCloseListener {
  void onKeyboardClose();
}
