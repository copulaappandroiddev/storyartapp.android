package com.storyart.emojikeyboard;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.view.KeyEvent;

public class EmojiEditText extends AppCompatEditText {
    private String fontPath;

    public EmojiEditText(Context context) {
        super(context);
    }

    public EmojiEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EmojiEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @CallSuper
    public void backspace() {
        final KeyEvent event = new KeyEvent(0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
        dispatchKeyEvent(event);
    }

    public void setTypefaceFromAssetPath(@Nullable String fontPath) {
        if ((this.fontPath = fontPath) != null) {
            Typeface font = Typeface.createFromAsset(getContext().getAssets(), fontPath);
            setTypeface(font);
        }
    }

    public String getFontPath() {
        return fontPath;
    }
}
