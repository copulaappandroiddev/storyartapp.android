package com.storyart.emojikeyboard;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PorterDuff;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import com.storyart.emojikeyboard.emoji.EmojiCategory;
import com.storyart.emojikeyboard.listeners.OnEmojiBackspaceClickListener;
import com.storyart.emojikeyboard.listeners.OnEmojiClickListener;
import com.storyart.emojikeyboard.listeners.RepeatListener;
import com.vanniktech.emoji.R;

import java.util.concurrent.TimeUnit;

@SuppressLint("ViewConstructor")
public final class EmojiView extends LinearLayout implements ViewPager.OnPageChangeListener {
    private static final long INITIAL_INTERVAL = TimeUnit.SECONDS.toMillis(1) / 2;
    private static final int NORMAL_INTERVAL = 50;

    @ColorInt
    private final int emojiTabIconSelected;
    @ColorInt
    private final int emojiTabIconColor;

    private final ImageButton[] emojiTabs;
    private final EmojiPagerAdapter emojiPagerAdapter;

    @Nullable
    private OnEmojiBackspaceClickListener onEmojiBackspaceClickListener;

    private int emojiTabLastSelectedIndex = -1;

    EmojiView(final Context context, final OnEmojiClickListener clickListener, @NonNull final RecentEmoji recentEmoji) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.emoji_view, this);

        setOrientation(VERTICAL);
        setBackgroundColor(ContextCompat.getColor(context, R.color.emoji_background));

        emojiTabIconColor = ContextCompat.getColor(context, R.color.emoji_icons);
        emojiTabIconSelected = ContextCompat.getColor(context, R.color.emoji_icons_selected);

        final ViewPager emojisPagerView = findViewById(R.id.emojis_pager);
        emojisPagerView.addOnPageChangeListener(this);

        final LinearLayout emojisTabView = findViewById(R.id.emojis_tab);

        final EmojiCategory[] categories = EmojiManager.getInstance().getCategories();
        emojiTabs = new ImageButton[categories.length + 1];

        emojiTabs[0] = inflateButton(context, R.drawable.emoji_recent, emojisTabView);
        emojiTabs[0].setOnClickListener(new EmojiTabsClickListener(emojisPagerView, 0));
        for (int i = 0; i < categories.length; i++) {
            int p = i + 1;
            emojiTabs[p] = inflateButton(context, categories[i].getIcon(), emojisTabView);
            emojiTabs[p].setOnClickListener(new EmojiTabsClickListener(emojisPagerView, p));
        }

        ImageButton backspace = findViewById(R.id.btn_backspace);
        backspace.setColorFilter(emojiTabIconColor, PorterDuff.Mode.SRC_IN);
        backspace.setOnTouchListener(new RepeatListener(INITIAL_INTERVAL, NORMAL_INTERVAL, new OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (onEmojiBackspaceClickListener != null) {
                    onEmojiBackspaceClickListener.onEmojiBackspaceClick(view);
                }
            }
        }));

        emojiPagerAdapter = new EmojiPagerAdapter(clickListener, recentEmoji);
        emojisPagerView.setAdapter(emojiPagerAdapter);

        final int startIndex = emojiPagerAdapter.numberOfRecentEmojis() > 0 ? 0 : 1;
        emojisPagerView.setCurrentItem(startIndex);
        onPageSelected(startIndex);
    }


    public void setOnEmojiBackspaceClickListener(@Nullable final OnEmojiBackspaceClickListener onEmojiBackspaceClickListener) {
        this.onEmojiBackspaceClickListener = onEmojiBackspaceClickListener;
    }

    private ImageButton inflateButton(final Context context, @DrawableRes final int icon, final ViewGroup parent) {
        final ImageButton button = (ImageButton) LayoutInflater.from(context).inflate(R.layout.emoji_category, parent, false);

        button.setImageDrawable(ContextCompat.getDrawable(context, icon));
        button.setColorFilter(emojiTabIconColor, PorterDuff.Mode.SRC_IN);

        parent.addView(button);

        return button;
    }

    @Override
    public void onPageSelected(final int i) {
        if (emojiTabLastSelectedIndex != i) {
            if (i == 0) {
                emojiPagerAdapter.invalidateRecentEmojis();
            }

            if (emojiTabLastSelectedIndex >= 0 && emojiTabLastSelectedIndex < emojiTabs.length) {
                emojiTabs[emojiTabLastSelectedIndex].setSelected(false);
                emojiTabs[emojiTabLastSelectedIndex].setColorFilter(emojiTabIconColor, PorterDuff.Mode.SRC_IN);
            }

            emojiTabs[i].setSelected(true);
            emojiTabs[i].setColorFilter(emojiTabIconSelected, PorterDuff.Mode.SRC_IN);

            emojiTabLastSelectedIndex = i;
        }
    }

    @Override
    public void onPageScrolled(final int i, final float v, final int i2) {
        // No-op.
    }

    @Override
    public void onPageScrollStateChanged(final int i) {
        // No-op.
    }

    private static class EmojiTabsClickListener implements OnClickListener {
        private final ViewPager emojisPager;
        private final int position;

        EmojiTabsClickListener(final ViewPager emojisPager, final int position) {
            this.emojisPager = emojisPager;
            this.position = position;
        }

        @Override
        public void onClick(final View v) {
            emojisPager.setCurrentItem(position);
        }
    }
}
