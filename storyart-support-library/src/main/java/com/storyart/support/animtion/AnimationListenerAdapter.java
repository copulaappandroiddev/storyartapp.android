package com.storyart.support.animtion;

import android.view.animation.Animation;

public abstract class AnimationListenerAdapter implements Animation.AnimationListener {
    @Override
    public void onAnimationEnd(Animation animation) {
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
    }

    @Override
    public void onAnimationStart(Animation animation) {
    }
}
