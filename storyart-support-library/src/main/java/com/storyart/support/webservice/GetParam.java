package com.storyart.support.webservice;

/**
 * Created by eliasigbalajobi on 1/16/16.
 */

import java.util.HashMap;
import java.util.Map;

public class GetParam {
    private static final String AND_DELMER = "&", EQUAL_DELMER = "=";
    private Map<String, String> map = new HashMap<>();

    private static String toUrlQuery(GetParam getParam) {
        Map<String, String> map = getParam.map;

        StringBuilder builder = new StringBuilder();
        for (String key : map.keySet()) {
            String value = map.get(key);
            builder.append(key).append(EQUAL_DELMER).append(value).append(AND_DELMER);
        }
        builder.deleteCharAt(builder.length() - 1);//delete the AND_DELMER
        return builder.toString();
    }

    public GetParam put(String key, boolean value) {
        return put(String.valueOf(key), String.valueOf(value));
    }

    public GetParam put(String key, int value) {
        return put(String.valueOf(key), String.valueOf(value));
    }

    public GetParam put(String key, long value) {
        return put(String.valueOf(key), String.valueOf(value));
    }

    public GetParam put(String key, String value) {
        if (key == null)
            throw new IllegalArgumentException("Key cannot be null");

        this.map.put(key, value);
        return this;
    }

    @Override
    public String toString() {
        return GetParam.toUrlQuery(this);
    }
}

