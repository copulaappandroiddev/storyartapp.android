package com.storyart.support.webservice;

import okhttp3.*;

import java.io.File;
import java.util.concurrent.TimeUnit;

/**
 * Created by eliasigbalajobi on 2/24/16.
 */
public class NettyApiProvider {
    private static final String POST = "POST", GET = "GET";
    private static final int SIZE_OF_CACHE = 1024 * 28;

    private static final int defConnectionTimeout = 20;
    private String endpoint;
    private OkHttpClient.Builder connectionBuilder;

    public NettyApiProvider(String endpoint) {
        this.endpoint = endpoint;

        connectionBuilder = new OkHttpClient.Builder();
        connectionBuilder.connectTimeout(defConnectionTimeout, TimeUnit.SECONDS);
        connectionBuilder.readTimeout(defConnectionTimeout, TimeUnit.SECONDS);
    }

    public void cache(File cacheDir, int size) {
        Cache responseCache = new Cache(cacheDir, (size == 0) ? SIZE_OF_CACHE : size);
        connectionBuilder.cache(responseCache);
    }

    public void interceptor(Interceptor interceptor) {
        connectionBuilder.addInterceptor(interceptor);
    }

    private Result request(String method, String url, RequestBody request) throws Exception {
        Request.Builder builder = new Request.Builder();
        builder.url(bind(url)).method(method, request);

        OkHttpClient connection = connectionBuilder.build();
        Response response = connection.newCall(builder.build()).execute();
        return new Result(response.code(), response.body().string());
    }

    public Result get(String url, GetParam params) throws Exception {
        String endpoint = url + ((params != null) ? "?" + params : "");
        return request(GET, endpoint, null);
    }

    private String bind(String url) {
        return (endpoint != null) ? endpoint + url : url;
    }

    public static class Result {
        public String response;
        public int responseCode;

        Result(int responseCode, String response) {
            this.response = response;
            this.responseCode = responseCode;
        }
    }
}
