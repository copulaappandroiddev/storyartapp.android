package com.storyart.support.webservice;

/**
 * Created by eliasigbalajobi on 3/23/16.
 */
public class RestResponse<T> {
    public String statusMessage, nextPageToken;
    public int statusCode;
    public T result;
}
