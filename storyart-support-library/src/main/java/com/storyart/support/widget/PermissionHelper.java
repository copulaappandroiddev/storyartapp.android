package com.storyart.support.widget;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by heeleeaz on 7/24/16.
 */
public class PermissionHelper {
    public static final int RATIONALE_PERM = 2, REQUEST_PERM = 0, PERMITTED = 1;
    private Activity context;

    public PermissionHelper(Activity activity) {
        this.context = activity;
    }

    public static boolean isPermitted(Activity context, String permission) {
        return new PermissionHelper(context).isPermitted(permission);
    }

    private boolean requestPermissionRationale(String permission) {
        return ActivityCompat.shouldShowRequestPermissionRationale(context, permission);
    }

    private boolean isPermitted(String permission) {
        return ContextCompat.checkSelfPermission(
                context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public int requestPermission(String permission, int resultCode) {
        if (isPermitted(context, permission)) return PERMITTED;
        if (requestPermissionRationale(permission)) return RATIONALE_PERM;

        ActivityCompat.requestPermissions(context, new String[]{permission}, resultCode);
        return REQUEST_PERM;

    }
}
