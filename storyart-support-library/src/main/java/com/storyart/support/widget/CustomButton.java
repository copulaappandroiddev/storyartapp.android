package com.storyart.support.widget;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import com.storyart.support.svgresolver.R;
import com.storyart.support.tooltip.SimpleTooltip;

/**
 * Created by heeleaz on 11/9/17.
 */
public class CustomButton extends AppCompatButton {
    private static final String TAG = CustomButton.class.getSimpleName();

    private SimpleTooltip.Builder mSimpleTooltipBuilder;
    private SimpleTooltip mSimpleTooltip;

    public CustomButton(Context context) {
        super(context);
        init(context, null, 0);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        mSimpleTooltipBuilder = new SimpleTooltip.Builder(context);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.View, defStyle, 0);


        float hintMargin = a.getDimension(R.styleable.View_hintMargin, 0);
        if (hintMargin != 0) mSimpleTooltipBuilder.margin(hintMargin);

        int hintView = a.getResourceId(R.styleable.View_hintContentView, 0);
        if (hintView != 0) mSimpleTooltipBuilder.contentView(hintView);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.CustomTextView, defStyle, 0);
        String fontPath = attributes.getString(R.styleable.CustomTextView_fontpath);
        if (fontPath != null) {
            setTypeface(Typeface.createFromAsset(getContext().getAssets(), fontPath));
        }
        attributes.recycle();

        Drawable hintArrowDrawable = a.getDrawable(R.styleable.View_hintArrowDrawable);
        if (hintArrowDrawable != null) {
            mSimpleTooltipBuilder.arrowDrawable(hintArrowDrawable);
        } else mSimpleTooltipBuilder.showArrow(false);

        mSimpleTooltipBuilder.dismissOnInsideTouch(false);
        mSimpleTooltipBuilder.dismissOnOutsideTouch(false);
        a.recycle();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        if (mSimpleTooltipBuilder != null) mSimpleTooltipBuilder.anchorView(this);
    }

    public void showButtonTooltip(String saveKey, int gravity, boolean dismissOnTouch) {
        if (mSimpleTooltip != null && mSimpleTooltip.isShowing()) return;

        SharedPreferences preferences =
                getContext().getSharedPreferences(TAG, Context.MODE_PRIVATE);

        boolean asShownTooltip = preferences.getBoolean(saveKey, false);
        if (!asShownTooltip) {
            try {
                (mSimpleTooltip = mSimpleTooltipBuilder.gravity(gravity).animated(true)
                        .dismissOnOutsideTouch(dismissOnTouch).build()).show();
            } catch (Exception e) {
                //occurs as a result view being already added to parent view
            }
            preferences.edit().putBoolean(saveKey, true).apply();
        }
    }

    public void dismissTooltip() {
        if (mSimpleTooltip != null) mSimpleTooltip.dismiss();
    }
}
