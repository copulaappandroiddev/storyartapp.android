package com.storyart.support.widget;
//  w ww .  j a v  a  2  s  .com

import android.content.Context;
import android.util.AttributeSet;

public class CustomImageView extends android.support.v7.widget.AppCompatImageView {
    private SizeChangedListener mSizeChangedListener;

    public CustomImageView(Context context) {
        super(context);
    }

    public CustomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if (mSizeChangedListener != null)
            mSizeChangedListener.onSizeChanged(w, h, oldw, oldh);
    }

    public void setSizeChangedListener(SizeChangedListener l) {
        mSizeChangedListener = l;
    }

    public interface SizeChangedListener {
        void onSizeChanged(int w, int h, int oldw, int oldh);
    }
}