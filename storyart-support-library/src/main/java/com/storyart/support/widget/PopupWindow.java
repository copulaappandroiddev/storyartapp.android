package com.storyart.support.widget;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListPopupWindow;
import com.storyart.support.svgresolver.R;
import com.storyart.support.util.ViewUtils;

/**
 * Created by heeleaz on 3/24/17.
 */
public class PopupWindow implements AdapterView.OnItemClickListener {
    private ListPopupWindow listPopupWindow;
    private ArrayAdapter<String> mAdapter;

    private PopupClickListener optionListener;

    public PopupWindow(Context context, View anchorView) {
        mAdapter = new ArrayAdapter<>(context, R.layout.simple_spinner_dropdown);

        listPopupWindow = new ListPopupWindow(context);
        listPopupWindow.setAdapter(mAdapter);
        listPopupWindow.setWidth(ViewUtils.dpToPx(140));
        listPopupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        listPopupWindow.setModal(true);
        listPopupWindow.setOnItemClickListener(this);
        listPopupWindow.setAnchorView(anchorView);

    }

    public void show(PopupClickListener listener) {
        this.optionListener = listener;
        listPopupWindow.show();
    }

    public void addString(String value) {
        mAdapter.add(value);
        mAdapter.notifyDataSetChanged();
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        listPopupWindow.dismiss();

        if (optionListener != null)
            optionListener.onAction(position, mAdapter.getItem(position));
    }

    public interface PopupClickListener {
        void onAction(int position, String text);
    }//END
}
