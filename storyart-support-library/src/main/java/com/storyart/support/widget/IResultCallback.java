package com.storyart.support.widget;

import android.content.Intent;

/**
 * Created by heeleaz on 10/29/17.
 */
public interface IResultCallback {
    int RESULT_OK = 1, RESULT_CANCEL = 2;

    void onFragmentResult(int requestCode, int resultCode, Intent data);
}
