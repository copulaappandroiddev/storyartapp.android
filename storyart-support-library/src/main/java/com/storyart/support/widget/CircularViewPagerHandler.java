package com.storyart.support.widget;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

public class CircularViewPagerHandler implements ViewPager.OnPageChangeListener, ViewPager.OnAdapterChangeListener {
    private ViewPager mViewPager;
    private ViewPager.OnPageChangeListener mListener;

    public CircularViewPagerHandler(ViewPager viewPager) {
        mViewPager = viewPager;
        mViewPager.addOnAdapterChangeListener(this);
    }

    public void setOnPageChangeListener(final ViewPager.OnPageChangeListener listener) {
        mListener = listener;
    }

    @Override
    public void onPageSelected(final int position) {
        int itemCount = mViewPager.getAdapter().getCount() - 1;

        if (position == 0) {// skip fake page (first), go to last page
            mViewPager.setCurrentItem(itemCount - 1, false);
        }

        if (position == itemCount) {// skip fake page (last), go to first page
            //jump position 1, and not position 0. Position 0 is the fake page
            mViewPager.setCurrentItem(1, false);
        }

        if (mListener != null) mListener.onPageSelected(position - 1);
    }


    @Override
    public void onPageScrollStateChanged(final int state) {
        if (mListener != null) mListener.onPageScrollStateChanged(state);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (mListener != null) {
            mListener.onPageScrolled(position - 1, positionOffset, positionOffsetPixels);
        }
    }

    @Override
    public void onAdapterChanged(@NonNull ViewPager viewPager, @Nullable PagerAdapter oldAdapter, @Nullable PagerAdapter newAdapter) {
        mViewPager.setCurrentItem(0, false);
    }

    public static abstract class CircularFragmentPagerAdapter extends FragmentPagerAdapter {
        public CircularFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        protected abstract int getCircularCount();

        protected abstract Fragment getCircularItem(int position);

        /**
         * do not use this method. use {@link #getCircularItem(int)} instead,
         * it provides a position in round-robin fashion, so as to avoid OutOfBoundException
         */
        @Override
        public Fragment getItem(int position) {
            return getCircularItem(position % getCircularCount());
        }


        /**
         * do not override this method. use {@link #getCircularCount()}
         */
        @Override
        public int getCount() {
            return getCircularCount() + 2;
        }
    }
}