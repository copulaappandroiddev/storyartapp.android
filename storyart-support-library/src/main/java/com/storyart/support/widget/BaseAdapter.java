package com.storyart.support.widget;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseAdapter<T> extends android.widget.BaseAdapter {
    private List<T> items;
    private Context context;
    private LayoutInflater inflater;

    public BaseAdapter(Context context) {
        this(context, new ArrayList<T>());
    }

    public BaseAdapter(Context context, List<T> items) {
        this.context = context;
        this.items = items;
        inflater = LayoutInflater.from(this.context);

        if (this.items == null || this.items.size() <= 0)
            onItemEmpty();

        addHandlers(items);
    }

    public android.widget.BaseAdapter getAdapter() {
        return this;
    }

    public void addHandler(T handler) {
        items.add(handler);
    }

    public void addHandler(int position, T handler) {
        items.add(position, handler);
    }

    public void addHandlers(List<T> handlers) {
        items.addAll(handlers);
    }

    public void addHandlers(T[] handlers) {
        for (T t : handlers) items.add(t);
    }

    public void addHandlers(int position, List<T> handlers) {
        items.addAll(position, handlers);
    }

    public void addOrReplaceHandler(T handler, int position) {
        if (!this.setHandler(handler)) addHandler(position, handler);
    }

    public void addOrReplaceHandler(T handler) {
        if (!this.setHandler(handler)) addHandler(handler);
    }

    public void addOrReplaceHandlers(List<T> handlers) {
        if (handlers == null) return;
        for (T t : handlers) addOrReplaceHandler(t);
    }

    public boolean setHandler(T handler) {
        int index = items.indexOf(handler);
        if (index > -1) {
            items.remove(index);
            items.add(index, handler);
            return true;
        }
        return false;
    }

    public void removeHandler(T handler) {
        if (handler != null) items.remove(handler);
    }

    public void removeHandler(int index) {
        items.remove(index);
    }

    public void removeHandlers(List<T> handlers) {
        if (handlers != null) items.addAll(handlers);
    }

    protected String getString(int res_id) {
        return context.getString(res_id);
    }

    public Context getContext() {
        return context;
    }

    public abstract View getView(int position, View convertView, LayoutInflater inflater);

    public void onItemEmpty() {
    }

    public int indexOf(T handler) {
        return items.indexOf(handler);
    }

    /**
     * run the specified Runnable in main thread Handler
     */
    protected void runOnUIThread(Runnable runnable) {
        Handler handler = new Handler(context.getMainLooper());
        handler.post(runnable);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public T getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public List<T> getItems() {
        return items;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (items.size() <= 0) {
            onItemEmpty();
            return convertView;
        }
        return this.getView(position, convertView, inflater);
    }

    public void clear() {
        if (items != null) items.clear();
    }

    protected void showOnlyView(final View showView) {
        ViewGroup parent = (ViewGroup) showView.getParent();
        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            parent.getChildAt(i).setVisibility(View.GONE);
        }
        showView.setVisibility(View.VISIBLE);
    }
}
