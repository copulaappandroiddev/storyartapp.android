package com.storyart.support.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.HorizontalScrollView;

public class HorizontalStretchGridView extends GridView {
    public HorizontalStretchGridView(Context context) {
        super(context);
    }

    public HorizontalStretchGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HorizontalStretchGridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public HorizontalStretchGridView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int itemCount = getAdapter().getCount();
        int spacing = getHorizontalSpacing();
        int columnWidth = getColumnWidth();

        ViewGroup.LayoutParams params = getLayoutParams();
        params.width = (spacing + columnWidth) * itemCount;
        setLayoutParams(params);
    }

    @Override
    protected void onAttachedToWindow() {
        if (!(getParent().getParent() instanceof HorizontalScrollView))
            throw new IllegalStateException("view must be a child of a view which is a child of HorizontalScrollView");

        super.onAttachedToWindow();
    }
}
