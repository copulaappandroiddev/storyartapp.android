package com.storyart.support.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.StyleRes;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

public class TabPanel {
    private List<Tab> tabList = new ArrayList<>();
    private Context context;
    private LinearLayout tabBarLayout;
    private HorizontalScrollView horizontalScrollView;
    private TabListener tabListener;
    private int tabViewStyleRes, tabBarStyleRes;

    private int currentTab = 0;
    private int textColorSelected, textColorUnselected;

    public TabPanel(Context context) {
        this.context = context;
    }

    public void setTabListener(TabListener listener) {
        tabListener = listener;
    }

    public Tab newTab(String text) {
        Tab tab = new Tab();
        tab.text = text;
        tabList.add(tab);
        return tab;
    }

    public Tab newTab(Drawable d) {
        Tab tab = new Tab();
        tab.drawable = d;
        tabList.add(tab);
        return tab;
    }

    public void setTextSelectedColor(@ColorInt int color) {
        this.textColorSelected = color;
    }

    public void setTextUnselectedColor(@ColorInt int color) {
        this.textColorUnselected = color;
    }

    public void setTabStyle(@StyleRes int styleRes) {
        this.tabViewStyleRes = styleRes;
    }

    public void setTabBarStyle(@StyleRes int styleRes) {
        this.tabBarStyleRes = styleRes;
    }

    private View __prepare() {
        tabBarLayout = new LinearLayout(new ContextThemeWrapper(context, tabBarStyleRes), null, 0);
        tabBarLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        tabBarLayout.setOrientation(LinearLayout.HORIZONTAL);

        LinearLayout.LayoutParams tabLayoutParams = new LinearLayout.LayoutParams(0, -1);
        tabLayoutParams.weight = 1;
        for (int i = 0; i < tabList.size(); i++) {
            final Tab tab = tabList.get(i);
            if (tab.text != null) tab.view = handleTabAsText(tab, i);
            if (tab.drawable != null) tab.view = handleTabAsImage(tab, i);

            //tabLayoutParams.height = height;
            tab.view.setLayoutParams(tabLayoutParams);
            tabBarLayout.addView(tab.view);
        }

        horizontalScrollView = new HorizontalScrollView(context);
        horizontalScrollView.setLayoutParams(new HorizontalScrollView.LayoutParams(-1, -1));
        horizontalScrollView.setHorizontalScrollBarEnabled(false);
        horizontalScrollView.setFillViewport(true);
        horizontalScrollView.addView(tabBarLayout);
        return horizontalScrollView;
    }

    private View handleTabAsText(final Tab tab, final int position) {
        final Button button = new Button(
                new ContextThemeWrapper(context, tabViewStyleRes), null, 0);

        button.setText(tab.text);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textColorUnselected != 0) {
                    for (Tab t : tabList) {
                        if (t.view instanceof Button) {
                            Button tt = (Button) t.view;
                            tt.setTextColor(textColorUnselected);
                            tt.setSelected(false);
                        }
                    }
                }

                if (textColorSelected != 0) button.setTextColor(textColorSelected);
                tab.view.setSelected(true);

                if (tabListener != null) {
                    tabListener.onSelected(position, tabList.get(position));
                }
            }
        });
        return button;
    }

    private View handleTabAsImage(final Tab tab, final int position) {
        LinearLayout layout = new LinearLayout(
                new ContextThemeWrapper(context, tabViewStyleRes), null, 0);
        layout.setGravity(Gravity.CENTER);


        final ImageView imageView = new ImageView(context);
        layout.addView(imageView);

        imageView.setImageDrawable(tab.drawable);

        layout.setClickable(true);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (Tab t : tabList) {
                    if (t.view instanceof LinearLayout) {
                        ((LinearLayout) t.view).getChildAt(0).setSelected(false);
                    }
                }

                imageView.setSelected(true);
                imageView.performClick();

                tab.view.setSelected(true);

                if (tabListener != null) {
                    tabListener.onSelected(position, tabList.get(position));
                }
            }
        });
        return layout;
    }

    public void selectTab(int position) {
        (tabBarLayout.getChildAt(currentTab)).setSelected(false);

        View button = tabBarLayout.getChildAt(position);
        button.callOnClick();
        button.setSelected(true);


        int width = horizontalScrollView.getMeasuredWidth();
        int right = button.getRight();
        int left = button.getLeft();
        int buttonWidth = (right - left);

        if (position > currentTab) {
            int shiftX = right + buttonWidth;//initial shift to end of button
            if (shiftX > width) {
                horizontalScrollView.smoothScrollTo(shiftX, 0);
            }
        } else {
            int shiftX = left - buttonWidth;
            horizontalScrollView.smoothScrollTo(shiftX, 0);
        }

        this.currentTab = position;
    }

    public View getTabView(int pos) {
        return tabBarLayout.getChildAt(pos);
    }

    public void setText(int pos, int text) {
        setText(pos, context.getString(text));
    }

    public void setText(int pos, String text) {
        Tab tab = tabList.get(pos);
        ((Button) tab.view).setText(tab.text = text);
    }

    public int getTabCount() {
        return tabBarLayout.getChildCount();
    }

    public View getView() {
        return __prepare();
    }

    public interface TabListener {
        void onSelected(int position, Tab tab);
    }

    public class Tab {
        private String text;
        private Drawable drawable;
        private View view;
    }
}
