package com.storyart.support.widget;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewTreeObserver;

public class SoftkeyHeightProvider implements ViewTreeObserver.OnGlobalLayoutListener {
    protected View rootView;
    private Listener mListener;

    public SoftkeyHeightProvider(View rootView) {
        this.rootView = rootView;
    }

    public void observer(Listener listener) {
        this.mListener = listener;
        rootView.getViewTreeObserver().addOnGlobalLayoutListener(this);
    }

    @Override
    public void onGlobalLayout() {
        if (mListener != null) mListener.onVisibilityListener(isKeyboardVisible());
        removeLayoutObserver();
    }

    protected void removeLayoutObserver() {
        try {
            rootView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        } catch (Exception e) {
            //TODO: coming back to resolve
        }
    }

    public int getEditableViewHeight() {
        Rect r = new Rect();
        rootView.getWindowVisibleDisplayFrame(r);
        return r.bottom;
    }

    public boolean isKeyboardVisible() {
        int windowHeight = getEditableViewHeight();
        int heightDiff = rootView.getRootView().getHeight() - windowHeight;

        return heightDiff > 240;
    }

    public int getSoftControlHeight() {
        int windowHeight = getEditableViewHeight();
        return rootView.getRootView().getHeight() - windowHeight;
    }

    public interface Listener {
        void onVisibilityListener(boolean isVisible);
    }
}