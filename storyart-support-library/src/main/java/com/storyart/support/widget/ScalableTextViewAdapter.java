package com.storyart.support.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.TypedValue;
import android.widget.TextView;

public class ScalableTextViewAdapter {
    public static final int NO_LINE_LIMIT = -1;

    private TextView mTextView;
    private SizeTester mSizeTester;

    private RectF mTextRect = new RectF();
    private RectF mAvailableSpaceRect;
    private TextPaint mPaint;
    private float mMaxTextSize;
    private float mSpacingMult = 1.0f, mSpacingAdd = 0.0f, mMinTextSize = 20;
    private int mWidthLimit;
    private int mMaxLines;
    private boolean mInitialized;

    private TextSizeChangedCallback mTextSizeChangedCallback;

    public ScalableTextViewAdapter(TextView textView, SizeTester sizeTester) {
        this.mTextView = textView;
        this.mSizeTester = sizeTester;

        mPaint = new TextPaint(textView.getPaint());
        mMaxTextSize = textView.getTextSize();
        mAvailableSpaceRect = new RectF();

        if (mMaxLines == 0) mMaxLines = NO_LINE_LIMIT;
        mInitialized = true;
    }

    public void setTextSizeChangedCallback(TextSizeChangedCallback callback) {
        this.mTextSizeChangedCallback = callback;
    }

    public void adjustTextSize() {
        if (!mInitialized) return;

        int startSize = (int) mMinTextSize;

        int heightLimit = mTextView.getMeasuredHeight() -
                (mTextView.getCompoundPaddingBottom() + mTextView.getCompoundPaddingTop());
        mWidthLimit = mTextView.getMeasuredWidth() -
                (mTextView.getCompoundPaddingLeft() + mTextView.getCompoundPaddingRight());

        mAvailableSpaceRect.right = mWidthLimit;
        mAvailableSpaceRect.bottom = heightLimit;

        int newTextSize = efficientTextSizeSearch(startSize, (int) mMaxTextSize, mSizeTester, mAvailableSpaceRect);
        int oldTextSize = (int) mTextView.getTextSize();
        mTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, newTextSize);

        if (mTextSizeChangedCallback != null && oldTextSize != newTextSize) {
            mTextSizeChangedCallback.onTextSizeChanged(oldTextSize, newTextSize);
        }
    }

    private int efficientTextSizeSearch(int start, int end,
                                        SizeTester sizeTester, RectF availableSpace) {
        //not found in cache. raw search size, and cache it
        return binarySearch(start, end, sizeTester, availableSpace);
    }

    private int binarySearch(int start, int end, SizeTester sizeTester, RectF availableSpace) {
        int lastBest = start;
        int lo = start;
        int hi = end - 1;
        int mid = 0;
        while (lo <= hi) {
            mid = (lo + hi) >>> 1;
            int midValCmp = sizeTester.onTestSize(mid, availableSpace);
            if (midValCmp < 0) {
                lastBest = lo;
                lo = mid + 1;
            } else if (midValCmp > 0) {
                hi = mid - 1;
                lastBest = hi;
            } else {
                return mid;
            }
        }
        // make sure to return last best
        // this is what should always be returned
        return lastBest;
    }

    public float getMinTextSize() {
        return mMinTextSize;
    }

    public void setMinTextSize(float minTextSize) {
        this.mMinTextSize = minTextSize;
        adjustTextSize();
    }

    public TextPaint getPaint() {
        return mPaint;
    }

    public int getWidthLimit() {
        return mWidthLimit;
    }

    public void setWidthLimit(int widthLimit) {
        this.mWidthLimit = widthLimit;
    }

    public StaticLayout getTextLayout() {
        return new StaticLayout(mTextView.getText(), mPaint, mWidthLimit,
                Layout.Alignment.ALIGN_NORMAL, mSpacingMult, mSpacingAdd, true);
    }

    public int getTextLayoutWidth() {
        return getTextLayout().getWidth();
    }

    public int getTextLayoutHeight() {
        return getTextLayout().getHeight();
    }

    public RectF getAvailableSpaceRect() {
        return mAvailableSpaceRect;
    }

    public int getMaxLines() {
        return mMaxLines;
    }

    public void setMaxLines(int maxLines) {
        this.mMaxLines = maxLines;
        adjustTextSize();
    }

    public RectF getTextRect() {
        return mTextRect;
    }

    public void setLines(int lines) {
        this.mMaxLines = lines;
        adjustTextSize();
    }

    public void setMaxLinesNoAdjust(int maxLines) {
        this.mMaxLines = maxLines;
    }

    public void setMaxTextSize(int unit, float size) {
        Context c = mTextView.getContext();
        Resources r;

        if (c == null) r = Resources.getSystem();
        else r = c.getResources();


        mMaxTextSize = TypedValue.applyDimension(unit, size, r.getDisplayMetrics());
        adjustTextSize();
    }

    public void setLineSpacing(float add, float mult) {
        this.mSpacingMult = mult;
        this.mSpacingAdd = add;
    }

    public void onTextChanged(CharSequence text, int start, int before, int after) {
        try {
            adjustTextSize();
        } catch (Exception ignored) {
        }
    }

    public void onSizeChanged(int w, int h, int ow, int oh) {
        if (w != ow || h != oh) adjustTextSize();
    }

    public void setSingleLine(boolean singleLine) {
        if (singleLine) {
            mMaxLines = 1;
        } else {
            mMaxLines = NO_LINE_LIMIT;
        }
        adjustTextSize();
    }

    private Rect getMinTextViewRect() {
        TextPaint paint = new TextPaint(mPaint);
        paint.setTextSize(getMinTextSize());
        StaticLayout layout = new StaticLayout(mTextView.getText(), paint,
                mWidthLimit, Layout.Alignment.ALIGN_NORMAL, mSpacingMult,
                mSpacingAdd, true);

        return new Rect(0, 0, layout.getWidth(), layout.getHeight());
    }

    public interface SizeTester {
        /**
         * @param suggestedSize  Size of text to be tested
         * @param availableSpace available space in which text must fit
         * @return an integer < 0 if after applying {@code suggestedSize} to
         * text, it takes less space than {@code availableSpace}, > 0
         * otherwise
         */
        int onTestSize(int suggestedSize, RectF availableSpace);
    }

    public interface TextSizeChangedCallback {
        void onTextSizeChanged(int oldSize, int newSize);
    }
}
