package com.storyart.support.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.storyart.support.svgresolver.R;
import com.storyart.support.util.ViewUtils;

public class CustomTextView extends AppCompatTextView {
    private String fontPath;

    public CustomTextView(Context context) {
        this(context, null, 0);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.CustomTextView, defStyle, 0);
        setTypefaceFromAssetPath(attributes.getString(R.styleable.CustomTextView_fontpath));

        boolean marquee = attributes.getBoolean(R.styleable.CustomTextView_scroll_marquee, false);
        if (marquee) {
            setMarqueeRepeatLimit(-1);
            setHorizontallyScrolling(true);
            setEllipsize(TextUtils.TruncateAt.MARQUEE);
            setSingleLine(true);
            setSelected(true);
        }

        Drawable leftDrawable = attributes.getDrawable(R.styleable.CustomTextView_leftDrawable);
        if (leftDrawable != null) {
            float fs = getTextSize();
            int dWidth = (int) attributes.getDimension(R.styleable.CustomTextView_leftDrawableWidth, fs);
            int dHeight = (int) attributes.getDimension(R.styleable.CustomTextView_leftDrawableWidth, fs);
            leftDrawable = ViewUtils.createScaledBitmap(getContext(), leftDrawable, dWidth, dHeight);
            setCompoundDrawablesWithIntrinsicBounds(leftDrawable, null, null, null);
        }

        attributes.recycle();
    }

    public void setTypefaceFromAssetPath(@Nullable String fontPath) {
        if ((this.fontPath = fontPath) != null) {
            Typeface font = Typeface.createFromAsset(getContext().getAssets(), fontPath);
            setTypeface(font);
        }
    }

    public String getFontPath() {
        return fontPath;
    }
}