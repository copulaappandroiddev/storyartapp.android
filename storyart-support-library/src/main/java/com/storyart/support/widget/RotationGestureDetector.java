package com.storyart.support.widget;

import android.graphics.Matrix;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

public class RotationGestureDetector implements CustomImageView.SizeChangedListener {
    private static final String TAG = RotationGestureDetector.class.getSimpleName();

    private OnScaleGestureListener mOnScaleGestureListener;
    private ScaleGestureDetector mScaleDetector;


    private int mLastAngle;
    /* Pivot Point for Transforms */
    private int mPivotX, mPivotY;

    private Matrix mImageMatrix = new Matrix();
    private CustomImageView mCustomImageView;
    private ScaleGestureDetector.SimpleOnScaleGestureListener mScaleListener
            = new ScaleGestureDetector.SimpleOnScaleGestureListener() {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            // ScaleGestureDetector calculates a scale factor based on whether
            // the fingers are moving apart or together
            float scaleFactor = detector.getScaleFactor();
            //Pass that factor to a scale for the image
            mImageMatrix.postScale(scaleFactor, scaleFactor, mPivotX, mPivotY);
            mCustomImageView.setImageMatrix(mImageMatrix);

            return true;
        }
    };

    public RotationGestureDetector(CustomImageView imageView, OnScaleGestureListener listener) {
        this.mCustomImageView = imageView;
        this.mOnScaleGestureListener = listener;

        //mCustomImageView.setScaleType(ImageView.ScaleType.MATRIX);
        mCustomImageView.setSizeChangedListener(this);

        mScaleDetector = new ScaleGestureDetector(imageView.getContext(), mScaleListener);
    }

    /*
     * Operate on two-finger events to rotate the image.
     * This method calculates the change in angle between the
     * pointers and rotates the image accordingly.  As the user
     * rotates their fingers, the image will follow.
     */
    private boolean doRotationEvent(MotionEvent event) {
        //Calculate the angle between the two fingers
        float deltaX = event.getX(0) - event.getX(1);
        float deltaY = event.getY(0) - event.getY(1);
        double radians = Math.atan(deltaY / deltaX);
        //Convert to degrees
        int degrees = (int) (radians * 180 / Math.PI);

        /*
         * Must use getActionMasked() for switching to pick up pointer events.
         * These events have the pointer index encoded in them so the return
         * from getAction() won't match the exact action constant.
         */
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN:
            case MotionEvent.ACTION_POINTER_UP:
                //Mark the initial angle
                mLastAngle = degrees;
                break;

            case MotionEvent.ACTION_MOVE:
                // ATAN returns a converted value between -90deg and +90deg
                // which creates a point when two fingers are vertical where the
                // angle flips sign.  We handle this case by rotating a small amount
                // (5 degrees) in the direction we were traveling
                if ((degrees - mLastAngle) > 45) {
                    //Going CCW across the boundary
                    mImageMatrix.postRotate(-5, mPivotX, mPivotY);
                } else if ((degrees - mLastAngle) < -45) {
                    //Going CW across the boundary
                    mImageMatrix.postRotate(5, mPivotX, mPivotY);
                } else {
                    //Normal rotation, rotate the difference
                    mImageMatrix.postRotate(degrees - mLastAngle, mPivotX, mPivotY);
                }
                //Post the rotation to the image
                if (mOnScaleGestureListener != null)
                    mOnScaleGestureListener.onRotate(degrees, mImageMatrix);

                mLastAngle = degrees;//Save the current angle
                break;
        }

        return true;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            // We don't care about this event directly, but we declare
            // interest so we can get later multi-touch events.
            return true;
        }

        switch (event.getPointerCount()) {
            case 3:
                // With three fingers down, zoom the image
                // using the ScaleGestureDetector
            case 2:
                return doRotationEvent(event);
            default:
                return false;
        }
    }

    @Override
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        if (w != oldw || h != oldh) {
            //Shift the image to the center of the view
            int translateX = (w - mCustomImageView.getWidth() / 2);
            int translateY = (h - mCustomImageView.getHeight()) / 2;
            mImageMatrix.setTranslate(translateX, translateY);
            mCustomImageView.setImageMatrix(mImageMatrix);
            //Get the center point for future scale and rotate transforms
            mPivotX = w / 2;
            mPivotY = h / 2;
        }
    }

    public interface OnScaleGestureListener {
        void onRotate(int degree, Matrix matrix);
    }
}