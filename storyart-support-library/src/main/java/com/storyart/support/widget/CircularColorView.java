package com.storyart.support.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.ColorInt;
import android.util.AttributeSet;
import android.view.View;
import com.storyart.support.svgresolver.R;

/**
 * Created by heeleaz on 2/15/18.
 */
public class CircularColorView extends View {
    private int borderColor;
    private int borderSize;
    private boolean hasMeasured = false;

    private Paint borderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint fillPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    private RectF borderRect, fillRect;


    public CircularColorView(Context context) {
        this(context, null, 0);
    }

    public CircularColorView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircularColorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    protected void init(Context context, AttributeSet attrs, int defStyle) {
        TypedArray attributes = context
                .obtainStyledAttributes(attrs, R.styleable.CircularColorView, defStyle, 0);
        borderSize = attributes.getDimensionPixelSize(R.styleable.CircularColorView_borderRadius, 6);
        borderColor = attributes.getColor(R.styleable.CircularColorView_borderColor, Color.WHITE);
        attributes.recycle();

        borderPaint.setColor(borderColor);
        borderPaint.setStyle(Paint.Style.FILL);
        fillPaint.setStyle(Paint.Style.FILL);

        measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int measuredWidth = widthMeasureSpec & View.MEASURED_SIZE_MASK;
        int measuredHeight = heightMeasureSpec & View.MEASURED_SIZE_MASK;

        if (!hasMeasured) {
            borderRect = new RectF(0, 0, measuredWidth, measuredHeight);
            fillRect = new RectF(borderSize,
                    borderSize, measuredWidth - borderSize, measuredWidth - borderSize);
            hasMeasured = true;
        }
    }

    @Override
    public void setBackgroundColor(int color) {
        fillPaint.setColor(color);
        invalidate();
    }

    public void setBorderSize(int borderSize) {
        this.borderSize = borderSize;
        invalidate();
    }

    public void setBorderColor(@ColorInt int borderColor) {
        this.borderColor = borderColor;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int canvasSize = getWidth() < getHeight() ? getWidth() : getHeight();
        borderRect.bottom = borderRect.right = canvasSize;
        canvas.drawRoundRect(borderRect, canvasSize / 2, canvasSize / 2, borderPaint);

        fillRect.bottom = fillRect.right = (canvasSize - borderSize);
        fillRect.left = fillRect.top = borderSize;

        int rxy = (canvasSize - borderSize) / 2;
        canvas.drawRoundRect(fillRect, rxy, rxy, fillPaint);
    }
}
