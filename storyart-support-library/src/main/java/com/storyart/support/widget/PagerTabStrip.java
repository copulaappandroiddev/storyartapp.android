package com.storyart.support.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.DrawableRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.storyart.support.svgresolver.R;

/**
 * Created by heeleaz on 11/8/17.
 */
public class PagerTabStrip extends HorizontalScrollView implements View.OnClickListener {
    private int indicatorWidth, indicatorHeight;
    private float notIndicatingAlpha;
    private int indicatorSpacing;

    private int currentTabPosition = 0;

    private OnPageSelectedListener mOnPageSelectedListener;
    private LinearLayout indicatorContainerView;

    public PagerTabStrip(Context context) {
        super(context);
        init(context, null, 0);
    }

    public PagerTabStrip(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public PagerTabStrip(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    public void setOnPageSelectedListener(OnPageSelectedListener listener) {
        this.mOnPageSelectedListener = listener;
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.PagerTabStrip, defStyle, 0);
        indicatorWidth = (int) attributes.getDimension(R.styleable.PagerTabStrip_indicatorWidth, indicatorWidth);
        indicatorHeight = (int) attributes.getDimension(R.styleable.PagerTabStrip_indicatorHeight, indicatorWidth);
        indicatorSpacing = (int) attributes.getDimension(R.styleable.PagerTabStrip_indicatorSpacing, 0);
        notIndicatingAlpha = attributes.getFloat(R.styleable.PagerTabStrip_notIndicatingAlpha, 1);
        attributes.recycle();

        setFillViewport(true);

        addView(indicatorContainerView = new LinearLayout(context));
        indicatorContainerView.setOrientation(LinearLayout.HORIZONTAL);
        indicatorContainerView.setGravity(Gravity.CENTER);
    }

    public void addIndicator(@DrawableRes int indicatorDrawable) {
        ImageView button = new ImageView(getContext());
        button.setImageResource(indicatorDrawable);
        button.setLayoutParams(new ViewGroup.LayoutParams(indicatorWidth, indicatorHeight));

        addIndicator(button);
    }

    public void addIndicator(@LayoutRes int resId, String text) {
        View view = View.inflate(getContext(), resId, null);
        ((TextView) view.findViewById(android.R.id.message)).setText(text);

        addIndicator(view);
    }

    public void addIndicator(@LayoutRes int resId, @StringRes int text) {
        addIndicator(resId, getContext().getString(text));
    }

    private void addIndicator(@NonNull View view) {
        view.setSelected(false);

        LinearLayout wrapper = new LinearLayout(getContext());

        wrapper.setGravity(Gravity.CENTER);
        wrapper.addView(view);
        wrapper.setPadding(indicatorSpacing, 0, indicatorSpacing, 0);
        wrapper.setOnClickListener(this);

        LinearLayout.LayoutParams params;
        if (indicatorSpacing == 0) {
            params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.weight = 1;
            indicatorContainerView.setWeightSum(indicatorContainerView.getChildCount());
        } else {
            params = new LinearLayout.
                    LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }

        indicatorContainerView.addView(wrapper, params);
    }

    public void selectPage(int position) {
        View currentTab = indicatorContainerView.getChildAt(currentTabPosition);
        currentTab.setAlpha(notIndicatingAlpha);
        currentTab.setSelected(false);


        View selectedTab = indicatorContainerView.getChildAt(position);
        selectedTab.setAlpha(1);
        selectedTab.setSelected(true);

        int width = getMeasuredWidth();
        int right = selectedTab.getRight();
        int left = selectedTab.getLeft();
        int buttonWidth = (right - left);

        if (position > currentTabPosition) {
            int shiftX = right + buttonWidth;//initial shift to end of button
            if (shiftX > width) {
                smoothScrollTo(shiftX, 0);
            }
        } else {
            int shiftX = left - buttonWidth;
            smoothScrollTo(shiftX, 0);
        }

        this.currentTabPosition = position;

    }

    @Override
    public void onClick(View v) {
        if (mOnPageSelectedListener != null) {
            mOnPageSelectedListener.onPageSelected(this, indicatorContainerView.indexOfChild(v));
        }
    }

    public interface OnPageSelectedListener {
        void onPageSelected(PagerTabStrip indicator, int index);
    }
}