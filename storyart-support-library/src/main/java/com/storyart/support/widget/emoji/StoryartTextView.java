package com.storyart.support.widget.emoji;

import android.content.Context;
import android.graphics.RectF;
import android.support.v7.widget.AppCompatTextView;
import android.text.StaticLayout;
import android.util.AttributeSet;
import android.util.TypedValue;
import com.storyart.support.widget.ScalableTextViewAdapter;

import static com.storyart.support.widget.ScalableTextViewAdapter.NO_LINE_LIMIT;

public class StoryartTextView extends AppCompatTextView {
    private ScalableTextViewAdapter mScalableTextViewAdapter;
    private boolean lockManualTextSize = false;
    private int adjustTextRectHeightMult;
    private boolean isWidthScalable = true;

    public StoryartTextView(Context context) {
        this(context, null, 0);
    }

    public StoryartTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StoryartTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        mScalableTextViewAdapter = new ScalableTextViewAdapter(this, new MySizeTester());
    }

    @Override
    public void setText(final CharSequence text, BufferType type) {
        super.setText(text, type);
        if (mScalableTextViewAdapter != null) mScalableTextViewAdapter.adjustTextSize();
    }

    public int getMaxLines() {
        return mScalableTextViewAdapter.getMaxLines();
    }

    @Override
    public void setMaxLines(int maxLines) {
        super.setMaxLines(maxLines);
        mScalableTextViewAdapter.setMaxLines(maxLines);
    }

    @Override
    public void setSingleLine() {
        super.setSingleLine();
        setSingleLine(true);
    }

    @Override
    public void setSingleLine(boolean singleLine) {
        super.setSingleLine(singleLine);
        mScalableTextViewAdapter.setSingleLine(singleLine);
    }

    @Override
    public void setLines(int lines) {
        super.setLines(lines);
        mScalableTextViewAdapter.setLines(lines);
    }


    public void setMaxTextSize(float size) {
        mScalableTextViewAdapter.setMaxTextSize(TypedValue.COMPLEX_UNIT_PX, size);
    }

    @Override
    public void setLineSpacing(float add, float mult) {
        super.setLineSpacing(add, mult);
        mScalableTextViewAdapter.setLineSpacing(add, mult);
    }

    /**
     * Set the lower text size limit and invalidate the view
     *
     * @param minTextSize
     */
    public void setMinTextSize(float minTextSize) {
        mScalableTextViewAdapter.setMinTextSize(minTextSize);
    }

    @Override
    public void setTextSize(float size) {
        if (!lockManualTextSize) super.setTextSize(size);
        lockManualTextSize = true;
    }

    public void adjustTextRectHeightMult(int adjustTextRectHeightMult) {
        this.adjustTextRectHeightMult = adjustTextRectHeightMult;
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int before, int after) {
        if (mScalableTextViewAdapter != null)
            mScalableTextViewAdapter.onTextChanged(text, start, before, after);
    }

    @Override
    protected void onSizeChanged(int w, int h, int ow, int oh) {
        super.onSizeChanged(w, h, ow, oh);
        if (mScalableTextViewAdapter != null)
            mScalableTextViewAdapter.onSizeChanged(w, h, ow, oh);
    }

    public int getTextHeight() {
        return mScalableTextViewAdapter.getTextLayoutHeight();
    }

    public int getTextWidth() {
        return mScalableTextViewAdapter.getTextLayoutWidth();
    }

    public void setWidthScalable(boolean scalable) {
        this.isWidthScalable = scalable;
    }

    private class MySizeTester implements ScalableTextViewAdapter.SizeTester {
        @Override
        public int onTestSize(int suggestedSize, RectF availableSpace) {
            mScalableTextViewAdapter.getPaint().setTextSize(suggestedSize);

            if (mScalableTextViewAdapter.getWidthLimit() < 0)
                mScalableTextViewAdapter.setWidthLimit(0);


            StaticLayout layout = mScalableTextViewAdapter.getTextLayout();

            // return early if we have more lines
            if (getMaxLines() != NO_LINE_LIMIT && layout.getLineCount() > getMaxLines()) {
                return 1;
            }

            int maxWidth;
            if (isWidthScalable) {
                maxWidth = -1;
                for (int i = 0; i < layout.getLineCount(); i++) {
                    if (layout.getLineWidth(i) > maxWidth) {
                        maxWidth = (int) layout.getLineWidth(i);
                    }
                }
            } else {
                maxWidth = mScalableTextViewAdapter.getWidthLimit();
            }

            int textHeight = Math.abs(Math.abs(layout.getLineAscent(0)) - Math.abs(layout.getLineDescent(0)));
            RectF textRect = mScalableTextViewAdapter.getTextRect();
            textRect.right = maxWidth;
            textRect.bottom = layout.getHeight() + (textHeight * adjustTextRectHeightMult);
            textRect.offsetTo(0, 0);

            // may be too small, don't worry we will find the best match
            if (availableSpace.contains(textRect)) return -1;
            else return 1;
        }
    }//END
}