package com.storyart.support.widget.emoji;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import com.storyart.emojikeyboard.EmojiEditText;
import com.storyart.support.widget.SupportViewHelper;

public class StoryartEditText extends EmojiEditText implements TextWatcher, View.OnKeyListener {
    private int lastSpecialRequestsCursorPosition = 0;
    private String specialRequests = "";
    private String fontPath;

    private KeyPreImeListener mKeyPreImeListener;
    private PropertyChangedListener mPropertyChangedListener;

    public StoryartEditText(Context context) {
        super(context);
    }

    public StoryartEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StoryartEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs);
    }

    public void setPropertyChangedListener(PropertyChangedListener propertyChangedListener) {
        this.mPropertyChangedListener = propertyChangedListener;
    }

    public void setKeyPreImeListener(KeyPreImeListener mKeyPreImeListener) {
        this.mKeyPreImeListener = mKeyPreImeListener;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        lastSpecialRequestsCursorPosition = getSelectionStart();
    }

    @Override
    public void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
    }

    @Override
    public void afterTextChanged(Editable s) {
        removeTextChangedListener(this);

        if (getLineCount() > getMaxLines()) {
            setText(specialRequests);
            setSelection(lastSpecialRequestsCursorPosition);
        } else specialRequests = getText().toString();

        addTextChangedListener(this);
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        return keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN && (getLineCount() >= getMaxLines());
    }

    @Override
    public void setTextColor(int color) {
        if (mPropertyChangedListener != null) {
            if (mPropertyChangedListener.beforePropertyChanged(this, PropertyChangedListener.TEXT_COLOR, color))
                return;
        }

        super.setTextColor(color);

        if (mPropertyChangedListener != null) {
            mPropertyChangedListener.afterPropertyChanged(this, PropertyChangedListener.TEXT_COLOR, color);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        setOnKeyListener(this);
        addTextChangedListener(this);
    }

    public void setMaxLineBasedOnHeight(float dimension) {
        float textSize = getTextSize();
        if (dimension == 0 || textSize == 0) return;

        int maxLines = (int) Math.floor((dimension / textSize)) - 1;
        setMaxLines(maxLines);
    }

    public CharSequence getTextMakeNewLines() {
        return SupportViewHelper.getTextMakeNewLines(getLayout());
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (mKeyPreImeListener != null)
            mKeyPreImeListener.onKeyPreIme(this, keyCode, event);
        return super.onKeyPreIme(keyCode, event);
    }

    public void setFontPath(@Nullable String fontPath) {
        if ((this.fontPath = fontPath) != null) {
            Typeface font = Typeface.createFromAsset(getContext().getAssets(), fontPath);
            setTypeface(font);
        }
    }

    public interface KeyPreImeListener {
        boolean onKeyPreIme(StoryartEditText editText, int keyCode, KeyEvent event);
    }

    public String getFontPath() {
        return fontPath;
    }

    public interface PropertyChangedListener {
        int TEXT_COLOR = 0;

        boolean beforePropertyChanged(StoryartEditText storyartEditText, int property, Object object);

        boolean afterPropertyChanged(StoryartEditText storyartEditText, int property, Object object);
    }
}