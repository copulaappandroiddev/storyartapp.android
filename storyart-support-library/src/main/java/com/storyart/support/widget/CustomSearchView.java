package com.storyart.support.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;
import android.widget.SearchView;
import com.storyart.support.svgresolver.R;

import java.lang.reflect.Field;

/**
 * Created by heeleaz on 2/11/18.
 */
public class CustomSearchView extends SearchView {
    private AutoCompleteTextView mSearchAutoComplete;

    public CustomSearchView(Context context) {
        this(context, null, 0);
    }

    public CustomSearchView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomSearchView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        TypedArray a = context.
                obtainStyledAttributes(attrs, R.styleable.CustomSearchView, defStyleAttr, 0);

        int searchTextColor = a.getColor(R.styleable.CustomSearchView_searchTextColor, Color.WHITE);
        int searchHintTextColor = a.getColor(R.styleable.CustomSearchView_searchHintTextColor, Color.GRAY);
        float searchTextSize = a.getDimensionPixelSize(R.styleable.CustomSearchView_searchTextSize, 15);

        a.recycle();

        try {
            Field field = SearchView.class.getDeclaredField("mSearchSrcTextView");
            field.setAccessible(true);
            mSearchAutoComplete = (AutoCompleteTextView) field.get(this);
        } catch (Exception e) {
        }

        if (mSearchAutoComplete != null) {
            mSearchAutoComplete.setTextColor(searchTextColor);
            mSearchAutoComplete.setTextSize(searchTextSize);
            mSearchAutoComplete.setHintTextColor(searchHintTextColor);
        }
    }
}
