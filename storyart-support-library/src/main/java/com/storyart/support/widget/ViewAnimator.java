package com.storyart.support.widget;

import android.animation.Animator;
import android.view.View;
import android.view.ViewPropertyAnimator;

/**
 * Created by heeleaz on 6/4/17.
 */
public class ViewAnimator {
    public static void alpha(AnimationCallback callback, View view, int alpha, int duration) {
        ViewPropertyAnimator animator = view.animate();
        animator.alpha(alpha).setDuration(duration).setListener(new SimpleAnimatorListener(callback)).start();
    }

    public static void scaleXY(View view, float scale, int duration) {
        scaleXY(null, view, scale, duration);
    }

    public static void scaleXY(AnimationCallback callback, View view, float scale, int duration) {
        ViewPropertyAnimator animator = view.animate();
        animator.scaleX(scale).scaleY(scale).setListener(new SimpleAnimatorListener(callback)).setDuration(duration).start();
    }

    public interface AnimationCallback {
        void onAnimationStart();

        void onAnimationEnd();
    }

    public static class SimpleAnimatorListener implements Animator.AnimatorListener {
        private AnimationCallback callback;

        public SimpleAnimatorListener(AnimationCallback callback) {
            this.callback = callback;
        }

        @Override
        public void onAnimationStart(Animator animation) {
            if (this.callback != null) callback.onAnimationStart();
        }

        @Override
        public void onAnimationEnd(Animator animation) {
            if (callback != null) callback.onAnimationEnd();
        }

        @Override
        public void onAnimationCancel(Animator animation) {
        }

        @Override
        public void onAnimationRepeat(Animator animation) {
        }
    }//END
}
