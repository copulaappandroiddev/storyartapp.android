package com.storyart.support.widget;

import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.widget.Toast;

/**
 * Created by heeleaz on 10/31/17.
 */
public class DoubleBackPressed {
    private static DoubleBackPressed sInstance = new DoubleBackPressed();
    private long lastClickTime;
    private String activityClazzName;

    private DoubleBackPressed() {
    }

    public static DoubleBackPressed instance() {
        return sInstance;
    }

    public boolean dispatch(FragmentActivity activity, String message) {
        if (activityClazzName != null && !activityClazzName.equals(activity.getLocalClassName())) {
            lastClickTime = 0;
        }

        if (System.currentTimeMillis() - lastClickTime < 1000) {
            return true;
        } else {
            if (!TextUtils.isEmpty(message)) {
                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
            }
        }

        lastClickTime = System.currentTimeMillis();
        this.activityClazzName = activity.getLocalClassName();
        return false;
    }
}
