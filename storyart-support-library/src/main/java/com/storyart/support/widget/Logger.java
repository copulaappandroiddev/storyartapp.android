package com.storyart.support.widget;

import android.util.Log;

/**
 * Created by heeleaz on 11/27/17.
 */
public class Logger {
    private static boolean IS_DEBUG = true;
    private String callerName;
    private Object tag;

    private Logger(String callerName) {
        this.callerName = callerName;
    }

    public static Logger getInstance(String loggerName) {
        return new Logger(loggerName);
    }

    public void i(String message) {
        if (IS_DEBUG) {
            if (message != null) Log.d(callerName, message);
            Log.d(callerName, "Message is null");
        }
    }

    public void e(Exception e) {
        e(null, e);
    }

    public void e(String message, Exception e) {
        if (IS_DEBUG) {
            String exMessage = (e != null) ? e.getMessage() : "Message=NULL";
            message = message != null ? message : "";

            Log.e(callerName, message + " ex-message: " + exMessage, e);
        }
    }

    public void e(String message, Error e) {
        if (IS_DEBUG) e(message, new Exception(e.getMessage()));
    }

    public Object getTag() {
        return tag;
    }

    public void setTag(Object tag) {
        this.tag = tag;
    }
}
