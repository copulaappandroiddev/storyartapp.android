package com.storyart.support.widget;

import android.text.Layout;
import android.widget.EditText;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by heeleaz on 11/27/17.
 */
public class SupportViewHelper {

    public static boolean isStringEmpty(String s) {
        return s == null || s.equalsIgnoreCase("null") || s.isEmpty();
    }

    public static List<CharSequence> getLinesText(Layout layout) {
        final List<CharSequence> lines = new ArrayList<>();

        if (layout != null) {
            final int lineCount = layout.getLineCount();
            final CharSequence text = layout.getText();

            for (int i = 0, startIndex = 0; i < lineCount; i++) {
                int endIndex = layout.getLineEnd(i);
                lines.add(text.subSequence(startIndex, endIndex));
                startIndex = endIndex;
            }
        }
        return lines;
    }

    public static CharSequence getTextMakeNewLines(Layout layout) {
        StringBuilder builder = new StringBuilder();
        for (CharSequence s : getLinesText(layout)) {
            String inString = s.toString();

            builder.append(inString);
            //just in case there is no newLine between line endings.. add one
            if (inString.length() > 0 && inString.charAt(inString.length() - 1) != '\n')
                builder.append("\n");

        }
        return builder.toString();
    }

    public static int calcMaxLinesBasedOnHeight(EditText editText, float dimension) {
        Layout layout = editText.getLayout();
        float textHeight;
        if (layout != null) {
            textHeight = Math.abs(Math.abs(layout.getLineAscent(0))
                    - Math.abs(layout.getLineDescent(0)));
        } else {
            textHeight = editText.getTextSize();
        }

        if (dimension == 0 || textHeight == 0) return 0;

        return (int) (dimension / (textHeight * 1f));

    }

    public static void setMaxLinesBasedOnHeight(EditText editText, float dimension) {
        editText.setMaxLines(calcMaxLinesBasedOnHeight(editText, dimension));
    }

    public static void setCursorDrawable(EditText editText, int drawableRes) {
        try {
            Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
            f.setAccessible(true);
            f.set(editText, drawableRes);
        } catch (Exception e) {
        }
    }
}
