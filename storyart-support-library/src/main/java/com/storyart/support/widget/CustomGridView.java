package com.storyart.support.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.GridView;
import com.storyart.support.svgresolver.R;

public class CustomGridView extends GridView {
    private boolean mAutoSpacing;

    public CustomGridView(Context context) {
        super(context);
        init(context, null, 0);
    }

    public CustomGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public CustomGridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomGridView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet defStyle, int defStyleAttr) {
        TypedArray attributes = context
                .obtainStyledAttributes(defStyle, R.styleable.CustomGridView, defStyleAttr, 0);
        mAutoSpacing = attributes.getBoolean(R.styleable.CustomGridView_autoSpacing, false);
        attributes.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (mAutoSpacing) setVerticalSpacing(getHorizontalSpacing());
    }
}
