package com.storyart.support.pagination;

import android.widget.AbsListView;

class EndScrollListener implements AbsListView.OnScrollListener {

    private final Callback callback;
    private int visibleThreshold = 5;

    EndScrollListener(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItemPosition, int visibleItemCount, int totalItemCount) {
        if (totalItemCount == 0) return;
        int lastInScreen = firstVisibleItemPosition + visibleItemCount;
        if (lastInScreen == totalItemCount && (totalItemCount % visibleThreshold) == 0) {
            callback.onEndReached();
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    void setThreshold(int threshold) {
        this.visibleThreshold = Math.max(0, threshold);
    }

    public interface Callback {
        void onEndReached();
    }

}