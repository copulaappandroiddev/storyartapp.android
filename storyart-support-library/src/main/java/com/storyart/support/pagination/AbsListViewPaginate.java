package com.storyart.support.pagination;

import android.widget.AbsListView;

public final class AbsListViewPaginate extends Paginate implements EndScrollListener.Callback {

    private final Callbacks callbacks;

    AbsListViewPaginate(AbsListView absListView, Paginate.Callbacks callbacks, int loadingTriggerThreshold) {
        this.callbacks = callbacks;

        // Attach scrolling listener in order to perform end offset check on each scroll event
        EndScrollListener scrollListener = new EndScrollListener(this);
        scrollListener.setThreshold(loadingTriggerThreshold);
        absListView.setOnScrollListener(scrollListener);
    }

    @Override
    public void onEndReached() {
        if (!callbacks.hasLoadedAllItems()) callbacks.onLoadMore();
    }

    public static class Builder {
        private final AbsListView absListView;
        private final Paginate.Callbacks callbacks;

        private int loadingTriggerThreshold = 5;

        public Builder(AbsListView absListView, Paginate.Callbacks callbacks) {
            this.absListView = absListView;
            this.callbacks = callbacks;
        }

        public Builder setLoadingTriggerThreshold(int threshold) {
            this.loadingTriggerThreshold = threshold;
            return this;
        }

        public Paginate build() {
            if (absListView.getAdapter() == null) {
                throw new IllegalStateException("Adapter is null");
            }

            return new AbsListViewPaginate(absListView, callbacks, loadingTriggerThreshold);
        }
    }

}