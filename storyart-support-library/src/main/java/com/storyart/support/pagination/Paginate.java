package com.storyart.support.pagination;

import android.widget.AbsListView;


public abstract class Paginate {

    public static AbsListViewPaginate.Builder with(AbsListView absListView, Callbacks callback) {
        return new AbsListViewPaginate.Builder(absListView, callback);
    }

    public interface Callbacks {
        void onLoadMore();

        boolean hasLoadedAllItems();
    }

}