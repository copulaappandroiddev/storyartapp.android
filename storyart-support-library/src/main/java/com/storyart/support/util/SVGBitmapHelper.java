package com.storyart.support.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Picture;

/**
 * Created by heeleaz on 11/30/17.
 */
public class SVGBitmapHelper {
    public static Bitmap pictureToBitmap(Picture picture) {
        if (picture.getWidth() != -1) {
            Bitmap bitmap = Bitmap.createBitmap(
                    picture.getWidth(), picture.getHeight(), Bitmap.Config.ARGB_8888);
            new Canvas(bitmap).drawPicture(picture);
            return bitmap;
        } else return null;
    }
}
