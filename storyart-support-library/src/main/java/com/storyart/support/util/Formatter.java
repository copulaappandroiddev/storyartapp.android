package com.storyart.support.util;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Formatter {
    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static String bytesToHex(byte[] bytes, String seperator) {
        StringBuilder sb = new StringBuilder(bytes.length * 2);
        for (int i = 0; i < bytes.length; i++)
            sb.append(String.format("%02x%s", bytes[i] & 0xff, (i < bytes.length - 1) ? ":" : ""));
        return sb.toString();
    }

    public static long dateToMilli(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return sdf.parse(date).getTime();
        } catch (ParseException e) {
            return 0;
        }
    }

    public static String getCalenderDateInString(String prepend, String append) {
        Calendar calendar = Calendar.getInstance(Locale.ENGLISH);

        String inString = prepend;
        inString += calendar.get(Calendar.YEAR);
        inString += calendar.get(Calendar.MONTH);
        inString += calendar.get(Calendar.DAY_OF_MONTH);
        inString += calendar.get(Calendar.HOUR_OF_DAY);
        inString += calendar.get(Calendar.MINUTE);
        inString += calendar.get(Calendar.SECOND);
        return inString + append;
    }
}
