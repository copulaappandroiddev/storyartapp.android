package com.storyart.support.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.*;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.util.Locale;

/**
 * Created by heeleaz on 10/27/17.
 */
public class ViewUtils {
    public static void showOnly(View view) {
        ViewGroup parent = (ViewGroup) view.getParent();
        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            parent.getChildAt(i).setVisibility(View.GONE);
        }
        view.setVisibility(View.VISIBLE);
    }

    public static Drawable createScaledBitmap(Context ctx, Drawable d, int w, int h) {
        Bitmap image = ((BitmapDrawable) d).getBitmap();
        return new BitmapDrawable(ctx.getResources(),
                Bitmap.createScaledBitmap(image, w, h, true));
    }

    public static void showDialogAsBottomPop(Dialog dialog) {
        Window window = dialog.getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.x = ViewGroup.LayoutParams.MATCH_PARENT;
        params.y = ViewGroup.LayoutParams.WRAP_CONTENT;
        params.gravity = Gravity.BOTTOM;
        window.setAttributes(params);
    }

    public static void showDialogAsToastPop(Dialog dialog, int yFromBottom) {
        Window window = dialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setDimAmount(0);

        WindowManager.LayoutParams params = window.getAttributes();
        params.gravity = Gravity.BOTTOM;
        params.y = ViewUtils.dpToPx(yFromBottom);

        window.setAttributes(params);
    }

    public static void sToast(Context ctx, String message) {
        Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
    }

    public static void sToast(Context ctx, int message) {
        Toast.makeText(ctx, ctx.getString(message), Toast.LENGTH_SHORT).show();
    }

    public static void longToast(Context ctx, int message) {
        Toast.makeText(ctx, ctx.getString(message), Toast.LENGTH_LONG).show();
    }

    public static int dpToPx(int dp) {
        float scale = Resources.getSystem().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    public static int pxToDp(int px) {
        float scale = Resources.getSystem().getDisplayMetrics().density;
        return (int) (px / scale - 0.5f);
    }

    public static void colorStatusBar(Activity activity, int color, float hsv2Mult) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            float[] hsv = new float[3];
            Color.colorToHSV(color, hsv);
            hsv[2] *= hsv2Mult; // value component

            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.HSVToColor(hsv));
        }
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier(
                "status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static void showSoftInputKeyboard(Activity activity) {
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    public static void hideSoftInputKeyboard(View view, Context context) {
        InputMethodManager imm = (InputMethodManager)
                context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public static void fullscreenActivity(Activity activity) {
        activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public static String formatDuration(long millis) {
        long second = (millis / 1000) % 60;
        long minute = (millis / (1000 * 60)) % 60;
        long hour = (millis / (1000 * 60 * 60)) % 24;

        if (hour == 0) {
            return String.format(Locale.getDefault(), "%02d:%02d", minute, second);
        } else {
            return String.format(Locale.getDefault(), "%02d:%02d:%02d", hour, minute, second);
        }
    }

    public static void increaseTouchArea(final View v, final int dimension) {
        final View parent = (View) v.getParent();
        parent.post(new Runnable() {
            public void run() {
                Rect rect = new Rect();
                v.getHitRect(rect);
                rect.top -= dimension;    // increase top hit area
                rect.left -= dimension;   // increase left hit area
                rect.bottom += dimension; // increase bottom hit area
                rect.right += dimension;  // increase right hit area

                parent.setTouchDelegate(new TouchDelegate(rect, v));
            }
        });
    }

    public static Bitmap getBitmapFromView(View view, Bitmap.Config config) {
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), config);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);


        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null) bgDrawable.draw(canvas);
        else canvas.drawColor(Color.BLACK);


        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }
}

