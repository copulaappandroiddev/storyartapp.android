package com.storyart.support.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

public class BitmapUtils {
    public static Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        // We ask for the bounds if they have been set as they would be most
        // correct, then we check we are  > 0
        final int width = !drawable.getBounds().isEmpty() ?
                drawable.getBounds().width() : drawable.getIntrinsicWidth();

        final int height = !drawable.getBounds().isEmpty() ?
                drawable.getBounds().height() : drawable.getIntrinsicHeight();

        // Now we check we are > 0
        final Bitmap bitmap = Bitmap.createBitmap(width <= 0 ? 1 : width, height <= 0 ? 1 : height,
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public static Bitmap createScaledBitmap(Bitmap bitmap, float scale) {
        if (scale < 1) return bitmap;

        int w = (int) (bitmap.getWidth() / scale);
        int h = (int) (bitmap.getHeight() / scale);
        return Bitmap.createScaledBitmap(bitmap, w, h, true);
    }

    public static Bitmap createScaledBitmap(Context ctx, int drawable, float scale) {
        Bitmap bm = BitmapFactory.decodeResource(ctx.getResources(), drawable);
        return createScaledBitmap(bm, scale);
    }
}
